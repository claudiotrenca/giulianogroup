﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace CustomControls

	<DefaultProperty("Text"), ToolboxData("<{0}:UnorderedListDataPager runat=server></{0}:UnorderedListDataPager>")> _
	Public Class UnorderedListDataPager
		Inherits DataPager

		<Bindable(True), Category("Appearance"), DefaultValue(""), Localizable(True)> Property Text() As String
			Get
				Dim s As String = CStr(ViewState("Text"))
				If s Is Nothing Then
					Return String.Empty
				Else
					Return s
				End If
			End Get

			Set(ByVal Value As String)
				ViewState("Text") = Value
			End Set
		End Property

		Protected Overrides ReadOnly Property TagKey() As HtmlTextWriterTag
			Get
				Return HtmlTextWriterTag.Ul
			End Get
		End Property

		Protected Overrides Sub RenderContents(writer As HtmlTextWriter)
			If HasControls() Then
				For Each child As Control In Controls
					Dim item = TryCast(child, DataPagerFieldItem)
					If item Is Nothing OrElse Not item.HasControls() Then
						child.RenderControl(writer)
						Continue For
					End If

					For Each button As Control In item.Controls
						Dim space = TryCast(button, LiteralControl)
						If space IsNot Nothing AndAlso space.Text = "&nbsp;" Then
							Continue For
						End If

						writer.RenderBeginTag(HtmlTextWriterTag.Li)
						button.RenderControl(writer)
						writer.RenderEndTag()
					Next
				Next
			End If
		End Sub

		'Private Sub UnorderedListDatasender_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender

		'    'For Each ctrl As Control In Me.Controls

		'    '    For Each obj In ctrl.Controls

		'    '        If obj.GetType() Is GetType(System.Web.UI.WebControls.HyperLink) Then

		'    '            Dim currentLink As HyperLink = DirectCast(obj, HyperLink)
		'    '            currentLink.NavigateUrl = HttpContext.Current.Request.RawUrl.ToString & "/" & HttpContext.Current.Request.QueryString("page")

		'    '        End If

		'    '    Next

		'    'Next

		'    For Each Pitem As DataPagerFieldItem In sender.Controls
		'        For Each c As Control In Pitem.Controls
		'            If TypeOf c Is HyperLink Then
		'                Dim tmp As HyperLink = TryCast(c, HyperLink)
		'                tmp.NavigateUrl = HttpContext.Current.Request.RawUrl.ToString & "/" & HttpContext.Current.Request.QueryString("page")
		'            End If
		'        Next
		'    Next

		'End Sub
	End Class

End Namespace