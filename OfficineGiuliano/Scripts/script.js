$(window).scroll(function() {    
    var scroll = $(window).scrollTop();    
    if (scroll <= 60) {
        $(".navbar-general").removeClass("shrink");
    } else if (scroll > 60) {
        $(".navbar-general").addClass("shrink");
    }
});

// $(window).scroll(function() {    
//     var scroll = $(window).scrollTop();    
//     if (scroll <= 60) {
//         $(".navbar-mobile").removeClass("shrink");
//     } else if (scroll > 60) {
//         $(".navbar-mobile").addClass("shrink");
//     }
// });


 /***************** Preloader ******************/
$(window).load(function() { // makes sure the whole site is loaded
    $('#status').fadeOut(); // will first fade out the loading animation
    $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('body').delay(350).css({
        'overflow': 'visible'
    });
});
$(document).ready(function() { /**********************************************************************************************************/
    
    
       
    if ($('#owl-slider-home').length) {

      $("#owl-slider-home").owlCarousel({
            
            pagination : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem: true,
            autoPlay: 4000

        });
        
     }

    //setActiveLink();


    // plus and minus collapse scheda prodotto noleggio ///

    var selectIds = $('#collapseOne, #collapseTwo, #collapseThree');
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
        $(this).prev().find('.ion').toggleClass('ion-ios-minus-outline ion-ios-plus-outline');
    });
    
    $('.panel-heading a').click(function() {
        $('.panel-heading').removeClass('activeState');
        $(this).parents('.panel-heading').addClass('activeState');
    });
 

    $('#myTabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('#form-richiesta-info').click(function() {
       $('.form-richiesta-info').toggleClass('show');
        return false;
    });
    /********** SELECT PICKER ************/

    // if($('.selectpicker').length){
    //   $('.selectpicker').selectpicker({
    //       style: 'select-custom'
    //   });
    // }

    // if($('.selectpicker-contatti').length){
    //   $('.selectpicker-contatti').selectpicker({
    //       style: 'select-contatti'
    //   });
    // }
    
    if($('.sp-wrap').length) {
        $('.sp-wrap').smoothproducts();
    }

   
    
});

/***************** Mobile support ******************/
// if ($(window).width() < 800) {
 
// }

