﻿Imports Microsoft.AspNet.Identity

Public Class SiteMaster
    Inherits BaseClass.SiteMasterpage
    Private Const AntiXsrfTokenKey As String = "__AntiXsrfToken"
    Private Const AntiXsrfUserNameKey As String = "__AntiXsrfUserName"
    Private _antiXsrfTokenValue As String

    Private Menu As New DataClass.Menu

    Protected Sub Page_Init(sender As Object, e As EventArgs)
        ' Il codice seguente facilita la protezione da attacchi XSRF
        Dim requestCookie = Request.Cookies(AntiXsrfTokenKey)
        Dim requestCookieGuidValue As Guid
        If requestCookie IsNot Nothing AndAlso Guid.TryParse(requestCookie.Value, requestCookieGuidValue) Then
            ' Utilizzare il token Anti-XSRF dal cookie
            _antiXsrfTokenValue = requestCookie.Value
            Page.ViewStateUserKey = _antiXsrfTokenValue
        Else
            ' Generare un nuovo token Anti-XSRF e salvarlo nel cookie
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N")
            Page.ViewStateUserKey = _antiXsrfTokenValue

            Dim responseCookie = New HttpCookie(AntiXsrfTokenKey) With {
                 .HttpOnly = True,
                 .Value = _antiXsrfTokenValue
            }
            If FormsAuthentication.RequireSSL AndAlso Request.IsSecureConnection Then
                responseCookie.Secure = True
            End If
            Response.Cookies.[Set](responseCookie)
        End If

        AddHandler Page.PreLoad, AddressOf master_Page_PreLoad

    End Sub

    Protected Sub master_Page_PreLoad(sender As Object, e As EventArgs)
        If Not IsPostBack Then
            ' Impostare il token Anti-XSRF
            ViewState(AntiXsrfTokenKey) = Page.ViewStateUserKey
            ViewState(AntiXsrfUserNameKey) = If(Context.User.Identity.Name, [String].Empty)
        Else
            ' Convalidare il token Anti-XSRF
            If DirectCast(ViewState(AntiXsrfTokenKey), String) <> _antiXsrfTokenValue OrElse DirectCast(ViewState(AntiXsrfUserNameKey), String) <> (If(Context.User.Identity.Name, [String].Empty)) Then
                Throw New InvalidOperationException("Convalida del token Anti-XSRF non riuscita.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        InitPage()
        CreateCanonical()

    End Sub

    Protected Sub Form1_Load(sender As Object, e As EventArgs) Handles Form1.Load
        Form1.Action = FormActionWriter.ActionUrl()
    End Sub

    Protected Sub Unnamed_LoggingOut(sender As Object, e As LoginCancelEventArgs)
        Context.GetOwinContext().Authentication.SignOut()
    End Sub

    Private Sub CreateCanonical()

        Dim _PageNumber As Integer
        Dim head As HtmlHead = Page.Header
        Dim seoTag As HtmlLink = New HtmlLink()
        seoTag.Attributes.Add("rel", "canonical")

        If Page.RouteData.Values("Pagina") IsNot Nothing Then
            _PageNumber = Convert.ToInt32(Page.RouteData.Values("Pagina"))
        Else
            _PageNumber = 1
        End If

        If _PageNumber > 1 Then
            Dim url As String = Request.Url.ToString
            url = url.TrimEnd("/")
            url = url.Substring(0, url.LastIndexOf("/") + 1)
            seoTag.Href = url
        Else
            seoTag.Href = Request.Url.ToString
        End If
        head.Controls.Add(seoTag)

    End Sub

    Private Sub InitPage()

        If Page.RouteData.Values.Count > 0 Then

            _Provincia = IIf(Page.RouteData.Values("provincia") <> Nothing, Page.RouteData.Values("provincia"), ConfigurationManager.AppSettings("default_prov"))
            _PageLink = IIf(Page.RouteData.Values("PageLink") <> Nothing, Page.RouteData.Values("PageLink"), String.Empty)
            _Rif = IIf(Page.RouteData.Values("rif") <> Nothing, Page.RouteData.Values("rif"), Nothing)

            If _Rif = Nothing Then
                If Not String.IsNullOrEmpty(_PageLink) Then
                    _Rif = DataClass.Menu.GetIdFromLink(_PageLink)
                Else
                    _Rif = DataClass.Menu.GetIdFromLink(ConfigurationManager.AppSettings("DefaultPage"))
                End If
            End If
        Else
            _Rif = DataClass.Menu.GetIdFromLink(ConfigurationManager.AppSettings("DefaultPage"))
        End If

    End Sub

    Private Sub InitContent()

        _Content = DataClass.vwPost.GetDetailFromMenu(_Rif)

        If _Content IsNot Nothing Then
            _PageTitle = _Content.MetaTitle
            _PageDescription = _Content.MetaDescription
            _PageKeywords = _Content.MetaKeywords
            _PageCanonical = _Content.MetaCanonical
            _PageContent = _Content.Contenuto
        End If

    End Sub

End Class