﻿Imports System.Web
Imports System.Web.Services
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Text

Public Class AutoComplete
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim prefixText As String = context.Request.QueryString("q")
        Dim sb As New StringBuilder

        Using dbConn As New SqlConnection(ConfigurationManager.ConnectionStrings("Accesso").ConnectionString)
            Using cmd As New SqlCommand

                cmd.CommandText = ("select distinct titolo_categoria from MV_CATEGORIE where titolo_categoria like @SearchText + '%'")
                cmd.Parameters.AddWithValue("@SearchText", prefixText)
                cmd.Connection = dbConn
                dbConn.Open()

                Dim sdr As SqlDataReader = cmd.ExecuteReader
                While sdr.Read
                    sb.Append(sdr("titolo_categoria")) _
                        .Append(Environment.NewLine)
                End While

            End Using

        End Using

        context.Response.Write(sb.ToString)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class