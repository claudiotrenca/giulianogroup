﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class SiteMaster
    
    '''<summary>
    '''Controllo HeadPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents HeadPh As Global.System.Web.UI.WebControls.ContentPlaceHolder
    
    '''<summary>
    '''Controllo Form1.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents Form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''Controllo HeaderPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents HeaderPh As Global.System.Web.UI.WebControls.ContentPlaceHolder
    
    '''<summary>
    '''Controllo HeaderMaster.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents HeaderMaster As Global.Emmemedia.Controls.Common.Header.HeaderMaster
    
    '''<summary>
    '''Controllo SliderPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents SliderPh As Global.System.Web.UI.WebControls.ContentPlaceHolder
    
    '''<summary>
    '''Controllo ContentUpperPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ContentUpperPh As Global.System.Web.UI.WebControls.ContentPlaceHolder
    
    '''<summary>
    '''Controllo ContentMainPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ContentMainPh As Global.System.Web.UI.WebControls.ContentPlaceHolder
    
    '''<summary>
    '''Controllo ContentFooterPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ContentFooterPh As Global.System.Web.UI.WebControls.ContentPlaceHolder
    
    '''<summary>
    '''Controllo FooterPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents FooterPh As Global.System.Web.UI.WebControls.ContentPlaceHolder
    
    '''<summary>
    '''Controllo Footer03.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents Footer03 As Global.Emmemedia.Footer03
    
    '''<summary>
    '''Controllo FooterScriptsPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents FooterScriptsPh As Global.System.Web.UI.WebControls.ContentPlaceHolder
End Class
