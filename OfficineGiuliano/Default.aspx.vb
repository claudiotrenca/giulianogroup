﻿Public Class _Default
    Inherits BaseClass.SitePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

    End Sub

    Private Sub _Default_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

        InitContent()

    End Sub

    Private Sub InitContent()

        Dim _PkContent As Integer = DataClass.PostMenuMapping.GetPostIdFromMenu(Me.Master.Rif)

        _Content = DataClass.vwPost.GetDetail(_PkContent)

        '     _Content = DataClass.vwPost.GetDetailFromMenu(Me.Master.Rif)

        If _Content IsNot Nothing Then
            _PageTitolo = _Content.Titolo
            _PageSottotitolo = _Content.SottoTitolo
            _PageSezione = _Content.Sezione
            _PageContent = _Content.Contenuto
            _PageFooterContent = _Content.ContenutoInferiore
            _PageUpperContent = _Content.ContenutoSuperiore
            _PageContenutoBreve = _Content.ContenutoBreve

            InitMeta()
        End If

        MainContent.Text = _PageContent
        UpperContent.Text = _PageUpperContent
        FooterContent.Text = _PageFooterContent

    End Sub

    Private Sub InitMeta()
        Dim MetaPage As New MetaClass
        MetaPage.MetaTitle = _Content.MetaTitle
        MetaPage.MetaDescription = _Content.MetaDescription
        MetaPage.MetaKeyword = _Content.MetaKeywords
        MetaPage.MetaCanonical = _Content.MetaCanonical

        Me.Master.Meta = MetaPage
        Me.Master.InitMetaTag()
    End Sub

    Private Sub CheckControls()

    End Sub

    'Private Sub NewsletterSubcriptionButton_Click(sender As Object, e As EventArgs) Handles NewsletterSubcriptionButton.Click
    '    If Page.IsValid Then
    '        Using context As New DataClass.DataEntities
    '            Dim row = context.NewsLetterSubscription.SingleOrDefault(Function(p) p.Email = NewsletterSubcription.Text.Trim)
    '            Try
    '                If row Is Nothing Then
    '                    Dim NewRow As New DataClass.NewsLetterSubscription
    '                    NewRow.DataInserimento = Date.Now
    '                    NewRow.DataModifica = Date.Now
    '                    NewRow.Attivo = True
    '                    NewRow.Email = NewsletterSubcription.Text.Trim

    '                    context.Configuration.AutoDetectChangesEnabled = True
    '                    context.NewsLetterSubscription.Attach(NewRow)
    '                    context.Entry(NewRow).State = Entity.EntityState.Added
    '                    context.SaveChanges()
    '                Else
    '                    row.DataModifica = Date.Now
    '                    row.Attivo = True
    '                    context.SaveChanges()
    '                End If
    '            Catch ex As Exception
    '                '     Throw ex
    '            End Try
    '        End Using
    '    End If
    'End Sub

End Class