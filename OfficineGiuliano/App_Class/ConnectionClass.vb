﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb

public partial class ConnectionClass


    Public Shared ReadOnly Property SqlDBCnStr() As String
        Get
            Return ConfigurationManager.ConnectionStrings("accesso").ConnectionString
        End Get
    End Property

    Public Shared ReadOnly Property cnDBSql() As SqlClient.SqlConnection
        Get
            Return New SqlConnection(SqlDBCnStr)
        End Get
    End Property

    Public Shared Function NUMERO_RECORD(ByVal value As String) As Integer
        Dim dt As New DataTable
        leggi(value, ConnectionClass.cnDBSql, dt)

        Return dt.Rows.Count()
    End Function

    Public Shared Function CreateRow(ByVal Text As String, ByVal Value As String, ByVal dt As DataTable) As DataRow
        Dim dr As DataRow = dt.NewRow()
        dr(0) = Text
        dr(1) = Value
        Return dr
    End Function

    Public Shared Function fix_sql(sql As String) As String

        sql = Replace(sql, "'", "''")
        sql = Replace(sql, "%", "[%]")
        sql = Replace(sql, "[", "[[]")
        sql = Replace(sql, "]", "[]]")
        sql = Replace(sql, "_", "[_]")
        sql = Replace(sql, "#", "[#]")
        Return sql

    End Function

    Private Shared Function VERIFICA_XSS_SEC(ByVal sql As String) As Boolean
        If (InStr(sql, ";") > 0) Or (InStr(UCase(sql), "@TABLE") > 0) Or (InStr(UCase(sql), "@COLUMN") > 0) Or (InStr(UCase(sql), "@CURSOR") > 0) Or (InStr(UCase(sql), "FETCH") > 0) Or (InStr(UCase(sql), "WHILE") > 0) Then
            Return True
        Else
            Return False
        End If
    End Function

#Region "SQL"
    Private Shared Function open(ByVal cn As SqlConnection) As Boolean
        Try
            Select Case cn.State
                Case ConnectionState.Closed
                    cn.Open()
                    Return True
                Case ConnectionState.Open
                    Return True
                Case Else
                    'TODO: Insert log
                    Return False
            End Select
        Catch ex As Exception
            'TODO: Insert log
            Return False
        End Try
    End Function

    Private Shared Function close(ByVal cn As SqlConnection) As Boolean
        Try
            Select Case cn.State
                Case ConnectionState.Closed
                    Return True
                Case ConnectionState.Open
                    cn.Close()
                    Return True
                Case Else
                    'TODO: Insert log
                    Return False
            End Select
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function exec(ByVal sql As String, ByVal cn As SqlConnection) As Boolean

        If VERIFICA_XSS_SEC(sql) Then
            Return False
            Exit Function
        End If
        sql = Replace(sql, "!-!", ";")

        If Not (open(cn)) Then
            Return False
            Exit Function
        End If
        Dim trans As SqlTransaction = cn.BeginTransaction(IsolationLevel.ReadCommitted)
        Dim cmd As New SqlCommand(sql, cn, trans)
        Try
            cmd.ExecuteNonQuery()
            trans.Commit()
            Return True
        Catch ex As Exception
            trans.Rollback()
            trans.Dispose()
            'TODO: Insert log
            Return False
            Exit Function
        Finally
            close(cn)
            cmd.Dispose()
            trans.Dispose()
        End Try

    End Function

    Public Shared Function exec(ByRef cmd As SqlCommand, ByVal cn As SqlConnection) As Boolean

        If Not (open(cn)) Then
            Return False
            Exit Function
        End If
        Dim trans As SqlTransaction = cn.BeginTransaction(IsolationLevel.ReadCommitted)
        '  Dim cmd As New SqlCommand(Sql, cn, trans)
        cmd.Connection = cn
        cmd.Transaction = trans
        Try
            cmd.ExecuteNonQuery()
            trans.Commit()
            Return True
        Catch ex As Exception
            trans.Rollback()
            trans.Dispose()
            'TODO: Insert log
            Return False
            Exit Function
        Finally
            close(cn)
            cmd.Dispose()
            trans.Dispose()
        End Try

    End Function

    Public Shared Sub log(ByVal tabella As String, ByVal tipo_operazione As String, ByVal utente As String, ByVal testo As String)
        exec("INSERT INTO s_operazioni (tabella, tipo_operazione, utente, sql) VALUES ('" + tabella + "'," + tipo_operazione + ",'" + utente + "','" + Replace(testo, "'", "''") + "')", cnDBSql)
    End Sub

    Public Shared Function scalar(ByVal sql As String, ByVal cn As SqlConnection) As String
        If VERIFICA_XSS_SEC(sql) Then

            Exit Function
        End If
        sql = Replace(sql, "!-!", ";")

        Dim result As String = ""
        If Not (open(cn)) Then
            'TODO: Insert log
            Return False
            Exit Function
        End If
        Dim cmd As New SqlCommand(sql, cn)
        Try
            result = cmd.ExecuteScalar
            Return result
        Catch ex As Exception
            'TODO: Insert log
            Return "error"
            Exit Function
        Finally
            close(cn)
        End Try
        cmd.Dispose()
    End Function

    Public Function execSP(ByVal name As String, ByVal cn As SqlConnection) As Boolean
        If Not (open(cn)) Then
            'TODO: Insert log
            Return False
            Exit Function
        End If
        Dim trans As SqlTransaction = cn.BeginTransaction(IsolationLevel.ReadCommitted)
        Dim cmd As New SqlCommand(name, cn, trans)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cmd.ExecuteNonQuery()
            trans.Commit()
            Return True
        Catch ex As Exception
            trans.Rollback()
            trans.Dispose()
            'TODO: Insert log
            Return False
            Exit Function
        Finally
            close(cn)
        End Try
        cmd.Dispose()
        trans.Dispose()
    End Function

    Public Shared Function leggi(ByVal sql As String, ByVal cn As SqlConnection, ByVal dt As DataTable) As DataTable
        If VERIFICA_XSS_SEC(sql) Then
            Return Nothing
            Exit Function
        End If
        sql = Replace(sql, "!-!", ";")

        Dim cmd As New SqlCommand(sql, cn)
        cmd.CommandTimeout = 600
        If Not (open(cn)) Then
            'TODO: Insert log
            Return Nothing
            Exit Function
        End If

        Try
            dt.Load(cmd.ExecuteReader)
        Catch ex As Exception
            'UTILS_VB.INVIA_EMAIL("patrizio.musilli@gmail.com", "patrizio.musilli@gmail.com", "Errore esecuzione query", sql, "walter")
            Return Nothing
            Exit Function
        Finally
            close(cn)
        End Try

        cmd.Dispose()
        Return dt
    End Function





    Public Shared Function leggi(ByVal value As String, ByVal TABELLA As String, ByVal cPAGE As Integer, ByVal endPAGE As Integer) As DataTable
        If VERIFICA_XSS_SEC(value) Then
            Return Nothing
            Exit Function
        End If
        value = Replace(value, "!-!", ";")

        Dim sqlAdapter1 As New SqlDataAdapter(value, ConnectionClass.cnDBSql)
        Dim ds As New DataSet
        Try
            sqlAdapter1.Fill(ds, cPAGE, endPAGE, TABELLA)
            Return ds.Tables(0)
        Catch ex As Exception
            'TODO: Insert log
            Return Nothing
            Exit Function
        Finally
            close(ConnectionClass.cnDBSql)
        End Try
    End Function

#End Region

#Region "OLEDB"
    Private Shared Function open(ByVal cn As OleDbConnection) As Boolean
        Try
            Select Case cn.State
                Case ConnectionState.Closed
                    cn.Open()
                    Return True
                Case ConnectionState.Open
                    Return True
                Case Else
                    'TODO: Insert log
                    Return False
            End Select
        Catch ex As Exception
            'TODO: Insert log
            Return False
        End Try
    End Function

    Private Shared Function close(ByVal cn As OleDbConnection) As Boolean
        Try
            Select Case cn.State
                Case ConnectionState.Closed
                    Return True
                Case ConnectionState.Open
                    cn.Close()
                    Return True
                Case Else
                    'TODO: Insert log
                    Return False
            End Select
        Catch ex As Exception
            'TODO: Insert log
            Return False
        End Try
    End Function

    Public Shared Function leggi(ByVal sql As String, ByVal cn As OleDbConnection, ByVal dt As DataTable) As DataTable
        If VERIFICA_XSS_SEC(sql) Then

            Exit Function
        End If
        sql = Replace(sql, "!-!", ";")

        Dim cmd As New OleDbCommand(sql, cn)

        Try
            '            If Not (open(cn)) Then
            'TODO: Insert log
            '               Return Nothing
            '              Exit Function
            '         End If
            cn.Open()
            dt.Load(cmd.ExecuteReader)
            cn.Close()
            '        If Not (close(cn)) Then
            '           'TODO: Insert log
            '          Return Nothing
            '         Exit Function
            '    End If
            cmd.Dispose()
            Return dt
        Catch ex As Exception
            'TODO: Insert log
            Return Nothing
            Exit Function
        Finally
            close(cn)
        End Try
    End Function

    Public Shared Function MakeExcelConnection(ByVal fileName As String) As Data.OleDb.OleDbConnection
        Dim conn As String
        conn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=Excel 8.0;"
        Dim connection As Data.OleDb.OleDbConnection = New Data.OleDb.OleDbConnection(conn)
        Return connection
    End Function

    Public Shared Function GetExcelSheetNames(ByVal connection As OleDbConnection) As String()
        Dim dt As New DataTable

        Try
            If Not (open(connection)) Then
                'TODO: Insert log
                Return Nothing
                Exit Function
            End If
            dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            If (dt.Rows.Count = 0) Then
                Return Nothing
            End If

            Dim excelSheets(dt.Rows.Count) As String
            Dim i As Int16 = 0

            Dim row As DataRow
            For Each row In dt.Rows
                excelSheets(i) = row("TABLE_NAME").ToString()
                i += 1
            Next

            Return excelSheets
        Catch ex As Exception
            'TODO: Insert log
            Return Nothing
        Finally
            If Not (close(connection)) Then
                'TODO: Insert log
            End If
        End Try
    End Function
#End Region
End Class
