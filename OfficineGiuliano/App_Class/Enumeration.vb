﻿Public Enum LoginStatus
    Success = 1
    Failed = 0
    Locked = -1
    RequiresVerification = -10
    UserUnknow = -20
    PassError = -30
End Enum

Public Enum PageTemplate
    OneColumn = 1
    TwoColumnLeftSide = 2
    TwoColumnRightSide = 3
End Enum