﻿Imports System
Imports System.Data
Imports System.Collections
Imports System.Net.Mail

Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.Collections.Generic
Imports System.Web.Script.Services

Imports System.Drawing
Imports System.IO

Public Class Utilities

    Public Shared Function TRASFORMA_DATA(value As String) As String
        Return "CONVERT(DATETIME, '" & Mid(value, 7, 4) & "-" & Mid(value, 4, 2) & "-" & Mid(value, 1, 2) & " 00:00:00', 102)"
    End Function

    Public Shared Function CreateRow(ByVal Text As String, ByVal Value As String, ByVal dt As DataTable) As DataRow
        Dim dr As DataRow = dt.NewRow()
        dr(0) = Text
        dr(1) = Value
        Return dr
    End Function

    Public Shared Function FixSQL(stringa) As String
        stringa = Replace(stringa, "'", "''")
        stringa = Replace(stringa, "%", "[%]")
        stringa = Replace(stringa, "[", "[[]")
        stringa = Replace(stringa, "]", "[]]")
        ' stringa = Replace(stringa, "_", "[_]")
        '  stringa = Replace(stringa, "#", "[#]")
        If stringa > "" Then stringa = stringa.trim
        Return stringa
    End Function

    Public Shared Function EvalNull(ByVal str As Object) As String
        If str Is DBNull.Value Then
            Return ""
        Else
            Return str
        End If
    End Function

    Public Shared Function fix_str(stringa) As String

        Return FixSQL(stringa)
    End Function

    Public Shared Function ottimizza_testo(value As String) As String

        Dim ret As String = value
        ret = Replace(ret, "_", " ")
        ret = Replace(ret, "-", " ")
        Return P_LETTER(ret)
    End Function

    Public Shared Function P_LETTER(stringa)
        If stringa > "" Then
            Return UCase(Left(stringa, 1)) & LCase(Right(stringa, Len(stringa) - 1))
        End If
    End Function

    Public Shared Function StringToUrl(ByVal _string As String) As String
        Return str_to_url(_string)
    End Function

    Public Shared Function str_to_url(value As String) As String

        Dim ret As String = value

        ret = Replace(ret, " ", "-")
        ret = Replace(ret, ".", "-")
        ret = Replace(ret, ",", "-")
        ret = Replace(ret, """", "-")
        ret = Replace(ret, ":", "-")
        ret = Replace(ret, ";", "-")
        ret = Replace(ret, "*", "-")
        ret = Replace(ret, "'", "-")
        ret = Replace(ret, "?", "-")
        ret = Replace(ret, "è", "e")
        ret = Replace(ret, "ù", "u")
        ret = Replace(ret, "ò", "o")
        ret = Replace(ret, "à", "a")
        ret = Replace(ret, "#", "-")
        ret = Replace(ret, "*", "-")
        ret = Replace(ret, "&", "-")
        ret = Replace(ret, "?", "-")
        ret = Replace(ret, "!", "-")
        ret = Replace(ret, "(", "-")
        ret = Replace(ret, ")", "-")
        ret = Replace(ret, "=", "-")
        ret = Replace(ret, "$", "-")
        ret = Replace(ret, "%", "-")
        ret = Replace(ret, "€", "-")
        ret = Replace(ret, "[", "-")
        ret = Replace(ret, "]", "-")
        ret = Replace(ret, "@", "-")

        If ret <> "" Then
            Return (ret.ToLower)
        Else
            Return "***errore***"
        End If
    End Function

    Public Shared Function INSERT_MAIL_FORM(ByVal DATA As String, ByVal SITO_FROM As String, ByVal SITO_TO As String, ByVal FROM As String, ByVal OGGETTO As String, ByVal TESTO As String, Optional ByVal nome_sito As String = "Contatti Sito", Optional ByVal idm As String = "", Optional ByVal scrivi As Boolean = True) As Boolean
        Dim sql As String
        sql = " insert into _MAIL_FORM (DATA, FROM_MAIL, OGGETTO, TESTO, ID_MERCHANT) values (" & TRASFORMA_DATA(DATA) & ", '" & FROM & "', '" & OGGETTO & "', '" & TESTO & "', '" & idm & "')"

        If scrivi Then ConnectionClass.exec(sql, ConnectionClass.cnDBSql)

        INVIA_EMAIL(FROM, "commerciale@viaconnoi.it", OGGETTO, TESTO, nome_sito)

        Return True
    End Function
    Public Shared Function INSERT_MAIL_FORM_ALL(ByVal DATA As String, ByVal SITO_FROM As String, ByVal SITO_TO As String, ByVal FROM As String, ByVal OGGETTO As String, ByVal TESTO As String, ByVal allegato As String, Optional ByVal nome_sito As String = "Contatti Sito", Optional ByVal idm As String = "", Optional ByVal scrivi As Boolean = True) As Boolean
        Dim sql As String
        sql = " insert into _MAIL_FORM (DATA, FROM_MAIL, OGGETTO, TESTO, ID_MERCHANT) values (" & TRASFORMA_DATA(DATA) & ", '" & FROM & "', '" & OGGETTO & "', '" & TESTO & "', '" & idm & "')"

        If scrivi Then ConnectionClass.exec(sql, ConnectionClass.cnDBSql)

        INVIA_EMAIL(FROM, SITO_TO, OGGETTO, TESTO, nome_sito, allegato)
        '   INVIA_EMAIL(SITO_FROM, FROM, OGGETTO, "Grazie per averci inviato la tua richiesta. <br/> Atelier Signore", nome_sito)
        '  INVIA_EMAIL(SITO_FROM, "patrizio.musilli@gmail.com", OGGETTO, "nuova mail " & SITO_FROM, nome_sito)

        Return True
    End Function

    Public Shared Function INSERT_MAIL_FORM_M(ByVal DATA As String, ByVal SITO_FROM As String, ByVal SITO_TO As String, ByVal FROM As String, ByVal OGGETTO As String, ByVal TESTO As String, Optional nome_sito As String = "Contatti Sito") As Boolean

        INVIA_EMAIL(FROM, SITO_TO, OGGETTO, TESTO, nome_sito)
        'INVIA_EMAIL(SITO_FROM, "patrizio.musilli@gmail.com", OGGETTO, "nuova mail " & SITO_FROM, nome_sito)
        INVIA_EMAIL(SITO_FROM, SITO_FROM, "Copia " & OGGETTO, TESTO, nome_sito)

        Return True
    End Function

    Public Shared Function INSERT_MAIL_FORM_M_old(ByVal DATA As String, ByVal SITO_FROM As String, ByVal SITO_TO As String, ByVal FROM As String, ByVal OGGETTO As String, ByVal TESTO As String, ByVal allegato As String, Optional nome_sito As String = "Contatti Sito") As Boolean

        INVIA_EMAIL(FROM, SITO_TO, OGGETTO, TESTO, nome_sito, allegato)
        ' INVIA_EMAIL(SITO_FROM, "patrizio.musilli@gmail.com", OGGETTO, "nuova mail " & SITO_FROM, nome_sito)
        INVIA_EMAIL(SITO_FROM, SITO_FROM, "Copia " & OGGETTO, TESTO, nome_sito)

        Return True
    End Function

    Public Shared Function INVIA_EMAIL(ByVal send_from As String, ByVal send_to As String, ByVal mess_obj As String, ByVal msg_body As String, nome_visualizzato As String) As Boolean
        Dim messaggio As MailMessage = New MailMessage()
        messaggio.From = New MailAddress(send_from, nome_visualizzato)
        messaggio.To.Add(New MailAddress(send_to, nome_visualizzato))
        messaggio.Subject = mess_obj
        messaggio.SubjectEncoding = System.Text.Encoding.UTF8
        messaggio.Body = msg_body
        messaggio.BodyEncoding = System.Text.Encoding.UTF8
        messaggio.IsBodyHtml = True
        Try
            Dim msg As SmtpClient = New SmtpClient()
            msg.Send(messaggio)

            Return True
        Catch
            Return False
        End Try
    End Function

    Public Shared Function INVIA_EMAIL(ByVal send_from As String, ByVal send_to As String, ByVal mess_obj As String, ByVal msg_body As String, nome_visualizzato As String, ByVal allegato As String) As Boolean
        Dim messaggio As MailMessage = New MailMessage()
        messaggio.From = New MailAddress(send_from, nome_visualizzato)
        messaggio.To.Add(New MailAddress(send_to, nome_visualizzato))
        messaggio.Subject = mess_obj
        messaggio.SubjectEncoding = System.Text.Encoding.UTF8
        messaggio.Body = msg_body
        messaggio.BodyEncoding = System.Text.Encoding.UTF8
        messaggio.IsBodyHtml = True
        Dim Attch As Net.Mail.Attachment = New Net.Mail.Attachment(allegato)
        messaggio.Attachments.Add(Attch)

        Try
            Dim msg As SmtpClient = New SmtpClient()
            msg.Send(messaggio)

            Return True
        Catch
            Return False
        End Try
    End Function

    Public Shared Function ISCRIVI_NEWSLETTER(ByVal email As String, id_merchant As String) As String

        Dim ds As New DataTable
        ConnectionClass.leggi("select * from _MAIL_NEWS where id_merchant = '" & id_merchant & "' and MAIL = '" & email & "'", ConnectionClass.cnDBSql, ds)

        If ds.Rows.Count = 0 Then

            If ConnectionClass.exec("insert into _MAIL_NEWS (mail, id_merchant) values ('" & email & "', '" & id_merchant & "')", ConnectionClass.cnDBSql) Then

                Return "Mail inserita grazie !"
            End If

        Else
            If ds.Rows(0)("flag_attivo") = "0" Then
                If ConnectionClass.exec("update _MAIL_NEWS set flag_attivo = 1 where pk_codice = " & ds.Rows(0)("PK_CODICE"), ConnectionClass.cnDBSql) Then

                    Return "Mail inserita grazie !"
                End If

            Else
                Return "Mail presente nella nostra lista !"
            End If
        End If
    End Function

    Public Shared Function verifica_email(str As String) As Boolean
        Dim str_v As String = "^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
        Dim r = New System.Text.RegularExpressions.Regex(str_v, RegexOptions.IgnoreCase)

        Return r.Match(str).Success

    End Function

    Function NoIllegal(ByVal strInput)
        Dim R As Regex
        R = New Regex("[^a-zA-Z\s]", RegexOptions.Compiled)

        NoIllegal = R.Replace(strInput, "")
        NoIllegal = Replace(NoIllegal, " ", "_")
    End Function

    Public Shared Sub CARICA_IMAGES(path As String, nome_file As String, w As Int16, h As Int16, folder As String)
        Dim dyr As String = HttpContext.Current.Server.MapPath("~\public\image\")
        Dim h_ As Int16 = h
        Dim pref As String = ""

        If h = -1 Then
            Dim fileID As New FileInfo(dyr + path + "\" & nome_file)
            Dim orgIMG As System.Drawing.Image = System.Drawing.Image.FromFile(fileID.FullName)
            h_ = CInt((orgIMG.Height * w) / orgIMG.Width)
        End If

        Try
            If (Resize(pref, nome_file, dyr + path + "\", dyr + folder + "\", w, h_)) Then
                If path = folder Then File.Delete(dyr + "\" + folder + "\" + pref + nome_file)

            Else
                '  msg("Impossibile effettuare il resize")
            End If
        Catch ex As Exception
            'scrivi log
            '  msg("Impossibile effettuare il resize")
        End Try
    End Sub

    Public Shared Function Resize(ByVal pref As String, ByVal name As String, ByVal path As String, ByVal upload_dir As String, ByVal width As Int16, ByVal height As Int16) As Boolean
        Dim newWidth, newHeight As Integer ' new width/height for the thumbnail
        Const upload_max_size As Integer = 30000 ' max size of the upload (KB) note: this doesn't override any server upload limits
        Dim originalimg As System.Drawing.Image ' used to hold the original image
        Dim msag As String ' display results
        Dim upload_ok As Boolean ' did the upload work ?
        Dim l2 As Integer  ' temp variable used when calculating new size

        upload_ok = False
        Dim fileFld As New FileInfo(path & name)

        If LCase(HttpContext.Current.Request.ServerVariables("REQUEST_METHOD")) = "post" Then
            'fileFld = Request.Files(0) ' get the first file uploaded from the form (note:- you can use this to itterate through more than one image)

            If fileFld.Length > upload_max_size * 1024 Then
                msag = "Immagine superiore ai " & upload_max_size & "Kb"
            Else
                Try
                    originalimg = System.Drawing.Image.FromFile(fileFld.FullName)

                    Dim new_width As Int32 = width

                    If originalimg.Width < originalimg.Height Then
                        new_width = CInt(originalimg.Width * height / originalimg.Height)
                    End If

                    If (originalimg.Width / new_width) > (originalimg.Width / height) Then
                        l2 = originalimg.Width
                        newWidth = new_width
                        newHeight = originalimg.Height * (new_width / l2)
                        If newHeight > height Then
                            newWidth = newWidth * (height / newHeight)
                            newHeight = height
                        End If
                    Else
                        l2 = originalimg.Height
                        newHeight = height
                        newWidth = originalimg.Width * (height / l2)
                        If newWidth > new_width Then
                            newHeight = newHeight * (new_width / newWidth)
                            newWidth = new_width
                        End If
                    End If

                    Dim thumb As New Bitmap(width, height)

                    Dim gr_dest As Graphics = Graphics.FromImage(thumb)
                    gr_dest.InterpolationMode = 2
                    gr_dest.SmoothingMode = 4
                    gr_dest.PixelOffsetMode = 4
                    gr_dest.CompositingQuality = 4

                    Dim sb = New SolidBrush(System.Drawing.Color.White)
                    gr_dest.FillRectangle(sb, 0, 0, CInt((width - new_width) / 2), thumb.Height)
                    gr_dest.FillRectangle(sb, CInt((width - new_width) / 2), 0, new_width, thumb.Height)
                    gr_dest.FillRectangle(sb, CInt((width - new_width) / 2) + new_width, 0, CInt((width - new_width) / 2), thumb.Height)

                    'Re-draw the image to the specified height and width
                    gr_dest.DrawImage(originalimg, CInt((width - new_width) / 2), 0, new_width, thumb.Height)

                    Try
                        'Response.ContentType = "image/jpeg"

                        thumb.Save((upload_dir & pref & name), originalimg.RawFormat)

                        upload_ok = True
                    Catch
                        upload_ok = False
                    End Try
                    ' Housekeeping for the generated thumbnail
                    If Not thumb Is Nothing Then
                        thumb.Dispose()
                        thumb = Nothing
                    End If
                Catch
                    upload_ok = False
                End Try
            End If
            ' House Keeping !
            If Not originalimg Is Nothing Then
                originalimg.Dispose()
                originalimg = Nothing
            End If
        End If
        Return upload_ok
    End Function

    Public Shared Function BuildPagers(value As String, TotalPages As Integer, currentPage As Integer, n_page As Integer) As PlaceHolder
        Dim strHTTP As String
        Dim pag_name As String = value

        If InStr(pag_name, "#") = 0 Then pag_name = pag_name & "?p=#"
        '  response.write("intstr " &  instr(pag_name, "#") & value)
        If HttpContext.Current.Request.QueryString("dest") <> Nothing Then strHTTP += "&dest=" & HttpContext.Current.Request.QueryString("dest")

        Dim paginazione As New PlaceHolder

        Dim i As Integer
        Dim lb As Label
        Dim _p As Integer = 0

        If TotalPages > 1 Then
            lb = New Label()
            lb.Text = "Ci sono " & TotalPages & " pagine con i risultati: "
            paginazione.Controls.Add(lb)

            Dim min_page As Integer = currentPage - n_page
            Dim max_page As Integer = currentPage + n_page

            For i = 1 To (TotalPages)
                If i = 1 Then
                    lb = New Label()
                    lb.Text = "<a href='" & Replace(pag_name & strHTTP, "#", "1") & "' class='num'>Prima</a>" & vbCrLf
                    paginazione.Controls.Add(lb)
                End If

                If (_p = 0) And (currentPage > 1) Then
                    lb = New Label()
                    lb.Text = "<a href='" & Replace(pag_name & strHTTP, "#", currentPage - 1) & "' class='num'>Indietro</a> " & vbCrLf
                    paginazione.Controls.Add(lb)
                    _p = 1
                End If

                If ((i >= min_page) And (i <= max_page)) Then

                    lb = New Label()
                    lb.ID = "Pagina" & i

                    If currentPage = i Then
                        lb.Text = "<span class='num_active'>" & i & "</span>" & vbCrLf
                    Else
                        lb.Text = "<a Class=""num""  href=""" & Replace(pag_name & strHTTP, "#", i) & """>" & i & "</a> " & vbCrLf
                    End If

                    paginazione.Controls.Add(lb)
                End If

                If i = TotalPages Then

                    If currentPage < TotalPages Then
                        lb = New Label()
                        lb.Text = "<a Class=""num"" href=""" & Replace(pag_name & strHTTP, "#", currentPage + 1) & """>Avanti</a> " & vbCrLf
                        paginazione.Controls.Add(lb)
                    End If
                    lb = New Label()
                    lb.Text = "<a Class=""num"" href='" & Replace(pag_name & strHTTP, "#", TotalPages) & "'>Ultima</a> " & vbCrLf
                    paginazione.Controls.Add(lb)
                End If

            Next
        End If
        Return paginazione
    End Function

    Public Shared Sub js_esegui(ByRef p As Page, ByVal value As String)
        Dim csname1 As String = "PopupScript"
        Dim csname2 As String = "ButtonClickScript"
        Dim cstype As Type = p.GetType()
        Dim cs As ClientScriptManager = p.ClientScript
        If (Not cs.IsStartupScriptRegistered(cstype, csname1)) Then
            Dim cstext1 As String = value
            cs.RegisterStartupScript(cstype, csname1, cstext1, True)
        End If
    End Sub
End Class