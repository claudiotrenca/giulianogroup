﻿Imports System
Imports System.Data
Imports System.Collections


Partial Public Class UsersClass
    Private _nominativo As String
    Private _email As String
    Private _password As String
    Private _livello As String
    Private _DATA_UA As String
    Private _ATTIVO As Integer
    Private _ERROR As String
    Private _referrer As String
    Private _id As Integer

    ReadOnly Property REFERRER() As String
        Get
            Return _referrer
        End Get
    End Property

    ReadOnly Property ID() As Integer
        Get
            Return _id
        End Get
    End Property


    Public Property NOMINATIVO() As String
        Get
            Return _nominativo
        End Get
        Set(ByVal value As String)
            _nominativo = value
        End Set
    End Property

    Public Property USER_EMAIL() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    Public Property PASS_WORD() As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property

    Public Property LIVELLO() As String
        Get
            Return _livello
        End Get
        Set(ByVal value As String)
            _livello = value
        End Set
    End Property
    Public Property ATTIVO() As String
        Get
            Return _ATTIVO
        End Get
        Set(ByVal value As String)
            _ATTIVO = value
        End Set
    End Property


    ReadOnly Property DATA_UA() As String
        Get
            Return _DATA_UA
        End Get
    End Property


    ReadOnly Property ERRORE() As String
        Get
            Return _ERROR
        End Get
    End Property

    Dim cn_str As String
    Dim tbNAME As String = "_UTENTI"

    Public Sub New(value As String)
        _referrer = value
    End Sub


    Public Function DATI_UTENTE(ByVal NOME_UT As String) As Boolean
        Dim dt As New DataTable
        ConnectionClass.leggi("Select pk_codice from " & tbNAME & " where ID_MERCHAND = '" & _referrer & "' and  ATTIVO = 1 AND USER_NAME = '" & NOME_UT & "'", ConnectionClass.cnDBSql, dt)
        If dt.Rows.Count > 0 Then
            ESTRAI_SCHEDA(dt.Rows(0)(0))
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub ESTRAI_SCHEDA(ByVal value As Integer)
        Dim ds As New DataTable
        Dim sql As String = "SELECT * FROM " & tbNAME & " WHERE PK_CODICE = " & value
        ds = ConnectionClass.leggi(sql, ConnectionClass.cnDBSql, ds)

        _id = value
        _nominativo = ds.Rows(0)("NOMINATIVO").trim
        _email = ds.Rows(0)("USER_NAME").trim
        _password = ds.Rows(0)("PASS_WORD").trim
        _livello = ds.Rows(0)("TIPO")
        _ATTIVO = ds.Rows(0)("ATTIVO")
        _DATA_UA = String.Format("{0:d}", ds.Rows(0)("DATA_UA").ToString)
    End Sub

    Sub AGGIORNA_ACCESSO(value As Integer)
        ConnectionClass.exec("update " & tbNAME & " set DATA_UA = " & Classi.Utility.TRASFORMA_DATA(Date.Now) & ", accessi = (accessi +1) where pk_codice = " & value, ConnectionClass.cnDBSql)
    End Sub

    Public Function ValidateLogin(ByVal customerName As String, ByVal password As String, Optional ByVal tipo As Integer = -1) As Integer
        Dim dt As New DataTable
        Dim str As String = "SELECT * FROM " & tbNAME &
                            " where (ID_MERCHAND = '" & _referrer & "' or ID_MERCHAND ='-1') and ATTIVO = 1 and USER_NAME='" & customerName &
                            "' and pass_word = '" & password.Trim & "'"
        If tipo >= 0 Then str += " and tipo = " & tipo



        If ConnectionClass.leggi(str, ConnectionClass.cnDBSql, dt).Rows.Count = 0 Then
            Return -1
        Else
            Return dt.Rows(0)("PK_CODICE")
        End If
    End Function

    Public Function mod_user(flag_attivo As Integer, nome_tabella As String, pk As Integer)
        Return ConnectionClass.exec("update " & tbNAME & " set attivo = " & flag_attivo & " where pk_codice = (select fk_utente from " & nome_tabella & " where pk_codice = " & pk & ")", ConnectionClass.cnDBSql)
    End Function

    Public Function ADD() As Integer
        Dim id As Integer = ValidateLogin(_email, _password)
        If id = -1 Then
            Dim sql As String = "SET NOCOUNT ON!-! INSERT INTO " & tbNAME & " (ID_MERCHAND, NOMINATIVO, USER_NAME, PASS_WORD, ATTIVO, TIPO_INTERNO, TIPO) values ('" & _referrer & "', '" &
                               _nominativo & "', '" & _email & "', '" & _password & "', " & _ATTIVO & ", " & _livello & ",  " & _livello & " )!-! SELECT SCOPE_IDENTITY() AS NewID!-!"

            Dim i As Integer

            i = ConnectionClass.leggi(sql, ConnectionClass.cnDBSql, New DataTable).Rows(0)(0)

            Return i
        Else
            _ERROR = id
            Return -1
        End If
    End Function


    Public Function del(pk As Integer) As Boolean
        Return ConnectionClass.exec("delete from " & tbNAME & " where pk_codice = " & pk, ConnectionClass.cnDBSql)
    End Function



    Public Function EDIT(ByVal PK As String) As Boolean
        Dim sql As String = "UPDATE " & tbNAME & " set NOMINATIVO = '" & _nominativo & "', " &
                "USER_NAME = '" & _email & "', " &
                "PASS_WORD = '" & _password & "', " &
                "TIPO = " & _livello & ", " &
                "ATTIVO = " & _ATTIVO
        sql += " where pk_codice = " & PK
        Dim blnOK As Boolean
        blnOK = ConnectionClass.exec(sql, ConnectionClass.cnDBSql)

        Return blnOK
    End Function




End Class

