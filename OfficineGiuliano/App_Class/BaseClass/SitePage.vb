﻿Imports Microsoft.VisualBasic
Imports System.Globalization
Imports System.Threading

Namespace BaseClass

    Public MustInherit Class SitePage
        Inherits System.Web.UI.Page

        Private Shared ReadOnly RegexBetweenTags As New Regex(">(?! )\s+", RegexOptions.Compiled)
        Private Shared ReadOnly RegexLineBreaks As New Regex("([\n\s])+?(?<= {2,})<", RegexOptions.Compiled)

        Protected BaseMaster As SiteMasterpage
        Protected _Meta As New MetaClass

        Protected Friend _Content As New DataClass.vwPost
        Protected Friend _LinkCulture As String = String.Empty
        Protected Friend _Provincia As String = String.Empty

        Protected _PageTitolo As String
        Protected _PageSottotitolo As String
        Protected _PageSezione As String
        Protected _PageContenutoBreve As String
        Protected _PageContent As String
        Protected _PageUpperContent As String
        Protected _PageFooterContent As String

        Public Property MetaPage As MetaClass
            Get
                Return _Meta
            End Get
            Set(value As MetaClass)
                _Meta = value
            End Set
        End Property

        Public Property PageTitolo As String
            Get
                Return _PageTitolo
            End Get
            Set(value As String)
                _PageTitolo = value
            End Set
        End Property

        Public Property PageSottotitolo As String
            Get
                Return _PageSottotitolo
            End Get
            Set(value As String)
                _PageSottotitolo = value
            End Set
        End Property

        Public Property PageSezione As String
            Get
                Return _PageSezione
            End Get
            Set(value As String)
                _PageSezione = value
            End Set
        End Property

        Public Property PageContenutoBreve As String
            Get
                Return _PageContenutoBreve
            End Get
            Set(value As String)
                _PageContenutoBreve = value
            End Set
        End Property

        Public Property PageContent As String
            Get
                Return _PageContent
            End Get
            Set(value As String)
                _PageContent = value
            End Set
        End Property

        Public Property PageUpperContent As String
            Get
                Return _PageUpperContent
            End Get
            Set(value As String)
                _PageUpperContent = value
            End Set
        End Property

        Public Property PageFooterContent As String
            Get
                Return _PageFooterContent
            End Get
            Set(value As String)
                _PageFooterContent = value
            End Set
        End Property

        'Protected Overrides Sub Render(writer As HtmlTextWriter)
        '    Using htmlwriter As New HtmlTextWriter(New System.IO.StringWriter())
        '        MyBase.Render(htmlwriter)
        '        Dim html As String = RemoveWhitespaceFromHtml(htmlwriter.InnerWriter.ToString())
        '        'Dim html As String = (htmlwriter.InnerWriter.ToString())
        '        html = html.Replace("#CULTURE#", _LinkCulture)
        '        html = html.Replace("#PROVINCIA#", _Provincia)
        '        writer.Write(html)
        '    End Using
        'End Sub

        Public Shared Function RemoveWhitespaceFromHtml(html As String) As String

            html = RegexBetweenTags.Replace(html, ">")
            html = RegexLineBreaks.Replace(html, "<")

            Return html.Trim()

        End Function

        Protected Overrides Sub InitializeCulture()

            Dim lang As String = Page.RouteData.Values("culture")

            Select Case lang

                Case "ita"

                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("it-IT")
                    Thread.CurrentThread.CurrentUICulture = New CultureInfo("it-IT")

                Case "eng"

                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US")
                    Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-US")

                Case Else

                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("it-IT")
                    Thread.CurrentThread.CurrentUICulture = New CultureInfo("it-IT")

            End Select

            HttpContext.Current.Session("culture") = Thread.CurrentThread.CurrentUICulture.ThreeLetterISOLanguageName

        End Sub

        Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

            _Provincia = IIf(Page.RouteData.Values("provincia") <> Nothing, Page.RouteData.Values("provincia"), ConfigurationManager.AppSettings("default_prov"))

            If HttpContext.Current.Session("culture") <> "ita" Then
                _LinkCulture = "/" & HttpContext.Current.Session("culture") & "/"
            Else
                _LinkCulture = "/"
            End If

        End Sub

    End Class

End Namespace