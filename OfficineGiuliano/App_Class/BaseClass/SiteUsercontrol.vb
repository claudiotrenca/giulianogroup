﻿Imports Microsoft.VisualBasic

Namespace BaseClass
    Public MustInherit Class SiteUsercontrol
        Inherits System.Web.UI.UserControl

        Protected BasePage As SitePage
        Protected BaseMaster As SiteMasterpage

        Private Shared ReadOnly RegexBetweenTags As New Regex(">(?! )\s+", RegexOptions.Compiled)
        Private Shared ReadOnly RegexLineBreaks As New Regex("([\n\s])+?(?<= {2,})<", RegexOptions.Compiled)


        Protected Friend _Provincia As String = String.Empty
        Protected Friend _LinkCulture As String = String.Empty

        Protected Overrides Sub Render(writer As HtmlTextWriter)
            Using htmlwriter As New HtmlTextWriter(New System.IO.StringWriter())
                MyBase.Render(htmlwriter)
                Dim html As String = RemoveWhitespaceFromHtml(htmlwriter.InnerWriter.ToString())
                'Dim html As String = (htmlwriter.InnerWriter.ToString())

                writer.Write(html)
            End Using
        End Sub

        Public Shared Function RemoveWhitespaceFromHtml(html As String) As String
            html = RegexBetweenTags.Replace(html, ">")
            html = RegexLineBreaks.Replace(html, "<")

            Return html.Trim()
        End Function

    End Class

End Namespace

