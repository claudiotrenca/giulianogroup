﻿Imports Microsoft.VisualBasic
Imports System.Web

Public Class FormActionWriter

    Public Sub New()
    End Sub

    Public Shared Function ActionUrl() As String
        Dim context = HttpContext.Current
        Return If([String].IsNullOrEmpty(context.Request.ServerVariables("HTTP_X_ORIGINAL_URL")), context.Request.RawUrl, context.Request.ServerVariables("HTTP_X_ORIGINAL_URL"))
    End Function
End Class