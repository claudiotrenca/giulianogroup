﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Namespace BaseClass

    Public MustInherit Class SiteMasterpage
        Inherits System.Web.UI.MasterPage

        Protected Friend _Rif As Integer
        Protected Friend _PageLink As String

        Protected _PageTitle As String
        Protected _PageDescription As String
        Protected _PageKeywords As String
        Protected _PageCanonical As String

        Protected _PageTitolo As String
        Protected _PageSottotitolo As String
        Protected _PageSezione As String
        Protected _PageContenutoBreve As String
        Protected _PageContent As String
        Protected _PageUpperContent As String
        Protected _PageFooterContent As String
        Protected _Provincia As String

        Protected Friend _Content As DataClass.vwPost
        Protected Friend _Meta As MetaClass

        Protected Friend _H1Content As String
        Protected Friend _DivLinkContent As String
        Protected Friend _NoScriptContent As String

        Public Property Meta As MetaClass
            Get
                Return _Meta
            End Get
            Set(value As MetaClass)
                _Meta = value
            End Set
        End Property

        Public Property Rif As Integer
            Get
                Return _Rif
            End Get
            Set(value As Integer)
                _Rif = value
            End Set
        End Property

        Public Property PageTitle As String
            Get
                Return _PageTitle
            End Get
            Set(value As String)
                _PageTitle = value
            End Set
        End Property

        Public Property PageDescription As String
            Get
                Return _PageDescription
            End Get
            Set(value As String)
                _PageDescription = value
            End Set
        End Property

        Public Property PageKeywords As String
            Get
                Return _PageKeywords
            End Get
            Set(value As String)
                _PageKeywords = value
            End Set
        End Property

        Public Property PageCanonical As String
            Get
                Return _PageCanonical
            End Get
            Set(value As String)
                _PageCanonical = value
            End Set
        End Property

        Public Property PageContent As String
            Get
                Return _PageContent
            End Get
            Set(value As String)
                _PageContent = value
            End Set
        End Property

        Public Property PageUpperContent As String
            Get
                Return _PageUpperContent
            End Get
            Set(value As String)
                _PageUpperContent = value
            End Set
        End Property

        Public Property PageFooterContent As String
            Get
                Return _PageFooterContent
            End Get
            Set(value As String)
                _PageFooterContent = value
            End Set
        End Property

        Public Property Provincia As String
            Get
                Return _Provincia
            End Get
            Set(value As String)
                _Provincia = value
            End Set
        End Property

        Public Sub InitMetaTag(Optional ByVal _Str As String = "")

            Dim head As HtmlHead = Page.Header

            If Not _Meta.MetaTitle Is Nothing Then
                Page.Title = _Meta.MetaTitle.ToString.Replace("@PROV", _Provincia)
            Else
                Page.Title = _Str & " - " & ConfigurationManager.AppSettings("nome_sito_v")
            End If

            Dim meta_d As HtmlMeta = New HtmlMeta()
            meta_d.Name = "description"
            If Not _Meta.MetaDescription Is Nothing Then
                meta_d.Content = _Meta.MetaDescription.ToString.Replace("@PROV", _Provincia)
            Else
                meta_d.Content = ConfigurationManager.AppSettings("META_DESC")
            End If
            head.Controls.Add(meta_d)

            Dim meta_k As HtmlMeta = New HtmlMeta()
            meta_k.Name = "keywords"
            If Not _Meta.MetaKeyword Is Nothing Then
                meta_k.Content = _Meta.MetaKeyword
            End If
            head.Controls.Add(meta_k)

            Dim metacache As HtmlMeta = New HtmlMeta()
            metacache.HttpEquiv = "Expires"
            Dim dtt As DateTime = DateTime.Now
            dtt = dtt.AddDays(7)
            metacache.Content = dtt.ToString("r")
            head.Controls.Add(metacache)

        End Sub

    End Class

End Namespace