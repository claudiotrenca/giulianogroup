﻿Imports System.IO

Public Class UploadedFilesStorage
    Public Property Path() As String
    Public Property Key() As String
    Public Property LastUsageTime() As DateTime

    Public Property Files() As IList(Of UploadedFileInfo)
End Class

Public Class UploadedFileInfo
    Public Property UniqueFileName() As String
    Public Property OriginalFileName() As String
    Public Property FilePath() As String
    Public Property FileSize() As String
End Class

Public NotInheritable Class UploadControlHelper
    Private Const DisposeTimeout As Integer = 5
    Private Const FolderKey As String = "UploadDirectory"
    Private Const TempDirectory As String = "~/public/Temp/"
    Private Shared ReadOnly storageListLocker As Object = New Object()

    Private Sub New()
    End Sub

    Private Shared ReadOnly Property Context() As HttpContext
        Get
            Return HttpContext.Current
        End Get
    End Property

    Private Shared ReadOnly Property RootDirectory() As String
        Get
            Return Context.Request.MapPath(TempDirectory)
        End Get
    End Property

    Private Shared uploadedFilesStorageList_Renamed As IList(Of UploadedFilesStorage)

    Private Shared ReadOnly Property UploadedFilesStorageList() As IList(Of UploadedFilesStorage)
        Get
            Return uploadedFilesStorageList_Renamed
        End Get
    End Property

    Shared Sub New()
        uploadedFilesStorageList_Renamed = New List(Of UploadedFilesStorage)()
    End Sub

    Private Shared Function CreateTempDirectoryCore() As String
        Dim uploadDirectory As String = System.IO.Path.Combine(RootDirectory, System.IO.Path.GetRandomFileName())
        Directory.CreateDirectory(uploadDirectory)

        Return uploadDirectory
    End Function

    Public Shared Function GetUploadedFilesStorageByKey(ByVal key As String) As UploadedFilesStorage
        SyncLock storageListLocker
            Return GetUploadedFilesStorageByKeyUnsafe(key)
        End SyncLock
    End Function

    Private Shared Function GetUploadedFilesStorageByKeyUnsafe(ByVal key As String) As UploadedFilesStorage
        Dim storage As UploadedFilesStorage = UploadedFilesStorageList.Where(Function(i) i.Key = key).SingleOrDefault()
        If storage IsNot Nothing Then
            storage.LastUsageTime = DateTime.Now
        End If
        Return storage
    End Function

    Public Shared Function GenerateUploadedFilesStorageKey() As String
        Return Guid.NewGuid().ToString("N")
    End Function

    Public Shared Sub AddUploadedFilesStorage(ByVal key As String)
        SyncLock storageListLocker
            Dim storage As UploadedFilesStorage = New UploadedFilesStorage With {.Key = key, .Path = CreateTempDirectoryCore(), .LastUsageTime = DateTime.Now, .Files = New List(Of UploadedFileInfo)()}
            UploadedFilesStorageList.Add(storage)
        End SyncLock
    End Sub

    Public Shared Sub RemoveUploadedFilesStorage(ByVal key As String)
        SyncLock storageListLocker
            Dim storage As UploadedFilesStorage = GetUploadedFilesStorageByKeyUnsafe(key)
            If storage IsNot Nothing Then
                Directory.Delete(storage.Path, True)
                UploadedFilesStorageList.Remove(storage)
            End If
        End SyncLock
    End Sub

    Public Shared Sub RemoveOldStorages()
        If (Not Directory.Exists(RootDirectory)) Then
            Directory.CreateDirectory(RootDirectory)
        End If

        SyncLock storageListLocker
            Dim existingDirectories() As String = Directory.GetDirectories(RootDirectory)
            For Each directoryPath As String In existingDirectories
                Dim storage As UploadedFilesStorage = UploadedFilesStorageList.Where(Function(i) i.Path = directoryPath).SingleOrDefault()
                If storage Is Nothing OrElse (DateTime.Now - storage.LastUsageTime).TotalMinutes > DisposeTimeout Then
                    Directory.Delete(directoryPath, True)
                    If storage IsNot Nothing Then
                        UploadedFilesStorageList.Remove(storage)
                    End If
                End If
            Next directoryPath
        End SyncLock
    End Sub

    Public Shared Function AddUploadedFileInfo(ByVal key As String, ByVal originalFileName As String) As UploadedFileInfo
        Dim currentStorage As UploadedFilesStorage = GetUploadedFilesStorageByKey(key)
        Dim fileInfo As UploadedFileInfo = New UploadedFileInfo With {.FilePath = System.IO.Path.Combine(currentStorage.Path, System.IO.Path.GetRandomFileName()), .OriginalFileName = originalFileName, .UniqueFileName = GetUniqueFileName(currentStorage, originalFileName)}
        currentStorage.Files.Add(fileInfo)

        Return fileInfo
    End Function

    Public Shared Function GetDemoFileInfo(ByVal key As String, ByVal fileName As String) As UploadedFileInfo
        Dim currentStorage As UploadedFilesStorage = GetUploadedFilesStorageByKey(key)
        Return currentStorage.Files.Where(Function(i) i.UniqueFileName = fileName).SingleOrDefault()
    End Function

    Public Shared Function GetUniqueFileName(ByVal currentStorage As UploadedFilesStorage, ByVal fileName As String) As String
        Dim baseName As String = System.IO.Path.GetFileNameWithoutExtension(fileName)
        Dim ext As String = System.IO.Path.GetExtension(fileName)
        Dim index As Integer = 1

        Do While currentStorage.Files.Any(Function(i) i.UniqueFileName = fileName)
            fileName = String.Format("{0} ({1}){2}", baseName, index, ext)
            index += 1
        Loop

        Return fileName
    End Function

End Class