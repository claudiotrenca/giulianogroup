﻿Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Collections
Imports System.Text.RegularExpressions.Regex
Imports System.Net.Mail

Namespace Classi
    Public Class Cart
        Private _ERRORE As String
        Private _adds_CartItemP As String

        ReadOnly Property ERRORE() As String
            Get
                Return _ERRORE
            End Get
        End Property

        Dim tbNAME As String = "_carrello"
        Dim vwNAME As String = "vwcarrello"
        Private _referrer As String

        Public Sub New(ByVal _ref As String)
            _referrer = _ref
        End Sub

        Public Function soglia_free_ship() As Double

            Return ConnectionClass.leggi("SELECT SOGLIA_FREE_SHIP FROM _SISTEMA WHERE ID_MERCHANT = '" & _referrer & "' ", ConnectionClass.cnDBSql, New DataTable).Rows(0)(0)

        End Function

        Public Function CODICE_PROMO(value As String) As Double
            Dim dt As New DataTable
            ConnectionClass.leggi("SELECT VALORE FROM _CODICI_PROMO WHERE DESCRIZIONE = '" & value & "' and data_scad >= " & Classi.Utility.TRASFORMA_DATA(Now.ToString), ConnectionClass.cnDBSql, dt)

            Dim value_sc As Double = -1
            If dt.Rows.Count > 0 Then value_sc = dt.Rows(0)("VALORE")
            Return value_sc
        End Function

        Public Sub UpdateShoppingCartItem(shoppingCartID As Integer, quantity As Integer)
            ConnectionClass.exec("Update _CARRELLO Set QTA=" & quantity & " where PK_CODICE=" & shoppingCartID, ConnectionClass.cnDBSql)
        End Sub

        Public Sub UpdateShoppingCartItemP(shoppingCartID As Integer, quantity As Integer, fk_colore As Integer, fk_taglia As Integer)
            ConnectionClass.exec("Update _CARRELLO Set QTA=" & quantity & " where PK_CODICE=" & shoppingCartID & " AND fk_fantasia=" & fk_colore & " AND fk_taglia=" & fk_taglia, ConnectionClass.cnDBSql)
        End Sub

        Public Sub MigrateShoppingCartItems(currentShopperName As String, migrateShopperName As String)
            Dim updateStatement As String = "Update _CARRELLO Set IDUTENTE='" & migrateShopperName _
                  & "' where IDUTENTE='" & currentShopperName & "'"

            ConnectionClass.exec(updateStatement, ConnectionClass.cnDBSql)
        End Sub

        Public Sub DeleteShoppingCartItem(shoppingCartID As Integer)
            ConnectionClass.exec("Delete FROM _CARRELLO where PK_CODICE=" & shoppingCartID, ConnectionClass.cnDBSql)
        End Sub

        Public Sub ResetShoppingCart(currentShopperName As String)
            ConnectionClass.exec("Delete FROM _CARRELLO where id_merchant = '" & _referrer & "' and IDUTENTE = '" & currentShopperName & "'", ConnectionClass.cnDBSql)

        End Sub

        Public Function GetShoppingCartItem(customerName As String, productCode As Integer) As DataTable
            Return ConnectionClass.leggi("SELECT * FROM _CARRELLO WHERE id_merchant = '" & _referrer & "' and IDUTENTE = '" & customerName & "' AND FK_PRODOTTO = " & productCode, ConnectionClass.cnDBSql, New DataTable)

        End Function

        Public Function GetShoppingCartItems(customerName As String, lista As String) As DataTable
            '   if lista <> "-1" then
            '   Return ConnectionClass.leggi("SELECT * FROM vwCARRELLO WHERE id_merchant = '" & _referrer & "' and IDUTENTE='" & customerName & "' and LISTA_NOZZE = '" & lista & "' ", ConnectionClass.cnDBSql, new datatable)
            ' else
            Return ConnectionClass.leggi("SELECT * FROM vwCARRELLO WHERE id_merchant = '" & _referrer & "' and IDUTENTE='" & customerName & "' ", ConnectionClass.cnDBSql, New DataTable)
            'end if
        End Function

        Public Function GetDesideriItems(customerName As String) As DataTable
            Return ConnectionClass.leggi("SELECT * FROM vwDESIDERI WHERE nominativo = '" & customerName & "'", ConnectionClass.cnDBSql, New DataTable)

        End Function

        Public Sub DeleteDesideriItem(ID As Integer)
            ConnectionClass.exec("Delete FROM _Desideri where ID_DESIDERIO=" & ID, ConnectionClass.cnDBSql)
        End Sub
        Public Function ship_free(customerName As String, value As Decimal, nazione As String) As Decimal
            Dim spese_trans As Decimal
            Dim products As New DataTable()
            ConnectionClass.leggi("SELECT flag_trasporto_gratis, peso FROM vwCARRELLO WHERE id_merchant = '" & _referrer & "' and IDUTENTE='" _
                            & customerName & "' ", ConnectionClass.cnDBSql, products)

            Dim i As Integer
            Dim blnGRATIS As Boolean = True
            Dim blnPESO_5 As Boolean = True

            For i = 0 To products.Rows.Count - 1
                If blnGRATIS Then blnGRATIS = (products.Rows(i)("flag_trasporto_gratis") = 1)
                ' if blnPESO_5 then blnPESO_5 = (products.tables(0).rows(i)("peso") = 5)

                ' ** controllare **
            Next

            spese_trans = value
            If nazione = "Italia" Then
                ' if blnPESO_5 then spese_trans = 5
                If blnGRATIS Then spese_trans = 0
            End If
            Return spese_trans
        End Function

        Public Function GetOrderValueForCart(customerName As String) As Double
            Return ConnectionClass.leggi("select sum(PREZZO*QTA) as totalvalue from _CARRELLO where id_merchant = '" & _referrer & "' and idutente = '" _
                           & customerName & "'", ConnectionClass.cnDBSql, New DataTable).Rows(0)(0)
        End Function
        Public Function Get_MiniCart(customerName As String) As String
            Dim dt As New DataTable()
            ConnectionClass.leggi("SELECT count(qta) as QTA , sum(qta) as QTA_TOT, sum(QTA*PREZZO) as TOTALE FROM vwCARRELLO WHERE IDUTENTE = '" & customerName & "'", ConnectionClass.cnDBSql, dt)
            If dt.Rows(0)("QTA") <> 0 Then
                Dim risultato As String
                risultato = "<span class='pezzi'>" & dt.Rows(0)("QTA_TOT") & " pz. </span>" & String.Format("{0:c}", dt.Rows(0)("TOTALE")) & ""

                Return risultato
            Else
                Return "Vuoto"
            End If
        End Function

        Public Function Adds_CartItem(customerName As String, id As Integer, productCode As String, DESCRIZIONE As String, MODELLO As String, PREZZO As Double, OFFERTA As String, LISTA As String, IVA As String, Optional ByVal _qta As Integer = 1) As String

            Dim ds As New DataTable()
            ds = GetShoppingCartItem(customerName, id)

            If (ds.Rows.Count > 0) Then
                Dim qta As Integer = ds.Rows(0)("QTA") + _qta
                UpdateShoppingCartItem(ds.Rows(0)("PK_CODICE"), qta)

                Return "OK"
            Else

                Dim FK_PRODOTTO As Integer = id
                Dim description As String = DESCRIZIONE
                Dim productName As String = MODELLO
                Dim unitPrice As String = PREZZO
                Dim FK_PRODUTTORE As String = OFFERTA
                unitPrice = Replace(unitPrice, ",", ".")

                Dim insertStatement As String
                insertStatement = "INSERT INTO _CARRELLO (ID_MERCHANT, CODICE_PRODOTTO, MODELLO, DESCRIZIONE, PREZZO, IDUTENTE, QTA, FK_PRODOTTO, fk_PRODUTTORE, LISTA_NOZZE, DATA_INSERT, IVA)" _
                                                    & "values ('" & _referrer & "', '" & productCode & "', '" & productName & "', '" & (productCode & "-" & DESCRIZIONE) & "', " _
                                                    & unitPrice & ", '" & customerName & "' ," & _qta & " , " & FK_PRODOTTO & ", " & FK_PRODUTTORE & ", '" & LISTA & "', " & Classi.Utility.TRASFORMA_DATA(Date.Now) & ", '" &
                                                    IVA & "')"
                If ConnectionClass.exec(insertStatement, ConnectionClass.cnDBSql) Then
                    Return "OK"
                Else
                    _ERRORE = insertStatement
                    Return "NO"
                End If
            End If
        End Function

        Public Function Adds_CartItem(customerName As String, id As String, productCode As String, PREZZO As Double, OFFERTA As String, LISTA As String, IVA As String) As String
            Dim ds As New DataTable()
            ds = GetShoppingCartItem(customerName, productCode)

            If (ds.Rows.Count > 0) Then
                Dim qta As Integer = ds.Rows(0)("QTA") + 1
                UpdateShoppingCartItem(ds.Rows(0)("PK_CODICE"), qta)
                Return "OK"
            Else
                Dim FK_PRODOTTO As Integer = id
                Dim unitPrice As String = PREZZO
                Dim FK_PRODUTTORE As String = OFFERTA
                unitPrice = Replace(unitPrice, ",", ".")

                Dim insertStatement As String = ""
                'insertStatement = "INSERT INTO _CARRELLO (ID_MERCHANT, CODICE_PRODOTTO, MODELLO, DESCRIZIONE, PREZZO, IDUTENTE, QTA, FK_PRODOTTO, fk_PRODUTTORE, LISTA_NOZZE, DATA_INSERT, IVA)" _
                '								& "values ('" & _referrer & "', '" & productCode & "', '" & productName & "', '" & (productCode & "-" & DESCRIZIONE) & "', " _
                '								& unitPrice & ", '" & customerName & "' , 1, " & FK_PRODOTTO & ", " & FK_PRODUTTORE & ", '" & LISTA & "', " & CommonFunction.TRASFORMA_DATA(Date.Now) & ", '" & _
                '								IVA & "')"
                'If ConnectionClass.exec(insertStatement, ConnectionClass.cnDBSql) Then
                'Return "OK"
                'Else
                '_ERRORE = insertStatement
                'Return "NO"
                'End If
                Return "OK"
            End If
        End Function

        Function Adds_CartItemP(customerName As String, PKArticolo As Integer, CodiceArticolo As String, cate As Integer, macro As Integer, prezzo As Double, desc As String, fk_colore As String, colore As String,
                                    fk_taglia As String, taglia As String, qta As String, iva_s As Double) As String
            Dim ds As New DataTable()
            ds = GetShoppingCartItem(customerName, CodiceArticolo)

            If (ds.Rows.Count > 0) Then
                Dim qtaP As Integer = ds.Rows(0)("QTA") + Integer.Parse(qta)
                UpdateShoppingCartItemP(ds.Rows(0)("PK_CODICE"), qtaP, Integer.Parse(fk_colore), Integer.Parse(fk_taglia))
                Return "OK"
            Else
                Dim FK_PRODOTTO As Integer = PKArticolo
                Dim description As String = desc
                Dim unitPrice As String = prezzo
                unitPrice = Replace(unitPrice, ",", ".")

                Dim insertStatement As String
                insertStatement = "INSERT INTO [dbo].[_CARRELLO]" &
                                        "([CODICE_PRODOTTO],[DESCRIZIONE],[PREZZO],[IDUTENTE],[QTA],[FK_TAGLIA],[TAGLIA],[FK_FANTASIA],[FANTASIA],[FK_PROD],[ID_MERCHANT],[DATA_INSERT],[IVA])" &
                                        "VALUES " &
                                        "('" & CodiceArticolo & "'" &
                                        ",'" & description & "'" &
                                        "," & unitPrice &
                                        ",'" & customerName & "'" &
                                        "," & Integer.Parse(qta) &
                                        "," & Integer.Parse(fk_taglia) &
                                        "," & Integer.Parse(taglia) &
                                        "," & Integer.Parse(fk_colore) &
                                        ",'" & colore & "'" &
                                        "," & FK_PRODOTTO &
                                        "," & _referrer &
                                        "," & Classi.Utility.TRASFORMA_DATA(Date.Now) &
                                        ",'" & iva_s & "')"
                If ConnectionClass.exec(insertStatement, ConnectionClass.cnDBSql) Then
                    Return "OK"
                Else
                    _ERRORE = insertStatement
                    Return "NO"
                End If
            End If
        End Function
        Function Adds_Desideri(customerName As String, PKArticolo As Integer, CodiceArticolo As String, cate As Integer, macro As Integer, prezzo As Double, desc As String, fk_colore As String, colore As String,
                                    fk_taglia As String, taglia As String, qta As String, iva_s As Double) As String
            Dim ds As New DataTable()
            ds = GetShoppingCartItem(customerName, CodiceArticolo)

            If (ds.Rows.Count > 0) Then
                Dim qtaP As Integer = ds.Rows(0)("QTA") + Integer.Parse(qta)
                UpdateShoppingCartItemP(ds.Rows(0)("PK_CODICE"), qtaP, Integer.Parse(fk_colore), Integer.Parse(fk_taglia))
                Return "OK"
            Else
                Dim FK_PRODOTTO As Integer = PKArticolo
                Dim description As String = desc
                Dim unitPrice As String = prezzo
                unitPrice = Replace(unitPrice, ",", ".")

                Dim insertStatement As String
                insertStatement = "INSERT INTO [dbo].[_desideri]" &
                                        "([FK_Utente],[FK_CodiceArticolo],[FK_Fantasia],[FK_Taglia])" &
                                        "VALUES (" & HttpContext.Current.Session("PK_utente") & "," & PKArticolo & "," & fk_colore & "," & fk_taglia & ")"
                If ConnectionClass.exec(insertStatement, ConnectionClass.cnDBSql) Then
                    Return "OK"
                Else
                    _ERRORE = insertStatement
                    Return "NO"
                End If
            End If
        End Function

        Public Function scala_qta_ordine(id_order As String, TIPO As String) As String
            'Dim ds As new datatable ()
            '		 ConnectionClass.LEGGI("select * from _DETTAGLI where fk_ordine = " & id_order, ConnectionClass.cnDBSql, ds)
            '
            '		 For Each dr As DataRow In ds.Rows
            '		   if tipo = "1" then
            '		      ConnectionClass.exec("UPDATE _PRODOTTI SET QTA = QTA - " & dr("QTA") & " where pk_codice = " & dr("FK_PRODOTTO"), ConnectionClass.cnDBSql)
            '			  ConnectionClass.exec("UPDATE _PRODOTTI SET QTA = 0  where qta < 0 and pk_codice = " & dr("FK_PRODOTTO"), ConnectionClass.cnDBSql)
            '		   else
            '			  ConnectionClass.exec("UPDATE _LISTA_NOZZE  SET QTA_DISP = QTA_DISP - " & dr("QTA") & " where fk_prodotto = " & dr("FK_PRODOTTO"), ConnectionClass.cnDBSql)
            '			  ConnectionClass.exec("UPDATE _LISTA_NOZZE SET QTA_DISP = 0  where QTA_DISP < 0 and fk_prodotto = " & dr("FK_PRODOTTO"), ConnectionClass.cnDBSql)
            '		   end if
            '		 next

        End Function

    End Class
End Namespace

