﻿Imports Microsoft.VisualBasic


''' <summary>
''' This is a hack to force no mobile URL resolution in FriendlyUrls.  There's some kind of bug in the current version that
''' causes it to do an internal failed resolve of a mobile master even though there is none.
''' </summary>
Public Class BugFixFriendlyUrlResolver
	Inherits Microsoft.AspNet.FriendlyUrls.Resolvers.WebFormsFriendlyUrlResolver
	Protected Overrides Function TrySetMobileMasterPage(httpContext As HttpContextBase, page As Page, mobileSuffix As String) As Boolean
		Return False
		'return base.TrySetMobileMasterPage(httpContext, page, mobileSuffix);
	End Function
End Class
