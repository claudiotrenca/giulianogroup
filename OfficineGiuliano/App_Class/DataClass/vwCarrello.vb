'------------------------------------------------------------------------------
' <auto-generated>
'     Codice generato da un modello.
'
'     Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
'     Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Namespace DataClass

    Partial Public Class vwCarrello
        Public Property IDUTENTE As String
        Public Property CODICE_PRODOTTO As String
        Public Property FK_PRODOTTO As Nullable(Of Integer)
        Public Property DATA_INSERT As Date
        Public Property PREZZO As Nullable(Of Double)
        Public Property QTA As Nullable(Of Double)
        Public Property PK As Integer
        Public Property FkCategoria As Nullable(Of Integer)
        Public Property Modello As String
        Public Property Codice As String
        Public Property CodiceFornitore As String
        Public Property Famiglia As String
        Public Property FamigliaLink As String
        Public Property Marca As String
        Public Property MarcaLink As String
        Public Property PrezzoFinale As Nullable(Of Decimal)
        Public Property PrezzoRivenditore As Nullable(Of Decimal)
        Public Property PrezzoListino As Nullable(Of Decimal)
        Public Property ImmagineProdotto As String
        Public Property Peso As Nullable(Of Decimal)
        Public Property Altezza As Nullable(Of Decimal)
        Public Property Larghezza As Nullable(Of Decimal)
        Public Property Profondita As Nullable(Of Decimal)
        Public Property FlagNoleggio As Nullable(Of Boolean)
        Public Property FlagPrivato As Nullable(Of Boolean)
        Public Property Categoria As String
        Public Property CategoriaLink As String
        Public Property Macro As String
        Public Property MacroLink As String
        Public Property FkMacro As Integer
        Public Property PK_CODICE As Integer
    
    End Class

End Namespace
