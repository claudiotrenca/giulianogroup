﻿Imports Microsoft.VisualBasic
Imports System.Linq.Expressions

Imports System.Linq.Dynamic

Namespace DataClass

    Partial Public Class Utenti

        Public Shared Function Delete(ByVal _pk As Integer) As Boolean

            Dim res As Boolean = False

            Using context = New DataEntities()

                Dim obj = context.Utenti.FirstOrDefault(Function(p) p.PK = _pk)

                If obj IsNot Nothing Then

                    Try

                        context.Utenti.Remove(obj)
                        context.SaveChanges()

                        res = True
                    Catch ex As Exception

                        Throw ex

                    End Try

                End If

            End Using

            Return res

        End Function

        Public Shared Function Login(ByVal _user As String, ByVal _pass As String) As LoginStatus
            Dim row As DataClass.Utenti
            Dim result As LoginStatus
            Using context As New DataClass.DataEntities
                row = (From p In context.Utenti Where p.Username = _user And p.Password = _pass Select p).FirstOrDefault
            End Using
            If row IsNot Nothing Then
                If row.FlagAttivo = False Then
                    result = LoginStatus.Locked
                Else
                    result = LoginStatus.Success
                    HttpContext.Current.Session.Add("IDUtente", row.PK)
                    HttpContext.Current.Session.Add("LivelloUtente", row.Tipo)
                    HttpContext.Current.Session.Add("NomeUtente", row.Cognome.Trim & " " & row.Nome.Trim)
                End If
            Else
                result = LoginStatus.Failed
            End If
            Return result
        End Function

        Public Shared Function GetDetail(ByVal _pk As Integer) As DataClass.Utenti
            Dim queryResult As DataClass.Utenti
            Using context As New DataClass.DataEntities
                queryResult = (From p In context.Utenti Where p.PK = _pk Select p).FirstOrDefault
            End Using
            Return queryResult
        End Function

        Public Shared Function CreateUser(ByVal _Nome As String, ByVal _Cognome As String, ByVal _RagioneSociale As String, ByVal _Indirizzo As String, ByVal _Comune As String, ByVal _Email As String, ByVal _Telefono As String, ByVal _Piva As String, ByVal _Password As String) As Boolean
            Dim res As Boolean = False
            Using context As New DataClass.DataEntities
                Dim row = context.Utenti.FirstOrDefault(Function(p) p.PartitaIVA = _Piva)
                Try
                    If row Is Nothing Then
                        Dim NewRow As New DataClass.Utenti
                        NewRow.Nome = _Nome
                        NewRow.Cognome = _Cognome
                        NewRow.RagioneSociale = _RagioneSociale
                        NewRow.PartitaIVA = _Piva
                        NewRow.Indirizzo = _Indirizzo
                        NewRow.Comune = _Comune
                        NewRow.Telefono = _Telefono
                        NewRow.Email = _Email
                        NewRow.Username = _Email
                        NewRow.Password = _Password
                        NewRow.FlagAttivo = True

                        context.Configuration.AutoDetectChangesEnabled = True
                        context.Utenti.Attach(NewRow)
                        context.Entry(NewRow).State = Entity.EntityState.Added
                        context.SaveChanges()
                    Else
                        res = False
                    End If
                    res = True
                Catch ex As Exception
                    Throw ex
                End Try
            End Using
            Return res
        End Function

    End Class

End Namespace