'------------------------------------------------------------------------------
' <auto-generated>
'     Codice generato da un modello.
'
'     Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
'     Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Namespace DataClass

    Partial Public Class Ordini
        Public Property PK_CODICE As Integer
        Public Property FK_UTENTE As Nullable(Of Integer)
        Public Property USER_UTENTE As String
        Public Property DATA As Nullable(Of Date)
        Public Property TOTALE_MERCE As Nullable(Of Decimal)
        Public Property TOTALE_IVA As Nullable(Of Decimal)
        Public Property TOTALE_SPESE As Nullable(Of Decimal)
        Public Property TOTALE As Nullable(Of Decimal)
        Public Property IND_CONS As String
        Public Property CAP_CONS As String
        Public Property PROV_CONS As String
        Public Property COMUNE_CONS As String
        Public Property TIPO_PAGAMENTO As String
        Public Property PAGATO As Nullable(Of Boolean)
        Public Property EVASO As Nullable(Of Boolean)
        Public Property SPEDITO As Nullable(Of Boolean)
        Public Property NOTE_ORD As String
        Public Property TEL As String
        Public Property STATO_ORD As String
        Public Property NAZIONE As String
        Public Property REGIONE As String
        Public Property LINK_TRACK As String
        Public Property SPEDIZIONIERE As String
        Public Property codice_promo As String
    
    End Class

End Namespace
