﻿Namespace DataClass
    Partial Public Class Products

        Private _PkProdotto As Integer

        Private _Modello As String

        Private _Codice As String

        Private _CodiceFornitore As String

        Private _Macro As String

        Private _Categoria As String

        Private _DescrizioneBreve As String = String.Empty

        Private _DescrizioneEstesa As String = String.Empty

        Private _DescrizioneCaratteristiche As String = String.Empty

        Private _Prezzo As System.Nullable(Of Double)

        Private _PrezzoListino As System.Nullable(Of Double)

        Private _PrezzoRivenditore1 As System.Nullable(Of Double)

        Private _PrezzoRivenditore2 As System.Nullable(Of Double)

        Private _PrezzoPromo As System.Nullable(Of Double)

        Private _ImmagineGrande As String

        Private _Marca As String

        Private _Famiglia As String

        Private _FamigliaLink As String

        Private _Quantita As Integer

        Private _FlagPromo As Boolean

        Private _FlagAttivo As Boolean

        Private _FlagNoleggio As Boolean

        Private _MacroLink As String

        Private _CategoriaLink As String

        Private _SubCategoriaLink As String

        Private _Allegato As String

        Public Sub New()
            MyBase.New()
        End Sub

        Public Property PkProdotto() As Integer
            Get
                Return Me._PkProdotto
            End Get
            Set(value As Integer)
                If ((Me._PkProdotto = value) _
                            = False) Then
                    Me._PkProdotto = value
                End If
            End Set
        End Property

        Public Property Modello As String
            Get
                Return _Modello
            End Get
            Set(value As String)
                Me._Modello = value
            End Set
        End Property

        Public Property DescrizioneBreve As String
            Get
                Return _DescrizioneBreve
            End Get
            Set(value As String)
                Me._DescrizioneBreve = value
            End Set
        End Property

        Public Property DescrizioneEstesa As String
            Get
                Return _DescrizioneEstesa
            End Get
            Set(value As String)
                Me._DescrizioneEstesa = value
            End Set
        End Property

        Public Property DescrizioneCaratteristiche As String
            Get
                Return _DescrizioneCaratteristiche
            End Get
            Set(value As String)
                Me._DescrizioneCaratteristiche = value
            End Set
        End Property

        Public Property Marca As String
            Get
                Return _Marca
            End Get
            Set(value As String)
                Me._Marca = value
            End Set
        End Property

        Public Property Famiglia As String
            Get
                Return _Famiglia
            End Get
            Set(value As String)
                Me._Famiglia = value
            End Set
        End Property

        Public Property FamigliaLink As String
            Get
                Return _FamigliaLink
            End Get
            Set(value As String)
                Me._FamigliaLink = value
            End Set
        End Property

        Public Property MacroLink As String
            Get
                Return _MacroLink
            End Get
            Set(value As String)
                Me._MacroLink = value
            End Set
        End Property

        Public Property CategoriaLink As String
            Get
                Return _CategoriaLink
            End Get
            Set(value As String)
                Me._CategoriaLink = value
            End Set
        End Property

        Public Property SubCategoriaLink As String
            Get
                Return _SubCategoriaLink
            End Get
            Set(value As String)
                Me._SubCategoriaLink = value
            End Set
        End Property

        Public ReadOnly Property Link As String
            Get
                Return ("/" & Emmemedia.Classi.Utility.StringToUrl(Me.Modello) & "_" & Me.PkProdotto & ".htm").ToLower
            End Get
        End Property

        Public Property Macro() As String
            Get
                Return Me._Macro
            End Get
            Set(value As String)
                If (String.Equals(Me._Macro, value) = False) Then
                    Me._Macro = value
                End If
            End Set
        End Property

        Public Property Categoria() As String
            Get
                Return Me._Categoria
            End Get
            Set(value As String)
                If (String.Equals(Me._Categoria, value) = False) Then
                    Me._Categoria = value
                End If
            End Set
        End Property

        Public Property Codice() As String
            Get
                Return Me._Codice
            End Get
            Set(value As String)
                If (String.Equals(Me._Codice, value) = False) Then
                    Me._Codice = value
                End If
            End Set
        End Property

        Public Property Allegato() As String
            Get
                Return Me._Allegato
            End Get
            Set(value As String)
                If (String.Equals(Me._Codice, value) = False) Then
                    Me._Allegato = value
                End If
            End Set
        End Property

        Public Property CodiceFornitore() As String
            Get
                Return Me._CodiceFornitore
            End Get
            Set(value As String)
                If (String.Equals(Me._CodiceFornitore, value) = False) Then
                    Me._CodiceFornitore = value
                End If
            End Set
        End Property

        Public Property Prezzo() As System.Nullable(Of Double)
            Get
                Return Me._Prezzo
            End Get
            Set(value As System.Nullable(Of Double))
                If (Me._Prezzo.Equals(value) = False) Then
                    Me._Prezzo = value
                End If
            End Set
        End Property

        Public Property PrezzoPromo() As System.Nullable(Of Double)
            Get
                Return Me._PrezzoPromo
            End Get
            Set(value As System.Nullable(Of Double))
                If (Me._PrezzoPromo.Equals(value) = False) Then
                    Me._PrezzoPromo = value
                End If
            End Set
        End Property

        Public Property PrezzoListino() As System.Nullable(Of Double)
            Get
                Return Me._PrezzoListino
            End Get
            Set(value As System.Nullable(Of Double))
                If (Me._PrezzoListino.Equals(value) = False) Then
                    Me._PrezzoListino = value
                End If
            End Set
        End Property

        Public Property PrezzoRivenditore1 As System.Nullable(Of Double)
            Get
                Return _PrezzoRivenditore1
            End Get
            Set(value As System.Nullable(Of Double))
                If (Me._PrezzoRivenditore1.Equals(value) = False) Then
                    Me._PrezzoRivenditore1 = value
                End If
            End Set
        End Property

        Public Property PrezzoRivenditore2 As System.Nullable(Of Double)
            Get
                Return _PrezzoRivenditore2
            End Get
            Set(value As System.Nullable(Of Double))
                If (Me._PrezzoRivenditore2.Equals(value) = False) Then
                    Me._PrezzoRivenditore2 = value
                End If
            End Set
        End Property

        Public Property Quantita As System.Nullable(Of Integer)
            Get
                Return _Quantita
            End Get
            Set(value As System.Nullable(Of Integer))
                If (Me._Quantita.Equals(value) = False) Then
                    Me._Quantita = value
                End If
            End Set
        End Property

        Public Property ImmagineGrande() As String
            Get
                Return Me._ImmagineGrande
            End Get
            Set(value As String)
                If (String.Equals(Me._ImmagineGrande, value) = False) Then
                    Me._ImmagineGrande = value
                End If
            End Set
        End Property

        Public ReadOnly Property ImmagineMedia As String
            Get
                If Me.ImmagineGrande.Length > 0 Then

                    Return (Me.ImmagineGrande.Substring(0, Me.ImmagineGrande.LastIndexOf(".")) & "_medium.jpg").ToLower

                Else

                    Return ("/public/image/noimage.gif")

                End If

            End Get
        End Property

        Public Property FlagAttivo As Boolean
            Get
                Return _FlagAttivo
            End Get
            Set(value As Boolean)
                _FlagAttivo = value
            End Set
        End Property

        Public Property FlagNoleggio As Boolean
            Get
                Return _FlagNoleggio
            End Get
            Set(value As Boolean)
                _FlagNoleggio = value
            End Set
        End Property

        Public Property FlagPromo As Boolean
            Get
                Return _FlagPromo
            End Get
            Set(value As Boolean)
                _FlagPromo = value
            End Set
        End Property

    End Class

End Namespace