'------------------------------------------------------------------------------
' <auto-generated>
'     Codice generato da un modello.
'
'     Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
'     Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Namespace DataClass

    Partial Public Class PostMeta
        Public Property PK As Integer
        Public Property FkContenuto As Nullable(Of Integer)
        Public Property MetaTitle As String
        Public Property MetaDescription As String
        Public Property MetaKeywords As String
        Public Property MetaCanonical As String
        Public Property MetaImmaginePage As String
        Public Property MicrodataSchema As String
        Public Property OgType As String
    
    End Class

End Namespace
