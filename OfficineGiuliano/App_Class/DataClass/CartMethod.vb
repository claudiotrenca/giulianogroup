﻿Namespace DataClass
    Public Class CartMethod

        Public Shared Function GetTotal(ByVal customerName As String) As String
            Dim rv As Decimal
            Using context As New DataClass.DataEntities
                Dim obj = Aggregate row In context.Carrello
                            Where row.IDUTENTE = customerName
                            Into Sum(row.PREZZO * row.QTA)

                rv = obj.GetValueOrDefault
            End Using
            Return rv
        End Function

        Public Shared Function GetItemCount(ByVal customerName As String) As String
            Dim rv As Integer
            Using context As New DataClass.DataEntities
                Dim obj = Aggregate row In context.Carrello
                           Where row.IDUTENTE = customerName
                           Into Sum(row.QTA)
                rv = obj.GetValueOrDefault
            End Using
            Return rv
        End Function

        Public Shared Function GetDistinctItemCount(ByVal customerName As String) As String
            Dim rv As Integer
            Using context As New DataClass.DataEntities
                Dim obj = Aggregate row In context.Carrello
                           Where row.IDUTENTE = customerName
                           Into Count
                rv = obj
            End Using
            Return rv
        End Function

        Public Shared Function RemoveItem(ByVal _id As Integer) As Boolean
            Dim res As Boolean = False
            Using context As New DataClass.DataEntities
                Dim obj = context.Carrello.FirstOrDefault(Function(p) p.PK_CODICE = _id)
                If obj IsNot Nothing Then
                    Try
                        context.Carrello.Remove(obj)
                        context.SaveChanges()
                        res = True
                    Catch ex As Exception
                        Throw ex
                    End Try

                End If
            End Using
            Return res
        End Function

        Public Shared Function UpdateItemQuantity(ByVal _id As Integer, ByVal _qt As Integer) As Boolean
            Dim res As Boolean = False
            Using context As New DataClass.DataEntities
                Dim obj = context.Carrello.FirstOrDefault(Function(p) p.PK_CODICE = _id)
                If obj IsNot Nothing Then
                    Try
                        obj.QTA = _qt
                        context.SaveChanges()
                        res = True
                    Catch ex As Exception
                        Throw ex
                    End Try

                End If
            End Using
            Return res
        End Function

        Public Shared Function GetList(ByVal _user As String) As List(Of vwCarrello)
            Dim lista As List(Of DataClass.vwCarrello)
            Using context As New DataClass.DataEntities
                lista = (From p In context.vwCarrello Where p.IDUTENTE = _user Select p).ToList
            End Using
            Return lista
        End Function


        Public Shared Function AddItem(ByVal _FkProdotto As Integer, ByVal _Modello As String, ByVal _qta As Integer, ByVal _prezzo As Double, ByVal _user As String) As Boolean
            Dim res As Boolean = False
            Using context As New DataClass.DataEntities
                Dim row = context.Carrello.SingleOrDefault(Function(p) p.FK_PRODOTTO = _FkProdotto And p.IDUTENTE = _user)
                Try
                    If row Is Nothing Then
                        Dim NewRow As New DataClass.Carrello
                        NewRow.DATA_INSERT = Date.Now
                        NewRow.MODELLO = _Modello
                        NewRow.FK_PRODOTTO = _FkProdotto
                        NewRow.IDUTENTE = _user
                        NewRow.PREZZO = _prezzo
                        NewRow.QTA = _qta
                        context.Configuration.AutoDetectChangesEnabled = True
                        context.Carrello.Attach(NewRow)
                        context.Entry(NewRow).State = Entity.EntityState.Added
                        context.SaveChanges()
                    Else
                        row.QTA = row.QTA + _qta
                        context.SaveChanges()
                    End If
                    res = True
                Catch ex As Exception
                    Throw ex
                End Try
            End Using
            Return res
        End Function

        Public Shared Function MigrateShoppingCartItems(ByVal OldUserID As String, ByVal NewUserID As String) As Boolean
            Dim res As Boolean = False
            Using context As New DataClass.DataEntities
                Dim obj = context.Carrello.FirstOrDefault(Function(p) p.IDUTENTE = OldUserID)
                If obj IsNot Nothing Then
                    Try
                        obj.IDUTENTE = NewUserID
                        context.SaveChanges()
                        res = True
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If
            End Using
            Return res
        End Function

    End Class

End Namespace