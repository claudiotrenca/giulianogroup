﻿Imports Emmemedia.DataClass

Namespace DataClass
    Partial Public Class MailContact

        Public Shared Function Insert(ByVal _email As String, ByVal _subject As String, ByVal _text As String) As Boolean

            Dim res As Boolean = False

            Dim Obj As New DataClass.MailContact With {
                                                 .Email = _email,
                                                 .Subject = _subject,
                                                 .DateAdd = Date.Now,
                                                 .Text = _text
                                                 }

            Using context As New DataEntities
                context.Configuration.AutoDetectChangesEnabled = True
                context.MailContact.Attach(Obj)
                context.Entry(Obj).State = Entity.EntityState.Added
                Try
                    If context.SaveChanges() > 0 Then
                        res = True
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            End Using

            Return res

        End Function

        Public Shared Function UpdateRead(ByVal _PK As Integer, ByVal _FlagNew As Boolean) As Boolean

            Dim res As Boolean = False
            Using context = New DataEntities()
                Dim obj = context.MailContact.FirstOrDefault(Function(p) p.PK = _PK)

                If obj IsNot Nothing Then
                    With obj
                        .[New] = _FlagNew
                    End With

                    Try
                        context.SaveChanges()
                        res = True
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If
            End Using

            Return res

        End Function



        Public Shared Function Delete(ByVal _PK As String) As Boolean

            Dim res As Boolean = False

            Using context = New DataEntities()
                Dim obj = context.MailContact.FirstOrDefault(Function(p) p.PK = _PK)

                If obj IsNot Nothing Then
                    Try
                        context.MailContact.Remove(obj)
                        context.SaveChanges()
                        res = True
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If

            End Using

            Return res

        End Function



    End Class
End Namespace
