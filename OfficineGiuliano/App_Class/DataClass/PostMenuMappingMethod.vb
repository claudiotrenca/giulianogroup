﻿Namespace DataClass
    Partial Public Class PostMenuMapping

        Public Shared Function GetPostIdFromMenu(ByVal _id As Integer) As Integer
            Dim _rv As Object
            Dim dc As New DataEntities
            Try
                _rv = (From p In dc.PostMenuMapping Where p.FkMenu = _id Select p.FkPost).FirstOrDefault
            Catch ex As Exception
                Throw ex
            End Try
            If _rv IsNot Nothing OrElse _rv IsNot DBNull.Value Then
                Return CInt(_rv)
            Else
                Return 0
            End If
        End Function

        Public Shared Function Delete(ByVal _PK As String) As Boolean

            Dim res As Boolean = False

            Using context = New DataEntities()
                Dim obj = context.PostMenuMapping.FirstOrDefault(Function(p) p.PK = _PK)

                If obj IsNot Nothing Then
                    Try
                        context.PostMenuMapping.Remove(obj)
                        context.SaveChanges()
                        res = True
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If

            End Using

            Return res

        End Function

    End Class

End Namespace