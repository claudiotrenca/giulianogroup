'------------------------------------------------------------------------------
' <auto-generated>
'     Codice generato da un modello.
'
'     Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
'     Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Namespace DataClass

    Partial Public Class Agenzie
        Public Property PK_Codice As Integer
        Public Property Agenzia As String
        Public Property Tipo As String
        Public Property Decorrenza As Nullable(Of Date)
        Public Property Localita As String
        Public Property Provincia As String
        Public Property Regione As String
        Public Property Cap As String
        Public Property Indirizzo As String
        Public Property Societa As String
        Public Property PartitaIVA As String
        Public Property Email As String
        Public Property Telefono As String
        Public Property Fax As String
        Public Property Responsabile As String
        Public Property Logo As String
        Public Property CodiceCitta As Nullable(Of Integer)
    
    End Class

End Namespace
