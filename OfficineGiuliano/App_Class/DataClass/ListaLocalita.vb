﻿Namespace DataClass
    Public Class ListaLocalita
        Private _localita As String

        Public Sub New()
            MyBase.New()
        End Sub

        Public Property Localita As String
            Get
                Return _localita
            End Get
            Set(value As String)
                Me._localita = value
            End Set
        End Property

    End Class

    Public Class ListaDate
        Private _datePartenza As String

        Public Sub New()
            MyBase.New()
        End Sub

        Public Property DatePartenza As String
            Get
                Return _datePartenza
            End Get
            Set(value As String)
                Me._datePartenza = value
            End Set
        End Property

    End Class

End Namespace