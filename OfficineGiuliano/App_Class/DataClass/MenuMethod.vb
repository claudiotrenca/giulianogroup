﻿Imports System.Linq.Dynamic

Namespace DataClass

    Partial Public Class Menu

        Public Shared Function GetList(ByVal _IDGruppoMenu As Integer) As List(Of DataClass.Menu)

            Dim rv As List(Of DataClass.Menu)

            Using dc As New DataEntities
                Try
                    rv = (From r In dc.Menu Where r.FkGruppoMenu = _IDGruppoMenu And r.FlagVisibile = True And r.ParentID Is Nothing Select r Order By r.Indice).ToList
                Catch ex As Exception
                    rv = Nothing
                End Try
            End Using

            Return rv

        End Function

        Public Shared Function GetList(ByVal _IDGruppoMenu As Integer, ByVal _ParentId As Integer) As List(Of DataClass.vwMenu)

            Dim rv As List(Of DataClass.vwMenu)

            Using dc As New DataEntities
                Try
                    rv = (From r In dc.vwMenu Where r.ParentID = _ParentId And r.FkGruppoMenu = _IDGruppoMenu Select r Order By r.Indice).ToList
                Catch ex As Exception
                    rv = Nothing
                End Try
            End Using

            Return rv

        End Function

        Public Shared Function GetDetail(ByVal _id As Integer) As DataClass.Menu
            Dim dc As New DataEntities
            Dim rv As DataClass.Menu

            Try
                rv = (From r In dc.Menu Where r.PK = _id Select r).FirstOrDefault
            Catch ex As Exception
                rv = Nothing
            End Try
            Return rv
        End Function

        Public Shared Function GetIdFromLink(ByVal _str As String) As Integer
            Dim _rv As Object
            Dim dc As New DataEntities
            Try
                _rv = (From p In dc.Menu Where p.Link = _str And p.Tipo = "link" Select p.PK).FirstOrDefault
            Catch ex As Exception
                Throw ex
            End Try
            If _rv IsNot Nothing OrElse _rv IsNot DBNull.Value Then
                Return CInt(_rv)
            Else
                Return 0
            End If
        End Function

        Public Shared Function GetIdFromLink(ByVal _str As String, ByVal _idMacro As String) As Integer
            Dim _rv As Object
            Dim dc As New DataEntities
            Try
                _rv = (From p In dc.Menu Where p.Link = _str And p.ParentID = _idMacro Select p.PK).FirstOrDefault
            Catch ex As Exception
                Throw ex
            End Try
            If _rv IsNot Nothing OrElse _rv IsNot DBNull.Value Then
                Return CInt(_rv)
            Else
                Return 0
            End If
        End Function

        Public Shared Function GetCategoryList() As List(Of DataClass.vwMenu)

            Dim rv As List(Of DataClass.vwMenu)

            Using dc As New DataEntities
                Try
                    rv = (From p In dc.vwMenu Where p.ParentID IsNot Nothing And p.Tipo = "categoria" Select p Order By p.Indice).ToList
                Catch ex As Exception
                    rv = Nothing
                End Try
            End Using

            Return rv

        End Function

        Public Shared Function GetCategoryList(ByVal _id As Integer) As List(Of DataClass.vwMenu)

            Dim rv As List(Of DataClass.vwMenu)

            Using dc As New DataEntities
                Try
                    rv = (From p In dc.vwMenu Where p.ParentID = _id And p.Tipo = "categoria" Select p Order By p.Indice).ToList
                Catch ex As Exception
                    rv = Nothing
                End Try
            End Using

            Return rv

        End Function

        Public Shared Function GetCategoryIdFromLink(ByVal _str As String) As Integer
            Dim _rv As Object
            Dim dc As New DataEntities
            Try
                _rv = (From p In dc.Menu Where p.Link = _str And p.Tipo = "link" Select p.FkCategory).FirstOrDefault
            Catch ex As Exception
                Throw ex
            End Try
            If _rv IsNot Nothing OrElse _rv IsNot DBNull.Value Then
                Return CInt(_rv)
            Else
                Return 0
            End If
        End Function

        Public Shared Function GetCategoryIdFromLink(ByVal _str As String, ByVal _idMacro As String) As Integer
            Dim _rv As Object
            Dim dc As New DataEntities
            Try
                _rv = (From p In dc.Menu Where p.Link = _str And p.ParentID = _idMacro Select p.PK).FirstOrDefault
            Catch ex As Exception
                Throw ex
            End Try
            If _rv IsNot Nothing OrElse _rv IsNot DBNull.Value Then
                Return CInt(_rv)
            Else
                Return 0
            End If
        End Function

        Public Shared Function GetNameFromID(ByVal _id As String) As String
            Dim _rv As String
            Dim dc As New DataEntities
            Try
                _rv = (From p In dc.Menu Where p.PK = _id Select p.Descrizione).FirstOrDefault
            Catch ex As Exception
                Throw ex
            End Try
            Return (_rv)
        End Function

        Public Shared Function Insert(ByVal _Descrizione As String, ByVal _Link As String, ByVal _FlagVisibile As Boolean, ByVal _ParentID As Integer, ByVal _Indice As Integer, ByVal _IDM As String, Optional ByVal _Culture As String = "it-IT") As Boolean

            Dim res As Boolean = False

            Dim Obj As New DataClass.Menu With {
                                                 .Culture = _Culture,
                                                 .Descrizione = _Descrizione,
                                                 .FlagVisibile = _FlagVisibile,
                                                 .IDM = _IDM,
                                                 .Indice = _Indice,
                                                 .Link = _Link,
                                                 .ParentID = _ParentID,
                                                 .Tipo = "link"
                                                 }

            Using context As New DataEntities
                context.Configuration.AutoDetectChangesEnabled = True
                context.Menu.Attach(Obj)
                context.Entry(Obj).State = Entity.EntityState.Added
                Try
                    If context.SaveChanges() > 0 Then
                        res = True
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            End Using

            Return res

        End Function

        Public Shared Function Modify(ByVal _pk As Integer, ByVal _Descrizione As String, ByVal _Link As String, ByVal _FlagVisibile As Boolean, ByVal _ParentID As Integer, ByVal _Indice As Integer, Optional ByVal _Culture As String = "it-IT", Optional ByVal _IDM As String = "") As Boolean

            Dim res As Boolean = False
            Using context = New DataEntities()
                Dim obj = context.Menu.FirstOrDefault(Function(p) p.PK = _pk)

                If obj IsNot Nothing Then
                    With obj
                        .Culture = _Culture
                        .Descrizione = _Descrizione
                        .FlagVisibile = _FlagVisibile
                        .IDM = _IDM
                        .Indice = _Indice
                        .Link = _Link
                        .ParentID = _ParentID
                        .Tipo = "link"
                    End With

                    Try
                        context.SaveChanges()
                        res = True
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If
            End Using

            Return res

        End Function

        Public Shared Function Delete(ByVal _PK As String) As Boolean

            Dim res As Boolean = False

            Using context = New DataEntities()
                Dim obj = context.Menu.FirstOrDefault(Function(p) p.PK = _PK)

                If obj IsNot Nothing Then
                    Try
                        context.Menu.Remove(obj)
                        context.SaveChanges()
                        res = True
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If

            End Using

            Return res

        End Function

    End Class

End Namespace