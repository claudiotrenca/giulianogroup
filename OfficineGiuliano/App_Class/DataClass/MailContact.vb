'------------------------------------------------------------------------------
' <auto-generated>
'     Codice generato da un modello.
'
'     Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
'     Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Namespace DataClass

    Partial Public Class MailContact
        Public Property PK As Integer
        Public Property DateAdd As Nullable(Of Date)
        Public Property Email As String
        Public Property Subject As String
        Public Property Text As String
        Public Property [New] As Nullable(Of Boolean)
    
    End Class

End Namespace
