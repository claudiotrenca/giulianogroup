﻿Namespace DataClass
    Public Class Fotogallery

        Private _Descrizione As String
        Private _Percorso As String
        Public Property Descrizione As String
            Get
                Return _Descrizione
            End Get
            Set(value As String)
                Me._Descrizione = value
            End Set
        End Property
        Public Property Percorso As String
            Get
                Return _Percorso
            End Get
            Set(value As String)
                Me._Percorso = value
            End Set
        End Property
    End Class
End Namespace