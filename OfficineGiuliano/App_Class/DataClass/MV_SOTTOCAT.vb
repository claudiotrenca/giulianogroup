'------------------------------------------------------------------------------
' <auto-generated>
'     Codice generato da un modello.
'
'     Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
'     Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Namespace DataClass

    Partial Public Class MV_SOTTOCAT
        Public Property ID_SOTTOCAT As Integer
        Public Property CODICE_SOTTOCAT As Nullable(Of Integer)
        Public Property TITOLO_SOTTOCAT As String
        Public Property DESCRIZIONE_SOTTOCAT As String
        Public Property CODICE_CAT As Nullable(Of Integer)
        Public Property LINK_SOTTOCAT As String
        Public Property IMMAGINE_SOTTOCAT As String
    
    End Class

End Namespace
