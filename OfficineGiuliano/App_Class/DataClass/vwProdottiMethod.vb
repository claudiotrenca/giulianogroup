﻿Imports Microsoft.VisualBasic
Imports System.Linq.Expressions
Imports System.Linq.Dynamic

Namespace DataClass

    Partial Public Class vwProdotti

        Public Shared Function GetListFromSubCategory(ByVal _FkSubCategoria As Integer, ByVal start As Integer, ByVal number As Integer, ByVal sortBy As String, ByVal sortDirection As String) As List(Of DataClass.Products)

            Dim sorting As String = sortBy & " " & sortDirection

            Dim lProduct As List(Of DataClass.Products)
            Dim queryResult As List(Of DataClass.vwProdotti)

            Using context As New DataClass.DataEntities

                queryResult = (From p In context.vwProdotti Where p.FkSubCategoria = _FkSubCategoria And p.FlagAttivo = True Order By p.Modello Ascending Select p).ToList

                lProduct = queryResult.AsEnumerable().Select(Of DataClass.Products)(Function(p) New DataClass.Products() With {
                                                                                                    .PkProdotto = p.PK,
                                                                                                    .Codice = p.Codice,
                                                                                                    .Modello = p.Modello,
                                                                                                    .Prezzo = p.PrezzoFinale,
                                                                                                    .PrezzoListino = p.PrezzoListino,
                                                                                                    .PrezzoRivenditore1 = p.PrezzoRivenditore,
                                                                                                    .PrezzoRivenditore2 = p.PrezzoRivenditore,
                                                                                                    .PrezzoPromo = 0,
                                                                                                    .ImmagineGrande = p.ImmagineProdotto,
                                                                                                    .Macro = p.Macro,
                                                                                                    .Marca = p.Marca,
                                                                                                    .Famiglia = p.Famiglia,
                                                                                                    .FamigliaLink = p.FamigliaLink,
                                                                                                    .MacroLink = p.MacroLink,
                                                                                                    .CategoriaLink = p.CategoriaLink,
                                                                                                    .SubCategoriaLink = p.SubCategoriaLink,
                                                                                                    .Quantita = p.Qta,
                                                                                                    .FlagAttivo = p.FlagAttivo,
                                                                                                    .FlagNoleggio = p.FlagNoleggio,
                                                                                                    .Categoria = p.Categoria}).ToList

            End Using

            Dim DistinctItems = lProduct.GroupBy(Function(x) x.PkProdotto).[Select](Function(y) y.First())
            Return DistinctItems.Skip(start).Take(number).ToList

        End Function

        Public Shared Function GetListFromCategory(ByVal _FkCategoria As Integer, ByVal start As Integer, ByVal number As Integer, ByVal sortBy As String, ByVal sortDirection As String) As List(Of DataClass.Products)

            Dim sorting As String = sortBy & " " & sortDirection

            Dim lProduct As List(Of DataClass.Products)
            Dim queryResult As List(Of DataClass.vwProdotti)

            Using context As New DataClass.DataEntities

                queryResult = (From p In context.vwProdotti Where p.FkCategoria = _FkCategoria And p.FlagAttivo = True Order By p.Categoria Ascending, p.SubCategoria Ascending, p.Modello Ascending Select p).ToList

                lProduct = queryResult.AsEnumerable().Select(Of DataClass.Products)(Function(p) New DataClass.Products() With {
                                                                                                    .PkProdotto = p.PK,
                                                                                                    .Codice = p.Codice,
                                                                                                    .Modello = p.Modello,
                                                                                                    .Prezzo = p.PrezzoFinale,
                                                                                                    .PrezzoListino = p.PrezzoListino,
                                                                                                    .PrezzoRivenditore1 = p.PrezzoRivenditore,
                                                                                                    .PrezzoRivenditore2 = p.PrezzoRivenditore,
                                                                                                    .PrezzoPromo = 0,
                                                                                                    .ImmagineGrande = p.ImmagineProdotto,
                                                                                                    .Macro = p.Macro,
                                                                                                    .Marca = p.Marca,
                                                                                                    .Famiglia = p.Famiglia,
                                                                                                    .FamigliaLink = p.FamigliaLink,
                                                                                                    .MacroLink = p.MacroLink,
                                                                                                    .CategoriaLink = p.CategoriaLink,
                                                                                                    .SubCategoriaLink = p.SubCategoriaLink,
                                                                                                    .Quantita = p.Qta,
                                                                                                    .FlagAttivo = p.FlagAttivo,
                                                                                                    .FlagNoleggio = p.FlagNoleggio,
                                                                                                    .Categoria = p.Categoria}).ToList

            End Using

            Dim DistinctItems = lProduct.GroupBy(Function(x) x.PkProdotto).[Select](Function(y) y.First())
            Return DistinctItems.Skip(start).Take(number).ToList

        End Function

        Public Shared Function GetListFromMacro(ByVal _FkMacro As Integer, ByVal start As Integer, ByVal number As Integer, ByVal sortBy As String, ByVal sortDirection As String) As List(Of DataClass.Products)

            Dim sorting As String = sortBy & " " & sortDirection

            Dim lProduct As List(Of DataClass.Products)
            Dim queryResult As List(Of DataClass.vwProdotti)

            Using context As New DataClass.DataEntities

                queryResult = (From p In context.vwProdotti Where p.FkMacro = _FkMacro And p.FlagAttivo = True Order By p.Macro Ascending, p.Categoria Ascending, p.SubCategoria Ascending, p.Modello Ascending Select p).ToList

                lProduct = queryResult.AsEnumerable().Select(Of DataClass.Products)(Function(p) New DataClass.Products() With {
                                                                                                    .PkProdotto = p.PK,
                                                                                                    .Codice = p.Codice,
                                                                                                    .Modello = p.Modello,
                                                                                                    .Prezzo = p.PrezzoFinale,
                                                                                                    .PrezzoListino = p.PrezzoListino,
                                                                                                    .PrezzoRivenditore1 = p.PrezzoRivenditore,
                                                                                                    .PrezzoRivenditore2 = p.PrezzoRivenditore,
                                                                                                    .PrezzoPromo = 0,
                                                                                                    .ImmagineGrande = p.ImmagineProdotto,
                                                                                                    .Macro = p.Macro,
                                                                                                    .Marca = p.Marca,
                                                                                                    .Famiglia = p.Famiglia,
                                                                                                    .FamigliaLink = p.FamigliaLink,
                                                                                                    .MacroLink = p.MacroLink,
                                                                                                    .SubCategoriaLink = p.SubCategoriaLink,
                                                                                                    .CategoriaLink = p.CategoriaLink,
                                                                                                    .Quantita = p.Qta,
                                                                                                    .FlagAttivo = p.FlagAttivo,
                                                                                                    .FlagNoleggio = p.FlagNoleggio,
                                                                                                    .Categoria = p.Categoria}).ToList

            End Using

            Dim DistinctItems = lProduct.GroupBy(Function(x) x.PkProdotto).[Select](Function(y) y.First())
            Return DistinctItems.Skip(start).Take(number).ToList

        End Function

        Public Shared Function GetPromoList(ByVal number As Integer) As List(Of DataClass.Products)

            Dim lProduct As List(Of DataClass.Products)
            Dim queryResult As List(Of DataClass.vwProdotti)

            Using context As New DataClass.DataEntities

                queryResult = (From p In context.vwProdotti Where p.FlagPromo = True And p.FlagAttivo = True Select p).ToList

                lProduct = queryResult.AsEnumerable().Select(Of DataClass.Products)(Function(p) New DataClass.Products() With {
                                                                                                    .PkProdotto = p.PK,
                                                                                                    .Codice = p.Codice,
                                                                                                    .Modello = p.Modello,
                                                                                                    .Prezzo = p.PrezzoFinale,
                                                                                                    .PrezzoListino = p.PrezzoListino,
                                                                                                    .PrezzoRivenditore1 = p.PrezzoRivenditore,
                                                                                                    .PrezzoRivenditore2 = p.PrezzoRivenditore,
                                                                                                    .PrezzoPromo = 0,
                                                                                                    .ImmagineGrande = p.ImmagineProdotto,
                                                                                                    .Macro = p.Macro,
                                                                                                    .Marca = p.Marca,
                                                                                                    .Famiglia = p.Famiglia,
                                                                                                    .FamigliaLink = p.FamigliaLink,
                                                                                                    .MacroLink = p.MacroLink,
                                                                                                    .CategoriaLink = p.CategoriaLink,
                                                                                                     .SubCategoriaLink = p.SubCategoriaLink,
                                                                                                    .Quantita = p.Qta,
                                                                                                    .FlagAttivo = p.FlagAttivo,
                                                                                                    .FlagNoleggio = p.FlagNoleggio,
                                                                                                    .Categoria = p.Categoria}).ToList

            End Using

            Dim randomProducts As List(Of Products) = lProduct.OrderBy(Function(x) Guid.NewGuid()).Take(number).ToList

            Return randomProducts
        End Function

        Public Shared Function GetVetrinaList(ByVal number As Integer) As List(Of DataClass.Products)

            Dim lProduct As List(Of DataClass.Products)
            Dim queryResult As List(Of DataClass.vwProdotti)

            Using context As New DataClass.DataEntities

                queryResult = (From p In context.vwProdotti Where p.FlagVetrina = True And p.FlagAttivo = True Select p).ToList

                lProduct = queryResult.AsEnumerable().Select(Of DataClass.Products)(Function(p) New DataClass.Products() With {
                                                                                                    .PkProdotto = p.PK,
                                                                                                    .Codice = p.Codice,
                                                                                                    .Modello = p.Modello,
                                                                                                    .Prezzo = p.PrezzoFinale,
                                                                                                    .PrezzoListino = p.PrezzoListino,
                                                                                                    .PrezzoRivenditore1 = p.PrezzoRivenditore,
                                                                                                    .PrezzoRivenditore2 = p.PrezzoRivenditore,
                                                                                                    .PrezzoPromo = 0,
                                                                                                    .ImmagineGrande = p.ImmagineProdotto,
                                                                                                    .Macro = p.Macro,
                                                                                                    .Marca = p.Marca,
                                                                                                    .Famiglia = p.Famiglia,
                                                                                                    .FamigliaLink = p.FamigliaLink,
                                                                                                    .MacroLink = p.MacroLink,
                                                                                                    .CategoriaLink = p.CategoriaLink,
                                                                                                     .SubCategoriaLink = p.SubCategoriaLink,
                                                                                                    .Quantita = p.Qta,
                                                                                                    .FlagAttivo = p.FlagAttivo,
                                                                                                    .FlagNoleggio = p.FlagNoleggio,
                                                                                                    .Categoria = p.Categoria}).ToList

            End Using

            Dim randomProducts As List(Of Products) = lProduct.OrderBy(Function(x) Guid.NewGuid()).Take(number).ToList

            Return randomProducts

        End Function

        Public Shared Function GetDetail(ByVal _pk As Integer) As DataClass.Products

            Dim Product As New DataClass.Products
            Dim queryResult As DataClass.vwProdotti

            Using context As New DataClass.DataEntities
                queryResult = (From p In context.vwProdotti Where p.PK = _pk Select p).FirstOrDefault
            End Using

            With Product
                .PkProdotto = queryResult.PK
                .Codice = queryResult.Codice
                .Modello = queryResult.Modello
                .DescrizioneBreve = queryResult.DescrizioneBreve
                .DescrizioneEstesa = queryResult.DescrizioneEstesa
                .DescrizioneCaratteristiche = queryResult.Caratteristiche
                .Prezzo = queryResult.PrezzoFinale
                .PrezzoListino = queryResult.PrezzoListino
                .PrezzoRivenditore1 = queryResult.PrezzoRivenditore
                .PrezzoRivenditore2 = queryResult.PrezzoRivenditore
                .PrezzoPromo = 0
                .ImmagineGrande = queryResult.ImmagineProdotto
                .Macro = queryResult.Macro
                .Marca = queryResult.Marca
                .Famiglia = queryResult.Famiglia
                .FamigliaLink = queryResult.FamigliaLink
                .MacroLink = queryResult.MacroLink
                .CategoriaLink = queryResult.CategoriaLink
                .Quantita = queryResult.Qta
                .FlagAttivo = queryResult.FlagAttivo
                .FlagNoleggio = queryResult.FlagNoleggio
                .Categoria = queryResult.Categoria
                .Allegato = queryResult.Allegato
                .SubCategoriaLink = queryResult.SubCategoriaLink
            End With

            Return Product

        End Function

        Public Shared Function GetCategory(ByVal _pk As Integer) As Integer
            Dim queryResult As Integer
            Using context As New DataClass.DataEntities
                queryResult = (From p In context.vwProdotti Where p.PK = _pk Select p.FkSubCategoria).FirstOrDefault
            End Using
            Return queryResult
        End Function

    End Class

End Namespace