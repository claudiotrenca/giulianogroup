﻿Imports System.Linq.Dynamic

Namespace DataClass

    Partial Public Class Post

        Public Shared Function GetDetail(ByVal _id As Integer) As Post

            Dim dc As New DataEntities
            Dim rv As Post

            Try
                rv = (From p In dc.Post Where p.PK = _id Select p).FirstOrDefault
            Catch ex As Exception
                rv = Nothing
            End Try

            Return rv

        End Function

        Public Shared Function GetDetailFromMenu(ByVal _id As Integer) As Post

            Dim dc As New DataEntities
            Dim rv As Post

            Try
                rv = (From p In dc.Post Where p.FkMenu = _id Select p).FirstOrDefault
            Catch ex As Exception
                rv = Nothing
            End Try

            Return rv

        End Function

        Public Shared Function GetList() As List(Of Post)

            Dim dc As New DataEntities
            Dim lPost As List(Of Post)

            Try
                lPost = (From p In dc.Post Select p Order By p.PK Descending).ToList
            Catch ex As Exception
                lPost = Nothing
            End Try

            Return lPost

        End Function

        Public Shared Function GetListFromCategory(ByVal Category As Integer) As List(Of Post)

            Dim dc As New DataEntities
            Dim lPost As List(Of Post)

            Try
                lPost = (From p In dc.Post Where p.FkMenu Select p Order By p.PK Descending).ToList
            Catch ex As Exception
                lPost = Nothing
            End Try

            Return lPost

        End Function

        Public Shared Function Insert(ByVal _Titolo As String, ByVal _SottoTitolo As String, ByVal _Contenuto As String, ByVal _ContenutoBreve As String, ByVal _Link As String, ByVal _Sezione As String, ByVal _Fondo As String, ByVal _FlagVisibile As Boolean, ByVal _FlagVetrina As Boolean, ByVal _LinkNascosti As String, ByVal _Immagine As String, ByVal _Key1 As String, ByVal _key2 As String, ByVal _Key3 As String, ByVal _IDM As String, ByVal _FKMenu As Integer, Optional ByVal _CodLang As String = "it-IT") As Boolean

            Dim res As Boolean = False

            Dim Obj As New DataClass.Post With {
                                                 .CodLang = _CodLang,
                                                 .Contenuto = _Contenuto,
                                                 .ContenutoBreve = _ContenutoBreve,
                                                 .Data = Date.Now,
                                                 .FkMenu = _FKMenu,
                                                 .FlagVetrina = _FlagVetrina,
                                                 .FlagVisibile = _FlagVisibile,
                                                 .Fondo = _Fondo,
                                                 .IDM = _IDM,
                                                 .Immagine = _Immagine,
                                                 .Key1 = _Key1,
                                                 .Key2 = _key2,
                                                 .Key3 = _Key3,
                                                 .Link = _Link,
                                                 .LinkNascosti = _LinkNascosti,
                                                 .Sezione = _Sezione,
                                                 .SottoTitolo = _SottoTitolo,
                                                 .Titolo = _Titolo
                                                 }

            Using context As New DataEntities
                context.Configuration.AutoDetectChangesEnabled = True
                context.Post.Attach(Obj)
                context.Entry(Obj).State = Entity.EntityState.Added
                Try
                    If context.SaveChanges() > 0 Then
                        res = True
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            End Using

            Return res

        End Function

        Public Shared Function Modify(ByVal _pk As Integer, ByVal _Titolo As String, ByVal _SottoTitolo As String, ByVal _Contenuto As String, ByVal _ContenutoBreve As String, ByVal _Link As String, ByVal _Sezione As String, ByVal _Fondo As String, ByVal _FlagVisibile As Boolean, ByVal _FlagVetrina As Boolean, ByVal _LinkNascosti As String, ByVal _Immagine As String, ByVal _Key1 As String, ByVal _key2 As String, ByVal _Key3 As String, ByVal _IDM As String, ByVal _FKMenu As Integer, Optional ByVal _CodLang As String = "it-IT") As Boolean

            Dim res As Boolean = False
            Using context = New DataEntities()
                Dim obj = context.Post.FirstOrDefault(Function(p) p.PK = _pk)

                If obj IsNot Nothing Then
                    With obj
                        .CodLang = _CodLang
                        .Contenuto = _Contenuto
                        .ContenutoBreve = _ContenutoBreve
                        .Data = Date.Now
                        .FkMenu = _FKMenu
                        .FlagVetrina = _FlagVetrina
                        .FlagVisibile = _FlagVisibile
                        .Fondo = _Fondo
                        .IDM = _IDM
                        .Immagine = _Immagine
                        .Key1 = _Key1
                        .Key2 = _key2
                        .Key3 = _Key3
                        .Link = _Link
                        .LinkNascosti = _LinkNascosti
                        .Sezione = _Sezione
                        .SottoTitolo = _SottoTitolo
                        .Titolo = _Titolo
                    End With

                    Try
                        context.SaveChanges()
                        res = True
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If
            End Using

            Return res

        End Function

        Public Shared Function Delete(ByVal _PK As String) As Boolean

            Dim res As Boolean = False

            Using context = New DataEntities()
                Dim obj = context.Post.FirstOrDefault(Function(p) p.PK = _PK)

                If obj IsNot Nothing Then
                    Try
                        context.Post.Remove(obj)
                        context.SaveChanges()
                        res = True
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If

            End Using

            Return res

        End Function

    End Class

    Partial Public Class vwPost

        Public Shared Function GetDetail(ByVal _id As Integer) As vwPost

            Dim dc As New DataEntities
            Dim rv As vwPost

            'Try
            '    rv = (From p In dc.vwPost Where p.PK = _id And p.FlagVisibile = True Select p).FirstOrDefault
            'Catch ex As Exception
            '    rv = Nothing
            'End Try

            Using context = New DataEntities()
                rv = context.vwPost.FirstOrDefault(Function(p) p.PK = _id)
            End Using

            Return rv

        End Function

        Public Shared Function GetDetailFromMenu(ByVal _id As Integer) As vwPost

            Dim dc As New DataEntities
            Dim rv As vwPost

            Try
                rv = (From p In dc.vwPost Where p.FkMenu = _id And p.FlagVisibile = True Select p).FirstOrDefault
            Catch ex As Exception
                rv = Nothing
            End Try

            Return rv

        End Function

        'Public Shared Function GetDetailFromMenu(ByVal _id As Integer) As vwPost

        '    Dim dc As New DataEntities
        '    Dim rv As vwPost

        '    Try
        '        rv = (From p In dc.vwPost Where p.FkMenu = _id And p.FlagVisibile = True Select p).FirstOrDefault
        '    Catch ex As Exception
        '        rv = Nothing
        '    End Try

        '    Return rv

        'End Function

        Public Shared Function GetList() As List(Of vwPost)

            Dim dc As New DataEntities
            Dim lPost As List(Of vwPost)

            Try
                lPost = (From p In dc.vwPost Select p Order By p.PK Descending).ToList
            Catch ex As Exception
                lPost = Nothing
            End Try

            Return lPost

        End Function

        Public Shared Function GetListFromCategory(ByVal Category As Integer) As List(Of vwPost)

            Dim dc As New DataEntities
            Dim lPost As List(Of vwPost)

            Try
                lPost = (From p In dc.vwPost Where p.FkMenu Select p Order By p.PK Descending).ToList
            Catch ex As Exception
                lPost = Nothing
            End Try

            Return lPost

        End Function

    End Class

End Namespace