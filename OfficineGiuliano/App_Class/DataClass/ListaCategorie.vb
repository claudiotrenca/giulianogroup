﻿Namespace DataClass
	Partial Public Class ListaCategorie
		Inherits DataClass.vwMenu

		Private _ImmagineProdotto As String

		Public Sub New()
			MyBase.New()
		End Sub

		Public Property ImmagineProdotto() As String
			Get
				Return Me._ImmagineProdotto
			End Get
			Set(value As String)
				If (String.Equals(Me._ImmagineProdotto, value) = False) Then
					Me._ImmagineProdotto = value
				End If
			End Set
		End Property

	End Class

End Namespace