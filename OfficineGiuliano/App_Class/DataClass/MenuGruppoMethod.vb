﻿Namespace DataClass
    Partial Public Class MenuGruppo

        Public Shared Function GetList() As List(Of MenuGruppo)
            Dim rv As List(Of DataClass.MenuGruppo)

            Using dc As New DataEntities
                Try
                    rv = (From r In dc.MenuGruppo Select r Order By r.Nome).ToList
                Catch ex As Exception
                    rv = Nothing
                End Try
            End Using

            Return rv

        End Function

    End Class

End Namespace