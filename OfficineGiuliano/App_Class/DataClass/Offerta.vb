﻿Namespace DataClass
    Public Class Offerta

        Public Property IdProdotto As Integer
        Public Property Immagine As String
        Public Property Struttura As String
        Public Property Localita As String
        Public Property Categoria As String
        Public Property Macro As String
        Public Property LinkCategoria As String
        Public Property LinkMacro As String
        Public Property Prezzo As Decimal
        Public Property Periodo As String

    End Class

End Namespace