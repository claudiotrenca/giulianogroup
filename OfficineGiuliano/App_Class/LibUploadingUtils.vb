﻿Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Web
Imports System.Web.Caching

Public NotInheritable Class LibUploadingUtils
    Private Const RemoveTaskKeyPrefix As String = "DXRemoveTask_"

    Private Sub New()
    End Sub
    Public Shared Sub RemoveFileWithDelay(ByVal key As String, ByVal fullPath As String, ByVal delay As Integer)
        RemoveFileWithDelayInternal(key, fullPath, delay, AddressOf FileSystemRemoveAction)
    End Sub

    Public Shared Function FormatSize(ByVal value As Object) As String
        Dim amount As Double = Convert.ToDouble(value)
        Dim unit As String = "KB"
        If amount <> 0 Then
            If amount <= 1024 Then
                amount = 1
            Else
                amount /= 1024
            End If

            If amount > 1024 Then
                amount /= 1024
                unit = "MB"
            End If
            If amount > 1024 Then
                amount /= 1024
                unit = "GB"
            End If
        End If
        Return String.Format("{0:#,0} {1}", Math.Round(amount, MidpointRounding.AwayFromZero), unit)
    End Function

    Private Shared Sub FileSystemRemoveAction(ByVal key As String, ByVal value As Object, ByVal reason As CacheItemRemovedReason)
        Dim fileFullPath As String = value.ToString()
        If File.Exists(fileFullPath) Then
            File.Delete(fileFullPath)
        End If
    End Sub

    Private Shared Sub RemoveFileWithDelayInternal(ByVal fileKey As String, ByVal fileData As Object, ByVal delay As Integer, ByVal removeAction As CacheItemRemovedCallback)
        Dim key As String = RemoveTaskKeyPrefix & fileKey
        If HttpRuntime.Cache(key) Is Nothing Then
            Dim absoluteExpiration As DateTime = DateTime.UtcNow.Add(New TimeSpan(0, delay, 0))
            HttpRuntime.Cache.Insert(key, fileData, Nothing, absoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, removeAction)
        End If
    End Sub

    Private Class AzureFileInfo
        Private privateFileKeyName As String
        Public Property FileKeyName() As String
            Get
                Return privateFileKeyName
            End Get
            Set(ByVal value As String)
                privateFileKeyName = value
            End Set
        End Property
        Private privateStorageAccountName As String
        Public Property StorageAccountName() As String
            Get
                Return privateStorageAccountName
            End Get
            Set(ByVal value As String)
                privateStorageAccountName = value
            End Set
        End Property
        Private privateAccessKey As String
        Public Property AccessKey() As String
            Get
                Return privateAccessKey
            End Get
            Set(ByVal value As String)
                privateAccessKey = value
            End Set
        End Property
        Private privateContainerName As String
        Public Property ContainerName() As String
            Get
                Return privateContainerName
            End Get
            Set(ByVal value As String)
                privateContainerName = value
            End Set
        End Property

        Public Sub New(ByVal fileKeyName As String, ByVal storageAccountName As String, ByVal accessKey As String, ByVal containerName As String)
            fileKeyName = fileKeyName
            storageAccountName = storageAccountName
            accessKey = accessKey
            containerName = containerName
        End Sub
    End Class

    Private Class AmazonFileInfo
        Private privateFileKeyName As String
        Public Property FileKeyName() As String
            Get
                Return privateFileKeyName
            End Get
            Set(ByVal value As String)
                privateFileKeyName = value
            End Set
        End Property
        Private privateAccessKeyID As String
        Public Property AccessKeyID() As String
            Get
                Return privateAccessKeyID
            End Get
            Set(ByVal value As String)
                privateAccessKeyID = value
            End Set
        End Property
        Private privateSecretAccessKey As String
        Public Property SecretAccessKey() As String
            Get
                Return privateSecretAccessKey
            End Get
            Set(ByVal value As String)
                privateSecretAccessKey = value
            End Set
        End Property
        Private privateBucketName As String
        Public Property BucketName() As String
            Get
                Return privateBucketName
            End Get
            Set(ByVal value As String)
                privateBucketName = value
            End Set
        End Property
        Private privateRegion As String
        Public Property Region() As String
            Get
                Return privateRegion
            End Get
            Set(ByVal value As String)
                privateRegion = value
            End Set
        End Property

        Public Sub New(ByVal fileKeyName As String, ByVal accessKeyID As String, ByVal secretAccessKey As String, ByVal bucketName As String, ByVal region As String)
            fileKeyName = fileKeyName
            accessKeyID = accessKeyID
            secretAccessKey = secretAccessKey
            bucketName = bucketName
            region = region
        End Sub
    End Class
End Class