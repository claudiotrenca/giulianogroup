﻿Imports System
Imports System.Data
Imports System.Collections
Imports System.Data.SqlClient

Partial Public Class ContentClass
    Private _PK_CODICE As String
    Private _TITOLO_PAGE As String
    Private _DESCRIZIONE_PAGE As String
    Private _KEYWORD_PAGE As String
    Private _TITOLO As String
    Private _SOTTOTITOLO As String
    Private _CONTENUTO As String
    Private _CONTENUTO_V As String
    Private _DATA_UA As String
    Private _FLAG_VISIBILE As Integer
    Private _VETRINA As Integer
    Private _VISITE As Integer
    Private _RIF_TITOLO As String
    Private _FONDO As String
    Private _DIV_LINK As String
    Private _KEYWORDS_1 As String
    Private _KEYWORDS_2 As String
    Private _KEYWORDS_3 As String
    Private _IMMAGINE_PAGE As String
    Private _SEZIONE As String
    Private _gallery As String
    Private _DESC_BREVE As String
    Private _DATA As String
    Private _fk_categoria_dl As String
    Private _macro As String
    Private _referrer As String
    Private _NUM_REC As Integer

    ReadOnly Property NUM_REC() As Integer
        Get
            Return _NUM_REC
        End Get
    End Property

    ReadOnly Property REFERRER() As String
        Get
            Return _referrer
        End Get
    End Property
    Private _ERROR As String

    ReadOnly Property PK_CODICE() As String
        Get
            Return _PK_CODICE
        End Get
    End Property
    ReadOnly Property DATA() As String
        Get
            Return _DATA
        End Get
    End Property

    Public Property TITOLO_PAGE() As String
        Get
            Return _TITOLO_PAGE
        End Get
        Set(value As String)
            _TITOLO_PAGE = value
        End Set
    End Property
    Public Property DESCRIZIONE_PAGE() As String
        Get
            Return _DESCRIZIONE_PAGE
        End Get
        Set(value As String)
            _DESCRIZIONE_PAGE = value
        End Set
    End Property
    Public Property KEYWORD_PAGE() As String
        Get
            Return _KEYWORD_PAGE
        End Get
        Set(value As String)
            _KEYWORD_PAGE = value
        End Set
    End Property
    Public Property TITOLO() As String
        Get
            Return _TITOLO
        End Get
        Set(value As String)
            _TITOLO = value
        End Set
    End Property

    ReadOnly Property SOTTOTITOLO() As String
        Get
            Return _SOTTOTITOLO
        End Get
    End Property
    ReadOnly Property CONTENUTO() As String
        Get
            Return _CONTENUTO
        End Get
    End Property
    ReadOnly Property CONTENUTO_V() As String
        Get
            Return _CONTENUTO_V
        End Get
    End Property
    ReadOnly Property DATA_UA() As String
        Get
            Return _DATA_UA
        End Get
    End Property
    ReadOnly Property FLAG_VISIBILE() As Integer
        Get
            Return _FLAG_VISIBILE
        End Get
    End Property
    ReadOnly Property VETRINA() As Integer
        Get
            Return _VETRINA
        End Get
    End Property
    ReadOnly Property VISITE() As Integer
        Get
            Return _VISITE
        End Get
    End Property
    ReadOnly Property ERRORE() As String
        Get
            Return _ERROR
        End Get
    End Property
    ReadOnly Property RIF_TITOLO() As String
        Get
            Return _RIF_TITOLO
        End Get
    End Property
    ReadOnly Property FONDO() As String
        Get
            Return _FONDO
        End Get
    End Property
    Public Property DIV_LINK() As String
        Get
            Return _DIV_LINK
        End Get
        Set(value As String)
            _DIV_LINK = value
        End Set
    End Property
    ReadOnly Property KEYWORDS_1() As String
        Get
            Return _KEYWORDS_1
        End Get
    End Property
    ReadOnly Property KEYWORDS_2() As String
        Get
            Return _KEYWORDS_2
        End Get
    End Property
    ReadOnly Property KEYWORDS_3() As String
        Get
            Return _KEYWORDS_3
        End Get
    End Property
    ReadOnly Property IMMAGINE_PAGE() As String
        Get
            Return _IMMAGINE_PAGE
        End Get
    End Property

    ReadOnly Property SEZIONE() As String
        Get
            Return _SEZIONE
        End Get
    End Property
    ReadOnly Property fk_categoria_dl() As String
        Get
            Return _fk_categoria_dl
        End Get
    End Property
    ReadOnly Property MACRO() As String
        Get
            Return _macro
        End Get
    End Property

    ReadOnly Property GALLERY() As String
        Get
            Return _gallery
        End Get
    End Property
    ReadOnly Property DESC_BREVE() As String
        Get
            Return _DESC_BREVE
        End Get
    End Property

    Dim tbNAME As String = "_content"
    Dim sito_p As String = "Chirurgiestetiche.it"
    Dim vwNAME As String = "vwcontent"

    Public Sub New(ByVal _ref As String)
        _referrer = _ref
    End Sub

    Public Function EstraiInfoPagina(ByVal value As Integer, Optional ByVal link_page As String = "") As Boolean

        Dim dt As New DataTable()
        Dim sql As String = " select * from " & vwNAME & " where PK_CODICE = " & value
        ConnectionClass.leggi(sql, ConnectionClass.cnDBSql, dt)

        If dt.Rows.Count > 0 Then

            _PK_CODICE = dt.Rows(0)("PK_CODICE").ToString
            _TITOLO_PAGE = dt.Rows(0)("TITOLO_PAGE").ToString
            _DESCRIZIONE_PAGE = dt.Rows(0)("DESCRIZIONE_PAGE").ToString
            _KEYWORD_PAGE = dt.Rows(0)("KEYWORD_PAGE").ToString
            _TITOLO = dt.Rows(0)("TITOLO").ToString
            _DIV_LINK = dt.Rows(0)("DIV_LINK").ToString

            Return True
        Else
            Return False
        End If

    End Function

    Public Function ESTRAI_SCHEDA(ByVal value As Integer, Optional ByVal TESTO As String = "", Optional ByVal fcb As Boolean = False, Optional ByVal link_page As String = "") As Boolean
        Dim ds As New DataTable()
        Dim sql As String

        If value >= 0 Then
            sql = "SELECT PK_CODICE FROM " & vwNAME & "  WHERE  ID_MERCHAND = '" & _referrer & "' and PK_CODICE = " & value
        Else
            sql = "select PK_CODICE from " & vwNAME & " where ID_MERCHAND = '" & _referrer & "' and CATEGORIA = '" & TESTO & "'"
        End If

        ConnectionClass.leggi(sql, ConnectionClass.cnDBSql, ds)

        If ds.Rows.Count > 0 Then
            ESTRAI_SCHEDA_PK(ds.Rows(0)("PK_CODICE"), fcb, link_page)

            Return True
        Else
            Return False
        End If
    End Function

    Public Function ESTRAI_SCHEDA_PK(ByVal value As Integer, Optional ByVal fcb As Boolean = False, Optional ByVal link_page As String = "") As Boolean

        Dim ds As New DataTable()
        Dim sql As String
        sql = " select * from " & vwNAME & " where PK_CODICE = " & value
        ConnectionClass.leggi(sql, ConnectionClass.cnDBSql, ds)

        If ds.Rows.Count > 0 Then
            _PK_CODICE = ds.Rows(0)("PK_CODICE")
            _TITOLO_PAGE = ds.Rows(0)("TITOLO_PAGE").ToString
            _DESCRIZIONE_PAGE = ds.Rows(0)("DESCRIZIONE_PAGE").ToString
            _KEYWORD_PAGE = ds.Rows(0)("KEYWORD_PAGE").ToString
            _TITOLO = ds.Rows(0)("TITOLO").ToString
            _SOTTOTITOLO = ds.Rows(0)("SOTTOTITOLO").ToString
            _CONTENUTO = ds.Rows(0)("CONTENUTO").ToString
            _CONTENUTO_V = ds.Rows(0)("CONTENUTO").ToString
            _FLAG_VISIBILE = ds.Rows(0)("FLAG_VISIBILE")
            _VETRINA = ds.Rows(0)("VETRINA")
            _RIF_TITOLO = ds.Rows(0)("PK_CODICE").ToString
            _FONDO = ds.Rows(0)("FONDO").ToString
            _DIV_LINK = ds.Rows(0)("DIV_LINK").ToString
            _KEYWORDS_1 = ds.Rows(0)("KEYWORDS_1").ToString
            _KEYWORDS_2 = ds.Rows(0)("KEYWORDS_2").ToString
            _KEYWORDS_3 = ds.Rows(0)("KEYWORDS_3").ToString
            _IMMAGINE_PAGE = ds.Rows(0)("IMMAGINE_PAGE").ToString
            _SEZIONE = ds.Rows(0)("SEZIONE").ToString
            _DESC_BREVE = ds.Rows(0)("CONTENUTO_BREVE").ToString
            _DATA = Mid(ds.Rows(0)("DATA"), 1, 10)
            _fk_categoria_dl = ds.Rows(0)("FK_CATEGORIA_DL")
            _macro = ds.Rows(0)("MACRO")

            If fcb Then
                _CONTENUTO_V += "<div class='eventi-share'><iframe src='http://www.facebook.com/plugins/like.php?href=" & link_page &
                              "&amp;layout=button_count&amp;show_faces=false&amp;width=200&amp;action=like&amp;font=" &
                              "arial&amp;colorscheme=light&amp;height=35' scrolling='no' frameborder='0' style='border:none; " &
                              "overflow:hidden; width:200px; height:35px;' allowTransparency='true'></iframe></div>"
                _CONTENUTO_V += "<div id='shr'><p>Condividi | </p><!-- AddThis Button BEGIN -->" &
                                 "<div class='addthis_toolbox addthis_default_style '>" &
                                 "<a class='addthis_button_facebook'></a>" &
                                 "<a class='addthis_button_twitter'></a>" &
                                 "<a class='addthis_button_email'></a>" &
                                 "<a class='at300bs at15t_favorites'></a>" &
                                 "<a class='addthis_button_compact'></a>" &
                                 "</div>" &
                                 "<script type='text/javascript' src='http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4d807fe6640892bf'></script><!-- AddThis Button END --></div><br/><br/>"
            End If

            _CONTENUTO_V += contenuti_cat(_fk_categoria_dl)

            Dim ds_2 As New DataTable()
            ConnectionClass.leggi("select pk_codice, etichetta, percorso from _GALLERIA_ART where FK_CONTENT = " & value, ConnectionClass.cnDBSql, ds_2)

            Dim i As Integer = 1
            Dim link As String

            If ds_2.Rows.Count > 0 Then
                _gallery = "<div class='fg-content'>"
                For Each dr As DataRow In ds_2.Rows

                    If i = 1 Then link = "<a href='/public/images/content/" & dr(2).ToString & "' rel='photostream' title='" & dr(1).ToString & "'>"

                    _gallery += "<div class='fg-thumb'><a href='/public/images/content/" & dr(2).ToString & "' class='fancybox'  title='" & dr(1).ToString & "' >" &
                                "<img src='/public/images/content/" & dr(2).ToString & "' alt='" & dr(1).ToString & "' /></a></div>"

                    i += 1
                Next

                _gallery += "</div>"

            Else
                _gallery = ""
            End If

            If _IMMAGINE_PAGE <> "" Then
                _CONTENUTO_V = "<a href='/public/images/content/" & _IMMAGINE_PAGE & "' class='fancybox'><img src='/public/images/content/" & _IMMAGINE_PAGE & "' class='foto-articolo img-polaroid pull-left'  /></a>" & _CONTENUTO_V
            End If

            _CONTENUTO_V = _CONTENUTO_V & _gallery

            _VISITE = ds.Rows(0)("VISITE")
            _DATA_UA = String.Format("{0:d}", ds.Rows(0)("DATA_UA").ToString)
            ConnectionClass.exec("update _content set visite = visite+1 where pk_codice = " & value, ConnectionClass.cnDBSql)
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ESTRAI_SCHEDA_RIF(ByVal value As Integer, Optional ByVal fcb As Boolean = False, Optional ByVal link_page As String = "") As Boolean

        Dim ds As New DataTable()
        Dim sql As String
        sql = " select * from " & vwNAME & " where rif_titolo = '" & value & "'"
        ConnectionClass.leggi(sql, ConnectionClass.cnDBSql, ds)

        If ds.Rows.Count > 0 Then
            _PK_CODICE = ds.Rows(0)("PK_CODICE")
            _TITOLO_PAGE = ds.Rows(0)("TITOLO_PAGE").ToString
            _DESCRIZIONE_PAGE = ds.Rows(0)("DESCRIZIONE_PAGE").ToString
            _KEYWORD_PAGE = ds.Rows(0)("KEYWORD_PAGE").ToString
            _TITOLO = ds.Rows(0)("TITOLO").ToString
            _SOTTOTITOLO = ds.Rows(0)("SOTTOTITOLO").ToString
            _CONTENUTO = ds.Rows(0)("CONTENUTO").ToString
            _CONTENUTO_V = ds.Rows(0)("CONTENUTO").ToString
            _FLAG_VISIBILE = ds.Rows(0)("FLAG_VISIBILE")
            _VETRINA = ds.Rows(0)("VETRINA")
            _RIF_TITOLO = ds.Rows(0)("PK_CODICE").ToString
            _FONDO = ds.Rows(0)("FONDO").ToString
            _DIV_LINK = ds.Rows(0)("DIV_LINK").ToString
            _KEYWORDS_1 = ds.Rows(0)("KEYWORDS_1").ToString
            _KEYWORDS_2 = ds.Rows(0)("KEYWORDS_2").ToString
            _KEYWORDS_3 = ds.Rows(0)("KEYWORDS_3").ToString
            _IMMAGINE_PAGE = ds.Rows(0)("IMMAGINE_PAGE").ToString
            _SEZIONE = ds.Rows(0)("SEZIONE").ToString
            _DESC_BREVE = ds.Rows(0)("CONTENUTO_BREVE").ToString
            _DATA = Mid(ds.Rows(0)("DATA"), 1, 10)
            _fk_categoria_dl = ds.Rows(0)("FK_CATEGORIA_DL")
            _macro = ds.Rows(0)("MACRO")

            If fcb Then
                _CONTENUTO_V += "<div class='eventi-share'><iframe src='http://www.facebook.com/plugins/like.php?href=" & link_page &
                              "&amp;layout=button_count&amp;show_faces=false&amp;width=200&amp;action=like&amp;font=" &
                              "arial&amp;colorscheme=light&amp;height=35' scrolling='no' frameborder='0' style='border:none; " &
                              "overflow:hidden; width:200px; height:35px;' allowTransparency='true'></iframe></div>"
                _CONTENUTO_V += "<div id='shr'><p>Condividi | </p><!-- AddThis Button BEGIN -->" &
                                 "<div class='addthis_toolbox addthis_default_style '>" &
                                 "<a class='addthis_button_facebook'></a>" &
                                 "<a class='addthis_button_twitter'></a>" &
                                 "<a class='addthis_button_email'></a>" &
                                 "<a class='at300bs at15t_favorites'></a>" &
                                 "<a class='addthis_button_compact'></a>" &
                                 "</div>" &
                                 "<script type='text/javascript' src='http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4d807fe6640892bf'></script><!-- AddThis Button END --></div><br/><br/>"
            End If

            _CONTENUTO_V += contenuti_cat(_fk_categoria_dl)

            Dim ds_2 As New DataTable()
            ConnectionClass.leggi("select pk_codice, etichetta, percorso from _GALLERIA_ART where FK_CONTENT = " & value, ConnectionClass.cnDBSql, ds_2)

            Dim i As Integer = 1
            Dim link As String

            If ds_2.Rows.Count > 0 Then
                _gallery = "<div class='fg-content'>"
                For Each dr As DataRow In ds_2.Rows

                    If i = 1 Then link = "<a href='/public/images/content/" & dr(2).ToString & "' rel='photostream' title='" & dr(1).ToString & "'>"

                    _gallery += "<div class='fg-thumb'><a href='/public/images/content/" & dr(2).ToString & "' class='fancybox'  title='" & dr(1).ToString & "' >" &
                                "<img src='/public/images/content/" & dr(2).ToString & "' alt='" & dr(1).ToString & "' /></a></div>"

                    i += 1
                Next

                _gallery += "</div>"

            Else
                _gallery = ""
            End If

            If _IMMAGINE_PAGE <> "" Then
                _CONTENUTO_V = "<a href='/public/images/content/" & _IMMAGINE_PAGE & "' class='fancybox'><img src='/public/images/content/" & _IMMAGINE_PAGE & "' class='foto-articolo img-polaroid pull-left'  /></a>" & _CONTENUTO_V
            End If

            _CONTENUTO_V = _CONTENUTO_V & _gallery

            _VISITE = ds.Rows(0)("VISITE")
            _DATA_UA = String.Format("{0:d}", ds.Rows(0)("DATA_UA").ToString)
            ConnectionClass.exec("update _content set visite = visite+1 where pk_codice = " & value, ConnectionClass.cnDBSql)
            Return True
        Else
            Return False
        End If
    End Function

    Public Function LISTA_AFFILIAZIONI() As String
        Dim i As Integer
        Dim str As String = "<ul>"

        For Each dr As DataRow In ConnectionClass.leggi("select * from _AFFILIAZIONI where ID_MERCHAND = '" & _referrer & "'", ConnectionClass.cnDBSql, New DataTable).Rows
            str = str & "<li><a href='" & dr("link") & "' target='_blank'>" & dr("descrizione") & "</a></li>"
        Next

        If str <> "" Then str = str & "</ul>"
        Return str
    End Function

    Public Function ESTRAI_LISTA(ByVal value As Integer, Optional ByVal fcb As Boolean = False, Optional ByVal link_page As String = "") As String

        Dim ds As New DataTable()
        ConnectionClass.leggi("SELECT top 50 * FROM " & vwNAME & " WHERE ID_MERCHAND = '" & _referrer & "' and flag_visibile = 1 and fk_categoria_dl = " & value, ConnectionClass.cnDBSql, ds)

        If ds.Rows.Count > 0 Then
            If ds.Rows.Count = 1 Then
                'una sola riga
                ESTRAI_SCHEDA_PK(ds.Rows(0)("PK_CODICE"), fcb, link_page)
                Return _CONTENUTO_V
            Else
                Dim cls As Classi.Utility
                Dim link As String

                Dim contenuto As String = ""
                _TITOLO_PAGE = "_"
                _DESCRIZIONE_PAGE = ""
                _KEYWORD_PAGE = ""

                For Each dr As DataRow In ds.Rows
                    If _TITOLO_PAGE = "_" Then
                        _TITOLO_PAGE = cls.fix_str(dr("CATEGORIA").trim) & " - " & sito_p
                        _DESCRIZIONE_PAGE = sito_p & " : " & cls.fix_str(dr("CATEGORIA").trim)
                        _KEYWORD_PAGE = cls.fix_str(dr("CATEGORIA").trim)
                    End If

                    contenuto += "<div class='elencocontenuti'>"
                    link = "/#CATEGORIA#/#ID#/sk/#RIF#/#TITOLO#.htm"
                    link = Replace(link, "#CATEGORIA#", Classi.Utility.StringToUrl(dr("CATEGORIA").trim))
                    link = Replace(link, "#ID#", dr("PK_CODICE"))
                    link = Replace(link, "#TITOLO#", Classi.Utility.StringToUrl(dr("TITOLO").trim))
                    link = Replace(link, "#RIF#", dr("FK_CATEGORIA_DL"))

                    contenuto += " <h3><span class='stile1'><strong>" & dr("TITOLO") & " </strong></span></h3>" &
                                 "<p>" & dr("CONTENUTO_BREVE") & "</p>"
                    contenuto += " <p><a href='" & link & "'>approfondisci &raquo;</a></p>"
                    contenuto += "</div>"
                Next

                Return contenuto
            End If
        Else
            Return "&nbsp;"
        End If

    End Function

    Public Function ESTRAI_LISTA_BLOG(ByVal value As Integer, Optional ByVal fcb As Boolean = False, Optional ByVal link_page As String = "") As String

        Dim ds As New DataTable()
        ConnectionClass.leggi("SELECT top 1 * FROM " & vwNAME & " WHERE ID_MERCHAND = '" & _referrer & "' and flag_visibile = 1 and fk_categoria_dl = " & value & " order by pk_codice desc", ConnectionClass.cnDBSql, ds)

        If ds.Rows.Count > 0 Then
            If ds.Rows.Count = 1 Then
                'una sola riga
                ESTRAI_SCHEDA_PK(ds.Rows(0)("PK_CODICE"), fcb, link_page)
                Return _CONTENUTO_V
            Else

                Dim link As String

                Dim contenuto As String = ""
                _TITOLO_PAGE = "_"
                _DESCRIZIONE_PAGE = ""
                _KEYWORD_PAGE = ""

                For Each dr As DataRow In ds.Rows
                    If _TITOLO_PAGE = "_" Then
                        _TITOLO_PAGE = Classi.Utility.fix_str(dr("CATEGORIA").trim) & " - " & sito_p
                        _DESCRIZIONE_PAGE = sito_p & " : " & Classi.Utility.fix_str(dr("CATEGORIA").trim)
                        _KEYWORD_PAGE = Classi.Utility.fix_str(dr("CATEGORIA").trim)
                    End If

                    contenuto += "<div class='elencocontenuti'>"
                    link = "/#CATEGORIA#/#ID#/sk/#RIF#/#TITOLO#.htm"
                    link = Replace(link, "#CATEGORIA#", Classi.Utility.StringToUrl(dr("CATEGORIA").trim))
                    link = Replace(link, "#ID#", dr("PK_CODICE"))
                    link = Replace(link, "#TITOLO#", Classi.Utility.StringToUrl(dr("TITOLO").trim))
                    link = Replace(link, "#RIF#", dr("FK_CATEGORIA_DL"))

                    contenuto += " <h3><span class='stile1'><strong>" & dr("TITOLO") & " </strong></span></h3>" &
                                 "<p>" & dr("CONTENUTO_BREVE") & "</p>"
                    contenuto += " <p><a href='" & link & "'>approfondisci &raquo;</a></p>"
                    contenuto += "</div>"
                Next

                Return contenuto
            End If
        Else
            Return "&nbsp;"
        End If

    End Function

    Public Function contenuti_cat(ByVal value As Integer) As String
        'dim ds_1 as New datatable()
        '		ConnectionClass.leggi("", ConnectionClass.cnDBSql, ds_1)
        '		if ds_1.rows.count > 0 then

        Dim ds As New DataTable()
        ConnectionClass.leggi("SELECT * FROM " & vwNAME & " WHERE ID_MERCHAND = '" & _referrer & "' and flag_visibile = 1 and fk_categoria_dl in (SELECT PK_codice FROM _CATEGORIE_DL WHERE VISIBLE=1 AND RIF_TITOLO = '" & value & "')", ConnectionClass.cnDBSql, ds)

        If ds.Rows.Count > 0 Then

            Dim link As String

            Dim contenuto As String = ""
            contenuto += "<div id='vedianche'><h2>Vedi anche</h2>"

            For Each dr As DataRow In ds.Rows
                contenuto += "<div class='elencocontenuti'>"

                link = "/#CATEGORIA#/#ID#/sk/#RIF#/#TITOLO#.htm"
                link = Replace(link, "#CATEGORIA#", Classi.Utility.StringToUrl(dr("CATEGORIA").trim))
                link = Replace(link, "#ID#", dr("PK_CODICE"))
                link = Replace(link, "#TITOLO#", Classi.Utility.StringToUrl(dr("TITOLO").trim))
                link = Replace(link, "#RIF#", dr("FK_CATEGORIA_DL"))

                contenuto += " <h3><span class='stile1'><strong>" & dr("TITOLO") & " </strong></span></h3>" &
                             "<p>" & dr("CONTENUTO_BREVE") & "</p>"
                contenuto += " <p><a href='" & link & "'>approfondisci &raquo;</a></p>"
                contenuto += "</div>"

            Next
            contenuto += "</div>"
            Return contenuto

            'else
            '		  return "&nbsp;"
            '		end if
        Else
            Return "&nbsp;"
        End If
    End Function

    Private Function CreateRow(Text As String, Value As String, dt As DataTable) As DataRow
        Dim dr As DataRow = dt.NewRow()
        dr(0) = Text
        dr(1) = Value
        Return dr
    End Function

    Public Function LISTA_CATEGORIE(Optional ByVal n_start As Integer = 0) As DataView

        Dim MACRO As String
        MACRO = "*"

        Dim dt As DataTable = New DataTable()
        dt.Columns.Add(New DataColumn("PK_CODICE", GetType(String)))
        dt.Columns.Add(New DataColumn("DESCRIZIONE", GetType(String)))

        Dim i As Integer = 0
        Dim tipo As String

        For i = n_start To 2
            If i = 0 Then tipo = "-1"
            If i = 1 Then tipo = "index"
            If i = 2 Then tipo = "0"

            For Each dr As DataRow In ConnectionClass.leggi("select * from _categorie_dl where ID_MERCHANT = '" & _referrer & "' and RIF_TITOLO = '" & tipo & "' order by indice", ConnectionClass.cnDBSql, New DataTable).Rows
                dt.Rows.Add(CreateRow(dr("PK_CODICE"), dr("DESCRIZIONE"), dt))

                For Each dr2 As DataRow In ConnectionClass.leggi("SELECT * FROM _CATEGORIE_DL WHERE RIF_TITOLO = '" & dr("PK_CODICE") & "' ORDER BY indice ", ConnectionClass.cnDBSql, New DataTable).Rows
                    dt.Rows.Add(CreateRow(dr2("PK_CODICE"), "-- " & dr2("DESCRIZIONE"), dt))

                    For Each dr3 As DataRow In ConnectionClass.leggi("SELECT * FROM _CATEGORIE_DL WHERE RIF_TITOLO = '" & dr2("PK_CODICE") & "' ORDER BY indice ", ConnectionClass.cnDBSql, New DataTable).Rows
                        dt.Rows.Add(CreateRow(dr3("PK_CODICE"), "----- " & dr3("DESCRIZIONE"), dt))
                        For Each dr4 As DataRow In ConnectionClass.leggi("SELECT * FROM _CATEGORIE_DL WHERE RIF_TITOLO = '" & dr3("PK_CODICE") & "' ORDER BY indice ", ConnectionClass.cnDBSql, New DataTable).Rows
                            dt.Rows.Add(CreateRow(dr4("PK_CODICE"), "--------- " & dr4("DESCRIZIONE"), dt))
                            For Each dr5 As DataRow In ConnectionClass.leggi("SELECT * FROM _CATEGORIE_DL WHERE RIF_TITOLO = '" & dr4("PK_CODICE") & "' ORDER BY indice ", ConnectionClass.cnDBSql, New DataTable).Rows
                                dt.Rows.Add(CreateRow(dr5("PK_CODICE"), "------------ " & dr5("DESCRIZIONE"), dt))
                            Next
                        Next
                    Next
                Next
            Next
        Next

        Dim dv As DataView = New DataView(dt)
        Return dv
    End Function

    Public Function DEL(rif As String) As Boolean
        Return ConnectionClass.exec("Delete From " & tbNAME & " Where PK_CODICE = " & rif, ConnectionClass.cnDBSql)

    End Function

    Public Function LISTA_NEWS(Optional ByVal n_rec As Integer = 3) As String
        Dim i As Integer
        Dim str As String = "<ul id='news'>"

        For Each dr As DataRow In ConnectionClass.leggi("select top " & n_rec & " * from " & vwNAME & " where ID_MERCHAND = '" & _referrer & "' order by data desc", ConnectionClass.cnDBSql, New DataTable).Rows
            str = str & "<li><a href='" & dr("link") & "' target='_blank'>" & dr("descrizione") & "</a></li>"
        Next

        If str <> "" Then str = str & "</ul>"
        Return str
    End Function

    Public Function LISTA_CONTENUTI(ByVal rif As String, ByVal currentpage As Integer, ByVal endpage As Integer) As DataTable
        Dim strORD As String
        strORD = " order by DATA desc"

        Dim strSQL As String = " SELECT * FROM " & tbNAME & " WHERE ID_MERCHAND = '" & _referrer & "' and flag_visibile = 1 and fk_categoria_dl = " & rif

        _NUM_REC = ConnectionClass.NUMERO_RECORD(strSQL)
        Return ConnectionClass.leggi(strSQL, "vwPRODOTTI", currentpage, endpage)
    End Function

    Function VETRINE_NEWS(ByVal sezione As String, ByVal n_vetrine As String) As DataTable
        Dim dt As New DataTable
        dt.TableName = "_CONTENT"
        Dim str As String = ""
        If sezione <> "*" Then str = " and sezione = '" & sezione & "' "
        Return ConnectionClass.leggi("SELECT top " & n_vetrine & " c.* from _CONTENT C " &
                                   " where PK_CODICE > 0 " & str & " order by c.pk_codice desc", ConnectionClass.cnDBSql, dt)
    End Function

    Public Function ESTRAI_PROVINCE(DESCRIZIONE As String) As String
        Dim strSQL As String = "select distinct REGIONE, PROVINCIA, PROV from vwELENCO_CHIR_PROV where id_merchand = '" & _referrer & "' "
        Dim str1 As String = ""
        Dim str2 As String = ""
        Dim dt_det As New DataTable()
        Dim i As Integer = 0

        Dim stRES As String = ""

        For Each dr As DataRow In ConnectionClass.leggi(strSQL, ConnectionClass.cnDBSql, New DataTable).Rows

            If str1 <> dr("REGIONE").trim Then
                If i > 0 Then str2 = "</ul>"
                stRES += str2 & "<h2>I  r " & DESCRIZIONE & " in " & dr("REGIONE").trim & "<h2> <ul> "
            End If

            stRES += "<li>" & dr("PROVINCIA").trim & "</li>"

            str1 = dr("REGIONE").trim
            i += 1
        Next
        stRES += "</ul>"

        Return stRES
    End Function

    Public Function PagineImg(ByVal link As String, ByVal Pagina As Integer, ByVal tot_in_page As Integer, ByVal cartella_dest As String, ByVal percorso As String) As String
        Dim link_page As String = Replace(link, "lt_1", "lt_#")

        Dim sr As System.IO.DirectoryInfo = New System.IO.DirectoryInfo(percorso & cartella_dest & "\th\")

        Dim fi() As IO.FileInfo
        fi = sr.GetFiles("*.jpg")

        Dim n_img_tot As Integer = fi.GetLength(0)

        Dim resto As Integer
        Dim j As Integer

        Dim res_link As String = ""
        Dim link_d As String = ""

        If n_img_tot > tot_in_page Then
            If (n_img_tot Mod tot_in_page) > 0 Then
                resto = Fix(n_img_tot / tot_in_page) + 1
            Else
                resto = (n_img_tot / tot_in_page)
            End If
        Else
            resto = 0
        End If

        For j = 1 To resto

            If j <> Pagina Then
                res_link += "<a href='" & Replace(link_page, "#", j) & "'>" & j & "</a>  "
            Else
                res_link += "<span class='pag_attiva'>" & j & "</span>  "

                If (n_img_tot > 1) Then
                    If (j > 1) Then link_d = "<a href='" & Replace(link_page, "#", (j - 1)) & "'>INDIETRO</a>  "
                    link_d += "# "

                    If j <> resto Then link_d += "<a href='" & Replace(link_page, "#", (j + 1)) & "'>AVANTI</a>"
                End If

            End If

        Next

        '	res_link += "<a href='" & replace(link_page, "#", J+1 ) & "'>" & J & "</a> | "
        If link_d = "" Then
            link_d = res_link
        Else
            link_d = Replace(link_d, "#", res_link)
        End If

        Return link_d

    End Function

    Public Function CaricaImg(ByVal cartella_dest As String, ByVal Pagina As Integer, ByVal tot_in_page As Integer, ByVal percorso As String) As String
        Dim sr As System.IO.DirectoryInfo = New System.IO.DirectoryInfo(percorso & cartella_dest & "\th\")
        Dim fi() As System.IO.FileInfo
        Dim i As Integer
        Dim res As Integer
        Dim t_page As Integer

        fi = sr.GetFiles("*.jpg")

        If Pagina = 1 Then
            res = 0
            t_page = tot_in_page - 1
        Else
            res = ((Pagina - 1) * tot_in_page)
            t_page = (tot_in_page * Pagina) - 1
        End If

        Dim misX As Integer
        Dim misY As Integer

        Dim corpo As String = ""
        Dim testo As String = "<div class='thumb_wrapper'><div class='thumb'><ul><li>" &
                              "<a class='cloud-zoom' href='/public/gallery/#IMG#' title='#NAME#'>" &
                              "<img src='/public/gallery/#IMG_TH#' /></a></li></ul></div><span>FOTO #NAME#</span></div>"

        Dim num As Integer = 0

        For i = res To t_page
            Try
                num += 1

                If num = 5 Then
                    num = 0
                    misX = -200
                    misY = -4
                Else
                    misX = 180
                    misY = -4

                End If
                corpo += Replace(
                           Replace(
                              Replace(
                                  Replace(
                                      Replace(testo, "#IMG#", cartella_dest & "/" & fi(i).Name),
                                      "#IMG_TH#", cartella_dest & "/th/" & fi(i).Name),
                                              "#NAME#", Mid(fi(i).Name, 1, Len(fi(i).Name) - 4)),
                                                  "#X#", misX),
                                                      "#Y#", misY)

            Catch
                ' catch
            End Try

        Next
        Return corpo

    End Function

    Public Function lista_blog(rif_titolo As String, n_post As Integer, format_str As String, filtro As String) As String
        Dim _filtro As String = ""
        If filtro <> "posts" Then _filtro = " KEYWORDS_1 = '" & filtro & "' AND "

        Dim strSQL As String = "select top " & n_post & " PK_CODICE, FK_CATEGORIA_DL, TITOLO, DATA, IMMAGINE_PAGE, CONTENUTO, SEZIONE, KEYWORDS_1  " &
                               " from vwCONTENT where " & _filtro & "   FLAG_VISIBILE = 1 and  fk_categoria_dl in (select pk_codice from _categorie_dl where rif_titolo = '" & rif_titolo & "') order by data desc "

        Dim dt As New DataTable
        ConnectionClass.leggi(strSQL, ConnectionClass.cnDBSql, dt)
        Dim posts As String = "<ul>"
        Dim _formats As String = ""
        For Each dr As DataRow In dt.Rows

            _formats = format_str
            _formats = Replace(_formats, "#TITOLO#", dr("TITOLO"))
            _formats = Replace(_formats, "#DATA#", dr("DATA"))
            _formats = Replace(_formats, "#IMG#", "/public/images/content/" & dr("IMMAGINE_PAGE").trim)
            _formats = Replace(_formats, "#DESCR#", dr("CONTENUTO"))
            _formats = Replace(_formats, "#LINK#", "/blog/" & dr("PK_CODICE") & "/bt_1/" & Classi.Utility.StringToUrl(dr("KEYWORDS_1")) & "/" & Classi.Utility.StringToUrl(dr("SEZIONE")) & "/")

            posts += _formats

        Next
        Return posts & "</ul>"
    End Function

    Public Function ESTRAI_COMMENTI(value As String) As DataTable
        Return ConnectionClass.leggi("select * from _CONTENT_COMMENTI where fk_content = " & value & " order by pk_codice desc", ConnectionClass.cnDBSql, New DataTable)

    End Function

    Public Function VISUA_COMMENTI(value As String) As String

        Dim dt As New DataTable
        ConnectionClass.leggi("select * from _CONTENT_COMMENTI where fk_content = " & value & " order by data_ins desc", ConnectionClass.cnDBSql, dt)

        Dim i As Integer = 0
        Dim posts As String = "<div class='row'><div class='span8 white-line-bottom'><h4 class='title-prev'>Commenti</h4></div></div>"
        Dim _formats As String = ""
        For Each dr As DataRow In dt.Rows
            i += 1
            _formats += "<div class='row'><div class='span8'>"
            _formats += "<div class='media'> "
            _formats += "<a class='pull-left  profile-photo' href='#'> "
            _formats += "<img class='media-object' src='/images/default-profile.png'></a> "
            _formats += "<div class='media-body'>"
            _formats += "<div class='well commento'>"
            _formats += "<h4 class='media-heading pull-left blog-author'>" & dr("UTENTE") & "</h4>"
            _formats += "<h4 class='media-heading pull-right date'>&nbsp;</h4>"
            _formats += "<p>" & dr("COMMENTO") & "</p>"
            _formats += "</div></div>"
            _formats += "</div></div>"
            _formats += "</div>"

            posts += _formats
            _formats = ""
        Next

        Return posts

    End Function

    Public Function N_COMMENTI(value As String) As String

        Return ConnectionClass.leggi("select count(*) as TOT from _CONTENT_COMMENTI where fk_content = " & value & " ", ConnectionClass.cnDBSql, New DataTable).Rows(0)(0).ToString

    End Function

    Public Function LISTA_POST_CAT(rif_titolo As String) As DataTable
        Dim strSQL As String = "select  KEYWORDS_1  " &
                                " from vwCONTENT where  FLAG_VISIBILE = 1 and fk_categoria_dl in (select pk_codice from _categorie_dl where rif_titolo = '" & rif_titolo & "') group by keywords_1   "
        Dim dt As New DataTable
        ConnectionClass.leggi(strSQL, ConnectionClass.cnDBSql, dt)
        Return dt

    End Function

    Public Function LISTA_RECENTI(rif_titolo As String, pk_codice As String) As DataTable
        Dim strSQL As String = "select TOP 6 PK_CODICE, FK_CATEGORIA_DL, TITOLO, DATA, IMMAGINE_PAGE, CONTENUTO, SEZIONE, KEYWORDS_1,KEYWORDS_3, FONDO, CATEGORIA  " &
                                " from vwCONTENT where FLAG_VISIBILE = 1 and fk_categoria_dl = " & rif_titolo & " and pk_codice <> " & pk_codice &
                                "  order by data desc   "
        Dim dt As New DataTable
        ConnectionClass.leggi(strSQL, ConnectionClass.cnDBSql, dt)
        Return dt

    End Function

    Public Function LISTA_RECENTI(rif_titolo As String, pk_codice As String, autore As String) As DataTable
        Dim strSQL As String = "select TOP 6 PK_CODICE, FK_CATEGORIA_DL, TITOLO, DATA, IMMAGINE_PAGE, CONTENUTO, SEZIONE, KEYWORDS_1,KEYWORDS_3, FONDO, CATEGORIA  " &
                                " from vwCONTENT where FLAG_VISIBILE = 1 and fk_categoria_dl = " & rif_titolo & " and pk_codice <> " & pk_codice &
                                " and keywords_3 = '" & autore & "' order by data desc   "
        Dim dt As New DataTable
        ConnectionClass.leggi(strSQL, ConnectionClass.cnDBSql, dt)
        Return dt

    End Function

    Public Function lista_macro(value As String, num As Integer) As DataTable

        Dim strSQL As String = "select TOP " & num & " PK_CODICE, FK_CATEGORIA_DL, TITOLO, DATA, IMMAGINE_PAGE, CONTENUTO, SEZIONE, KEYWORDS_1,  KEYWORDS_2,  KEYWORDS_3, FONDO  " &
                                " from vwCONTENT WHERE  fk_categoria_dl = '" & value & "'  order by  data desc "
        Dim dt As New DataTable
        ConnectionClass.leggi(strSQL, ConnectionClass.cnDBSql, dt)
        Return dt

    End Function

    Public Function ListaPiuLetti(ByVal _NumeroArticoli As Integer) As DataTable

        Dim strSQL As String = "select TOP " & _NumeroArticoli & " TOT,  PK_CODICE, FK_CATEGORIA_DL, TITOLO, DATA, IMMAGINE_PAGE, CONTENUTO, SEZIONE, KEYWORDS_1,  KEYWORDS_2,  KEYWORDS_3, FONDO  " &
                             " from vwCONTENT_COMMENTS where id_merchand = " & _referrer & "  order by visite desc , data desc "
        Dim dt As New DataTable
        ConnectionClass.leggi(strSQL, ConnectionClass.cnDBSql, dt)
        Return dt
    End Function

    Public Function ListaPiuLetti(ByVal _NumeroArticoli As Integer, ByVal _IdCategoria As Integer) As DataTable

        Dim strSQL As String = "select TOP " & _NumeroArticoli & " TOT,  PK_CODICE, FK_CATEGORIA_DL, TITOLO, DATA, IMMAGINE_PAGE, CONTENUTO, SEZIONE, KEYWORDS_1,  KEYWORDS_2,  KEYWORDS_3, FONDO  " &
                             " from vwCONTENT_COMMENTS where  fk_categoria_dl = " & _IdCategoria & "  order by visite desc , data desc "
        Dim dt As New DataTable
        ConnectionClass.leggi(strSQL, ConnectionClass.cnDBSql, dt)
        Return dt
    End Function

    Public Function lista_rif(value As String, num As Integer) As DataTable

        Dim strSQL As String = "select TOP " & num & " TOT,  PK_CODICE, FK_CATEGORIA_DL, TITOLO, DATA, IMMAGINE_PAGE, CONTENUTO, SEZIONE, KEYWORDS_1,  KEYWORDS_2,  KEYWORDS_3, FONDO  " &
                                " from vwCONTENT_COMMENTS where  fk_categoria_dl = " & value & "  order by indice asc , data desc "
        Dim dt As New DataTable
        ConnectionClass.leggi(strSQL, ConnectionClass.cnDBSql, dt)
        Return dt

    End Function

    Public Function lista_rif_indice(value As String, num As Integer) As DataTable

        Dim strSQL As String = "select TOP " & num & " TOT,  PK_CODICE, FK_CATEGORIA_DL, TITOLO, DATA, IMMAGINE_PAGE, CONTENUTO, SEZIONE, KEYWORDS_1,  KEYWORDS_2,  KEYWORDS_3, FONDO  from vwCONTENT_COMMENTS where  fk_categoria_dl = " & value & " and (rif_titolo is null or rif_titolo = '')  order by indice asc , data desc " 'and indice <> ''
        Dim dt As New DataTable
        ConnectionClass.leggi(strSQL, ConnectionClass.cnDBSql, dt)
        Return dt

    End Function

    Public Function INSERT_BLOG(rif As String, _UTENTE As String, _commento As String) As Boolean
        ' Dim strSQL As String = "INSERT INTO _CONTENT_COMMENTI (DATA_INS, FK_CONTENT, UTENTE, COMMENTO) VALUES (" &Classi.Utility.TRASFORMA_DATA(Date.Now) & ", " & rif & _
        '                      ", '" & _UTENTE & "', '" & _commento & "')"

        ' Return ConnectionClass.exec(strSQL, ConnectionClass.cnDBSql)

        '   @data smalldatetime,
        '   @rif int,
        '   @UTENTE nvarchar(30),
        '   @commento nvarchar(4000)

        Dim cnn As New SqlConnection
        cnn.ConnectionString = ConfigurationManager.ConnectionStrings("accesso").ConnectionString
        cnn.Open()

        Dim cmd As New SqlCommand
        cmd.Connection = cnn
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "sto_CommentiContentInsert"

        cmd.Parameters.Add("@DATA", SqlDbType.SmallDateTime).Value = FormatDateTime(Date.Now, DateFormat.ShortDate)
        cmd.Parameters.Add("@rif", SqlDbType.Int).Value = rif
        cmd.Parameters.Add("@utente", SqlDbType.NVarChar, 50).Value = _UTENTE
        cmd.Parameters.Add("@commento", SqlDbType.NVarChar, 4000).Value = _commento

        Try
            If cmd.ExecuteNonQuery() > 0 Then

                Return True
            Else

                Return False
            End If

        Catch ex As Exception

            Return False
        Finally
            cnn.Close()
        End Try

    End Function

    Public Function CARICA_WEDSITE(rif_titolo As String) As Integer
        Dim strSQL As String = "select  PK_CODICE from vwCONTENT where KEYWORDS_2 = '" & rif_titolo & "' "
        Dim dt As New DataTable
        ConnectionClass.leggi(strSQL, ConnectionClass.cnDBSql, dt)

        If dt.Rows.Count > 0 Then
            Return dt.Rows(0)(0)
        Else
            Return -1
        End If

    End Function

    Public Function PIU_LETTI()
        Return ConnectionClass.leggi("select TOP 4 PK_CODICE , TITOLO , DATA , KEYWORDs_3 , IMMAGINE_PAGE from vwcontent where ID_MERCHAND = '" & _referrer & "' and VETRINA = 1 and FLAG_VISIBILE = 1 and fk_categoria_dl <> 1924 order by VISITE desc", ConnectionClass.cnDBSql, New DataTable)

    End Function
    Public Function RECENTI()
        Return ConnectionClass.leggi("select distinct TOP 4 PK_CODICE , TITOLO , DATA , KEYWORDs_3 , IMMAGINE_PAGE from vwcontent where ID_MERCHAND = '" & _referrer & "' and VETRINA = 1 and FLAG_VISIBILE = 1 order by DATA desc", ConnectionClass.cnDBSql, New DataTable)
        ' select distinct TOP 10  * from vwcontent where ID_MERCHAND = '453' and VETRINA = 1 and FLAG_VISIBILE = 1 order by DATA desc
    End Function

End Class