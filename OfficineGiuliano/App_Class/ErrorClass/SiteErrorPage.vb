﻿Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ErrorClasses
    Public MustInherit Class SiteErrorPage
        Inherits System.Web.UI.Page

#Region "Fields"

        ' Protected BrokenUrl As Literal
        'Protected CrashReportBody As Literal
        'Protected CrashReportTitle As Literal
        'Protected ErrorReportPanel As Panel
        'Protected NextSteps As NextStepsLinkList
        'Protected ReportBody As HiddenField
        'Protected ReportTitle As HiddenField
        'Protected StatusCode As HiddenField
        'Protected SubmitQuickErrorReportButton As LinkButton

        Protected Friend _httpStatusCode As Int32


#End Region

#Region "Methods (helpers)"

        Protected Function GetBrokenUrl() As [String]
            If Request.QueryString("aspxerrorpath") IsNot Nothing Then
                Return Server.UrlDecode(Request.QueryString("aspxerrorpath"))
            End If

            Dim query As [String] = Server.UrlDecode(Request.QueryString.ToString())
            If Not [String].IsNullOrEmpty(query) Then
                Const pattern As [String] = "^(404|500);(.+):80(.+)$"
                Dim match As Match = Regex.Match(query, pattern)
                If match.Success Then
                    Return match.Groups(3).Value
                End If
            End If

            Return Request.RawUrl
        End Function

        Protected Function GetUserName() As [String]
            Dim user As [String] = "Anonymous"
            If Request.IsAuthenticated AndAlso Page.User IsNot Nothing Then
                user = Page.User.Identity.Name
            End If
            Return user
        End Function

        Protected Sub RegisterThankYouScript()
            Dim script = New StringBuilder()
            script.Append("<script language='javascript'>")
            script.AppendFormat("alert('Grazie per aver segnalato {0}.');<", If(_httpStatusCode = 404, "la pagina mancante", "l'errore"))
            script.Append("/script>")

            If Not ClientScript.IsStartupScriptRegistered("JSScript") Then
                ClientScript.RegisterStartupScript([GetType](), "JSScript", script.ToString())
            End If
        End Sub

        Protected Friend Shared Sub SendQuickErrorReport(url As [String], user As [String], browser As HttpBrowserCapabilities, title As [String], body As [String])

            Dim message = New MailMessage()
            message.[To].Add(ApplicationErrorModule.Settings.CrashReportEmailAddress)
            message.Subject = "Quick Error Report: " + title

            Dim html = New StringBuilder()

            html.Append("<style>")
            html.Append("*, body, p, table { font-family: calibri; font-size: 11pt; }")
            html.Append("table { border-collapse: collapse; }")
            html.Append("table tr th { text-align: left; padding-top: 10px; padding-bottom: 10px; }")
            html.Append("table tr td { border: 1px solid #cccccc; vertical-align: top; padding: 5px; }")
            html.Append("table tr td.label { font-weight: bold; color: rgb(38, 95, 159); }")
            html.Append("</style>")

            If url IsNot Nothing Then
                html.AppendFormat("Url: {0}<br />", url)
            End If

            If browser IsNot Nothing Then
                html.AppendFormat("Browser: {0} {1} {2}<br />", browser.Platform, browser.Browser, browser.Version)
            End If

            If user IsNot Nothing Then
                html.AppendFormat("User: {0}<br />", user)
            End If

            html.Append("<br />")

            html.Append(body)

            message.Body = html.ToString()
            message.IsBodyHtml = True

            Dim smtp = New SmtpClient()
            smtp.Send(message)
        End Sub


#End Region


    End Class

End Namespace