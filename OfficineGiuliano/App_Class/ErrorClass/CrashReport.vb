﻿Imports Microsoft.VisualBasic

Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Text
Imports System.Web
Imports System.Web.UI

Namespace ErrorClasses
    Public Class CrashReport

#Region "Properties"

        Public Property Body() As [String]
            Get
                Return m_Body
            End Get
            Set(value As [String])
                m_Body = value
            End Set
        End Property
        Private m_Body As [String]
        Public Property Exception() As Exception
            Get
                Return m_Exception
            End Get
            Set(value As Exception)
                m_Exception = value
            End Set
        End Property
        Private m_Exception As Exception
        Public Property Title() As [String]
            Get
                Return m_Title
            End Get
            Set(value As [String])
                m_Title = value
            End Set
        End Property
        Private m_Title As [String]

#End Region

#Region "Methods (construction)"

        Public Sub New(ex As Exception, Optional comment As [String] = Nothing)
            Exception = ex
            Dim context As HttpContext = HttpContext.Current

            Dim sb As New StringBuilder()

            sb.Append("<div id='crash-report'>")
            sb.AppendFormat("<h2>{0}: {1}</h2>", ex.[GetType]().Name, ex.Message)

            If comment IsNot Nothing Then
                sb.AppendFormat("<p>{0}</p>", comment)
            End If

            sb.Append("<table class='style-one'>")
            sb.AppendFormat("<tr><td class='label'>Time</td><td>{0:dddd - MMM d, yyyy h:mm tt} {1} ({2:h:mm tt} UTC)</td></tr>", DateTime.Now, TimeZone.CurrentTimeZone.StandardName, DateTime.UtcNow)
            sb.AppendFormat("<tr><td class='label'>Host</td><td>{0}</td></tr>", context.Request.Url.Host)
            sb.AppendFormat("<tr><td class='label'>Request</td><td>{0}</td></tr>", context.Request.Url)

            If context.Request.UrlReferrer IsNot Nothing Then
                sb.AppendFormat("<tr><td class='label'>Referrer</td><td>{0}</td></tr>", context.Request.UrlReferrer)
            End If

            sb.AppendFormat("<tr><td class='label'>User Identity</td><td>{0}</td></tr>", (If(context.Request.IsAuthenticated, context.User.Identity.Name, "Anonymous")))
            sb.AppendFormat("<tr><td class='label'>Authentication</td><td>{0}</td></tr>", (If(context.Request.IsAuthenticated, context.User.Identity.AuthenticationType, "None")))
            sb.AppendFormat("<tr><td class='label'>IP Address</td><td>{0}</td></tr>", context.Request.UserHostAddress)
            sb.AppendFormat("<tr><td class='label'>Browser</td><td>{0} {1} {2}</td></tr>", context.Request.Browser.Platform, context.Request.Browser.Browser, context.Request.Browser.Version)

            sb.Append(FormatException(ex, Me, 0))

            If context.Request IsNot Nothing Then
                sb.Append("<tr><th colspan='2'><h3>Request Variables</h3></th></tr>")

                DescribeValues("Query String", context.Request.QueryString, sb, Nothing, Nothing)
                DescribeValues("Form Variables", context.Request.Form, sb, {"__VIEWSTATE"}, Nothing)
                DescribeValues("Cookies", context.Request.Cookies, sb, Nothing, {"Total_UserPassword"})
                DescribeValues("Server Variables", context.Request.ServerVariables, sb, {"HTTP_COOKIE", "ALL_HTTP", "ALL_RAW"}, {"AUTH_PASSWORD"})
            End If

            sb.Append("</table>")
            sb.Append("</div>")

            Body = sb.ToString()
        End Sub

#End Region

#Region "Methods (helpers)"

        Private Shared Function FormatException(ex As Exception, report As CrashReport, depth As Int32) As [String]
            report.Title = ex.Message

            Dim sb = New StringBuilder()

            sb.AppendFormat("<tr><th colspan='2'><h3>Exception #{0}</h3></th></tr>", Math.Abs(depth))

            If Not [String].IsNullOrEmpty(ex.Source) Then
                sb.AppendFormat("<tr><td class='label'>Source</td><td>{0}</td></tr>", ex.Source)
            End If

            sb.AppendFormat("<tr><td class='label'>Message</td><td>{0}</td></tr>", FormatExceptionMessage(ex))
            sb.AppendFormat("<tr><td class='label'>Stack Trace</td><td>{0}</td></tr>", FormatExceptionStackTrace(ex))

            If ex.InnerException IsNot Nothing Then
                sb.Append(FormatException(ex.InnerException, report, depth + 1))
            End If

            Return sb.ToString()
        End Function

        Private Shared Function FormatExceptionMessage(ex As Exception) As [String]
            Dim httpException = TryCast(ex, HttpException)
            If httpException IsNot Nothing Then
                Return [String].Format("HTTP Error {0}: {1}", httpException.ErrorCode, httpException.Message)
            End If

            Dim viewStateException = TryCast(ex, ViewStateException)
            If viewStateException IsNot Nothing Then
                Return "View State Error: The server was still processing your last action (slow down). This error sometimes occurs when you perform an action in the system and then perform another action before your first action has finished being processed by the server. (Refer to http://support.microsoft.com/kb/555353)"
            End If

            Return [String].Format("{0}: {1}", ex.[GetType]().Name, ex.Message)
        End Function

        Private Shared Function FormatExceptionStackTrace(ex As Exception) As [String]
            Dim html As [String]

            If ex.StackTrace IsNot Nothing Then
                html = ex.StackTrace.Replace(" at ", vbLf & "<br /> at ").Trim()

                If html.StartsWith("<br /> at ") Then
                    html = html.Remove(0, 6)
                End If

                html = html.Replace(" in ", "<br>&nbsp;&nbsp;&nbsp;&nbsp;in ")
            Else
                html = "&nbsp;"
            End If

            Return html
        End Function

        Private Shared Sub DescribeValues(name As [String], collection As NameValueCollection, sb As StringBuilder, exclude As IEnumerable(Of [String]), mask As [String]())

            sb.AppendFormat("<tr><td class='label' valign='top' style='white-space:nowrap'>{0}</td>", name)

            If collection IsNot Nothing AndAlso collection.Count > 0 Then
                Dim excludeKeys As List(Of [String]) = If(exclude Is Nothing, New List(Of [String])(), New List(Of [String])(exclude))
                Dim maskedKeys As List(Of [String]) = If(mask Is Nothing, New List(Of [String])(), New List(Of [String])(mask))

                sb.Append("<td><table class='namevaluecollection'><tr><th>Key</th><th>Value</th></tr>")

                For Each key As [String] In collection.Keys
                    If excludeKeys.Contains(key) Then
                        Continue For
                    End If

                    Dim value As [String] = If(maskedKeys.Contains(key), (If([String].IsNullOrEmpty(collection(key)), [String].Empty, "***********")), collection(key))

                    sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", HttpContext.Current.Server.HtmlEncode(key), HttpContext.Current.Server.HtmlEncode(value))
                Next

                sb.Append("</table></td></tr>")
            Else
                sb.Append("<td>(empty)</td></tr>")
            End If
        End Sub

        Private Shared Sub DescribeValues(name As [String], collection As HttpCookieCollection, sb As StringBuilder, exclude As IEnumerable(Of [String]), mask As IEnumerable(Of [String]))
            sb.AppendFormat("<tr><td class='label' valign='top' style='white-space:nowrap'>{0}</td>", name)

            If collection IsNot Nothing AndAlso collection.Count > 0 Then
                Dim excludeKeys As List(Of [String]) = If(exclude Is Nothing, New List(Of [String])(), New List(Of [String])(exclude))
                Dim maskedKeys As List(Of [String]) = If(mask Is Nothing, New List(Of [String])(), New List(Of [String])(mask))

                sb.Append("<td><table class='namevaluecollection'><tr><th>Key</th><th>Value</th></tr>")

                For Each key As [String] In collection.Keys
                    If excludeKeys.Contains(key) Then
                        Continue For
                    End If

                    If collection(key) Is Nothing Then
                        Continue For
                    End If

                    Dim value As [String] = If(maskedKeys.Contains(key), (If([String].IsNullOrEmpty(collection(key).Value), [String].Empty, "***********")), collection(key).Value)

                    sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", HttpContext.Current.Server.HtmlEncode(key), HttpContext.Current.Server.HtmlEncode(value))
                Next

                sb.Append("</table></td></tr>")
            Else
                sb.Append("<td>(empty)</td></tr>")
            End If
        End Sub

#End Region
    End Class

End Namespace