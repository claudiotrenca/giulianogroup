﻿Imports Microsoft.VisualBasic

Imports System.Diagnostics
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Hosting
Imports System.Web.Security
Imports System.Web.SessionState
Imports System.Xml.Linq

Namespace ErrorClasses
    Public Class ApplicationErrorModule
        Implements IHttpModule

        Private Const DefaultProgrammerEmailAddress As [String] = "claudio.trenca@emmemedia.com"
        Private Const DefaultSenderEmailAddressPrefix As [String] = "claudio.trenca@emmemedia.com"

#Region "Classes"

        Public Class Settings
            Public NotInheritable Class Names
                Private Sub New()
                End Sub
                Public Shared Enabled As [String] = "ErrorClasses.ApplicationErrorModule.Enabled"

                Public Shared CrashReportEmailAddress As [String] = "ErrorClasses.ApplicationErrorModule.CrashReportEmail.Address"
                Public Shared CrashReportEmailEnabled As [String] = "ErrorClasses.ApplicationErrorModule.CrashReportEmail.Enabled"
                Public Shared CrashReportPath As [String] = "ErrorClasses.ApplicationErrorModule.CrashReportPath"
                Public Shared CrashReportUrl As [String] = "ErrorClasses.ApplicationErrorModule.CrashReportUrl"

                ''' <summary>
                ''' The key used to index the cache includes the session ID (whenever possible) to make it session-safe.
                ''' </summary>
                Public Shared ReadOnly Property CrashReportKey() As [String]
                    Get
                        Dim key As [String] = "ErrorClasses.ApplicationErrorModule.CrashReport"
                        If HttpContext.Current.Session IsNot Nothing Then
                            key += "." + HttpContext.Current.Session.SessionID
                        End If
                        Return key
                    End Get
                End Property
            End Class

            Public Shared ReadOnly Property CrashReportPath() As [String]
                Get
                    Return AppSettingsHelper.GetValue(Names.CrashReportPath, False)
                End Get
            End Property

            Public Shared ReadOnly Property CrashReportEmailAddress() As [String]
                Get
                    Return AppSettingsHelper.GetValue(Names.CrashReportEmailAddress, False, DefaultProgrammerEmailAddress)
                End Get
            End Property

            Public Shared ReadOnly Property CrashReportEmailEnabled() As [Boolean]
                Get
                    Return AppSettingsHelper.GetValueAsBoolean(Names.CrashReportEmailEnabled, False)
                End Get
            End Property

            Public Shared ReadOnly Property CrashReportEnabled() As [Boolean]
                Get
                    Return AppSettingsHelper.GetValueAsBoolean(Names.CrashReportEmailEnabled, False)
                End Get
            End Property

            Public Shared ReadOnly Property CrashReportUrl() As [String]
                Get
                    Return AppSettingsHelper.GetValue(Names.CrashReportUrl, False)
                End Get
            End Property

            Public Shared ReadOnly Property Enabled() As [Boolean]
                Get
                    Return AppSettingsHelper.GetValueAsBoolean(Names.Enabled, False)
                End Get
            End Property
        End Class

#End Region

#Region "Fields"

        Private Const ErrorPagePattern As [String] = "/Error(404|500)?\.aspx$"
        Private Const ModuleName As [String] = "ApplicationErrorModule"

#End Region

#Region "Properties"

        ''' <summary>
        ''' Returns True if the existingResponse attribute value under system.webServer/httpErrors is set to "Replace".
        ''' </summary>
        Private Shared ReadOnly Property ReplaceResponse() As [Boolean]
            Get
                Dim config As XDocument = XDocument.Load(HostingEnvironment.ApplicationPhysicalPath + "Web.config")
                If config.Root IsNot Nothing Then
                    Dim webServer As XElement = config.Root.Element("system.webServer")
                    If webServer IsNot Nothing Then
                        Dim httpErrors As XElement = webServer.Element("httpErrors")
                        If httpErrors IsNot Nothing Then
                            Dim existingResponse As [String] = httpErrors.Attribute("existingResponse").Value
                            Return existingResponse = "Replace"
                        End If
                    End If
                End If

                Return False
            End Get
        End Property

#End Region

#Region "Events"

        Public Delegate Sub UnhandledExceptionOccurredDelegate(ex As Exception)

        ''' <summary>
        ''' Allow client code to hook the event handler for an unhandled exception. A specific application might need
        ''' to perform a specific function when an error occurs.
        ''' </summary>
        Public Event UnhandledExceptionOccurred As UnhandledExceptionOccurredDelegate

#End Region

#Region "Methods (construction)"

        Public Sub Init(application As HttpApplication) Implements IHttpModule.Init
            AddHandler application.[Error], AddressOf Application_Error
            AddHandler application.BeginRequest, AddressOf Application_BeginRequest
        End Sub

        Public Sub Dispose() Implements IHttpModule.Dispose
        End Sub

#End Region

#Region "Methods (event handling)"

        Private Sub Application_BeginRequest(sender As [Object], e As EventArgs)
            HttpContext.Current.Items(ModuleName) = Me
        End Sub

        Private Sub Application_Error(sender As [Object], e As EventArgs)
            If Not Settings.Enabled Then
                Return
            End If

            Dim ex As Exception = HttpContext.Current.Server.GetLastError()

            If ex.[GetType]().Name = "AspNetSessionExpiredException" Then
                HttpContext.Current.Server.ClearError()
                HttpContext.Current.Response.Redirect(FormsAuthentication.LoginUrl)
            Else
                RaiseEvent UnhandledExceptionOccurred(ex)

                ExceptionOccurred(ex)
            End If
        End Sub

#End Region

#Region "Helpers"

        Private Shared Sub ExceptionOccurred(ex As Exception)
            ' If an unhandled exception is thrown here then this Application_Error event handler will re-catch it,
            ' wiping out the original exception, and it will not re-throw the exception -- so this line of code does
            ' not create an infinite loop (although it might appear to do so).

            ' throw new DemoException("What happens to an unhandled exception in ApplicationErrorModule.ExceptionOccurred?");

            ' If the current request is itself an error page then we need to allow the exception to pass through.

            Dim request As HttpRequest = HttpContext.Current.Request
            If Regex.IsMatch(request.Url.AbsolutePath, ErrorPagePattern) Then
                Return
            End If

            ' Otherwise, we should handle the exception here

            Dim response As HttpResponse = HttpContext.Current.Response
            Dim report As New CrashReport(ex)

            ' Save the crash report in the current cache so it is accessible to my custom error pages. The key used to
            ' index the cache includes the session ID, so it is session-safe. A session variable set here does not seem
            ' to be available in the error page's session state - although the session ID is available there.

            If HttpContext.Current.Cache IsNot Nothing Then
                HttpContext.Current.Cache(Settings.Names.CrashReportKey) = report
            End If

            ' Save the crash report on the file system

            Dim path As [String] = SaveCrashReport(report, request, Nothing)

            ' Send the crash report to the programmers

            Try
                SendEmail(report, path)
            Catch sendEmailException As Exception
                SaveCrashReport(New CrashReport(sendEmailException), request, "SendEmail")
            End Try

            ' Write the crash report to the browser if there is no replacement defined for the HTTP response

            If Not ReplaceResponse Then
                HttpContext.Current.Server.ClearError()

                If Not [String].IsNullOrEmpty(Settings.CrashReportUrl) Then
                    HttpContext.Current.Server.Transfer(Settings.CrashReportUrl)
                Else
                    Try
                        response.Clear()
                        response.StatusCode = 500
                        response.StatusDescription = "Server Error"
                        response.TrySkipIisCustomErrors = True
                        response.Write(report.Body)
                        response.[End]()
                    Catch
                    End Try
                End If
            End If


        End Sub

#End Region

#Region "Methods (saving)"

        Private Shared Function CreateFilePath(host As [String], action As [String]) As [String]
            If [String].IsNullOrEmpty(Settings.CrashReportPath) Then
                Return Nothing
            End If

            Const pattern As [String] = "{0}\{1}\{2:yyyy.MM.dd.HH.mm.ss}{3}.html"
            Dim path As [String] = [String].Format(pattern, Settings.CrashReportPath, host, DateTime.UtcNow, If([String].IsNullOrEmpty(action), [String].Empty, "." + action))
            Return path
        End Function

        Private Shared Function SaveCrashReport(report As CrashReport, request As HttpRequest, action As [String]) As [String]
            Dim path__1 As [String] = CreateFilePath(request.Url.Host, action)

            If [String].IsNullOrEmpty(path__1) Then
                Return Nothing
            End If

            Dim directory__2 As [String] = System.IO.Path.GetDirectoryName(path__1)
            If directory__2 IsNot Nothing AndAlso Not Directory.Exists(directory__2) Then
                Directory.CreateDirectory(directory__2)
            End If

            File.WriteAllText(path__1, report.Body)

            Return path__1
        End Function

#End Region

#Region "Methods (email notification)"

        ''' <summary>
        ''' Parses the domain from the requested URL, excluding the sub-domain, and uses this to create an email
        ''' address that follows a standard convention. For example: crash.reports@insitesystems.com
        ''' </summary>
        Private Shared Function CreateSenderEmailAddress() As [String]
            ' Remove the sub-domain from the host name.
            Dim host As [String] = HttpContext.Current.Request.Url.Host
            Dim index As Int32 = host.LastIndexOf("."c, host.LastIndexOf("."c) - 1)
            Dim domain As [String] = If(index < 0, host, host.Substring(index + 1))

            Return DefaultSenderEmailAddressPrefix + domain
        End Function

        Public Shared Sub SendEmail(report As CrashReport, attachment As [String])
            If Not Settings.CrashReportEmailEnabled Then
                Return
            End If

            If [String].IsNullOrEmpty(Settings.CrashReportEmailAddress) Then
                Return
            End If

            Dim email = New MailMessage() With {
                 .Sender = New MailAddress(CreateSenderEmailAddress())
            }
            email.[To].Add(Settings.CrashReportEmailAddress)
            email.Subject = report.Title
            email.Body = report.Body
            email.IsBodyHtml = True

            If attachment IsNot Nothing Then
                email.Attachments.Add(New Attachment(attachment))
            End If

            Dim smtp = New SmtpClient()
            smtp.Send(email)
        End Sub

#End Region

        'Public Sub Init1(context As HttpApplication) Implements IHttpModule.Init

        'End Sub
    End Class
End Namespace