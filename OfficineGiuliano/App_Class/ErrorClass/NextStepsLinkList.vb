﻿Imports System.Text
Imports System.Web.Security
Imports System.Web.UI
Imports Microsoft.VisualBasic

Namespace ErrorClasses
    Public Class NextStepsLinkList
        Inherits System.Web.UI.Control

        Private m_EnableTechnicalInformation As [Boolean]

        Public Property EnableTechnicalInformation() As [Boolean]
            Get
                Return m_EnableTechnicalInformation
            End Get
            Set(value As [Boolean])
                m_EnableTechnicalInformation = value
            End Set
        End Property

        Protected Overrides Sub Render(writer As HtmlTextWriter)
            MyBase.Render(writer)

            Dim sb As New StringBuilder()

            sb.Append("<ul>")

            Dim url As [String] = AppSettingsHelper.GetValue("Application.HelpUrl", False)
            If Not [String].IsNullOrEmpty(url) Then
                AppendLink(sb, "Invia una richiesta di aiuto", url)
            End If

            AppendLink(sb, "Vai alla Home Page", "/")

            url = AppSettingsHelper.GetValue("Application.SitemapUrl", False)
            If Not [String].IsNullOrEmpty(url) Then
                AppendLink(sb, "Vai alla pagina di Indice", url)
            End If

            AppendLink(sb, "Vai alla pagina di Login", FormsAuthentication.LoginUrl)

            If EnableTechnicalInformation Then
                sb.Append("<li><a href=""#"" onclick=""ToggleTechnicalInformationDisplay();return false;"">Guarda le informazioni tecniche</a></li>")
            End If

            sb.Append("</ul>")

            writer.Write(sb.ToString())
        End Sub

        Private Sub AppendLink(sb As StringBuilder, text As [String], url As [String])
            If [String].IsNullOrEmpty(text) Then
                Return
            End If

            sb.AppendFormat("<li><a href=""{0}"">{1}</a></li>", url, text)
        End Sub
    End Class
End Namespace