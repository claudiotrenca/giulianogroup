﻿Imports Microsoft.VisualBasic

Imports System.Configuration

Namespace ErrorClasses
    Public NotInheritable Class AppSettingsHelper
        Private Sub New()
        End Sub
#Region "Get String value"

        Public Shared Function GetValue(key As [String]) As [String]
            Return GetValue(key, True, Nothing)
        End Function

        Public Shared Function GetValue(key As [String], required As [Boolean]) As [String]
            Return GetValue(key, required, Nothing)
        End Function

        Public Shared Function GetValue(key As [String], required As [Boolean], defaultValue As [String]) As [String]
            Dim value As [String] = ConfigurationManager.AppSettings(key)

            If [String].IsNullOrEmpty(value) Then
                If required AndAlso defaultValue Is Nothing Then
                    Throw New CommerceException("Application setting not found: " + key)
                End If

                Return defaultValue
            End If

            Return value
        End Function

#End Region

#Region "Get Decimal value"

        Public Shared Function GetValueAsDecimal(key As [String]) As [Decimal]
            Return GetValueAsDecimal(key, True, Nothing).Value
        End Function

        Public Shared Function GetValueAsDecimal(key As [String], defaultValue As [Decimal]) As [Decimal]
            Return GetValueAsDecimal(key, True, defaultValue).Value
        End Function

        Public Shared Function GetValueAsNullableDecimal(key As [String]) As System.Nullable(Of [Decimal])
            Return GetValueAsDecimal(key, False, Nothing)
        End Function

        Public Shared Function GetValueAsDecimal(key As [String], required As [Boolean], defaultValue As System.Nullable(Of [Decimal])) As System.Nullable(Of [Decimal])
            Dim stringValue As [String] = GetValue(key, required AndAlso defaultValue Is Nothing, Nothing)

            If [String].IsNullOrEmpty(stringValue) Then
                Return defaultValue
            End If

            Dim value As [Decimal]

            If [Decimal].TryParse(stringValue, value) Then
                Return value
            End If

            Throw New CommerceException("Application setting is not valid Decimal value: " + key)
        End Function

#End Region

#Region "Get Int32 value"

        Public Shared Function GetValueAsInt32(key As [String]) As Int32
            Return GetValueAsInt32(key, True, Nothing).Value
        End Function

        Public Shared Function GetValueAsInt32(key As [String], defaultValue As Int32) As Int32
            Return GetValueAsInt32(key, True, defaultValue).Value
        End Function

        Public Shared Function GetValueAsNullableInt32(key As [String]) As System.Nullable(Of Int32)
            Return GetValueAsInt32(key, False, Nothing)
        End Function

        Public Shared Function GetValueAsInt32(key As [String], required As [Boolean], defaultValue As System.Nullable(Of Int32)) As System.Nullable(Of Int32)
            Dim stringValue As [String] = GetValue(key, required AndAlso defaultValue Is Nothing, Nothing)

            If [String].IsNullOrEmpty(stringValue) Then
                Return defaultValue
            End If

            Dim value As Int32

            If Int32.TryParse(stringValue, value) Then
                Return value
            End If

            Throw New CommerceException("Application setting is not valid Int32 value: " + key)
        End Function

#End Region

#Region "Get Boolean value"

        Public Shared Function GetValueAsBoolean(key As [String]) As [Boolean]
            Return GetValueAsBoolean(key, True, Nothing).Value
        End Function

        Public Shared Function GetValueAsBoolean(key As [String], defaultValue As [Boolean]) As [Boolean]
            Return GetValueAsBoolean(key, False, defaultValue).Value
        End Function

        Public Shared Function GetValueAsNullableBoolean(key As [String]) As System.Nullable(Of [Boolean])
            Return GetValueAsBoolean(key, False, Nothing)
        End Function

        Public Shared Function GetValueAsBoolean(key As [String], required As [Boolean], defaultValue As System.Nullable(Of [Boolean])) As System.Nullable(Of [Boolean])
            Dim stringValue As [String] = GetValue(key, required AndAlso defaultValue Is Nothing, Nothing)

            If [String].IsNullOrEmpty(stringValue) Then
                Return defaultValue
            End If

            Dim value As [Boolean]

            If [Boolean].TryParse(stringValue, value) Then
                Return value
            End If

            Throw New CommerceException("Application setting is not valid Boolean value: " + key)
        End Function

#End Region

#Region "Get DateTime value"

        Public Shared Function GetValueAsDateTime(key As [String]) As DateTime
            Return GetValueAsDateTime(key, True, Nothing).Value
        End Function

        Public Shared Function GetValueAsDateTime(key As [String], defaultValue As DateTime) As DateTime
            Return GetValueAsDateTime(key, True, defaultValue).Value
        End Function

        Public Shared Function GetValueAsNullableDateTime(key As [String]) As System.Nullable(Of DateTime)
            Return GetValueAsDateTime(key, False, Nothing)
        End Function

        Public Shared Function GetValueAsDateTime(key As [String], required As [Boolean], defaultValue As System.Nullable(Of DateTime)) As System.Nullable(Of DateTime)
            Dim stringValue As [String] = GetValue(key, required AndAlso defaultValue Is Nothing, Nothing)

            If [String].IsNullOrEmpty(stringValue) Then
                Return defaultValue
            End If

            Dim value As DateTime

            If DateTime.TryParse(stringValue, value) Then
                Return value
            End If

            Throw New CommerceException("Application setting is not valid DateTime value: " + key)
        End Function

#End Region
    End Class
End Namespace