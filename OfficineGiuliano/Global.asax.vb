﻿Imports System.Web.Optimization

Public Class Global_asax
    Inherits HttpApplication

    Sub Application_Start(sender As Object, e As EventArgs)

        BundleTable.EnableOptimizations = ConfigurationManager.AppSettings("EnableOptimizations")

        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)

        RegisterCustomRoutes(RouteTable.Routes)

    End Sub

    Private Sub RegisterCustomRoutes(routes As RouteCollection)

        ' REGOLE DI ESCLUSIONE

        routes.Ignore("{resource}.axd/{*pathInfo}")
        routes.Ignore("{resource}.ashx/{*pathInfo}")
        routes.Ignore("{resource}.css/{*pathInfo}")
        routes.Ignore("{resource}.js/{*pathInfo}")
        routes.Ignore("{resource}.woff/{*pathInfo}")
        routes.Ignore("{resource}.svg/{*pathInfo}")
        routes.Ignore("{resource}.eot/{*pathInfo}")
        routes.Ignore("{resource}.ttf/{*pathInfo}")
        routes.Ignore("{resource}.png/{*pathInfo}")
        routes.Ignore("{resource}.jpg/{*pathInfo}")
        routes.Ignore("{resource}.gif/{*pathInfo}")

        routes.MapPageRoute("carrello", "carrello.htm", "~/pages/common/carrello.aspx", False, New RouteValueDictionary(New With {.sezione = "common", .pagelink = "carrello"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))
        routes.MapPageRoute("accedi", "accedi.htm", "~/account/login.aspx", False, New RouteValueDictionary(New With {.sezione = "common", .pagelink = "sterilfarma-club"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))

        routes.MapPageRoute("store", "punti-vendita.htm", "~/pages/Store/StoreLocator.aspx", False, New RouteValueDictionary(New With {.sezione = "common", .pagelink = "punti-vendita"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))

        routes.MapPageRoute("Contatti", ConfigurationManager.AppSettings("PaginaContatti"), "~/Pages/Common/Contatti.aspx", False, New RouteValueDictionary(New With {.sezione = "common", .pagelink = "contatti"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))

        ' ROUTE OFFERTE
        routes.MapPageRoute("offerte4", "offerte-viaggi/{macro}/{categoria}/{titolo}_{codice}.htm", "~/pages/offerte/OfferteScheda.aspx", False, New RouteValueDictionary(New With {.sezione = "offerte", .pagelink = "offerte-viaggi"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))
        routes.MapPageRoute("offerte3", "offerte-viaggi/{macro}/{categoria}/", "~/pages/offerte/OfferteElenco.aspx", False, New RouteValueDictionary(New With {.sezione = "offerte", .pagelink = "offerte-viaggi"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))
        routes.MapPageRoute("offerte2", "offerte-viaggi/{macro}/", "~/pages/offerte/OfferteElenco.aspx", False, New RouteValueDictionary(New With {.sezione = "offerte", .pagelink = "offerte-viaggi"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))
        routes.MapPageRoute("offerte1", "offerte-viaggi.htm", "~/pages/offerte/OfferteElenco.aspx", False, New RouteValueDictionary(New With {.sezione = "offerte", .pagelink = "offerte-viaggi"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))

        ' BLOG
        routes.MapPageRoute("blog1", "blog.htm", "~/Pages/Blog/BlogElenco.aspx", False, New RouteValueDictionary(New With {.pagina = "1", .tipo = "blog", .pagelink = "blog"}), New RouteValueDictionary(New With {.pagina = "\d+"}))
        routes.MapPageRoute("BlogTag1", "blog/tag/{Tag}", "~/Pages/Blog/BlogElenco.aspx", False, New RouteValueDictionary(New With {.pagina = "1", .tipo = "blog", .view = "tag", .pagelink = "blog"}), New RouteValueDictionary(New With {.pagina = "\d+"}))
        routes.MapPageRoute("BlogTag2", "blog/tag/{Tag}/{Pagina}.htm", "~/Pages/Blog/BlogElenco.aspx", False, New RouteValueDictionary(New With {.tipo = "blog", .view = "tag", .pagelink = "blog"}), New RouteValueDictionary(New With {.pagina = "\d+"}))
        routes.MapPageRoute("BlogArchivio1", "blog/archivio/{Data}", "~/Pages/Blog/BlogElenco.aspx", False, New RouteValueDictionary(New With {.pagina = "1", .tipo = "blog", .view = "archivio", .pagelink = "blog"}), New RouteValueDictionary(New With {.pagina = "\d+"}))
        routes.MapPageRoute("BlogArchivio2", "blog/archivio/{Data}/{Pagina}.htm", "~/Pages/Blog/BlogElenco.aspx", False, New RouteValueDictionary(New With {.tipo = "blog", .view = "archivio", .pagelink = "blog"}), New RouteValueDictionary(New With {.pagina = "\d+"}))
        routes.MapPageRoute("blog2", "blog/{Pagina}.htm", "~/Pages/Blog/BlogElenco.aspx", False, New RouteValueDictionary(New With {.tipo = "blog", .pagelink = "blog"}), New RouteValueDictionary(New With {.pagina = "\d+"}))
        routes.MapPageRoute("Blog3", "blog/{Categoria}/", "~/Pages/Blog/BlogElenco.aspx", False, New RouteValueDictionary(New With {.pagina = "1", .tipo = "blog", .pagelink = "blog"}), New RouteValueDictionary(New With {.pagina = "\d+"}))
        routes.MapPageRoute("Blog4", "blog/{Categoria}/{Pagina}.htm", "~/Pages/Blog/BlogElenco.aspx", False, New RouteValueDictionary(New With {.tipo = "blog", .pagelink = "blog"}), New RouteValueDictionary(New With {.pagina = "\d+"}))
        routes.MapPageRoute("Blog5", "blog/{Categoria}/{Titolo}_{IDArticolo}.htm", "~/Pages/Blog/BlogArticolo.aspx", False, New RouteValueDictionary(New With {.tipo = "blog", .pagelink = "blog"}))

        ' ROUTE AGENZIE

        routes.MapPageRoute("Agenzie5", "agenzie-di-viaggi/{Regione}/{Provincia}/{citta}/{Agenzia}_id{ID}.htm", "~/Pages/Agenzie/AgenzieDettagli.aspx", False, New RouteValueDictionary(New With {.sezione = "agenzie", .pagelink = "agenzie-di-viaggi"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))
        routes.MapPageRoute("Agenzie4", "agenzie-di-viaggi/{Regione}/{Provincia}/{citta}/", "~/Pages/Agenzie/AgenzieElenco.aspx", False, New RouteValueDictionary(New With {.sezione = "agenzie", .pagelink = "agenzie-di-viaggi"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))
        routes.MapPageRoute("Agenzie3", "agenzie-di-viaggi/{Regione}/{Provincia}/", "~/Pages/Agenzie/AgenzieElenco.aspx", False, New RouteValueDictionary(New With {.sezione = "agenzie", .pagelink = "agenzie-di-viaggi"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))
        routes.MapPageRoute("Agenzie2", "agenzie-di-viaggi/{Regione}/", "~/Pages/Agenzie/AgenzieElenco.aspx", False, New RouteValueDictionary(New With {.sezione = "agenzie", .pagelink = "agenzie-di-viaggi"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))
        routes.MapPageRoute("Agenzie1", "agenzie-di-viaggi.htm", "~/Pages/Agenzie/AgenzieElenco.aspx", False, New RouteValueDictionary(New With {.sezione = "agenzie", .pagelink = "agenzie-di-viaggi"}), New RouteValueDictionary(New With {.sezione = "^[A-Za-z]+$"}))

        ' ROUTE PER DETTAGLIO PRODOTTI

        routes.MapPageRoute("GetProductDetail1", "{Macro}/{Categoria}/{SubCategoria}/{Prodotto}_{PK}.htm", "~/pages/prodotti/ProdottiDettagli.aspx", False, New RouteValueDictionary(New With {.tipo = "SchedaProdotto", .pk = "\d+"}))
        routes.MapPageRoute("GetProductDetail2", "{Macro}/{Categoria}/{Prodotto}_{PK}.htm", "~/pages/prodotti/ProdottiDettagli.aspx", False, New RouteValueDictionary(New With {.tipo = "SchedaProdotto", .pk = "\d+"}))

        ' ROUTE PER LISTA PRODOTTI RICERCA
        routes.MapPageRoute("GetAllSearchProductPage", "ricerca/{SearchKey}/", "~/Pages/Prodotti/ProdottiLista.aspx")
        routes.MapPageRoute("GetAllSearchProductPagePage", "ricerca/{SearchKey}/{Pagina}.htm", "~/Pages/Prodotti/ProdottiLista.aspx")

        ' ROUTE CONTENT
        routes.MapPageRoute("GetContentByLink3Culture", "{culture}/{PageLink}.htm", "~/Pages/Common/ContentPage.aspx", False, New RouteValueDictionary(New With {.culture = "ita", .tipo = "contenuti"}), New RouteValueDictionary(New With {.culture = "[a-z]{3}"}))

        routes.MapPageRoute("GetContentByLink1", "{Macro}/{Categoria}/{PageLink}.htm", "~/Pages/Common/ContentPage.aspx", False, New RouteValueDictionary(New With {.culture = "ita", .tipo = "contenuti"}), New RouteValueDictionary(New With {.culture = "[a-z]{3}"}))
        routes.MapPageRoute("GetContentByLink2", "{Macro}/{PageLink}.htm", "~/Pages/Common/ContentPage.aspx", False, New RouteValueDictionary(New With {.culture = "ita", .tipo = "contenuti"}), New RouteValueDictionary(New With {.culture = "[a-z]{3}"}))
        routes.MapPageRoute("GetContentByLink3", "{PageLink}.htm", "~/Pages/Common/ContentPage.aspx", False, New RouteValueDictionary(New With {.culture = "ita", .tipo = "contenuti"}), New RouteValueDictionary(New With {.culture = "[a-z]{3}"}))

        ' ROUTE PER ELENCO PRODOTTI

        routes.MapPageRoute("GetProductByMacroPage", "{Macro}/{Pagina}/", "~/Pages/Prodotti/ProdottiLista.aspx", False, New RouteValueDictionary(New With {.tipo = "prodotti", .provincia = "napoli"}), New RouteValueDictionary(New With {.pagina = "\d+"}))
        routes.MapPageRoute("GetProductByMacro", "{Macro}/", "~/Pages/Prodotti/ProdottiLista.aspx", False, New RouteValueDictionary(New With {.pagina = "1", .tipo = "prodotti", .provincia = "napoli"}), New RouteValueDictionary(New With {.pagina = "\d+"}))

        routes.MapPageRoute("GetProductByCategoryPage", "{Macro}/{Categoria}/{Pagina}/", "~/Pages/Prodotti/ProdottiLista.aspx", False, New RouteValueDictionary(New With {.tipo = "prodotti", .provincia = "napoli"}), New RouteValueDictionary(New With {.pagina = "\d+"}))
        routes.MapPageRoute("GetProductByCategory", "{Macro}/{Categoria}/", "~/Pages/Prodotti/ProdottiLista.aspx", False, New RouteValueDictionary(New With {.pagina = "1", .tipo = "prodotti", .provincia = "napoli"}), New RouteValueDictionary(New With {.pagina = "\d+"}))

        routes.MapPageRoute("GetProductByFamilyPage", "{Macro}/{Categoria}/{SubCategoria}/{Pagina}/", "~/Pages/Prodotti/ProdottiLista.aspx", False, New RouteValueDictionary(New With {.tipo = "prodotti", .provincia = "napoli"}), New RouteValueDictionary(New With {.pagina = "\d+"}))
        routes.MapPageRoute("GetProductByFamily", "{Macro}/{Categoria}/{SubCategoria}/", "~/Pages/Prodotti/ProdottiLista.aspx", False, New RouteValueDictionary(New With {.pagina = "1", .tipo = "prodotti", .provincia = "napoli"}), New RouteValueDictionary(New With {.pagina = "\d+"}))

        routes.MapPageRoute("registra", "nuovo-utente/", "~/account/nuovoUtente.aspx")
        routes.MapPageRoute("gestione-account", "gestione-account/", "~/account/account_home.aspx")

        routes.MapPageRoute("Acquista", "acquista/", "~/secure/checkout.aspx")
        routes.MapPageRoute("ConfermaOrdine", "conferma-ordine/", "~/secure/confirmed.aspx")
        routes.MapPageRoute("Paypal", "paga-paypal/", "~/secure/paypal.aspx")

        routes.MapPageRoute("GetContentByLinkCulture", "{Culture}/{PageLink}.htm", "~/Pages/Common/ContentPage.aspx")

    End Sub

End Class