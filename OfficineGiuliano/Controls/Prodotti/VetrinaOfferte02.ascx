﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VetrinaOfferte02.ascx.vb" Inherits="Emmemedia.Controls.Prodotti.VetrinaOfferte02" %>

<!-- Prodotti in evidenza -->
<div class="wrap">

    <div class="prodotti-home">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 title text-center">
                    <h3 class="h3">Prodotti di Punta a Noleggio
                    </h3>
                </div>
            </div>
            <asp:ListView ID="lvProdotti" runat="server" DataKeyNames="PkProdotto">
                <LayoutTemplate>
                    <div class="row">
                        <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>

                    <div class="border-box">

                        <asp:Image ID="ProdottoImmagine" runat="server" CssClass="img-responsive" />

                        <h3 class="h3">
                            <asp:Literal ID="ProdottoModello" runat="server"></asp:Literal>
                        </h3>
                        <h5 class="h5">
                            <asp:Literal ID="ProdottoCategoria" runat="server"></asp:Literal>
                        </h5>
                        <asp:HyperLink ID="lnkProdotto" runat="server" CssClass="btn btn-block">
                            VAI AL PRODOTTO  <i class="ion ion-chevron-right"></i></asp:HyperLink>
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
</div>