﻿Namespace Controls
    Namespace Prodotti

        Public Class VetrinaOfferte1
            Inherits BaseClass.SiteUsercontrol

            Protected _Prodotti As List(Of DataClass.Products)

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                _Provincia = IIf(Page.RouteData.Values("provincia") <> Nothing, Page.RouteData.Values("provincia"), ConfigurationManager.AppSettings("default_prov"))

                bindProduct()
            End Sub

            Private Sub bindProduct()

                _Prodotti = DataClass.vwProdotti.GetVetrinaList(8)

                lvProdotti.DataSource = _Prodotti
                lvProdotti.DataBind()

            End Sub

            Protected Sub lvProdotti_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvProdotti.ItemDataBound

                Dim dti As DataClass.Products = TryCast(e.Item.DataItem, DataClass.Products)

                If e.Item.ItemType = ListViewItemType.DataItem Then

                    Dim ltModello As Literal = DirectCast(e.Item.FindControl("ProdottoModello"), WebControls.Literal)
                    Dim ltCategoria As Literal = DirectCast(e.Item.FindControl("ProdottoCategoria"), WebControls.Literal)
                    Dim ltPrezzo As Literal = DirectCast(e.Item.FindControl("ProdottoPrezzo"), Literal)
                    Dim ProdottoImmagine As WebControls.Image = DirectCast(e.Item.FindControl("ProdottoImmagine"), WebControls.Image)
                    Dim lnkProdotto As HyperLink = DirectCast(e.Item.FindControl("lnkProdotto"), HyperLink)

                    If ltModello IsNot Nothing Then ltModello.Text = dti.Modello
                    If ltCategoria IsNot Nothing Then ltCategoria.Text = Classi.Utility.Capitalize(dti.Categoria)
                    If ltPrezzo IsNot Nothing Then
                        If Session("IDUtente") Is Nothing Then
                            ltPrezzo.Text = FormatCurrency(dti.Prezzo, 2)
                        Else
                            ltPrezzo.Text = FormatCurrency(dti.PrezzoRivenditore1, 2)
                        End If
                    End If
                    If lnkProdotto IsNot Nothing Then
                        lnkProdotto.NavigateUrl = "/" & dti.MacroLink & "/" & dti.CategoriaLink & "/" & _Provincia & "/" & Classi.Utility.StringToUrl(dti.Modello) & "_" & dti.PkProdotto & ".htm"
                    End If

                    If ProdottoImmagine IsNot Nothing Then
                        If dti.ImmagineGrande <> "" Then
                            ProdottoImmagine.ImageUrl = "/public/images/product/" & dti.ImmagineGrande
                        Else
                            ProdottoImmagine.ImageUrl = "/public/images/product/no_dispo.jpg"
                        End If
                        ProdottoImmagine.AlternateText = dti.Modello
                    End If

                End If

            End Sub

        End Class
    End Namespace
End Namespace