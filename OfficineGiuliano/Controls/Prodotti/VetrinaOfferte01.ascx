﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VetrinaOfferte01.ascx.vb" Inherits="Emmemedia.Controls.Prodotti.VetrinaOfferte1" %>
<!-- features products -->
<div class="wrap">

    <div class="prodotti-home">
        <div class="container-fluid">
            <asp:ListView ID="lvProdotti" runat="server" GroupItemCount="4" DataKeyNames="PkProdotto">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="GroupPlaceHolder" runat="server"></asp:PlaceHolder>
                </LayoutTemplate>
                <GroupTemplate>
                    <div class="row">
                        <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                    </div>
                </GroupTemplate>
                <ItemTemplate>
                    <div class="col-md-3 col-sm-6 col-xs-12 box-product">
                        <div class="image">
                            <asp:HyperLink ID="lnkProdotto" runat="server">
                                <span class="box-hover">
                                    <span class="box-hover-content">
                                        <span class="btn-primary">SCOPRI DI PI&Ugrave;</span>
                                    </span>
                                </span>
                                <div class="col-height">
                                    <asp:Image ID="ProdottoImmagine" runat="server" />
                                </div>
                            </asp:HyperLink>
                        </div>
                        <div class="details">
                            <div class="left">
                                <p class="category">
                                    <asp:Literal ID="ProdottoCategoria" runat="server"></asp:Literal>
                                </p>
                                <p class="h5">
                                    <asp:Literal ID="ProdottoModello" runat="server"></asp:Literal>
                                </p>
                            </div>
                            <div id="ProdottoDivPrezzo" runat="server" class="right">
                                <p class="category">A PARTIRE DA</p>
                                <p class="price">
                                    <asp:Literal ID="ProdottoPrezzo" runat="server"></asp:Literal>
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
</div>