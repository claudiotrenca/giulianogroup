﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MenuCategorie.ascx.vb" Inherits="Emmemedia.Controls.Prodotti.MenuCategorie" %>
<h4 class="h4">Categorie Prodotti
</h4>
<hr>

<asp:ListView ID="lvQuickOuter" runat="server">
    <LayoutTemplate>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <asp:HiddenField ID="QuickID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "pk")%>' />
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id='<%# "heading" & DataBinder.Eval(Container.DataItem, "pk")%>'>
                <h4 class="panel-title">
                    <a data-toggle="collapse" class="plusIcon" data-parent='#accordion' href='#<%# "collapse" & DataBinder.Eval(Container.DataItem, "pk")%>' aria-expanded="true" aria-controls='<%# "collapse" & DataBinder.Eval(Container.DataItem, "pk")%>'>
                        <%# DataBinder.Eval(Container.DataItem, "descrizione")%>
                    </a>
                </h4>
            </div>
            <div id='<%# "collapse" & DataBinder.Eval(Container.DataItem, "pk")%>' class="panel-collapse collapse" role="tabpanel" aria-labelledby='<%# "heading" & DataBinder.Eval(Container.DataItem, "pk")%>'>
                <div class="panel-body">
                    <asp:ListView ID="lvQuickInner" runat="server">
                        <LayoutTemplate>

                            <ul class="list-unstyled">
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                            </ul>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li>
                                <asp:HyperLink ID="lnkInner" runat="server"></asp:HyperLink>
                            </li>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </div>
        </div>
    </ItemTemplate>
</asp:ListView>
<script type="text/javascript">
    $(document).ready(function () {
        $(".collapse").collapse('hide');
        $("#collapse<%= _CodiceMacro %>").collapse('show');
    });
</script>