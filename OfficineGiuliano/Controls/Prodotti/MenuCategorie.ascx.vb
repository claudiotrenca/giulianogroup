﻿Namespace Controls
    Namespace Prodotti
        Public Class MenuCategorie
            Inherits BaseClass.SiteUsercontrol

            Protected _Categoria As String
            Protected _Macro As String
            Protected _SubCategoria As String

            Protected _CodiceMacro As Integer = 0
            Protected _CodiceCategoria As Integer = 0
            Protected _CodiceSubCategoria As Integer = 0

            Private _IdGruppoMenu As Integer

            Public Property IdGruppoMenu As Integer
                Get
                    Return _IdGruppoMenu
                End Get
                Set(value As Integer)
                    _IdGruppoMenu = value
                End Set
            End Property

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                _Macro = IIf(Page.RouteData.Values("macro") <> Nothing, Page.RouteData.Values("macro"), String.Empty)
                _Categoria = IIf(Page.RouteData.Values("categoria") <> Nothing, Page.RouteData.Values("categoria"), String.Empty)
                _Provincia = IIf(Page.RouteData.Values("provincia") <> Nothing, Page.RouteData.Values("provincia"), ConfigurationManager.AppSettings("default_prov"))
                _SubCategoria = IIf(Page.RouteData.Values("SubCategoria") <> Nothing, Page.RouteData.Values("SubCategoria"), String.Empty)

                _CodiceMacro = DataClass.Menu.GetIdFromLink(_Categoria)

                If _SubCategoria <> String.Empty Then
                    _CodiceCategoria = DataClass.Menu.GetIdFromLink(_SubCategoria, _CodiceMacro)
                End If

                BindNavMacro()

            End Sub

            Private Sub BindNavMacro()
                Dim result As List(Of DataClass.Menu)
                result = DataClass.Menu.GetList(_IdGruppoMenu)
                lvQuickOuter.DataSource = result
                lvQuickOuter.DataBind()
            End Sub

            Private Sub BindNavCat(ByVal index As Integer, ByRef lv As ListView)
                Dim result As List(Of DataClass.vwMenu)
                result = DataClass.Menu.GetList(_IdGruppoMenu, index)
                lv.DataSource = result
                lv.DataBind()
            End Sub

            Protected Sub lvQuickOuter_ItemCreated(sender As Object, e As ListViewItemEventArgs) Handles lvQuickOuter.ItemCreated
                Dim lvQuickInner As ListView = DirectCast(e.Item.FindControl("lvQuickInner"), ListView)
                AddHandler lvQuickInner.ItemDataBound, AddressOf lvQuickInner_ItemDataBound
            End Sub

            Private Sub lvQuickOuter_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvQuickOuter.ItemDataBound
                Dim hid As HiddenField = DirectCast(e.Item.FindControl("QuickID"), HiddenField)
                Dim lvQuickInner As ListView = DirectCast(e.Item.FindControl("lvQuickInner"), ListView)
                BindNavCat(hid.Value, lvQuickInner)
            End Sub

            Private Sub lvQuickInner_ItemDataBound(sender As Object, e As ListViewItemEventArgs)
                If e.Item.ItemType = ListViewItemType.DataItem Then
                    Dim lnk As HyperLink = DirectCast(e.Item.FindControl("lnkInner"), HyperLink)
                    Dim dti As DataClass.vwMenu = TryCast(e.Item.DataItem, DataClass.vwMenu)
                    lnk.NavigateUrl = "/" & Page.RouteData.Values("macro") & "/" & dti.MacroLInk & "/" & dti.Link & "/"
                    lnk.Text = dti.Descrizione
                    If Request.RawUrl.ToString.Contains(Server.UrlDecode(lnk.NavigateUrl)) Then
                        lnk.Attributes.Add("class", "prodActive")
                    End If

                End If
            End Sub
        End Class
    End Namespace
End Namespace