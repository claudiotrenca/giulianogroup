﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Controls.Offerte
    
    Partial Public Class OfferteSlider
        
        '''<summary>
        '''Controllo rpArticoliSlide.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents rpArticoliSlide As Global.System.Web.UI.WebControls.ListView
        
        '''<summary>
        '''Controllo OfferteRicercaBoxVerticale.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents OfferteRicercaBoxVerticale As Global.Emmemedia.Controls.Offerte.OfferteRicercaBoxVerticale
    End Class
End Namespace
