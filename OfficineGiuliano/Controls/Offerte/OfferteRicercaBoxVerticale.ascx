﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OfferteRicercaBoxVerticale.ascx.vb" Inherits="Emmemedia.Controls.Offerte.OfferteRicercaBoxVerticale" %>
<div class="search">
    <p class="h2">Cerca la tua vacanza.</p>
    <div class="form">
        <div class="form-group">
            <label>Dove vuoi andare?</label>
            <p>
                <asp:TextBox ID="FormDestinazione" runat="server" CssClass="form-control" placeholder="Destinazione"></asp:TextBox>
                <i class="ion-map"></i>
            </p>
        </div>
        <div class="form-group">
            <label>Quando vuoi partire?</label>
            <p>
                <asp:TextBox ID="FormData" runat="server" CssClass="datepicker form-control" placeholder="Scegli la data" data-provide="datepicker"></asp:TextBox>
                <i class="ion-android-calendar"></i>
            </p>
        </div>
        <div class="form-group">
            <label>Qual è il tuo budget?</label>
            <asp:TextBox ID="FormBudget" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <asp:LinkButton ID="FormInvia" runat="server" CssClass="btn btn-default btn-primary full-width">RICERCA</asp:LinkButton>
    </div>
</div>