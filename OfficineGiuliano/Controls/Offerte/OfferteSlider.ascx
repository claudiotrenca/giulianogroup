<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OfferteSlider.ascx.vb" Inherits="Emmemedia.Controls.Offerte.OfferteSlider" %>
<%@ Register Src="~/Controls/Offerte/OfferteRicercaBoxVerticale.ascx" TagPrefix="uc1" TagName="OfferteRicercaBoxVerticale" %>

<!-- slider -->
<div class="slider-wrap">
    <div class="slider">
        <div class="owl-carousel owl-theme hidden-xs hidden-sm">
            <asp:ListView runat="server" ID="rpArticoliSlide">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="item">

                        <img src='<%#DataBinder.Eval(Container.DataItem, "Imgpath")%>' alt='<%#DataBinder.Eval(Container.DataItem, "TITOLO")%>' />
                    </div>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <div class="item">
                        <img src="/Images/header/slide1.jpg" alt="">
                    </div>
                    <div class="item">
                         <img src="/Images/header/slide2.jpg" alt="">
                    </div>
                    <div class="item">
                         <img src="/Images/header/slide3.jpg" alt="">
                    </div>
                </EmptyDataTemplate>
            </asp:ListView>
        </div>
        <uc1:OfferteRicercaBoxVerticale runat="server" ID="OfferteRicercaBoxVerticale" />
    </div>
</div>