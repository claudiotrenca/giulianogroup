﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OfferteElencoOrizzontale.ascx.vb" Inherits="Emmemedia.Controls.Offerte.OfferteElencoOrizzontale" %>
<%@ Import Namespace="Emmemedia" %>

<asp:ListView ID="lvmacro" runat="server" GroupItemCount="6">
    <LayoutTemplate>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h3>Le Irresistibili</h3>
            </div>
        </div>
        <asp:PlaceHolder ID="GroupPlaceHolder" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>
    <GroupTemplate>
        <div class="row">
            <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
        </div>
    </GroupTemplate>
    <ItemTemplate>
        <div class="col-md-2">
            <a href='<%# "/offerte-viaggi/" & Classi.Utility.StringToUrl(DataBinder.Eval(Container.DataItem, "LINK_CATEGORIA")) & "/"%>' title='<%#DataBinder.Eval(Container.DataItem, "TITOLO_CATEGORIA")%>'>
                <img src='<%# "http://www.vantaggiirresistibili.it/frontend/imgCategorie/" & DataBinder.Eval(Container.DataItem, "IMMAGINE_CATEGORIA")  %>' class="img-destinazione" alt='<%#DataBinder.Eval(Container.DataItem, "TITOLO_CATEGORIA")%>' />
            </a>
            <h4><%#DataBinder.Eval(Container.DataItem, "TITOLO_CATEGORIA")%></h4>
        </div>
    </ItemTemplate>
</asp:ListView>