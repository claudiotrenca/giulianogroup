﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OfferteViaggiVetrina.ascx.vb" Inherits="Emmemedia.Controls.Offerte.OfferteViaggiVetrina" %>
<style type="text/css">
    .image2 {
        height: 200px;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
</style>
<div class="row">
    <div class="col-sm-6 col-xs-12 hidden-xs">
        <p class="h2">Le <b>irresistibili</b> di questo mese</p>
    </div>
    <div class="col-sm-6 col-xs-12 text-right">
        <p class="h3"><a href="/offerte-viaggi.htm">SCOPRI TUTTE LE OFFERTE</a></p>
    </div>
</div>
<br />
<br />
<!-- features results -->
<div class="row">
    <asp:ListView ID="lvRisultati" runat="server">
        <LayoutTemplate>
            <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="col-md-4 col-sm-6">
                <div class="result-box">
                    <div class="image image2" runat="server" id="OffertaImmagine">
                    </div>
                    <div class="details">
                        <h2 class="h3"><strong>
                            <asp:Literal ID="OffertaLocalita" runat="server"></asp:Literal>
                        </strong></h2>
                        <h3 class="h4">
                            <asp:Literal ID="OffertaStruttura" runat="server"></asp:Literal></h3>
                        <span class="label">
                            <asp:Literal ID="OffertaMacro" runat="server"></asp:Literal>
                        </span>

                        <p class="period">
                            <asp:Literal ID="OffertaPeriodo" runat="server"></asp:Literal>
                        </p>
                        <p class="price">
                            a partire da <span>
                                <asp:Literal ID="OffertaPrezzo" runat="server"></asp:Literal>
                            </span>
                        </p>
                    </div>
                    <asp:HyperLink ID="OffertaLink" runat="server"></asp:HyperLink>
                </div>
            </div>
        </ItemTemplate>
    </asp:ListView>
</div>