﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Controls.Offerte
    
    Partial Public Class OfferteRicercaBoxOrizzontale
        
        '''<summary>
        '''Controllo FormDestinazione.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormDestinazione As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo FormData.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormData As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo TextBox1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents TextBox1 As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo FormInvia.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormInvia As Global.System.Web.UI.WebControls.LinkButton
    End Class
End Namespace
