﻿Imports System.Data
Imports System.Data.SqlClient
Imports Emmemedia.Classi

Namespace Controls
    Namespace Offerte
        Public Class OfferteElencoOrizzontale
            Inherits BaseClass.SiteUsercontrol

            Private _Macro As String = String.Empty
            Private _Categoria As String = String.Empty
            Private _Offerta As String = String.Empty

            Private _CodiceMacro As Integer = 0
            Private _CodiceCategoria As Integer = 0
            Private _CodiceOfferta As Integer = 0

            Private Sub OfferteElencoVerticale_Init(sender As Object, e As EventArgs) Handles Me.Init

                _Macro = IIf(Page.RouteData.Values("macro") IsNot Nothing, Page.RouteData.Values("macro"), String.Empty)
                _Categoria = IIf(Page.RouteData.Values("Categoria") IsNot Nothing, Page.RouteData.Values("Categoria"), String.Empty)

                If _Categoria <> String.Empty And _Macro <> String.Empty Then
                    FindIdMacroFromLink(_Macro)
                    FindIdCategoriaFromLink(_Categoria, _CodiceMacro)

                ElseIf _Categoria = String.Empty And _Macro <> String.Empty Then
                    FindIdMacroFromLink(_Macro)

                ElseIf _Categoria = String.Empty And _Macro = String.Empty Then

                End If

            End Sub

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                ' visualizza tutte le macro
                bindElencoMacro()

            End Sub

            Private Sub FindIdMacroFromLink(ByVal _str As String)

                Dim sql As String
                sql = "SELECT [CODICE_CATEGORIA] FROM MV_CATEGORIE where CODICE_RIF = 0 AND lower(link_categoria) =  @link"

                Using dbConn As New SqlConnection(ConfigurationManager.ConnectionStrings("VantaggiIrresistibili").ConnectionString)

                    Using cmd As New SqlCommand

                        cmd.Connection = dbConn
                        cmd.CommandType = CommandType.Text
                        cmd.CommandText = sql
                        cmd.Parameters.AddWithValue("@link", _str.ToLower)

                        dbConn.Open()

                        Dim dt As New DataTable
                        dt.Load(cmd.ExecuteReader)

                        dbConn.Close()

                        If dt.Rows.Count > 0 Then

                            _CodiceMacro = dt(0)(0).ToString

                        End If

                    End Using

                End Using

            End Sub

            Private Sub FindIdCategoriaFromLink(ByVal _str As String, ByVal _CodiceRif As Integer)

                Dim sql As String
                sql = "SELECT top(1) [CODICE_CATEGORIA] FROM MV_CATEGORIE where lower(link_CATEGORIA) = @link and codice_rif = @codice"

                Using dbConn As New SqlConnection(ConfigurationManager.ConnectionStrings("VantaggiIrresistibili").ConnectionString)

                    Using cmd As New SqlCommand

                        cmd.Connection = dbConn
                        cmd.CommandType = CommandType.Text
                        cmd.CommandText = sql
                        cmd.Parameters.AddWithValue("@link", _str.ToLower)
                        cmd.Parameters.AddWithValue("@codice", _CodiceRif)

                        dbConn.Open()

                        Dim dt As New DataTable
                        dt.Load(cmd.ExecuteReader)

                        dbConn.Close()

                        If dt.Rows.Count > 0 Then

                            _CodiceCategoria = dt(0)(0).ToString

                        End If

                    End Using

                End Using

            End Sub

            Private Sub bindElencoMacro()

                Dim sql As String
                sql = "select distinct [CODICE_CATEGORIA] , [TITOLO_CATEGORIA] , [DESCRIZIONE_CATEGORIA] , [LINK_CATEGORIA] , [IMMAGINE_CATEGORIA] from MV_VW_CATEGORIE_ATTIVE where [CODICEFAMIGLIA_PRODOTTOFAMIGLIA] = @codicefamiglia ORDER BY TITOLO_CATEGORIA ASC"

                Using dbConn As New SqlConnection(ConfigurationManager.ConnectionStrings("VantaggiIrresistibili").ConnectionString)

                    Using cmd As New SqlCommand

                        cmd.Connection = dbConn
                        cmd.CommandType = CommandType.Text
                        cmd.CommandText = sql
                        cmd.Parameters.AddWithValue("codicefamiglia", ConfigurationManager.AppSettings("codiceutenza"))

                        dbConn.Open()

                        Dim dt As New DataTable
                        dt.Load(cmd.ExecuteReader)

                        dbConn.Close()

                        lvmacro.DataSource = dt
                        lvmacro.DataBind()

                    End Using

                End Using

            End Sub

        End Class
    End Namespace
End Namespace