﻿Imports System.Data
Imports System.Data.SqlClient
Imports Emmemedia.Classi

Namespace Controls
    Namespace Offerte
        Partial Public Class OfferteSlider
            Inherits BaseClass.SiteUsercontrol

            Protected _content As New ContentClass(ConfigurationManager.AppSettings("id_marchant"))

            Private _IdCategoria As String
            Private _NumeroArticoli As Integer

            Public Property NumeroArticoli() As Integer
                Get
                    If _NumeroArticoli <> Nothing Then
                        Return _NumeroArticoli
                    Else
                        Return 1
                    End If
                End Get
                Set(ByVal value As Integer)
                    _NumeroArticoli = value
                End Set
            End Property

            Public Property idCategoria() As String
                Get
                    Return _IdCategoria
                End Get
                Set(ByVal value As String)
                    _IdCategoria = value
                End Set
            End Property

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                If Not IsPostBack Then

                    bindArticoli()

                End If
            End Sub

            Private Sub bindArticoli()

                Using dbconn As New SqlConnection(ConfigurationManager.ConnectionStrings("accesso").ConnectionString)
                    Using cmd As New SqlCommand
                        cmd.Connection = dbconn
                        cmd.CommandType = CommandType.Text
                        cmd.CommandText = "SELECT * FROM Slider ORDER BY PK desc"

                        dbconn.Open()

                        Dim dt As New DataTable
                        dt.Load(cmd.ExecuteReader)

                        dbconn.Close()

                        rpArticoliSlide.DataSource = dt
                        rpArticoliSlide.DataBind()

                    End Using

                End Using

            End Sub

        End Class
    End Namespace
End Namespace