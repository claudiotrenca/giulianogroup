﻿Namespace Controls
    Namespace Offerte
        Public Class OfferteViaggiVetrina
            Inherits System.Web.UI.UserControl

            Private _DataPartenza As Date

            Public Property _NumeroVetrina As Integer = 6

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                _DataPartenza = Date.Now
                BindData()
            End Sub

            Private Sub BindData(Optional ByVal _IdMacro As String = "", Optional ByVal _IdCategoria As String = "")

                Dim lst As New List(Of DataClass.Offerta)
                Dim utenza As Integer = ConfigurationManager.AppSettings("Utenza")

                Using dc As New DataClass.ViaggiEntities

                    Dim queryResult = (From v In dc.MV_VW_PRODOTTI_UTENZE
                                       Select v.CODICE_PRODOTTO, v.STRUTTURA_PRODOTTO, v.PERIODO_PRODOTTO, v.PREZZO_PRODOTTO, v.LOCALITA_PRODOTTO, v.TITOLO_CATEGORIA, v.TITOLO_MACRO, v.LINK_CATEGORIA, v.LINK_MACRO, ImmagineProdotto = (
                                     ((From p In dc.MV_FOTOPRODOTTO
                                       Where
                                       Not p.URL_FOTOPRODOTTO Is Nothing And
                                       p.CODPRODOTTO_FOTOPRODOTTO = v.CODICE_PRODOTTO
                                       Select New With {
                                       p.URL_FOTOPRODOTTO
                                     }).Take(1).FirstOrDefault().URL_FOTOPRODOTTO)), DataInizio = (((From d In dc.MV_PREZZIPRODOTTO
                                                                                                     Where d.CODPRODOTTO_PREZZOPRODOTTO = v.CODICE_PRODOTTO
                                                                                                     Select New With {
                                                                                                                    d.DATAINIZIO_PREZZOPRODOTTO
                                                                                                                     }).OrderBy(Function(y) y.DATAINIZIO_PREZZOPRODOTTO).Take(1).FirstOrDefault().DATAINIZIO_PREZZOPRODOTTO)), DataFine = (((From d In dc.MV_PREZZIPRODOTTO
                                                                                                                                                                                                                                             Where d.CODPRODOTTO_PREZZOPRODOTTO = v.CODICE_PRODOTTO
                                                                                                                                                                                                                                             Select New With {d.DATAFINE_PREZZOPRODOTTO}).OrderByDescending(Function(y) y.DATAFINE_PREZZOPRODOTTO).Take(1).FirstOrDefault().DATAFINE_PREZZOPRODOTTO))).ToList

                    If _DataPartenza <> Nothing Then
                        queryResult = queryResult.AsEnumerable().Where(Function(p) p.DataInizio <= _DataPartenza And p.DataFine >= _DataPartenza).ToList
                    End If

                    lst = queryResult.AsEnumerable().Select(Of DataClass.Offerta)(Function(p) New DataClass.Offerta() With {
                                                                                                                        .IdProdotto = p.CODICE_PRODOTTO,
                                                                                                                        .Struttura = p.STRUTTURA_PRODOTTO,
                                                                                                                        .Categoria = p.TITOLO_CATEGORIA,
                                                                                                                        .Macro = p.TITOLO_MACRO,
                                                                                                                        .LinkCategoria = p.LINK_CATEGORIA,
                                                                                                                        .LinkMacro = p.LINK_MACRO,
                                                                                                                        .Localita = p.LOCALITA_PRODOTTO,
                                                                                                                        .Periodo = p.PERIODO_PRODOTTO,
                                                                                                                        .Immagine = p.ImmagineProdotto,
                                                                                                                        .Prezzo = p.PREZZO_PRODOTTO
                                                                                                                        }).ToList.OrderBy(Function(x) Guid.NewGuid()).Take(_NumeroVetrina).ToList

                    lvRisultati.DataSource = lst
                    lvRisultati.DataBind()

                End Using

            End Sub

            Private Sub lvRisultati_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvRisultati.ItemDataBound

                If e.Item.ItemType = ListViewItemType.DataItem Then

                    Dim dti As DataClass.Offerta = TryCast(e.Item.DataItem, DataClass.Offerta)

                    ' Dim OffertaImmagine As WebControls.Image = DirectCast(e.Item.FindControl("OffertaImmagine"), WebControls.Image)
                    Dim OffertaImmagine As HtmlControl = DirectCast(e.Item.FindControl("OffertaImmagine"), HtmlControl)

                    'background: url(images/bg.jpg) no-repeat center center fixed;
                    Dim OffertaLocalita As Literal = DirectCast(e.Item.FindControl("OffertaLocalita"), WebControls.Literal)
                    Dim OffertaStruttura As Literal = DirectCast(e.Item.FindControl("OffertaStruttura"), WebControls.Literal)
                    Dim OffertaMacro As Literal = DirectCast(e.Item.FindControl("OffertaMacro"), WebControls.Literal)
                    Dim OffertaPeriodo As Literal = DirectCast(e.Item.FindControl("OffertaPeriodo"), WebControls.Literal)
                    Dim OffertaPrezzo As Literal = DirectCast(e.Item.FindControl("OffertaPrezzo"), WebControls.Literal)
                    Dim OffertaLink As HyperLink = DirectCast(e.Item.FindControl("OffertaLink"), WebControls.HyperLink)

                    If OffertaImmagine IsNot Nothing Then
                        OffertaImmagine.Attributes.Add("style", " background: url(" & "http://www.vantaggiirresistibili.it/frontend" & (dti.Immagine.Replace("\", "/")).Replace(" ", "%20") & ") no-repeat center center; -webkit-background-size: cover; background-size: cover;")

                    End If

                    If OffertaLocalita IsNot Nothing Then
                        OffertaLocalita.Text = dti.Localita
                    End If
                    If OffertaStruttura IsNot Nothing Then
                        OffertaStruttura.Text = dti.Struttura
                    End If
                    If OffertaMacro IsNot Nothing Then
                        OffertaMacro.Text = dti.Macro
                    End If
                    If OffertaPeriodo IsNot Nothing Then
                        OffertaPeriodo.Text = dti.Periodo
                    End If
                    If OffertaPrezzo IsNot Nothing Then
                        If IsNumeric(dti.Prezzo) Then
                            OffertaPrezzo.Text = FormatCurrency(dti.Prezzo, 2)
                        Else
                            OffertaPrezzo.Text = dti.Prezzo
                        End If
                    End If
                    If OffertaLink IsNot Nothing Then
                        OffertaLink.NavigateUrl = "/offerte-viaggi/" & dti.LinkMacro & "/" & dti.LinkCategoria & "/" & Emmemedia.Classi.Utility.StringToUrl(dti.Struttura) & "_" & dti.IdProdotto & ".htm"
                    End If

                End If
            End Sub

        End Class
    End Namespace
End Namespace