<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OfferteRicercaBoxOrizzontale.ascx.vb" Inherits="Emmemedia.Controls.Offerte.OfferteRicercaBoxOrizzontale" %>
<div class="search">
    <div class="col-sm-12">
        <p class="h2">Cerca la tua vacanza.</p>
    </div>
    <form>

        <div class="form-group col-sm-3">
            <label>Destinazione</label>
            <p>
                <asp:TextBox ID="FormDestinazione" runat="server" CssClass="form-control" placeholder="Destinazione"></asp:TextBox>
                <i class="ion-map"></i>
            </p>
        </div>
        <div class="form-group col-sm-3">
            <label>Quando vuoi partire?</label>
            <p>
                <asp:TextBox ID="FormData" runat="server" CssClass="datepicker form-control" placeholder="Scegli la data" data-provide="datepicker"></asp:TextBox>
                <i class="ion-android-calendar"></i>
            </p>
        </div>
        <div class="form-group col-sm-3">
            <label>Qual è il tuo budget?</label>
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group col-sm-3">
            <br />
            <asp:LinkButton ID="FormInvia" runat="server" CssClass="btn btn-default btn-primary full-width">RICERCA</asp:LinkButton>
        </div>
        <div class="clearfix"></div>
    </form>
</div>