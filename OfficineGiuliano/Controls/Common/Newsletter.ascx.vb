﻿Imports System.Net.Mail

Namespace Controls
   Namespace Common
      Public Class Newsletter
         Inherits BaseClass.SiteUsercontrol

         Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

         End Sub

         Private Sub NewsletterSubcriptionButton_Click(sender As Object, e As EventArgs) Handles NewsletterSubcriptionButton.Click
            If Page.IsValid Then
               Using context As New DataClass.DataEntities
                  Dim row = context.NewsLetterSubscription.SingleOrDefault(Function(p) p.Email = NewsletterSubcription.Text.Trim)
                  Try
                     If row Is Nothing Then
                        Dim NewRow As New DataClass.NewsLetterSubscription
                        NewRow.DataInserimento = Date.Now
                        NewRow.DataModifica = Date.Now
                        NewRow.Attivo = True
                        NewRow.Email = NewsletterSubcription.Text.Trim

                        context.Configuration.AutoDetectChangesEnabled = True
                        context.NewsLetterSubscription.Attach(NewRow)
                        context.Entry(NewRow).State = Entity.EntityState.Added
                        context.SaveChanges()
                     Else
                        row.DataModifica = Date.Now
                        row.Attivo = True
                        context.SaveChanges()
                     End If

                     Dim sb As New StringBuilder
                     sb.Append("<strong>Officine Giuliano</strong><br /><br /><br />")
                     sb.Append("<p>Vi confermiamo l'iscrizione alla nostra newsletter</p>")
                     sb.Append("<br /><br />")

                     InviaMail(ConfigurationManager.AppSettings("mail_da_invio"), NewsletterSubcription.Text.Trim, "Iscrizione newsletter", sb.ToString)

                     Response.Redirect(ConfigurationManager.AppSettings("ThankYouNewsPath"))

                  Catch ex As Exception
                     Throw ex
                  End Try
               End Using
            End If
         End Sub

         Private Function InviaMail(ByVal _from As String, ByVal _to As String, ByVal _object As String, ByVal _body As String) As Boolean
            Try
               Dim messaggio As MailMessage = New MailMessage()
               messaggio.From = New MailAddress(_from, _from)
               messaggio.To.Add(New MailAddress(_to, _to))
               messaggio.Subject = _object
               messaggio.SubjectEncoding = System.Text.Encoding.UTF8
               messaggio.Body = _body
               messaggio.BodyEncoding = System.Text.Encoding.UTF8
               messaggio.IsBodyHtml = True

               Dim msg As SmtpClient = New SmtpClient()
               msg.Send(messaggio)

               Return True
            Catch
               Return False
            End Try
         End Function

      End Class
   End Namespace
End Namespace