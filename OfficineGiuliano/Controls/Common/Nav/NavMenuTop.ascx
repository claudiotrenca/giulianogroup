﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="NavMenuTop.ascx.vb" Inherits="Emmemedia.Controls.Common.Nav.NavMenuTop" %>

<ul>
    <asp:Repeater ID="dtMenu" runat="server">
        <ItemTemplate>
            <%# DataBinder.Eval(Container.DataItem, "DETTAGLIO").ToString   %>
        </ItemTemplate>
    </asp:Repeater>
</ul>