﻿Imports System.Data
Imports Emmemedia.Classi

Namespace Controls
    Namespace Common
        Namespace Nav
            Partial Public Class NavMenu01
                Inherits BaseClass.SiteUsercontrol

                Dim _prov, _testo, _mn As String
                Dim mese_anno As Object
                Dim testo_evidenza As String

                Public Property provincia As String
                    Get
                        Return _prov
                    End Get
                    Set(ByVal value As String)
                        _prov = value
                    End Set
                End Property

                Public Property testo As String
                    Get
                        Return _testo
                    End Get
                    Set(ByVal value As String)
                        _testo = value
                    End Set
                End Property

                Public Property MN As String
                    Get
                        Return _mn
                    End Get
                    Set(ByVal value As String)
                        _mn = value
                    End Set
                End Property

                Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                    If _prov Is Nothing Then
                        If Request.QueryString("provincia") <> "" Then
                            _prov = Request.QueryString("provincia")
                        Else
                            _prov = ConfigurationManager.AppSettings("default_prov")
                        End If
                    End If

                    dtMenu.DataSource = getMenu()
                    dtMenu.DataBind()
                    dtMenu.Visible = True

                End Sub

                Private Sub NavMenu_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender

                End Sub

                Private Function GetCustomerID() As String
                    If Context.User.Identity.Name <> "" Then
                        Return Context.User.Identity.Name
                    Else
                        If Session("AnonUID") Is Nothing Then
                            Session("AnonUID") = Guid.NewGuid()
                        End If
                        Return Session("AnonUID").ToString()
                    End If
                End Function

                Function getMenu() As ICollection

                    Dim MACRO As String
                    MACRO = "*"
                    Dim class_macro As String = ""
                    Dim class_cat As String = ""

                    Dim dt As DataTable = New DataTable()
                    dt.Columns.Add(New DataColumn("LINK", GetType(String)))
                    dt.Columns.Add(New DataColumn("DETTAGLIO", GetType(String)))

                    Dim dtMacro As New DataTable
                    ConnectionClass.leggi("SELECT * FROM vwMenu WHERE ParentId IS NULL AND IncludeInTopMenu = 'true' AND tipo = 'link' AND FlagPrivato = 'false' ORDER BY Indice", ConnectionClass.cnDBSql, dtMacro)
                    Dim dtCategorie As New DataTable
                    Dim str1 As String = ""
                    Dim inizio As Integer = 1
                    Dim i As Integer = 0
                    Dim link As String

                    For Each dr As DataRow In dtMacro.Rows

                        dtCategorie = ConnectionClass.leggi("SELECT * FROM menu WHERE FlagVisibile='true' AND IncludeInTopMenu='true' AND FlagPrivato = 'false' AND ParentID = '" & dr("PK") & "' ORDER BY INDICE ", ConnectionClass.cnDBSql, New DataTable)

                        If dtCategorie.Rows.Count > 0 Then
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false' href='#' title='" & Utility.StringToUrl(dr("Descrizione")) & "'>" & Utility.Capitalize(dr("Descrizione")) & "&nbsp;<span class='ion-ios-arrow-down'></span></a>", dt))
                        Else
                            If LCase(dr("Descrizione")) = "home" Then
                                link = "/"
                            Else
                                If dr("HasCategory") = True Then
                                    link = "/" & (dr("link")) & "/" ' & _prov & "/"
                                Else
                                    link = "/" & dr("link") & ".htm"
                                End If
                            End If
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<li><a href='" & link.ToLower & "'  title='" & Utility.StringToUrl(dr("Descrizione")) & "'>" & Utility.Capitalize(dr("Descrizione")) & "</a>", dt))
                        End If

                        str1 = ""

                        If str1 = "" And dtCategorie.Rows.Count > 0 Then
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<ul class='dropdown-menu'>", dt))
                        End If

                        For Each dr2 As DataRow In dtCategorie.Rows

                            If dr2("HasCategory") = True Then
                                link = "/" & dr("link") & "/" & dr2("link") & "/" ' & _prov & "/"
                            Else
                                link = "/" & dr("link") & "/" & dr2("link") & ".htm"
                            End If

                            If str1 = "" Then
                                dt.Rows.Add(Classi.Utility.CreateRow("", "<li>", dt))
                            End If

                            str1 = link

                            dt.Rows.Add(Classi.Utility.CreateRow("", "<a href='" & link.ToLower & "' title='" & Utility.StringToUrl(dr2("Descrizione")) & "'>" & Utility.Capitalize(dr2("Descrizione")) & "</a>", dt))

                            i += 1

                            If i Mod 3 = 0 Then
                                If str1 <> "" Then
                                    dt.Rows.Add(Classi.Utility.CreateRow("", "</li>", dt))
                                    str1 = ""
                                End If
                            End If
                        Next

                        If i > 0 And i = dtCategorie.Rows.Count Then

                            If dr("HasCategory") = True Then
                                link = "/" & (dr("link")) & "/" '& _prov & "/"
                            Else
                                link = "/" & dr("link") & ".htm"
                            End If

                            Dim strRow As String
                            If dr("immagine") IsNot DBNull.Value Then
                                strRow = "<li><a href='" & link & "' title='" & Utility.StringToUrl(dr("Descrizione")) & "'><img src='" & "/public/images/menu/" & dr("immagine") & "' class='hidden-sm hidden-xs img-responsive' alt='' />" & (dr("Descrizione")) & "</a></li>"
                            Else
                                strRow = "<li><a href='" & link & "' title='" & Utility.StringToUrl(dr("Descrizione")) & "'>" & (dr("Descrizione")) & "</a></li>"

                            End If

                            dt.Rows.Add(Classi.Utility.CreateRow("", strRow, dt))

                            i = 0
                            dt.Rows.Add(Classi.Utility.CreateRow("", "</ul>", dt))

                        End If

                        'If str1 <> "" Then
                        '    dt.Rows.Add(Classi.Utility.CreateRow("", "</li>", dt))
                        '    dt.Rows.Add(Classi.Utility.CreateRow("", "</ul>", dt))
                        'End If

                        dt.Rows.Add(Classi.Utility.CreateRow("", " </li>", dt))
                    Next
                    dt.Rows.Add(Classi.Utility.CreateRow("", "</ul>", dt))
                    Dim dv As DataView = New DataView(dt)
                    Return dv

                End Function

                Function getPrivateMenu() As ICollection

                    Dim MACRO As String
                    MACRO = "*"
                    Dim class_macro As String = ""
                    Dim class_cat As String = ""

                    Dim dt As DataTable = New DataTable()
                    dt.Columns.Add(New DataColumn("LINK", GetType(String)))
                    dt.Columns.Add(New DataColumn("DETTAGLIO", GetType(String)))

                    Dim dtMacro As New DataTable
                    ConnectionClass.leggi("select * from (SELECT distinct pk, Descrizione , link , [HasCategory] , indice FROM vwMenu WHERE ParentId IS NULL AND IncludeInTopMenu = 'true' AND tipo = 'link' AND FlagPrivato = 'true' ) as tab1 union 	(SELECT distinct ParentId , macro , macrolink , [HasCategory] , indice FROM vwMenu WHERE ParentId IS not NULL AND IncludeInTopMenu = 'true' AND tipo = 'link' AND FlagPrivato = 'true' ) ", ConnectionClass.cnDBSql, dtMacro)
                    Dim dtCategorie As New DataTable
                    Dim str1 As String = ""
                    Dim inizio As Integer = 1
                    Dim i As Integer
                    Dim link As String

                    For Each dr As DataRow In dtMacro.Rows

                        dtCategorie = ConnectionClass.leggi("SELECT * FROM menu WHERE FlagVisibile='true' AND IncludeInTopMenu='true' AND FlagPrivato = 'true' AND ParentID = '" & dr("PK") & "' ORDER BY INDICE ", ConnectionClass.cnDBSql, New DataTable)

                        If dtCategorie.Rows.Count > 0 Then
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<li class='dropdown-submenu'><a class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false' href='#' title='" & Utility.Capitalize(dr("Descrizione")) & "'>" & Utility.Capitalize(dr("Descrizione")) & "</a>", dt))
                        Else
                            If LCase(dr("Descrizione")) = "home" Then
                                link = "/"
                            Else
                                If dr("HasCategory") = True Then
                                    link = "/" & (dr("link")) & "/" ' & _prov & "/"
                                Else
                                    link = "/" & dr("link") & ".htm"
                                End If
                            End If
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<li><a href='" & link.ToLower & "'  title='" & Utility.Capitalize(dr("Descrizione")) & "'>" & Utility.Capitalize(dr("Descrizione")) & "</a>", dt))
                        End If

                        str1 = ""

                        For Each dr2 As DataRow In dtCategorie.Rows
                            If dr2("HasCategory") = True Then
                                link = "/" & dr("link") & "/" & dr2("link") & "/" '& _prov & "/"
                            Else
                                link = "/" & dr("link") & "/" & dr2("link") & ".htm"
                            End If

                            If str1 = "" Then
                                dt.Rows.Add(Classi.Utility.CreateRow("", "<ul class='dropdown-menu'>", dt))
                            End If

                            str1 = link
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<li><a href='" & link.ToLower & "' title='" & Replace(dr2("Descrizione"), "@PROV", _prov) & "'>" & Utility.Capitalize(dr2("Descrizione")) & "</a></li>", dt))
                        Next

                        i += 1
                        If str1 <> "" Then
                            dt.Rows.Add(Classi.Utility.CreateRow("", "</ul>", dt))
                        End If
                        dt.Rows.Add(Classi.Utility.CreateRow("", " </li>", dt))
                    Next

                    Dim dv As DataView = New DataView(dt)
                    Return dv

                End Function

            End Class
        End Namespace
    End Namespace
End Namespace