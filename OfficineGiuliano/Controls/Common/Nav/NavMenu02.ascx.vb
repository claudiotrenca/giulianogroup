﻿Imports Emmemedia.Classi

Namespace Controls
    Namespace Common
        Namespace Nav
            Public Class NavMenu02
                Inherits BaseClass.SiteUsercontrol

                Protected CssClass1 As String = String.Empty
                Protected CssClass2 As String = String.Empty
                Protected CssClass3 As String = String.Empty

                Public Property Provincia As String
                Public Property IDMenu As Integer

                Private Sub NavMenu_Init(sender As Object, e As EventArgs) Handles Me.Init

                    If _Provincia Is Nothing Then
                        If Page.RouteData.Values("provincia") <> "" Then
                            _Provincia = Page.RouteData.Values("provincia")
                        Else
                            _Provincia = ConfigurationManager.AppSettings("default_prov")
                        End If
                    End If

                    bindGruppoMenu()

                End Sub

                Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                    'If Session("IDUtente") IsNot Nothing Then
                    '    PanelAnonymous.Visible = False
                    'Else
                    '    PanelAnonymous.Visible = True
                    'End If

                End Sub

                Private Sub NavMenu_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
                    '   InitCart()
                End Sub

                'Private Sub InitCart()

                '    Dim rv As String
                '    rv = DataClass.CartMethod.GetDistinctItemCount(GetCustomerID())
                '    If rv <> String.Empty Then
                '        ltQtaCart.Text = rv
                '    End If

                'End Sub

                Private Sub bindGruppoMenu()

                    Dim row As New DataClass.MenuGruppo

                    Using dataContext As New DataClass.DataEntities
                        row = dataContext.MenuGruppo.FirstOrDefault(Function(p) p.PK = IDMenu)
                    End Using

                    CssClass1 = row.CssClass1.ToString
                    CssClass2 = row.CssClass2.ToString
                    CssClass3 = row.CssClass3.ToString

                End Sub

                Public Sub BindMenu()
                    dtMenu.DataSource = getMenu()
                    dtMenu.DataBind()
                    dtMenu.Visible = True
                End Sub

                Private Function GetCustomerID() As String
                    If Context.User.Identity.Name <> "" Then
                        Return Context.User.Identity.Name
                    Else
                        If Session("AnonUID") Is Nothing Then
                            Session("AnonUID") = Guid.NewGuid()
                        End If
                        Return Session("AnonUID").ToString()
                    End If
                End Function

                Function getMenu() As ICollection

                    Dim dt As New DataTable()
                    dt.Columns.Add(New DataColumn("LINK", GetType(String)))
                    dt.Columns.Add(New DataColumn("DETTAGLIO", GetType(String)))

                    Dim lst1 As New List(Of DataClass.vwMenu)
                    Dim lst2 As New List(Of DataClass.vwMenu)
                    Dim lst3 As New List(Of DataClass.vwMenu)

                    Dim str1 As String = ""
                    Dim inizio As Integer = 1
                    Dim i As Integer = 0
                    Dim link As String

                    Using dc As New DataClass.DataEntities

                        lst1 = (From p In dc.vwMenu Where p.ParentID Is Nothing And p.IncludeInTopMenu = True And p.Tipo = "link" And p.FkGruppoMenu = _IDMenu Order By p.Indice).ToList

                        For Each row1 As DataClass.vwMenu In lst1
                            lst2 = (From p In dc.vwMenu Where p.ParentID Is Nothing And p.IncludeInTopMenu = True And p.Tipo = "link" And p.FkGruppoMenu = _IDMenu And p.ParentID = row1.PK Order By p.Indice).ToList

                            If lst2.Count > 0 Then
                                dt.Rows.Add(Classi.Utility.CreateRow("", "<li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false' href='#' title='" & Utility.StringToUrl(row1.Descrizione) & "'>" & Utility.Capitalize(row1.Descrizione) & "&nbsp;<span class='ion-ios-arrow-down'></span></a>", dt))
                            Else
                                If LCase(row1.Descrizione) = "home" Then
                                    link = "/"
                                Else
                                    If row1.HasCategory = True Then
                                        link = "/" & (row1.Link) & "/" ' & _prov & "/"
                                    Else
                                        link = "/" & row1.Link & ".htm"
                                    End If
                                End If
                                dt.Rows.Add(Classi.Utility.CreateRow("", "<li><a href='" & link.ToLower & "'  title='" & Utility.StringToUrl(row1.Descrizione) & "'>" & Utility.Capitalize(row1.Descrizione) & "</a>", dt))
                            End If

                            str1 = ""

                            If str1 = "" And lst2.Count > 0 Then
                                dt.Rows.Add(Classi.Utility.CreateRow("", "<ul class='dropdown-menu'>", dt))
                            End If

                            For Each row2 As DataClass.vwMenu In lst2
                                If row2.HasCategory = True Then
                                    link = "/" & row1.Link & "/" & row2.Link & "/" ' & _prov & "/"
                                Else
                                    link = "/" & row1.Link & "/" & row2.Link & ".htm"
                                End If

                                If str1 = "" Then
                                    dt.Rows.Add(Classi.Utility.CreateRow("", "<li>", dt))
                                End If

                                str1 = link

                                dt.Rows.Add(Classi.Utility.CreateRow("", "<a href='" & link.ToLower & "' title='" & Utility.StringToUrl(row2.Descrizione) & "'>" & Utility.Capitalize(row2.Descrizione) & "</a>", dt))

                                i += 1

                                If i Mod 3 = 0 Then
                                    If str1 <> "" Then
                                        dt.Rows.Add(Classi.Utility.CreateRow("", "</li>", dt))
                                        str1 = ""
                                    End If
                                End If
                            Next

                            If i > 0 And i = lst2.Count Then
                                If row1.HasCategory = True Then
                                    link = "/" & (row1.Link) & "/" '& _prov & "/"
                                Else
                                    link = "/" & row1.Link & ".htm"
                                End If

                                Dim strRow As String
                                If row1.Immagine IsNot DBNull.Value Then
                                    strRow = "<li><a href='" & link & "' title='" & Utility.StringToUrl(row1.Descrizione) & "'><img src='" & "/public/images/menu/" & row1.Immagine & "' class='hidden-sm hidden-xs img-responsive' alt='' />" & (row1.Descrizione) & "</a></li>"
                                Else
                                    strRow = "<li><a href='" & link & "' title='" & Utility.StringToUrl(row1.Descrizione) & "'>" & (row1.Descrizione) & "</a></li>"
                                End If

                                dt.Rows.Add(Classi.Utility.CreateRow("", strRow, dt))
                                i = 0
                                dt.Rows.Add(Classi.Utility.CreateRow("", "</ul>", dt))

                            End If
                            dt.Rows.Add(Classi.Utility.CreateRow("", " </li>", dt))
                        Next
                        dt.Rows.Add(Classi.Utility.CreateRow("", "</ul>", dt))
                    End Using

                    Dim dv As DataView = New DataView(dt)
                    Return dv

                End Function

                Function getMenuold() As ICollection

                    Dim MACRO As String
                    MACRO = "*"
                    Dim class_macro As String = ""
                    Dim class_cat As String = ""

                    Dim dt As DataTable = New DataTable()
                    dt.Columns.Add(New DataColumn("LINK", GetType(String)))
                    dt.Columns.Add(New DataColumn("DETTAGLIO", GetType(String)))

                    Dim dtMacro As New DataTable
                    ConnectionClass.leggi("SELECT * FROM vwMenu WHERE ParentId IS NULL AND IncludeInTopMenu = 'true' AND tipo = 'link' AND FlagPrivato = 'false' ORDER BY Indice", ConnectionClass.cnDBSql, dtMacro)
                    Dim dtCategorie As New DataTable
                    Dim str1 As String = ""
                    Dim inizio As Integer = 1
                    Dim i As Integer
                    Dim link As String

                    For Each dr As DataRow In dtMacro.Rows

                        dtCategorie = ConnectionClass.leggi("SELECT * FROM menu WHERE FlagVisibile='true' AND IncludeInTopMenu='true' AND FlagPrivato = 'false' AND ParentID = '" & dr("PK") & "' ORDER BY INDICE ", ConnectionClass.cnDBSql, New DataTable)

                        If dtCategorie.Rows.Count > 0 Then
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false' href='#' title='" & Utility.Capitalize(dr("Descrizione")) & "'>" & Utility.Capitalize(dr("Descrizione")) & "&nbsp;<span class='ion-chevron-down'></span></a>", dt))
                        Else
                            If LCase(dr("Descrizione")) = "home" Then
                                link = "/"
                            Else
                                If dr("HasCategory") = True Then
                                    link = "/" & (dr("link")) & "/"
                                Else
                                    link = "/" & dr("link") & ".htm"
                                End If
                            End If
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<li><a href='" & link.ToLower & "'  title='" & Utility.Capitalize(dr("Descrizione")) & "'>" & Utility.Capitalize(dr("Descrizione")) & "</a>", dt))
                        End If

                        str1 = ""

                        For Each dr2 As DataRow In dtCategorie.Rows
                            If dr2("HasCategory") = True Then
                                link = "/" & dr("link") & "/" & dr2("link") & "/"
                            Else
                                link = "/" & dr("link") & "/" & dr2("link") & ".htm"
                            End If

                            If str1 = "" Then
                                dt.Rows.Add(Classi.Utility.CreateRow("", "<ul class='dropdown-menu'>", dt))
                            End If

                            str1 = link
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<li><a href='" & link.ToLower & "' title='" & Replace(dr2("Descrizione"), "@PROV", _Provincia) & "'>" & Utility.Capitalize(dr2("Descrizione")) & "</a></li>", dt))
                        Next

                        i += 1
                        If str1 <> "" Then
                            dt.Rows.Add(Classi.Utility.CreateRow("", "</ul>", dt))
                        End If
                        dt.Rows.Add(Classi.Utility.CreateRow("", " </li>", dt))
                    Next

                    Dim dv As DataView = New DataView(dt)
                    Return dv

                End Function

                Function getPrivateMenu() As ICollection

                    Dim MACRO As String
                    MACRO = "*"
                    Dim class_macro As String = ""
                    Dim class_cat As String = ""

                    Dim dt As DataTable = New DataTable()
                    dt.Columns.Add(New DataColumn("LINK", GetType(String)))
                    dt.Columns.Add(New DataColumn("DETTAGLIO", GetType(String)))

                    Dim dtMacro As New DataTable
                    ConnectionClass.leggi("select * from (SELECT distinct pk, Descrizione , link , [HasCategory] , indice FROM vwMenu WHERE ParentId IS NULL AND IncludeInTopMenu = 'true' AND tipo = 'link' AND FlagPrivato = 'true' ) as tab1 union 	(SELECT distinct ParentId , macro , macrolink , [HasCategory] , indice FROM vwMenu WHERE ParentId IS not NULL AND IncludeInTopMenu = 'true' AND tipo = 'link' AND FlagPrivato = 'true' ) ", ConnectionClass.cnDBSql, dtMacro)
                    Dim dtCategorie As New DataTable
                    Dim str1 As String = ""
                    Dim inizio As Integer = 1
                    Dim i As Integer
                    Dim link As String

                    For Each dr As DataRow In dtMacro.Rows

                        dtCategorie = ConnectionClass.leggi("SELECT * FROM menu WHERE FlagVisibile='true' AND IncludeInTopMenu='true' AND FlagPrivato = 'true' AND ParentID = '" & dr("PK") & "' ORDER BY INDICE ", ConnectionClass.cnDBSql, New DataTable)

                        If dtCategorie.Rows.Count > 0 Then
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<li class='dropdown-submenu'><a class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false' href='#' title='" & Utility.Capitalize(dr("Descrizione")) & "'>" & Utility.Capitalize(dr("Descrizione")) & "</a>", dt))
                        Else
                            If LCase(dr("Descrizione")) = "home" Then
                                link = "/"
                            Else
                                If dr("HasCategory") = True Then
                                    link = "/" & (dr("link")) & "/" & _Provincia & "/"
                                Else
                                    link = "/" & dr("link") & ".htm"
                                End If
                            End If
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<li><a href='" & link.ToLower & "'  title='" & Utility.Capitalize(dr("Descrizione")) & "'>" & Utility.Capitalize(dr("Descrizione")) & "</a>", dt))
                        End If

                        str1 = ""

                        For Each dr2 As DataRow In dtCategorie.Rows
                            If dr2("HasCategory") = True Then
                                link = "/" & dr("link") & "/" & dr2("link") & "/" & _Provincia & "/"
                            Else
                                link = "/" & dr("link") & "/" & dr2("link") & ".htm"
                            End If

                            If str1 = "" Then
                                dt.Rows.Add(Classi.Utility.CreateRow("", "<ul class='dropdown-menu'>", dt))
                            End If

                            str1 = link
                            dt.Rows.Add(Classi.Utility.CreateRow("", "<li><a href='" & link.ToLower & "' title='" & Replace(dr2("Descrizione"), "@PROV", _Provincia) & "'>" & Utility.Capitalize(dr2("Descrizione")) & "</a></li>", dt))
                        Next

                        i += 1
                        If str1 <> "" Then
                            dt.Rows.Add(Classi.Utility.CreateRow("", "</ul>", dt))
                        End If
                        dt.Rows.Add(Classi.Utility.CreateRow("", " </li>", dt))
                    Next

                    Dim dv As DataView = New DataView(dt)
                    Return dv

                End Function

            End Class
        End Namespace
    End Namespace
End Namespace