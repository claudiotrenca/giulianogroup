﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="NavSide.ascx.vb" Inherits="Emmemedia.Controls.Common.Nav.NavSide" %>
<div class="sidebar col-md-3 hidden-sm hidden-xs">
    <div class="sticky">
        <asp:ListView ID="lvQuickOuter" runat="server">
            <LayoutTemplate>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <asp:HiddenField ID="QuickID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "pk")%>' />
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id='<%# "heading" & DataBinder.Eval(Container.DataItem, "pk")%>'>
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="plusIcon" data-parent='#accordion' href='#<%# "collapse" & DataBinder.Eval(Container.DataItem, "pk")%>' aria-expanded="true" aria-controls='<%# "collapse" & DataBinder.Eval(Container.DataItem, "pk")%>'>
                                <span class="ion ion-ios-minus-outline"></span><span class="ion ion-ios-plus-outline"></span><%# DataBinder.Eval(Container.DataItem, "descrizione")%>
                            </a>
                        </h4>
                    </div>
                    <div id='<%# "collapse" & DataBinder.Eval(Container.DataItem, "pk")%>' class="panel-collapse collapse" role="tabpanel" aria-labelledby='<%# "heading" & DataBinder.Eval(Container.DataItem, "pk")%>'>
                        <div class="panel-body">
                            <asp:ListView ID="lvQuickInner" runat="server">
                                <LayoutTemplate>
                                    <ul class="list-unstyled">
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                                    </ul>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:HyperLink ID="lnkInner" runat="server"></asp:HyperLink>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </div>
</div>