﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="NavMenu02.ascx.vb" Inherits="Emmemedia.Controls.Common.Nav.NavMenu02" %>

<nav class="navbar navbar-default navbar-general bordered">
    <div class="container-fluid wrap-nav">
        <div class="row">
            <div class="col-lg-2 col-md-2">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img src="/images/logo@2x.png" alt="logo" class="main-logo" width="174"></a>
                </div>
            </div>
            <div class="col-lg-8 col-md-8">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class='<%=CssClass1  %>' style="text-transform: uppercase">
                        <asp:Repeater ID="dtMenu" runat="server">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "DETTAGLIO").ToString   %>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="social-mobile hidden-lg hidden-md">
                            <ul>
                                <li class="col-xs-6"><a class="border-right" target="_blank" href=""><i class="ion ion-social-facebook"></i></a></li>
                                <li class="col-xs-6"><a target="_blank" href=""><i class="ion ion-social-twitter"></i></a></li>
                            </ul>
                        </div>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 social-top hidden-sm hidden-xs">
                <ul>
                    <!--
                        <li><a target="_blank" href=""><i class="ion ion-social-instagram-outline"></i></a></li>
                        <li><a target="_blank" href=""><i class="ion ion-social-pinterest"></i></a></li>
                         -->
                    <li><a target="_blank" href=""><i class="ion ion-social-twitter"></i></a></li>
                    <li><a target="_blank" href=""><i class="ion ion-social-facebook"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>