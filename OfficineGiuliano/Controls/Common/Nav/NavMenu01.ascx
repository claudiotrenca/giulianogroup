﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="NavMenu01.ascx.vb" Inherits="Emmemedia.Controls.Common.Nav.NavMenu01" %>
<ul class="nav navbar-nav">
    <asp:Repeater ID="dtMenu" runat="server">
        <ItemTemplate>
            <%# DataBinder.Eval(Container.DataItem, "DETTAGLIO").ToString   %>
        </ItemTemplate>
    </asp:Repeater>
</ul>