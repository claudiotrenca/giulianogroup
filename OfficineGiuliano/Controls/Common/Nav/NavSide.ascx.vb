﻿Imports Emmemedia.Classi

Namespace Controls
    Namespace Common
        Namespace Nav

            Public Class NavSide
                Inherits System.Web.UI.UserControl

                Private _IdGruppoMenu As Integer

                Public Property IdGruppoMenu As Integer
                    Get
                        Return _IdGruppoMenu
                    End Get
                    Set(value As Integer)
                        _IdGruppoMenu = value
                    End Set
                End Property

                Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                    BindQuickTitle()
                End Sub

                Private Sub BindQuickTitle()
                    Dim result As List(Of DataClass.Menu)
                    result = DataClass.Menu.GetList(_IdGruppoMenu)
                    lvQuickOuter.DataSource = result
                    lvQuickOuter.DataBind()
                End Sub

                Private Sub bindQuickValues(ByVal index As Integer, ByRef lv As ListView)
                    Dim result As List(Of DataClass.vwMenu)
                    result = DataClass.Menu.GetList(_IdGruppoMenu, index)
                    lv.DataSource = result
                    lv.DataBind()
                End Sub

                Protected Sub lvQuickOuter_ItemCreated(sender As Object, e As ListViewItemEventArgs) Handles lvQuickOuter.ItemCreated
                    Dim lvQuickInner As ListView = DirectCast(e.Item.FindControl("lvQuickInner"), ListView)
                    AddHandler lvQuickInner.ItemDataBound, AddressOf lvQuickInner_ItemDataBound
                End Sub

                Private Sub lvQuickOuter_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvQuickOuter.ItemDataBound
                    Dim hid As HiddenField = DirectCast(e.Item.FindControl("QuickID"), HiddenField)
                    Dim lvQuickInner As ListView = DirectCast(e.Item.FindControl("lvQuickInner"), ListView)
                    bindQuickValues(hid.Value, lvQuickInner)
                End Sub

                Private Sub lvQuickInner_ItemDataBound(sender As Object, e As ListViewItemEventArgs)
                    If e.Item.ItemType = ListViewItemType.DataItem Then
                        Dim lnk As HyperLink = DirectCast(e.Item.FindControl("lnkInner"), HyperLink)
                        Dim dti As DataClass.vwMenu = TryCast(e.Item.DataItem, DataClass.vwMenu)
                        lnk.NavigateUrl = "/" & dti.MacroLInk & "/" & dti.Link & ".htm"
                        lnk.Text = dti.Descrizione
                        If Request.RawUrl.ToString.Contains(Server.UrlDecode(lnk.NavigateUrl)) Then
                            lnk.Attributes.Add("class", "prodActive")
                        End If
                    End If
                End Sub

            End Class

        End Namespace
    End Namespace
End Namespace