﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Path.ascx.vb" Inherits="Emmemedia.Controls.Common.Path" %>
<nav id="SitePath" runat="server" clientidmode="Static">

    <div class="breads">
        <div class="container-fluid wrap">

            <ol class="breadcrumb">
                <asp:Literal runat="server" ID="ltPath" />
            </ol>
        </div>
    </div>
</nav>