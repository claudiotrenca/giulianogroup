﻿Imports System.Data
Imports System.Data.SqlClient

Namespace Controls
    Namespace Common
        Partial Class Path
            Inherits BaseClass.SiteUsercontrol

            Private Categoria As New DataClass.Menu

            Protected _Cat As String
            Protected _Path As String

            Dim strPath As String = String.Empty
            Dim strLink As String = String.Empty

            Private _PK As Integer
            Private _Categoria As String
            Private _Macro As String
            Private _PageLink As String
            Private _SubCategoria As String

            Private _CodiceMacro As Integer
            Private _CodiceCategoria As Integer
            Private _codicePage As Integer
            Private _CodiceSubCategoria As Integer = 0

            Public Property percorso As String
                Get
                    Return _Path
                End Get
                Set(value As String)
                    _Path = value
                End Set
            End Property

            Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
                If Request.RawUrl.ToString.EndsWith(".css") OrElse Request.RawUrl.ToString.EndsWith(".js") OrElse Request.RawUrl.ToString.EndsWith(".PNG") Then
                    Exit Sub
                End If

                _PK = IIf(Page.RouteData.Values("PK") <> Nothing, Page.RouteData.Values("PK"), Nothing)
                _Macro = IIf(Page.RouteData.Values("macro") <> Nothing, Page.RouteData.Values("macro"), String.Empty)
                _Categoria = IIf(Page.RouteData.Values("categoria") <> Nothing, Page.RouteData.Values("categoria"), String.Empty)
                _SubCategoria = IIf(Page.RouteData.Values("SubCategoria") <> Nothing, Page.RouteData.Values("SubCategoria"), String.Empty)
                _Provincia = IIf(Page.RouteData.Values("provincia") <> Nothing, Page.RouteData.Values("provincia"), String.Empty)
                _PageLink = IIf(Page.RouteData.Values("pagelink") <> Nothing, Page.RouteData.Values("pagelink"), String.Empty)

            End Sub

            Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

                If Page.RouteData.Values("Tipo") = "prodotti" OrElse Page.RouteData.Values("Tipo") = "SchedaProdotto" Then

                    _CodiceMacro = DataClass.Menu.GetCategoryIdFromLink(_Macro)

                    If _Categoria <> String.Empty Then
                        _CodiceCategoria = DataClass.Menu.GetCategoryIdFromLink(_Categoria, _CodiceMacro)
                    End If

                    If _SubCategoria <> String.Empty Then
                        _CodiceSubCategoria = DataClass.Menu.GetCategoryIdFromLink(_SubCategoria, _CodiceCategoria)
                    End If

                    If _SubCategoria <> String.Empty Then
                        CreaNodo(_CodiceSubCategoria)
                    ElseIf _Categoria <> String.Empty Then
                        CreaNodo(_CodiceCategoria)
                    Else
                        CreaNodo(_CodiceMacro)
                    End If
                Else
                    _codicePage = DataClass.Menu.GetIdFromLink(_PageLink)
                    CreaNodo(_codicePage)
                End If

                ltPath.Visible = True
                ltPath.Text = "<li><a title='Home " & ConfigurationManager.AppSettings("nome_sito") & "' href='http://" & Request.Url.Host & "'>Home</a></li> " & strPath

            End Sub

            Private Sub CreaNodo(ByVal _id As Integer)

                Dim separatore As String = " > "
                Dim qr As DataClass.Menu

                strLink = ""

                Using context As New DataClass.DataEntities
                    qr = (From row In context.Menu Where row.PK = _id Select row).SingleOrDefault
                End Using

                If qr IsNot Nothing Then
                    If qr.ParentID Is Nothing Then
                        If strPath = String.Empty Then
                            strPath = "<li><a href='#'>" & Classi.Utility.Capitalize(qr.Descrizione) & "</a></li>"
                        Else
                            If _Provincia <> String.Empty Then
                                Dim lnk As String = CreaLink(_id)
                                strPath = "<li><a href='" & lnk & "'>" & Classi.Utility.Capitalize(qr.Descrizione) & "</a></li>" & strPath
                            Else
                                Dim lnk As String = CreaLink(_id)
                                If qr.Tipo = "categoria" Then
                                    lnk = lnk
                                Else
                                    lnk = lnk.Substring(0, lnk.Length - 1) & ".htm"
                                End If
                                strPath = "<li><a href='" & lnk & "'>" & Classi.Utility.Capitalize(qr.Descrizione) & "</a></li>" & strPath
                            End If
                        End If
                    Else
                        If strPath = String.Empty Then

                            If Page.RouteData.Values("Tipo") = "SchedaProdotto" Then
                                Dim Prodotto As String
                                Using context As New DataClass.DataEntities
                                    Prodotto = (From p In context.vwProdotti Where p.PK = _PK Select p.Modello).FirstOrDefault
                                End Using
                                strPath = "<li><a href='#'>" & Classi.Utility.Capitalize(Prodotto) & "</a></li>"
                                CreaNodo(qr.PK)
                            Else
                                strPath = "<li><a href='#'>" & Classi.Utility.Capitalize(qr.Descrizione) & "</a></li>"
                                CreaNodo(qr.ParentID)
                            End If
                        Else
                            Dim lnk As String = CreaLink(_id)

                            If qr.Tipo = "categoria" Then
                                lnk = lnk
                                strPath = "<li><a href='" & lnk & "'>" & Classi.Utility.Capitalize(qr.Descrizione) & "</a></li>" & strPath
                            Else
                                lnk = lnk.Substring(0, lnk.Length - 1) & ".htm"
                                strPath = "<li><a href='" & lnk & "'>" & Classi.Utility.Capitalize(qr.Descrizione) & "</a></li>" & strPath
                            End If

                            CreaNodo(qr.ParentID)
                        End If

                    End If
                End If

            End Sub

            Private Function CreaLink(ByVal _id As Integer) As String

                Dim qr As DataClass.Menu

                Using context As New DataClass.DataEntities
                    qr = (From row In context.Menu Where row.PK = _id Select row).SingleOrDefault
                End Using

                If qr.ParentID Is Nothing Then
                    strLink = "/" & qr.Link & "/" & strLink
                Else
                    strLink = qr.Link & "/" & strLink
                    CreaLink(qr.ParentID)
                End If

                Return strLink

            End Function

        End Class
    End Namespace
End Namespace