﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Footer.ascx.vb" Inherits="Emmemedia.Controls.Common.Footer" %>
<%@ Register Src="~/Controls/Common/FooterMenu.ascx" TagPrefix="uc1" TagName="FooterMenu" %>
<%@ Register Src="~/Controls/Common/Newsletter.ascx" TagPrefix="uc1" TagName="Newsletter" %>


<footer>
    <div class="container-fluid text-center">
        <div class="row hidden-xs">
            <uc1:FooterMenu runat="server" ID="FooterMenu" />
        </div>
        <div class="row">
            <uc1:Newsletter runat="server" ID="Newsletter" />
        </div>
    </div>
</footer>
<div class="post-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 textCenter">
                <p>
                    COPYRIGHT FLORALEVENTI 2016 - P.IVA 03411870615 - <a href="/privacy.htm">PRIVACY POLICY</a> - <a target="_blank" href="http://www.emmemedia.com">Emmemedia</a>
                </p>
            </div>
            <div class="col-md-6 col-sm-6 textRight">
                <p>
                    <span>
                        <a target="_blank" href="https://www.facebook.com/floraleventi.it/"><i class="ion-social-facebook xs"></i></a>
                        <!-- <a target="_blank" href=""><i class="ion-social-twitter xs"></i></a>
                        <a target="_blank" href=""><i class="ion-social-instagram-outline xs"></i></a>
                        <a target="_blank" href=""><i class="ion-social-linkedin xs"></i></a> -->
                    </span>
                </p>
            </div>
        </div>
    </div>
</div>