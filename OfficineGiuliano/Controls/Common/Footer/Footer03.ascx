<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Footer03.ascx.vb" Inherits="Emmemedia.Footer03" %>
<%@ Register Src="~/Controls/Common/Newsletter.ascx" TagPrefix="uc1" TagName="Newsletter" %>

<div class="pre-footer">

    <div class="container-fluid wrap">

        <div class="row">

            <div class="col-lg-12">

                <div class="row">
                    <div class="col-md-2 hidden-sm hidden-xs">
                        <h4 class="h4">Giuliano Rent
                        </h4>
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/azienda.htm">Azienda</a></li>
                            <li><a href="/noleggio/">Noleggio</a></li>
                            <li><a href="/assistenza.htm">Assistenza</a></li>
                            <li><a href="/vendita.htm">Vendita</a></li>
                            <li><a href="/contatti.htm">Contatti</a></li>
                            <li><a href="/blog.htm">Blog</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 hidden-sm hidden-xs">
                        <h4 class="h4">Macchinari
                        </h4>
                        <ul>
                            <li><a href="">Sollevatori</a></li>
                            <li><a href="">Mini Compressori</a></li>
                            <li><a href="">Scavatori</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 hidden-sm hidden-xs">
                        <h4 class="h4">Le Sedi
                        </h4>
                        <h5 class="h5">Casoria
                        </h5>
                        <p>
                            Via Circumvallazione Esterna, 29<br>
                            80026 - Casoria (NA)<br>
                            Tel. +39 081.7362820<br>
                            Fax +39 081.7364620
                        </p>
                        <h5 class="h5 marginedH5">Roma
                        </h5>
                        <p>
                            Via L. Da Vinci, 42/44<br>
                            00015 - Z. ind. Monterotondo Scalo<br>
                            Tel. +39 06.90060607<br>
                            Fax +39 06.9069925
                        </p>
                    </div>
                    <div class="col-md-2 hidden-sm hidden-xs">
                        <h4 class="h4">&nbsp;
                        </h4>
                        <h5 class="h5">Brescia
                        </h5>
                        <p>
                            Via E. Mattei, 28<br>
                            25030 - Torbole Casaglia<br>
                            Tel. +39 030.2151019<br>
                            Fax +39 030.2158056
                        </p>
                        <h5 class="h5 marginedH5">Torino
                        </h5>
                        <p>
                            Via Torino, 258<br>
                            10028 - Trofarello<br>
                            Tel. +39 011.6482711<br>
                            Fax +39 011.6482715
                        </p>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-md-4 col-sm-10 col-xs-12">

                        <uc1:Newsletter runat="server" ID="Newsletter" />

                        <ul class="social-btm">
                            <li><a target="_blank" href=""><i class="ion ion-social-facebook"></i></a></li>
                            <li><a target="_blank" href=""><i class="ion ion-social-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- <div class="hidden-lg hidden-md">

                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <h4 class="h4">
                                        Seguici sui Social
                                    </h4>
                                    <ul class="social-btm">
                                        <li><a target="_blank" href=""><i class="ion ion-social-facebook"></i></a></li>
                                        <li><a target="_blank" href=""><i class="ion ion-social-twitter"></i></a></li>
                                        <li><a target="_blank" href=""><i class="ion ion-social-instagram-outline"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div> -->
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="container-fluid wrap">

        <div class="row">
            <div class="col-md-2 logo-footer">
                <img class="imgResponsive" src="/images/footer-tel.png" alt="">
            </div>
            <div class="col-md-8 info">
                <p>
                    &copy; 2016 Giuliano Rent s.p.a - <span>Cap. Soc. � 1.000.000,00 int. vers., REA RM-1236989, C.F./P.IVA: 03676401213</span>
                </p>
            </div>
            <div class="col-md-2 credits">
                <a target="_blank" href="http://www.emmemedia.com"><b>Emmemedia</b></a>
            </div>
        </div>
    </div>
</footer>

<!-- footer -->