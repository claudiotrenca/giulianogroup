﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FooterMenu.ascx.vb" Inherits="Emmemedia.Controls.Common.FooterMenu" %>
<ul class="footer-menu">

    <asp:Repeater ID="dtMenu" runat="server">
        <ItemTemplate>
            <%# DataBinder.Eval(Container.DataItem, "DETTAGLIO").ToString   %>
        </ItemTemplate>
    </asp:Repeater>
</ul>