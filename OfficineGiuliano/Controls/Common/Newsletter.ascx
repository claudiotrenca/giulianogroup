﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Newsletter.ascx.vb" Inherits="Emmemedia.Controls.Common.Newsletter" %>
<script type="text/javascript">
    function ValidateCheckBox2(sender, args) {
        debugger;
        if (document.getElementById("<%=chkdati.ClientID %>").checked == true) {
            args.IsValid = true;
        } else {
            args.IsValid = false;
        }
    }
</script>
<h4 class="h4">Iscriviti alla nostra newsletter
</h4>
<div class="form-group">
    <div class="input-group">
        <asp:TextBox ID="NewsletterSubcription" runat="server" placeholder="INSERISCI LA TUA @EMAIL" CssClass="form-control" TextMode="Email" ValidationGroup="newsletter" aria-describedby="NewsletterSubcriptionButton"></asp:TextBox>
        <asp:LinkButton ID="NewsletterSubcriptionButton" ClientIDMode="Static" runat="server" CssClass="input-group-addon" ValidationGroup="newsletter" CausesValidation="True"> <i class="ion ion-ios-at"></i> </asp:LinkButton>
    </div>
    <p>
        Preso atto dell'<a style="text-decoration: underline" href="/privacy.htm" title="Privacy" target="blank">informativa sul trattamento dei dati personali, l'utente:</a><br>
        <asp:CheckBox ID="chkdati" runat="server" ValidationGroup="newsletter" /><asp:Literal ID="Literal7" runat="server" Text="desidera iscriversi alla newsletter ed esprime il consenso al trattamento per finalita' di cui all'art 3 lettera c) dell'informativa."></asp:Literal>
        <asp:CustomValidator runat="server" ID="CustomValidator1" Display="Dynamic" ClientValidationFunction="ValidateCheckBox2" ForeColor="Red" ValidationGroup="newsletter" >&nbsp;* E' necessario fornire il consenso</asp:CustomValidator>
    </p>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Campo Obbligatorio!"
        ForeColor="Red" Font-Size="11px" ControlToValidate="NewsletterSubcription" Display="Dynamic" ValidationGroup="newsletter" EnableClientScript="true">
    </asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato E-mail non corretto!"
        ForeColor="Red" Font-Size="11px" Display="Dynamic" ValidationGroup="newsletter" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
        ControlToValidate="NewsletterSubcription">
    </asp:RegularExpressionValidator>
</div>