﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Controls.Common.Header

    Partial Public Class Header01

        '''<summary>
        '''Controllo top_panel.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents top_panel As Global.System.Web.UI.WebControls.Panel

        '''<summary>
        '''Controllo lnkSterilfarmaClub.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents lnkSterilfarmaClub As Global.System.Web.UI.WebControls.HyperLink

        '''<summary>
        '''Controllo navMenu1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents navMenu1 As Global.Emmemedia.Controls.Common.Nav.NavMenu01
    End Class
End Namespace
