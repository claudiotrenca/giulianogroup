﻿Namespace Controls
    Namespace Common
        Namespace Header
            Public Class HeaderMaster
                Inherits System.Web.UI.UserControl

                Dim ContenutoHeader As String = String.Empty
                Dim ListaOggetti() As String

                Private Sub HeaderMaster_Init(sender As Object, e As EventArgs) Handles Me.Init
                    InitHeader()
                End Sub

                Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                    BindContent()
                End Sub

                Private Sub HeaderMaster_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender

                End Sub

                Private Sub InitHeader()
                    Using dc As New DataClass.DataEntities
                        ContenutoHeader = (From p In dc.Header Where p.FlagVisibile = True Select p.Contenuto).FirstOrDefault.ToString
                    End Using

                    Dim Separatore() As Char
                    Separatore = "|"

                    If Not String.IsNullOrEmpty(ContenutoHeader) Then
                        ListaOggetti = ContenutoHeader.Split(Separatore, StringSplitOptions.RemoveEmptyEntries)
                    End If

                End Sub

                Private Sub BindContent()

                    For Each elem As String In ListaOggetti
                        With elem.ToUpper
                            Select Case True
                                Case .StartsWith("#NAV#")
                                    Dim Pattern As String = "\[(\d)\]"
                                    Dim Re As New Regex(Pattern)
                                    Dim idMenu As Integer
                                    If Integer.TryParse(Re.Matches(elem).Item(0).Value.Replace("[", "").Replace("]", ""), idMenu) = True Then
                                        Dim obj As Nav.NavMenu02 = DirectCast(LoadControl("/controls/common/nav/navmenu02.ascx"), Nav.NavMenu02)
                                        obj.IDMenu = idMenu
                                        obj.BindMenu()
                                        PH.Controls.Add(obj)
                                    End If

                                Case .StartsWith("#LITERAL#")
                                    Dim lt As New Literal
                                    lt.Text = elem.Replace("#LITERAL#", "")
                                    PH.Controls.Add(lt)

                                Case .StartsWith("#LOGO#")
                                    Dim lt As New Literal
                                    lt.Text = elem.Replace("#LOGO#", "")
                                    PH.Controls.Add(lt)

                                Case Else
                                    Exit Select

                            End Select
                        End With
                    Next

                End Sub

            End Class
        End Namespace
    End Namespace
End Namespace