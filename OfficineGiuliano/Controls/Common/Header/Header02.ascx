<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Header02.ascx.vb" Inherits="Emmemedia.Controls.Common.Header.Header02" %>
<%@ Register Src="~/Controls/Common/Nav/NavMenu02.ascx" TagPrefix="uc1" TagName="NavMenu02" %>

<nav class="navbar navbar-default navbar-general bordered">
    <div class="container-fluid wrap-nav">
        <div class="row">
            <div class="col-lg-2 col-md-2">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img src="/images/logo@2x.png" alt="logo" class="main-logo" width="174"></a>
                </div>
            </div>
            <div class="col-lg-8 col-md-8">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    #NAV#[1]
                </div>
            </div>
            
        </div>
    </div>
</nav>