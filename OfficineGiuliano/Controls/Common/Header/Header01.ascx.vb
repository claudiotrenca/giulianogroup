﻿Imports System.Data

Namespace Controls
    Namespace Common
        Namespace Header
            Partial Public Class Header01
                Inherits BaseClass.SiteUsercontrol

                Protected _content As New ContentClass(ConfigurationManager.AppSettings("id_marchant"))

                Protected _Comuni As String

                Dim mese_anno As String
                Dim _testo, testo_evidenza As String
                Dim _mn As String

                Public Property testo As String
                    Get
                        Return _testo
                    End Get
                    Set(ByVal value As String)
                        _testo = value
                    End Set
                End Property

                Public Property MN As String
                    Get
                        Return _mn
                    End Get
                    Set(ByVal value As String)
                        _mn = value
                    End Set
                End Property

                Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                    top_panel.Visible = Request.IsAuthenticated

                    If Request.QueryString("provincia") <> "" Then
                        _Provincia = Request.QueryString("provincia")
                    Else
                        _Provincia = ConfigurationManager.AppSettings("default_prov")
                    End If

                    If Session("IDUtente") IsNot Nothing Then
                        lnkSterilfarmaClub.NavigateUrl = ConfigurationManager.AppSettings("PrivateUrl")
                    Else
                        lnkSterilfarmaClub.NavigateUrl = ConfigurationManager.AppSettings("LoginUrl")
                    End If

                End Sub

                Sub CARICA_CLICK(ByVal value As String)
                    Dim result As String = ConnectionClass.scalar("select PK_CODICE from _CLICK_RET where FROM_TO = '" & value & "'", ConnectionClass.cnDBSql)
                    If Not IsNothing(result) Then
                        ConnectionClass.exec("update _CLICK_RET set NUM = NUM+1 where PK_CODICE = " & result, ConnectionClass.cnDBSql)
                    Else
                        ConnectionClass.exec("insert into tbCLICK_RET (FROM_TO, NUM) VALUES ('" & value & "', 1)", ConnectionClass.cnDBSql)
                    End If
                End Sub

                Private Function GetCustomerID() As String
                    If Context.User.Identity.Name <> "" Then
                        Return Context.User.Identity.Name
                    Else
                        If Session("AnonUID") Is Nothing Then
                            Session("AnonUID") = Guid.NewGuid()
                        End If
                        Return Session("AnonUID").ToString()
                    End If
                End Function

            End Class
        End Namespace
    End Namespace
End Namespace