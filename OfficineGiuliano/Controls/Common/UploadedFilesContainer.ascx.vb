﻿Public Class UploadedFilesContainer
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FilesRoundPanel.Width = Width
        FilesRoundPanel.Height = Height
        FilesRoundPanel.HeaderText = HeaderText
    End Sub

    Public Property Width() As Integer
        Get
            Return m_Width
        End Get
        Set
            m_Width = Value
        End Set
    End Property
    Private m_Width As Integer
    Public Property Height() As Integer
        Get
            Return m_Height
        End Get
        Set
            m_Height = Value
        End Set
    End Property
    Private m_Height As Integer
    Public Property NameColumnWidth() As Integer
        Get
            Return m_NameColumnWidth
        End Get
        Set
            m_NameColumnWidth = Value
        End Set
    End Property
    Private m_NameColumnWidth As Integer
    Public Property SizeColumnWidth() As Integer
        Get
            Return m_SizeColumnWidth
        End Get
        Set
            m_SizeColumnWidth = Value
        End Set
    End Property
    Private m_SizeColumnWidth As Integer
    Public Property HeaderText() As String
        Get
            Return m_HeaderText
        End Get
        Set
            m_HeaderText = Value
        End Set
    End Property
    Private m_HeaderText As String

    Protected Friend Function GetOptionsString() As String
        Return (Convert.ToString((Convert.ToString("'") & GetStyleAttributeValue(NameColumnWidth)) + "', '") & GetStyleAttributeValue(SizeColumnWidth)) + "'"
    End Function
    Protected Friend Function GetStyleAttributeValue(width As Integer) As String
        Return If(width > 0, String.Format("width: {0}px; max-width: {0}px", width), String.Empty)
    End Function

End Class