﻿Public Class StoreLocator
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            BindRegione()

        End If
    End Sub

    Private Sub BindRegione()

        Dim lst As New List(Of DataClass.vwAgenzie)
        Using dc As New DataClass.DataEntities
            lst = (From p In dc.vwAgenzie Where p.regione <> "" Select p Order By p.regione).ToList
        End Using

        Dim distinctItems = lst.GroupBy(Function(x) x.regione).[Select](Function(y) y.First()).ToList
        distinctItems = distinctItems.AsQueryable.Where(Function(p) p.regione <> "").ToList '.OrderBy(Function(x) x.Regione).ToList
        Dim itm As New DataClass.vwAgenzie
        itm.regione = "Seleziona"
        distinctItems.Insert(0, itm)

        ddlRegione.DataSource = distinctItems
        ddlRegione.DataTextField = "regione"
        ddlRegione.DataValueField = "regione"
        ddlRegione.DataBind()

    End Sub

    Private Sub BindProvincia(ByVal _regione As String)

        Dim lst As New List(Of DataClass.vwAgenzie)
        Using dc As New DataClass.DataEntities
            lst = (From p In dc.vwAgenzie Where p.regione = _regione And p.sigla.Trim <> "" Select p Order By p.provincia).ToList
        End Using

        Dim distinctItems = lst.GroupBy(Function(x) x.sigla).[Select](Function(y) y.First()).ToList
        distinctItems = distinctItems.AsQueryable.Where(Function(p) p.sigla <> "").ToList
        Dim itm As New DataClass.vwAgenzie
        itm.provincia = "Seleziona"
        itm.sigla = "0"
        distinctItems.Insert(0, itm)

        ddlProvincia.DataSource = distinctItems
        ddlProvincia.DataTextField = "provincia"
        ddlProvincia.DataValueField = "sigla"
        ddlProvincia.DataBind()

    End Sub

    Private Sub BindComune(ByVal _sigla As String)

        Dim lst As New List(Of DataClass.vwAgenzie)
        Using dc As New DataClass.DataEntities
            lst = (From p In dc.vwAgenzie Where p.sigla = _sigla And p.Localita.Trim <> "" Select p Order By p.Localita).ToList
        End Using

        Dim distinctItems = lst.GroupBy(Function(x) x.Localita).[Select](Function(y) y.First()).ToList
        distinctItems = distinctItems.AsQueryable.Where(Function(p) p.Localita <> "").ToList

        Dim itm As New DataClass.vwAgenzie
        itm.Localita = "Seleziona"

        distinctItems.Insert(0, itm)

        ddlComune.DataSource = distinctItems
        ddlComune.DataTextField = "localita"
        ddlComune.DataValueField = "localita"
        ddlComune.DataBind()

    End Sub

    Protected Sub ddlRegione_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRegione.SelectedIndexChanged
        BindProvincia(ddlRegione.SelectedValue)
    End Sub

    Protected Sub ddlProvincia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvincia.SelectedIndexChanged
        BindComune(ddlProvincia.SelectedValue)
    End Sub

    Protected Sub ddlComune_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlComune.SelectedIndexChanged
        btnRicerca.Enabled = True
        btnRicerca.PostBackUrl = "/agenzie-di-viaggi/" & Classi.Utility.StringToUrl(ddlRegione.SelectedValue) & "/" & Classi.Utility.StringToUrl(ddlProvincia.SelectedValue) & "/" & Classi.Utility.StringToUrl(ddlComune.SelectedValue) & "/"
    End Sub

    Private Sub btnRicerca_Click(sender As Object, e As EventArgs) Handles btnRicerca.Click

    End Sub

End Class