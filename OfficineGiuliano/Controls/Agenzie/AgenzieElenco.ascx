<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AgenzieElenco.ascx.vb" Inherits="Emmemedia.AgenzieElenco" %>
<asp:ListView ID="lvAgenzie" runat="server">
    <LayoutTemplate>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <a href="http://www.Emmemedia.it/agenzie-di-viaggi/">
                    <asp:Image ID="Image2"   runat="server" ImageUrl="/images/banner-agenzie2.jpg" CssClass="img-responsive" AlternateText="Agenzie di viaggi" /></a>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <ul class="list-unstyled elenco-agenzie-home">
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                </ul>
            </div>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <li>
            <asp:HyperLink ID="link" runat="server">
                <h4>
                    <asp:Literal ID="ltAgenzia" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AGENZIA")%>' />
                    <br />
                    <small>
                        <asp:Literal ID="ltComune" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "COMUNE") & " - (" & DataBinder.Eval(Container.DataItem, "SIGLA") & ")"%>' />
                    </small></h4>
            </asp:HyperLink></li>
    </ItemTemplate>
</asp:ListView>