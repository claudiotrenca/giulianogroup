﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class AgenzieElenco
    Inherits BaseClass.SiteUsercontrol

    Private _Regione As String = String.Empty
    Private _Provincia As String = String.Empty
    Private _Comune As String = String.Empty

    Private _Filtro As String = String.Empty

    Public WriteOnly Property Regione As String
        Set(value As String)
            _Regione = value
        End Set
    End Property

    Public WriteOnly Property Provincia As String
        Set(value As String)
            _Provincia = value
        End Set
    End Property

    Public WriteOnly Property Comune As String
        Set(value As String)
            _Comune = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            bindArticoli()

            If _Regione <> "" Then
                _Filtro = " WHERE REGIONE = '" & _Regione & "'"
            End If

            If _Provincia <> "" Then
                _Filtro = " WHERE sigla = '" & _Provincia & "'"
            End If

            If _Comune <> "" Then
                _Filtro = " WHERE comune = '" & _Comune & "'"
            End If

        End If

    End Sub

    Private Sub bindArticoli()

        Dim sql As String
        Dim idm As String = ConfigurationManager.AppSettings("id_marchant")

        If _Filtro <> String.Empty Then
            sql = "SELECT TOP(5) pk_codice, agenzia , regione, provincia , sigla , comune , localita FROM vwAgenzie " & _Filtro & " ORDER BY NEWID() "

        Else
            sql = "SELECT TOP(5) pk_codice, agenzia ,  regione, provincia , sigla , comune , localita FROM vwagenzie ORDER BY NEWID() "

        End If

        Using con As New SqlConnection(ConfigurationManager.ConnectionStrings("accesso").ConnectionString)
            Using cmd As New SqlCommand

                cmd.Connection = con
                cmd.CommandType = CommandType.Text
                cmd.CommandText = sql

                con.Open()

                Dim dt As New DataTable
                dt.Load(cmd.ExecuteReader)

                con.Close()

                lvAgenzie.DataSource = dt
                lvAgenzie.DataBind()

            End Using

        End Using
    End Sub

    Protected Sub lvAgenzie_ItemCreated(sender As Object, e As ListViewItemEventArgs) Handles lvAgenzie.ItemCreated
        If e.Item.ItemType = ListViewItemType.DataItem Then

            Dim dataItem As ListViewDataItem = DirectCast(e.Item, ListViewDataItem)
            Dim drv As DataRowView = DirectCast(dataItem.DataItem, DataRowView)

        End If
    End Sub

    Protected Sub lvAgenzie_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvAgenzie.ItemDataBound

        Dim li As ListViewItem = e.Item

        If li.ItemType = ListViewItemType.DataItem Then

            Dim di As ListViewDataItem = DirectCast(li, ListViewDataItem)
            Dim dr As System.Data.DataRowView = DirectCast(di.DataItem, System.Data.DataRowView)

            Using lnk As HyperLink = DirectCast(e.Item.FindControl("LINK"), HyperLink)

                lnk.NavigateUrl = "/agenzie-di-viaggi/" & Classi.Utility.StringToUrl(dr("regione").ToString.Trim.ToLower) & "/" & Classi.Utility.StringToUrl(dr("provincia").ToString.Trim.ToLower) & "/" & Classi.Utility.StringToUrl(dr("comune").ToString.Trim.ToLower) & "/" & Classi.Utility.StringToUrl(dr("agenzia").ToString.Trim.ToLower) & "_id" & dr("pk_codice").ToString.Trim & ".htm"

            End Using

        End If
    End Sub
End Class