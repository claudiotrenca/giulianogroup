﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="StoreLocator.ascx.vb" Inherits="Emmemedia.StoreLocator" EnableViewState="true" %>
<!-- store locator home -->
<div class="search-agency-box">
    <h3 class="h2 text-center">Ricerca l'agenzia più vicina a te</h3>
    <br />
    <div class="form">
        <div class="col-sm-3 col-md-3">
            <div class="form-group">
                <label>Regione</label>
                <p>
                    <asp:DropDownList ID="ddlRegione" runat="server" CssClass="form-control selectpicker " AutoPostBack="true"></asp:DropDownList>
                </p>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="form-group">
                <label>Provincia</label>
                <p>
                    <asp:DropDownList ID="ddlProvincia" runat="server" CssClass="form-control selectpicker" AutoPostBack="true"></asp:DropDownList>
                </p>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="form-group">
                <label>Comune</label>
                <p>
                    <asp:DropDownList ID="ddlComune" runat="server" CssClass="form-control selectpicker" AutoPostBack="true"></asp:DropDownList>
                </p>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="form-group">
                <label></label>
                <asp:LinkButton ID="btnRicerca" runat="server" CssClass="btn btn-default btn-secondary full-width">RICERCA AGENZIE</asp:LinkButton>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<!-- search agency box -->