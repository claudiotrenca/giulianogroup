﻿Imports Emmemedia.DataClass

Namespace Controls
    Namespace Blog
        Public Class ArticoliCategoria
            Inherits BaseClass.SiteUsercontrol

            Private _Categoria As String
            Private _RifBlog As Integer

            Public Property Categoria() As String
                Get
                    Return _Categoria
                End Get
                Set(ByVal value As String)
                    _Categoria = value
                End Set
            End Property

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                _RifBlog = CInt(ConfigurationManager.AppSettings("RifBlog"))
                Bind()
            End Sub

            Private Sub ArticoliCategoria_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
                Bind()
            End Sub

            Private Sub Bind()

                Dim queryResult As IQueryable(Of vwPost) = New List(Of vwPost)().AsQueryable()
                Dim lst As New List(Of vwPost)

                Using dc As New DataEntities
                    queryResult = (From r In dc.vwPost Where r.FlagVisibile = True And r.MacroPK = _RifBlog Select r)
                    queryResult = queryResult.Where(Function(p) p.Sezione.ToLower.Contains(_Categoria.ToLower))
                    lst = queryResult.Take(4).ToList
                End Using

                lvArticoli.DataSource = lst
                lvArticoli.DataBind()

            End Sub

            Private Sub lvArticoli_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvArticoli.ItemDataBound
                Dim dti As DataClass.vwPost = TryCast(e.Item.DataItem, vwPost)

                If e.Item.ItemType = ListViewItemType.DataItem Then

                    Dim img As WebControls.Image = DirectCast(e.Item.FindControl("ArticoloImmagine"), WebControls.Image)
                    Dim lnk1 As HyperLink = DirectCast(e.Item.FindControl("ArticoloLink1"), WebControls.HyperLink)

                    If lnk1 IsNot Nothing Then
                        lnk1.NavigateUrl = "/blog/" & dti.CategoriaLink & "/" & Classi.Utility.StringToUrl(dti.Titolo) & "_" & dti.PK & ".htm"
                    End If

                    If img IsNot Nothing Then
                        If dti.Immagine <> "" Then
                            img.ImageUrl = "/public/images/content/" & dti.Immagine
                        Else
                            img.ImageUrl = "/public/image/noimage.gif"
                        End If
                    End If

                End If
            End Sub
        End Class
    End Namespace
End Namespace