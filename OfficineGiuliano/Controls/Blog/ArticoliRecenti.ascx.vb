﻿Imports Emmemedia.DataClass

Partial Public Class ArticoliRecenti
    Inherits BaseClass.SiteUsercontrol

    Private _Categoria As String
    Private RifBlog As Integer

    Public Property Categoria() As String
        Get
            Return _Categoria
        End Get
        Set(ByVal value As String)
            _Categoria = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        RifBlog = CInt(ConfigurationManager.AppSettings("RifBlog"))

        BindElencoPiuVisti()

    End Sub

    Private Sub BindElencoPiuVisti()

        Dim lst As New List(Of vwPost)

        Using dc As New DataEntities
            lst = (From r In dc.vwPost Where r.FlagVisibile = True And r.MacroPK = RifBlog Select r Order By r.Data Descending).Take(3).ToList
        End Using

        lvArticoli.DataSource = lst
        lvArticoli.DataBind()

    End Sub

End Class