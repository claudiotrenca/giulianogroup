<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ArticoliElenco.ascx.vb" Inherits="Emmemedia.ArticoliElenco" %>
<%@ Register Assembly="UnorderedListDataPager" Namespace="UnorderedListDataPager.CustomControls" TagPrefix="cc1" %>
<%@ Import Namespace="Emmemedia" %>


    <div class="row">
        <div class="col-sm-7 col-md-8 blog">
            <h4><asp:Literal ID="TipoRicerca" runat="server"></asp:Literal></h4>
            <br />
            <asp:ListView ID="lvElencoArticoli" runat="server" DataKeyNames="PK">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <hr />
                            <p class="BlogData"><%# FormatDateTime(Eval("Data"), DateFormat.ShortDate) %></p>
                            <hr />
                        </div>

                        <div class="col-md-4 post-info">
                            <div class="BlogImmagineAnteprima">
                                <asp:HyperLink ID="ArticoloLink1" runat="server">
                                    <asp:Image ID="ArticoloImmagine" runat="server" CssClass="img-responsive" AlternateText='<%# Eval("Titolo") %>' />
                                </asp:HyperLink>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <h2 class="BlogTitolo">
                                <asp:HyperLink ID="ArticoloLink2" runat="server"><%# Eval("Titolo") %></asp:HyperLink>
                            </h2>

                            <p class="BlogRiassunto"><%# Eval("Fondo") %> </p>
                            <p>
                                <asp:HyperLink ID="ArticoloLink3" runat="server" Text="Leggi tutto..."></asp:HyperLink>
                            </p>
                        </div>
                    </div>
                </ItemTemplate>                
            </asp:ListView>
            <cc1:UnorderedListDataPager ID="dpBottom" runat="server" class="pagination" PagedControlID="lvElencoArticoli" EnableViewState="true" QueryStringField="page">
                <Fields>
                    <asp:NextPreviousPagerField ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Link" RenderNonBreakingSpacesBetweenControls="false" FirstPageText="&laquo;&laquo;" PreviousPageText="&laquo;" />
                    <asp:NumericPagerField ButtonType="Link" RenderNonBreakingSpacesBetweenControls="true" CurrentPageLabelCssClass="btn disabled" ButtonCount="10" />
                    <asp:NextPreviousPagerField ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Link" RenderNonBreakingSpacesBetweenControls="false" LastPageText="&raquo;&raquo;" NextPageText="&raquo;" />
                </Fields>
            </cc1:UnorderedListDataPager>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <nav>
                <asp:PlaceHolder runat="server" ID="pnPAGINAZIONE" />
            </nav>
        </div>
    </div>
