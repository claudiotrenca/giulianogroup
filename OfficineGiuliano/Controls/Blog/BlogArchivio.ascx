﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BlogArchivio.ascx.vb" Inherits="Emmemedia.BlogArchivio" %>
<%@ Import Namespace="Emmemedia" %>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Archivio</a></h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse in">
        <asp:ListView ID="lvArchivio" runat="server">
            <LayoutTemplate>
                <ul class="list-group">
                    <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                </ul>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="list-group-item"><a href="/blog/archivio/<%# Classi.Utility.StringToUrl(Container.DataItem)  %>/"><%# Container.DataItem %></a></li>
            </ItemTemplate>
        </asp:ListView>
    </div>
</div>