﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="TagCloud.ascx.vb" Inherits="Emmemedia.TagCloud" %>
<%@ Import Namespace="Emmemedia" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#TagCloud">Tag Cloud</a></h4>
    </div>
    <div id="TagCloud" class="panel-collapse collapse in">
        <dx:ASPxCloudControl ID="CloudTags" runat="server" EnableViewState="False">
            <RankProperties>
                <dx:RankProperties Font-Size="9px" ForeColor="#225C87" />
                <dx:RankProperties Font-Size="11px" ForeColor="#3A5188" />
                <dx:RankProperties Font-Size="13px" ForeColor="#663F8A" />
                <dx:RankProperties Font-Size="16px" ForeColor="#703B8B" />
                <dx:RankProperties Font-Size="20px" ForeColor="#CC138F" />
            </RankProperties>
        </dx:ASPxCloudControl>
    </div>
</div>