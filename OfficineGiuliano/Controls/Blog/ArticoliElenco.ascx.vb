﻿Imports System.Data
Imports System.Globalization
Imports Emmemedia.DataClass
Imports DevExpress.Web



Public Class ArticoliElenco
    Inherits BaseClass.SiteUsercontrol

    Private _sortExpression As String = "Modello"
    Private _SortDirection As String = "Asc"

    Private _PageNumber As Integer
    Private _PageSize As Integer = 12
    Private _RecordNumber As Integer
    Private _StartRecord As Integer

    Private lst As New List(Of vwPost)

    Private _Categoria As String
    Private _CodiceCategoria As String

    Public Property PostNumber As Integer


    Private Menu As New DataClass.Menu

    Private Sub BlogElenco_Init(sender As Object, e As EventArgs) Handles Me.Init

        If Session("_PageSize") IsNot Nothing Then
            _PageSize = Session("_PageSize")
            dpBottom.PageSize = _PageSize
        End If

        If Page.RouteData.Values("Pagina") IsNot Nothing Then
            _PageNumber = Convert.ToInt32(Page.RouteData.Values("Pagina"))
        Else
            _PageNumber = 1
        End If

        _StartRecord = (_PageNumber - 1) * _PageSize

        dpBottom.Visible = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        GetDataList()

        If Page.RouteData.Values("View") IsNot Nothing Then

            If Page.RouteData.Values("View") = "archivio" Then

                Dim dataString() As String = Page.RouteData.Values("Data").ToString.Split("-")
                Dim MonthNumber = DateTime.ParseExact(dataString(0), "MMMM", CultureInfo.CurrentCulture).Month
                Dim YearNumber = dataString(1)
                Dim LastDayMonth = DateSerial(YearNumber, MonthNumber + 1, 0).Day
                Dim startDate As String = "01" & "/" & Str(MonthNumber) & "/" & Str(YearNumber)
                Dim EndDate As String = Str(LastDayMonth) & "/" & Str(MonthNumber) & "/" & Str(YearNumber)

                lst = lst.AsQueryable().Where(Function(p) p.Data >= CDate(startDate) And p.Data <= CDate(EndDate)).ToList

                TipoRicerca.Text = "Articoli di : " & dataString(0) & " " & dataString(1)

            ElseIf Page.RouteData.Values("View") = "tag" Then

                Dim tag As String = Page.RouteData.Values("tag")
                lst = lst.AsQueryable.Where(Function(p) p.Sezione.Contains(tag)).ToList

                TipoRicerca.Text = "Risultati per : " & tag
            End If

        ElseIf Page.RouteData.Values("Categoria") IsNot Nothing Then

            _Categoria = Page.RouteData.Values("Categoria")

            If _Categoria <> String.Empty Then

                _CodiceCategoria = Menu.GetIdFromLink(_Categoria, ConfigurationManager.AppSettings("RifBlog"))
                lst = lst.AsQueryable.Where(Function(p) p.CategoriaPK = _CodiceCategoria).ToList

                TipoRicerca.Text = "Risultati per : " & _Categoria

            End If

        Else
            TipoRicerca.Visible = False
        End If

        BindDataList()

    End Sub

    Private Sub GetDataList()

        Dim obj As List(Of vwPost)

        Using dc As New DataEntities
            obj = dc.vwPost.ToList

            lst = (From r In obj Where r.FlagVisibile = True And r.MacroPK = CInt(ConfigurationManager.AppSettings("RifBlog")) Select r Order By r.Data Descending).Take(PostNumber).ToList
        End Using

    End Sub

    Private Sub BindDataList()

        dpBottom.PageSize = _PageSize

        lvElencoArticoli.DataSource = lst
        lvElencoArticoli.DataBind()

    End Sub

    Protected Sub lvElencoArticoli_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvElencoArticoli.ItemDataBound

        Dim dti As DataClass.vwPost = TryCast(e.Item.DataItem, vwPost)

        If e.Item.ItemType = ListViewItemType.DataItem Then

            Dim img As WebControls.Image = DirectCast(e.Item.FindControl("ArticoloImmagine"), WebControls.Image)
            Dim lnk1 As HyperLink = DirectCast(e.Item.FindControl("ArticoloLink1"), WebControls.HyperLink)
            Dim lnk2 As HyperLink = DirectCast(e.Item.FindControl("ArticoloLink2"), WebControls.HyperLink)
            Dim lnk3 As HyperLink = DirectCast(e.Item.FindControl("ArticoloLink3"), WebControls.HyperLink)

            ' blog/{Categoria}/{Titolo}_{IDArticolo}.htm

            If lnk1 IsNot Nothing Then
                lnk1.NavigateUrl = "/blog/" & dti.CategoriaLink & "/" & Classi.Utility.StringToUrl(dti.Titolo) & "_" & dti.PK & ".htm"
            End If

            If lnk2 IsNot Nothing Then
                lnk2.NavigateUrl = "/blog/" & dti.CategoriaLink & "/" & Classi.Utility.StringToUrl(dti.Titolo) & "_" & dti.PK & ".htm"
            End If

            If lnk3 IsNot Nothing Then
                lnk3.NavigateUrl = "/blog/" & dti.CategoriaLink & "/" & Classi.Utility.StringToUrl(dti.Titolo) & "_" & dti.PK & ".htm"
            End If

            If img IsNot Nothing Then
                If dti.Immagine <> "" Then
                    img.ImageUrl = "/public/images/content/" & dti.Immagine
                Else
                    img.ImageUrl = "/public/image/noimage.gif"
                End If
            End If
        End If

    End Sub

    Protected Sub lvElencoArticoli_PagePropertiesChanging(sender As Object, e As PagePropertiesChangingEventArgs) Handles lvElencoArticoli.PagePropertiesChanging

        dpBottom.SetPageProperties(_StartRecord, _PageSize, False)

    End Sub

    Protected Sub lvElencoArticoli_PreRender(sender As Object, e As EventArgs) Handles lvElencoArticoli.PreRender

        If dpBottom IsNot Nothing Then
            dpBottom.SetPageProperties(_StartRecord, _PageSize, False)
        End If

        lvElencoArticoli.DataBind()

    End Sub

    Protected Sub lvElencoArticoli_DataBound(sender As Object, e As EventArgs) Handles lvElencoArticoli.DataBound
        FixPagerURLs(dpBottom)
    End Sub

    Private Sub FixPagerURLs(ByVal Pager As DataPager)

        For Each Pag As DataPagerFieldItem In Pager.Controls
            For Each Ctrl In Pag.Controls
                If TypeOf Ctrl Is HyperLink Then
                    Dim Hyp As HyperLink = Ctrl
                    Dim Req = Hyp.NavigateUrl.Replace(Request.Path, "").Replace("?", "")
                    Dim Params = Req.Split("&")
                    Dim NewPage = (From item In Params Where item.StartsWith("page")).FirstOrDefault
                    Dim NumberPage = NewPage.Substring(NewPage.IndexOf("=") + 1)
                    Dim URL = Request.RawUrl
                    URL = URL.Substring(0, URL.LastIndexOf("/") + 1)

                    Dim QueryStr As String() = Request.RawUrl.Split("?")
                    Dim newUrl As String

                    If URL.Contains("?") Then URL = URL.Substring(0, URL.IndexOf("?"))
                    If NewPage = "page=1" Then
                        newUrl = URL
                    Else
                        '	newUrl = String.Concat(URL, NumberPage, ".htm")
                    End If

                    If URL.Contains("pt_") Then
                        newUrl = Regex.Replace(URL, "pt_[0-9]{1}", "pt_" & NumberPage)
                    Else
                        newUrl = String.Concat(URL, NumberPage, ".htm")
                    End If

                    If QueryStr.GetLength(0) > 1 AndAlso QueryStr(1) IsNot Nothing Then
                        If QueryStr(1) <> "" Then
                            Hyp.NavigateUrl = String.Concat(newUrl, "?", QueryStr(1))
                        End If
                    Else
                        Hyp.NavigateUrl = newUrl
                    End If
                End If
            Next
        Next
    End Sub

End Class