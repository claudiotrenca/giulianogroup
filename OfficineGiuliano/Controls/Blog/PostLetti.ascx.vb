﻿Imports Emmemedia.DataClass

Public Class PostLetti
    Inherits BaseClass.SiteUsercontrol

    Private RifBlog As Integer


    Private Sub PostLetti_Init(sender As Object, e As EventArgs) Handles Me.Init
        RifBlog = CInt(ConfigurationManager.AppSettings("RifBlog"))
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        BindElencoPiuVisti()
    End Sub

    Private Sub BindElencoPiuVisti()

        Dim lst As New List(Of vwPost)

        Using dc As New DataEntities
            lst = (From r In dc.vwPost Where r.FlagVisibile = True And r.MacroPK = RifBlog Select r Order By r.Visite Descending).Take(5).ToList
        End Using


        lvPostLetti.DataSource = lst
        lvPostLetti.DataBind()

    End Sub
End Class