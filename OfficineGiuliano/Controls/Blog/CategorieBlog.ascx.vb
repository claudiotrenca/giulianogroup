﻿Imports Emmemedia.DataClass

Public Class CategorieBlog
    Inherits BaseClass.SiteUsercontrol

    Private Sub CategorieBlog_Init(sender As Object, e As EventArgs) Handles Me.Init
        BindElencoCategorie()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub BindElencoCategorie()

        Dim lst As New List(Of vwPost)
        Dim lst1 As New List(Of vwPost)

        Using dc As New DataEntities
            lst = (From p In dc.vwPost Select p).ToList
            lst1 = (From r In lst Where r.FlagVisibile = True And r.MacroPK = CInt(ConfigurationManager.AppSettings("RifBlog")) Select r Order By r.Categoria Ascending).ToList
        End Using

        Dim DistinctItems = lst1.GroupBy(Function(x) x.CategoriaLink).[Select](Function(y) y.First())

        lvCategorieBlog.DataSource = DistinctItems.ToList
        lvCategorieBlog.DataBind()

    End Sub

End Class