﻿Imports Emmemedia.DataClass

Public Class BlogArchivio
    Inherits BaseClass.SiteUsercontrol

    Private Sub BlogArchivio_Init(sender As Object, e As EventArgs) Handles Me.Init
        BindElencoArchivio()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub BindElencoArchivio()

        Dim lst1 As New List(Of vwPost)
        Dim lst2 As New List(Of String)
        Dim lst3 As New List(Of String)

        Using dc As New Emmemedia.DataClass.DataEntities
            lst1 = (From p In dc.vwPost Select p).ToList
            lst2 = (From r In lst1 Where r.FlagVisibile = True And r.MacroPK = CInt(ConfigurationManager.AppSettings("RifBlog")) Order By r.Data Descending Select FormatDateTime(r.Data, DateFormat.ShortDate)).ToList
        End Using

        lst3 = lst2.AsQueryable().Select(Of String)(Function(p) MonthName(Month(p.ToString)) & " " & Year(p.ToString)).ToList

        Dim DistinctItems = lst3.GroupBy(Function(x) x.ToString).[Select](Function(y) y.First())

        lvArchivio.DataSource = DistinctItems.ToList
        lvArchivio.DataBind()

    End Sub

End Class