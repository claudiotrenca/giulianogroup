﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ArticoliElenco
    
    '''<summary>
    '''Controllo TipoRicerca.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents TipoRicerca As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo lvElencoArticoli.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents lvElencoArticoli As Global.System.Web.UI.WebControls.ListView

    '''<summary>
    '''Controllo dpBottom.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents dpBottom As UnorderedListDataPager.CustomControls.UnorderedListDataPager

    '''<summary>
    '''Controllo pnPAGINAZIONE.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents pnPAGINAZIONE As Global.System.Web.UI.WebControls.PlaceHolder
End Class
