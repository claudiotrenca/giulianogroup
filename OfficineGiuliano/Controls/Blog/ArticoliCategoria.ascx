﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ArticoliCategoria.ascx.vb" Inherits="Emmemedia.Controls.Blog.ArticoliCategoria" %>
<%@ Import Namespace="Emmemedia" %>

<asp:ListView ID="lvArticoli" runat="server">
    <LayoutTemplate>
        <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>
    <ItemTemplate>
        <div class="col-sm-6 col-md-3">
            <p class="date"><%# FormatDateTime(Eval("Data"), DateFormat.ShortDate) %></p>
            <p class="h3"><%# Eval("Titolo") %></p>
            <div class="image">
                <asp:Image ID="ArticoloImmagine" runat="server" CssClass="img-responsive" AlternateText='<%# Eval("Titolo") %>' />
            </div>
            <asp:HyperLink ID="ArticoloLink1" runat="server">leggi di più  </asp:HyperLink>
        </div>
    </ItemTemplate>
    <EmptyDataTemplate>
        <div class="col-sm-12 col-md-3">
            Non ci sono articoli
        </div>
    </EmptyDataTemplate>
</asp:ListView>