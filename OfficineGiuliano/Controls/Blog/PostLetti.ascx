﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PostLetti.ascx.vb" Inherits="Emmemedia.PostLetti" %>
<%@ Import Namespace="Emmemedia" %>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#PostLetti">Articoli Più Letti</a></h4>
    </div>
    <div id="PostLetti" class="panel-collapse collapse in">
        <asp:ListView ID="lvPostLetti" runat="server">
            <LayoutTemplate>
                <ul class="list-group">
                    <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                </ul>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="list-group-item"><a href="/blog/<%# DataBinder.Eval(Container.DataItem, "CategoriaLink") %>/<%# Classi.Utility.StringToUrl(DataBinder.Eval(Container.DataItem, "Titolo")) & "_" & Eval("PK") %>.htm"><%#DataBinder.Eval(Container.DataItem, "TITOLO")%></a></li>
            </ItemTemplate>
        </asp:ListView>
    </div>
</div>