<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CategorieBlog.ascx.vb" Inherits="Emmemedia.CategorieBlog" %>
<%@ Import Namespace="Emmemedia" %>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#CategorieBlog">Categorie</a></h4>
    </div>
    <div id="CategorieBlog" class="panel-collapse collapse in">
        <asp:ListView ID="lvCategorieBlog" runat="server">
            <LayoutTemplate>
                <ul class="list-group">
                    <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                </ul>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="list-group-item"><a href="/blog/<%# DataBinder.Eval(Container.DataItem, "CategoriaLink") %>/"><%#DataBinder.Eval(Container.DataItem, "Categoria")%></a></li>
            </ItemTemplate>
        </asp:ListView>
        
    </div>
   
</div>




 