﻿Imports Emmemedia.DataClass
Imports DevExpress.Web

Public Class TagCloud
   Inherits BaseClass.SiteUsercontrol

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      BindTagCloud()
   End Sub

   Private Sub TagCloud_Init(sender As Object, e As EventArgs) Handles Me.Init

   End Sub

   Private Sub BindTagCloud()

      Dim lst As New List(Of vwPost)
      Dim tagsDictionary As New SortedDictionary(Of String, Integer)()

      Using dc As New DataEntities()

         lst = (From p In dc.vwPost Select p).ToList

         Dim temp = (From row In lst Where row.FlagVisibile = True And row.MacroPK = CInt(ConfigurationManager.AppSettings("RifBlog")) Select row.Sezione).ToList
         Dim postCount As Integer = temp.Count()

         For Each tags As String In temp
            Dim k As String() = tags.Split(","c)
            For Each s As String In k
               If tagsDictionary.ContainsKey(Classi.Utility.StringToUrl(s)) Then
                  tagsDictionary(Classi.Utility.StringToUrl(s)) += 1
               Else
                  tagsDictionary.Add(Classi.Utility.StringToUrl(s), 1)
               End If
            Next
         Next
      End Using

      Dim lst1 As List(Of KeyValuePair(Of String, Integer)) = tagsDictionary.ToList

      CloudTags.DataSource = lst1
      CloudTags.TextField = "Key"
      CloudTags.ValueField = "Value"
      CloudTags.NavigateUrlField = "Key"
      CloudTags.Scale = Scales.Logarithmic
      CloudTags.NavigateUrlFormatString = "/blog/tag/{0}/"

      CloudTags.DataBind()

   End Sub

   Private Shared Function CompareKVP(ByVal x As KeyValuePair(Of String, Integer), ByVal y As KeyValuePair(Of String, Integer)) As Integer

      Return x.Value.CompareTo(y.Value)

   End Function

   Protected Sub CloudTags_ItemDataBound(ByVal sender As Object, ByVal e As CloudControlItemEventArgs) Handles CloudTags.ItemDataBound
      e.Item.Text = e.Item.Text.ToUpper()
   End Sub

End Class