Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class NameEmailID
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Role",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .Name = c.String(nullable := False, maxLength := 256)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .Index(Function(t) t.Name, unique := True, name := "RoleNameIndex")
            
            CreateTable(
                "dbo.CustomerRole",
                Function(c) New With
                    {
                        .UserId = c.Int(nullable := False),
                        .RoleId = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) New With { t.UserId, t.RoleId }) _
                .ForeignKey("dbo.Role", Function(t) t.RoleId, cascadeDelete := True) _
                .ForeignKey("dbo.Customer", Function(t) t.UserId, cascadeDelete := True) _
                .Index(Function(t) t.UserId) _
                .Index(Function(t) t.RoleId)
            
            CreateTable(
                "dbo.Customer",
                Function(c) New With
                    {
                        .PK = c.Int(nullable := False, identity := True),
                        .Attivo = c.Byte(),
                        .Cap = c.String(),
                        .CodiceFiscale = c.String(),
                        .Cognome = c.String(),
                        .Comune = c.String(),
                        .DataInserimento = c.DateTime(),
                        .DataUltimoAccesso = c.DateTime(),
                        .Fax = c.String(),
                        .Indirizzo = c.String(),
                        .Nazione = c.String(),
                        .Newsletter = c.Byte(),
                        .Nome = c.String(),
                        .PartitaIVA = c.String(),
                        .Provincia = c.String(),
                        .RagioneSociale = c.String(),
                        .Regione = c.String(),
                        .Sconto = c.Int(),
                        .Tel1 = c.String(),
                        .Tel2 = c.String(),
                        .Tipo = c.Int(),
                        .Email = c.String(maxLength := 256),
                        .EmailConfirmed = c.Boolean(nullable := False),
                        .PasswordHash = c.String(),
                        .SecurityStamp = c.String(),
                        .PhoneNumber = c.String(),
                        .PhoneNumberConfirmed = c.Boolean(nullable := False),
                        .TwoFactorEnabled = c.Boolean(nullable := False),
                        .LockoutEndDateUtc = c.DateTime(),
                        .LockoutEnabled = c.Boolean(nullable := False),
                        .AccessFailedCount = c.Int(nullable := False),
                        .UserName = c.String(nullable := False, maxLength := 256)
                    }) _
                .PrimaryKey(Function(t) t.PK) _
                .Index(Function(t) t.UserName, unique := True, name := "UserNameIndex")
            
            CreateTable(
                "dbo.CustomerClaim",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .UserId = c.Int(nullable := False),
                        .ClaimType = c.String(),
                        .ClaimValue = c.String()
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Customer", Function(t) t.UserId, cascadeDelete := True) _
                .Index(Function(t) t.UserId)
            
            CreateTable(
                "dbo.CustomerLogin",
                Function(c) New With
                    {
                        .LoginProvider = c.String(nullable := False, maxLength := 128),
                        .ProviderKey = c.String(nullable := False, maxLength := 128),
                        .UserId = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) New With { t.LoginProvider, t.ProviderKey, t.UserId }) _
                .ForeignKey("dbo.Customer", Function(t) t.UserId, cascadeDelete := True) _
                .Index(Function(t) t.UserId)
            
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.CustomerRole", "UserId", "dbo.Customer")
            DropForeignKey("dbo.CustomerLogin", "UserId", "dbo.Customer")
            DropForeignKey("dbo.CustomerClaim", "UserId", "dbo.Customer")
            DropForeignKey("dbo.CustomerRole", "RoleId", "dbo.Role")
            DropIndex("dbo.CustomerLogin", New String() { "UserId" })
            DropIndex("dbo.CustomerClaim", New String() { "UserId" })
            DropIndex("dbo.Customer", "UserNameIndex")
            DropIndex("dbo.CustomerRole", New String() { "RoleId" })
            DropIndex("dbo.CustomerRole", New String() { "UserId" })
            DropIndex("dbo.Role", "RoleNameIndex")
            DropTable("dbo.CustomerLogin")
            DropTable("dbo.CustomerClaim")
            DropTable("dbo.Customer")
            DropTable("dbo.CustomerRole")
            DropTable("dbo.Role")
        End Sub
    End Class
End Namespace
