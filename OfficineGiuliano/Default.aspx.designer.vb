﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class _Default

    '''<summary>
    '''Controllo VetrinaOfferte02.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents VetrinaOfferte02 As Global.Emmemedia.Controls.Prodotti.VetrinaOfferte02

    '''<summary>
    '''Controllo UpperPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents UpperPh As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''Controllo UpperContent.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents UpperContent As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Controllo MainPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents MainPh As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''Controllo MainContent.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents MainContent As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Controllo FooterPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents FooterPh As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''Controllo FooterContent.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents FooterContent As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Proprietà Master.
    '''</summary>
    '''<remarks>
    '''Proprietà generata automaticamente.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Emmemedia.SiteMaster
        Get
            Return CType(MyBase.Master, Emmemedia.SiteMaster)
        End Get
    End Property
End Class
