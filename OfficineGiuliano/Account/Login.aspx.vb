﻿Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin.Security
Imports Owin

Partial Public Class Login
    Inherits BaseClass.SitePage

    Function GetCustomerID() As String
        If User.Identity.Name <> "" Then
            Return Context.User.Identity.Name
        Else
            If Session("AnonUID") Is Nothing Then
                Session("AnonUID") = Guid.NewGuid()
            End If
            Return Session("AnonUID").ToString()
        End If
    End Function

    Private Sub Login_Init(sender As Object, e As EventArgs) Handles Me.Init

    End Sub

    Private Sub Login_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        '   RegisterHyperLink.NavigateUrl = "Register"
        '   Abilitare questa opzione dopo aver abilitato la conferma dell'account per la funzionalità di reimpostazione della password
        ForgotPasswordHyperLink.NavigateUrl = "Forgot"
        OpenAuthLogin.ReturnUrl = Request.QueryString("ReturnUrl")
        Dim returnUrl = HttpUtility.UrlEncode(Request.QueryString("ReturnUrl"))
        '   If Not [String].IsNullOrEmpty(returnUrl) Then
        '       RegisterHyperLink.NavigateUrl += "?ReturnUrl=" & returnUrl
        '   End If
    End Sub

    Private Sub Login_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

        InitContent()

    End Sub

    Private Sub InitContent()

        Dim _PkContent As Integer = DataClass.PostMenuMapping.GetPostIdFromMenu(Me.Master.Rif)

        _Content = DataClass.vwPost.GetDetail(_PkContent)

        If _Content IsNot Nothing Then

            SliderContent.Text = _Content.Slider

            _PageTitolo = _Content.Titolo
            _PageSottotitolo = _Content.SottoTitolo
            _PageSezione = _Content.Sezione
            _PageContent = _Content.Contenuto
            _PageFooterContent = _Content.ContenutoInferiore
            _PageUpperContent = _Content.ContenutoSuperiore
            _PageContenutoBreve = _Content.ContenutoBreve

            InitMeta()
        End If

    End Sub

    Private Sub BindContent()

        MainContent.Text = _PageContent
        UpperContent.Text = _PageUpperContent
        FooterContent.Text = _PageFooterContent

    End Sub

    Private Sub InitMeta()
        Dim MetaPage As New MetaClass
        MetaPage.MetaTitle = _Content.MetaTitle
        MetaPage.MetaDescription = _Content.MetaDescription
        MetaPage.MetaKeyword = _Content.MetaKeywords
        MetaPage.MetaCanonical = _Content.MetaCanonical

        Me.Master.Meta = MetaPage
        Me.Master.InitMetaTag()
    End Sub

    Protected Sub LogIn(sender As Object, e As EventArgs) Handles LoginButton.Click

        '  riattivare successivamente
        'If IsValid Then
        '    ' Convalidare la password utente
        '    Dim manager = Context.GetOwinContext().GetUserManager(Of ApplicationUserManager)()
        '    Dim signinManager = Context.GetOwinContext().GetUserManager(Of ApplicationSignInManager)()

        '    ' Questa opzione non calcola il numero di tentativi di accesso non riusciti per il blocco dell'account
        '    ' Per abilitare il conteggio degli errori di password per attivare il blocco, impostare shouldLockout: = True
        '    Dim result = signinManager.PasswordSignIn(LoginEmail.Text, LoginPassword.Text, RememberMe.Checked, shouldLockout:=False)

        '    Select Case result
        '        Case SignInStatus.Success
        '            IdentityHelper.RedirectToReturnUrl(Request.QueryString("ReturnUrl"), Response)
        '            Exit Select
        '        Case SignInStatus.LockedOut
        '            Response.Redirect("/Account/Lockout")
        '            Exit Select
        '        Case SignInStatus.RequiresVerification
        '            Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}",
        '                                         Request.QueryString("ReturnUrl"),
        '                                         RememberMe.Checked),
        '                           True)
        '            Exit Select
        '        Case Else
        '            FailureText.Text = "Tentativo di accesso non valido"
        '            ErrorMessage.Visible = True
        '            Exit Select
        '    End Select
        'End If

        Dim res As LoginStatus = DataClass.Utenti.Login(LoginEmail.Text, LoginPassword.Text)

        Select Case res
            Case LoginStatus.Success
                DataClass.CartMethod.MigrateShoppingCartItems(GetCustomerID, Session("IDUtente"))
                FormsAuthentication.RedirectFromLoginPage(Session("IDUtente"), True)

                Exit Select
            Case LoginStatus.Failed
                FailureText.Text = "Username o password errata"
                ErrorMessage.Visible = True
                Exit Select
            Case LoginStatus.Locked
                FailureText.Text = "Utenza non attiva"
                ErrorMessage.Visible = True
                Exit Select
            Case Else
                FailureText.Text = "Tentativo di accesso non valido"
                ErrorMessage.Visible = True
                Exit Select
        End Select

    End Sub

    Private Sub BtnRegistrati_Click(sender As Object, e As EventArgs) Handles BtnRegistrati.Click
        If Page.IsValid Then
            If DataClass.Utenti.CreateUser(RegNome.Text.Trim, RegCognome.Text.Trim, RegNome.Text.Trim & " " & RegCognome.Text.Trim, RegIndirizzo.Text.Trim, RegCitta.Text.Trim, RegEmail.Text.Trim, RegTelefono.Text.Trim, RegEmail.Text.Trim, RegPassword.Text.Trim) Then
                spnTitle.InnerText = "REGISTRAZIONE"
                spnMsg.InnerText = "La registrazione è completata "
            Else
                spnTitle.InnerText = "REGISTRAZIONE"
                spnMsg.InnerText = "La registrazione non è stata effettuata, esiste già un utente con la stessa email "
            End If
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "none", "<script>$(document).ready(function() {$('#modalPopUp').modal('show'); });</script>", False)
        End If

    End Sub

End Class