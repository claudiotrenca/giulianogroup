﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ForgotPassword
    
    '''<summary>
    '''Controllo Path.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents Path As Global.Emmemedia.Controls.Common.Path
    
    '''<summary>
    '''Controllo UpperPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents UpperPh As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Controllo UpperContent.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents UpperContent As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo loginForm.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents loginForm As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Controllo ErrorMessage.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ErrorMessage As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Controllo FailureText.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents FailureText As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo Email.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents Email As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Controllo DisplayEmail.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents DisplayEmail As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Controllo FooterPh.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents FooterPh As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Controllo FooterContent.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents FooterContent As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Proprietà Master.
    '''</summary>
    '''<remarks>
    '''Proprietà generata automaticamente.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Emmemedia.SiteMaster
        Get
            Return CType(MyBase.Master,Emmemedia.SiteMaster)
        End Get
    End Property
End Class
