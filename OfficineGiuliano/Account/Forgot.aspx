﻿<%@ Page Title="Password dimenticata" Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Forgot.aspx.vb" Inherits="Emmemedia.ForgotPassword" Async="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentUpperPh" runat="Server">
    <div class="container-fluid">
        <uc1:Path runat="server" ID="Path" />
        <asp:PlaceHolder ID="UpperPh" runat="server"></asp:PlaceHolder>
        <asp:Literal runat="Server" ID="UpperContent" />
    </div>
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentMainPh">
    <div class="container-fluid">
        <p class="sub-title">DIMENTICATO LA PASSWORD?</p>
        <p>Scrivi qui la tua email, ti aiuteremo a recuperare la password.</p>
        <br />
        <br />
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-4 control-label custom-label">Email</label>
                <div class="col-sm-8">
                    <input type="email" class="form-control custom-input">
                    <i class="ion-ios-at form-icon"></i>
                </div>
            </div>
            <br />
            <br />
            <div class="form-group text-center">
                <button type="submit" class="btn-secondary">RECUPERA PASSWORD</button>
            </div>
        </div>

        <h2><%: Title %>.</h2>

        <div class="row">
            <div class="col-md-8">
                <asp:PlaceHolder ID="loginForm" runat="server">
                    <div class="form-horizontal">
                        <h4>Password dimenticata?</h4>
                        <hr />
                        <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                            <p class="text-danger">
                                <asp:Literal runat="server" ID="FailureText" />
                            </p>
                        </asp:PlaceHolder>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Posta elettronica</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                    CssClass="text-danger" ErrorMessage="Il campo Posta elettronica è obbligatorio." />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" OnClick="Forgot" Text="Collegamento posta elettronica" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="DisplayEmail" Visible="false">
                    <p class="text-info">
                        Controllare la posta elettronica per reimpostare la password.
                    </p>
                </asp:PlaceHolder>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentFooterPh" runat="Server">
    <asp:PlaceHolder ID="FooterPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="FooterContent" />
</asp:Content>