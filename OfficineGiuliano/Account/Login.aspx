﻿<%@ Page Title="Accedi" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Login.aspx.vb" Inherits="Emmemedia.Login" Async="true" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="Server">
    <asp:Literal runat="Server" ID="SliderContent" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentUpperPh" runat="Server">
    <div class="container-fluid">
        <uc1:Path runat="server" ID="Path" />
        <asp:PlaceHolder ID="UpperPh" runat="server"></asp:PlaceHolder>
        <asp:Literal runat="Server" ID="UpperContent" />
    </div>
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentMainPh">
    <!--sterilfarma-club-->
    <asp:PlaceHolder ID="MainPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="MainContent" />
    <div class="sterilfarma-club module-short">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="box-sx">
                        <div class="top">
                            <h2 class="h2">Sono un utente giá registrato:</h2>
                            <h3 class="h3">Accedi a Sterilfarma Club</h3>
                            <div class="clearfix">&nbsp;</div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label custom-label">Email</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <asp:TextBox runat="server" ID="LoginEmail" CssClass="form-control custom-input" ValidationGroup="Login" />
                                            <span class="input-group-addon"><i class="ion-ios-at form-icon"></i></span>
                                        </div>
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="LoginEmail"
                                            CssClass="text-danger" ErrorMessage="Il campo Posta elettronica è obbligatorio." ValidationGroup="Login" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label custom-label">Password</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <asp:TextBox runat="server" ID="LoginPassword" TextMode="Password" CssClass="form-control custom-input" ValidationGroup="Login" />
                                            <span class="input-group-addon"><i class="ion-android-more-horizontal form-icon"></i></span>
                                        </div>
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="LoginPassword" CssClass="text-danger" ErrorMessage="Il campo Password è obbligatorio." ValidationGroup="Login" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-8">
                                        <div class="checkbox" style="padding-left: 20px;">
                                            <asp:CheckBox runat="server" ID="RememberMe" Text="Memorizza account" />
                                        </div>
                                    </div>
                                </div>
                                <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                                    <p class="text-danger">
                                        <asp:Literal runat="server" ID="FailureText" />
                                    </p>
                                </asp:PlaceHolder>
                                <br />
                                <br />
                                <div class="form-group text-center">
                                    <asp:Button runat="server" ID="LoginButton" Text="Accedi" CssClass="btn btn-secondary" ValidationGroup="Login" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="bot">
                            <p class="loud">DIMENTICATO LA PASSWORD?</p>

                            <asp:HyperLink runat="server" ID="ForgotPasswordHyperLink" ViewStateMode="Disabled"><p>Clicca qui per recuperare la password.</p></asp:HyperLink>
                            <br />
                            <br />
                            <br />
                        </div>
                        <div class="box-sx-2">
                            <h2 class="h2">Vuoi risparmiare tempo?</h2>
                            <h3 class="h3">Effettua l'accesso da:</h3>
                            <div class="clearfix">&nbsp;</div>
                            <section id="socialLoginForm">
                                <uc:OpenAuthProviders runat="server" ID="OpenAuthLogin" Visible="false" />
                            </section>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-dx">
                        <div class="top">
                            <p class="h4">DESIDERO REGISTRARMI</p>
                            <p class="sub-title">EFFETTUA LA REGISTRAZIONE AL SITO</p>

                            <br />
                            <br />
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label custom-label">Nome</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <asp:TextBox ID="RegNome" runat="server" CssClass="form-control custom-input" ValidationGroup="Reg" />
                                            <span class="input-group-addon" id="basic-addon1"><i class="ion-ios-person-outline form-icon"></i></span>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="RegNome" Display="Dynamic" ValidationGroup="Reg">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label custom-label">Cognome</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <asp:TextBox ID="RegCognome" runat="server" CssClass="form-control custom-input" ValidationGroup="Reg" />
                                            <span class="input-group-addon" id="basic-addon2"><i class="ion-ios-person-outline form-icon"></i></span>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="RegCognome" Display="Dynamic" ValidationGroup="Reg">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label custom-label">Indirizzo</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <asp:TextBox ID="RegIndirizzo" runat="server" CssClass="form-control custom-input" ValidationGroup="Reg" />
                                            <span class="input-group-addon" id="basic-addon3"><i class="ion-ios-home form-icon"></i></span>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="RegIndirizzo" Display="Dynamic" ValidationGroup="Reg">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label custom-label">Città</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <asp:TextBox ID="RegCitta" runat="server" CssClass="form-control custom-input" ValidationGroup="Reg" />
                                            <span class="input-group-addon" id="basic-addon4"><i class="ion-ios-home form-icon"></i></span>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="RegCitta" Display="Dynamic" ValidationGroup="Reg">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label custom-label">Email</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <asp:TextBox ID="RegEmail" runat="server" TextMode="Email" CssClass="form-control custom-input" ValidationGroup="Reg" />
                                            <span class="input-group-addon" id="basic-addon5"><i class="ion-ios-at form-icon"></i></span>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="RegEmail" Display="Dynamic" ValidationGroup="Reg">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Campo E-mail non corretto!"
                                            ForeColor="Red" Font-Size="11px" Display="Dynamic" ValidationGroup="Reg" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ControlToValidate="RegEmail">
                                        </asp:RegularExpressionValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label custom-label">Telefono</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <asp:TextBox ID="RegTelefono" runat="server" CssClass="form-control custom-input" ValidationGroup="Reg" />
                                            <span class="input-group-addon" id="basic-addon6"><i class="ion-ios-telephone-outline form-icon"></i></span>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="RegTelefono" Display="Dynamic" ValidationGroup="Reg">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label custom-label">Password</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <asp:TextBox ID="RegPassword" runat="server" CssClass="form-control custom-input" ValidationGroup="Reg" />
                                            <span class="input-group-addon" id="basic-addon7"><i class="ion-android-more-horizontal form-icon"></i></span>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="RegPassword" Display="Dynamic" ValidationGroup="Reg">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label custom-label">Conferma Password</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <asp:TextBox ID="RegConfermaPassowrd" runat="server" CssClass="form-control custom-input" ValidationGroup="Reg" />
                                            <span class="input-group-addon" id="basic-addon8"><i class="ion-android-more-horizontal form-icon"></i></span>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="RegConfermaPassowrd" Display="Dynamic" ValidationGroup="Reg">
                                        </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Campo Passowrd non uguale!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="RegConfermaPassowrd" Display="Dynamic" ValidationGroup="Reg" ControlToCompare="RegPassword">
                                        </asp:CompareValidator>
                                    </div>
                                </div>
                                <br />
                                <div class="checkbox">
                                    <label>
                                        <asp:CheckBox ID="ChkPrivacy" runat="server" Checked="true" />
                                        Dichiaro di aver letto l'informativa sulla privacy e autorizzo il trattamento dei miei dati personali *
                                    </label>
                                </div>
                                <br />
                                <br />
                                <div class="form-group text-center">
                                    <asp:Button ID="BtnRegistrati" runat="server" Text="REGISTRATI" class="btn btn-secondary" ValidationGroup="Reg" />
                                </div>
                            </div>
                        </div>

                        <br />
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentFooterPh" runat="Server">
    <asp:PlaceHolder ID="FooterPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="FooterContent" />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterScriptsPh" runat="server">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle" runat="server"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg" runat="server"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CHIUDI</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#modalPopUp").on("hidden.bs.modal", function () {
            location.href = "/";
        });
    </script>
</asp:Content>