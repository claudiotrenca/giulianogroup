﻿Imports System.Security.Claims
Imports System.Threading.Tasks
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin
Imports Microsoft.Owin.Security

Public Class ApplicationSignInManager
    Inherits SignInManager(Of ApplicationUser, Integer)
    Public Sub New(userManager As ApplicationUserManager, authenticationManager As IAuthenticationManager)
        MyBase.New(userManager, authenticationManager)
    End Sub

    Public Shared Function Create(options As IdentityFactoryOptions(Of ApplicationSignInManager), context As IOwinContext) As ApplicationSignInManager
        Return New ApplicationSignInManager(context.GetUserManager(Of ApplicationUserManager)(), context.Authentication)
    End Function

    Public Overrides Function CreateUserIdentityAsync(user As ApplicationUser) As Task(Of ClaimsIdentity)
        Return user.GenerateUserIdentityAsync(DirectCast(UserManager, ApplicationUserManager))
    End Function
End Class

' Configurare la gestione utenti dell'applicazione utilizzata in questa applicazione. UserManager viene definito in ASP.NET Identity e viene utilizzato dall'applicazione.
Public Class ApplicationUserManager
    Inherits UserManager(Of ApplicationUser, Integer)
    Public Sub New(store As IUserStore(Of ApplicationUser, Integer))
        MyBase.New(store)
    End Sub

    Public Shared Function Create(options As IdentityFactoryOptions(Of ApplicationUserManager), context As IOwinContext) As ApplicationUserManager
        Dim manager = New ApplicationUserManager(New CustomUserStore(context.[Get](Of ApplicationDbContext)()))
        ' Configure validation logic for usernames
        manager.UserValidator = New UserValidator(Of ApplicationUser, Integer)(manager) With {
              .AllowOnlyAlphanumericUserNames = False,
              .RequireUniqueEmail = True
        }

        ' Configurare la logica di convalida per le password
        manager.PasswordValidator = New PasswordValidator() With {
          .RequiredLength = 6,
          .RequireNonLetterOrDigit = True,
          .RequireDigit = True,
          .RequireLowercase = True,
          .RequireUppercase = True
        }
        ' Registrare i provider di autenticazione a due fattori. Questa applicazione usa il numero di telefono e gli indirizzi e-mail come metodi per ricevere un codice di verifica dell'utente.
        ' Si può scrivere un provider personalizzato e inserirlo qui.
        manager.RegisterTwoFactorProvider("Codice telefono", New PhoneNumberTokenProvider(Of ApplicationUser, Integer)() With {
          .MessageFormat = "Il codice di sicurezza è {0}"
        })
        manager.RegisterTwoFactorProvider("Codice e-mail", New EmailTokenProvider(Of ApplicationUser, Integer)() With {
          .Subject = "Codice di sicurezza",
          .BodyFormat = "Il codice di sicurezza è {0}"
        })

        ' Configurare le impostazioni predefinite per il blocco dell'utente
        manager.UserLockoutEnabledByDefault = True
        manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5)
        manager.MaxFailedAccessAttemptsBeforeLockout = 5

        manager.EmailService = New EmailService()
        manager.SmsService = New SmsService()
        Dim dataProtectionProvider = options.DataProtectionProvider
        If dataProtectionProvider IsNot Nothing Then
            manager.UserTokenProvider = New DataProtectorTokenProvider(Of ApplicationUser, Integer)(dataProtectionProvider.Create("ASP.NET Identity"))
        End If
        Return manager
    End Function
End Class

Public Class EmailService
    Implements IIdentityMessageService
    Public Function SendAsync(message As IdentityMessage) As Task Implements IIdentityMessageService.SendAsync
        ' Inserire qui la parte di codice del servizio di posta elettronica per l'invio di un messaggio.
        Return Task.FromResult(0)
    End Function
End Class

Public Class SmsService
    Implements IIdentityMessageService
    Public Function SendAsync(message As IdentityMessage) As Task Implements IIdentityMessageService.SendAsync
        ' Inserire qui la parte di codice del servizio SMS per l'invio di un SMS.
        Return Task.FromResult(0)
    End Function
End Class