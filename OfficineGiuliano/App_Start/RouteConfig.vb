﻿Imports System.Web.Routing
Imports Microsoft.AspNet.FriendlyUrls

Public Module RouteConfig
    Sub RegisterRoutes(ByVal routes As RouteCollection)
		Dim settings = New FriendlyUrlSettings()
		settings.AutoRedirectMode = RedirectMode.Permanent

		routes.LowercaseUrls = True
		routes.RouteExistingFiles = False

		routes.EnableFriendlyUrls(settings, New BugFixFriendlyUrlResolver())
	End Sub



	
End Module
