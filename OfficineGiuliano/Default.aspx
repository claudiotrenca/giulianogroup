﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="Emmemedia._Default" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<%@ Register Src="~/Controls/Blog/ArticoliElenco.ascx" TagPrefix="uc1" TagName="ArticoliElenco" %>
<%@ Register Src="~/Controls/Prodotti/VetrinaOfferte02.ascx" TagPrefix="uc1" TagName="VetrinaOfferte02" %>
<%@ Register Src="~/Controls/Agenzie/StoreLocator.ascx" TagPrefix="uc1" TagName="StoreLocator" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="Server">
    <div class="slider-home">

        <div class="container-fluid wrap">

            <div id="owl-slider-home" class="owl-carousel owl-theme">

                <div class="item">
                    <a href="">
                        <img src="images/slide-01.jpg" alt="">
                        <div class="caption">
                            <div class="col-sm-offset-4 col-sm-4">
                                <h2 class="h2">Sollevatori Manitou
                                </h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                </p>
                                <p class="btn">
                                    scoprili ora <i class="ion ion-chevron-right"></i>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="item">
                    <a href="">
                        <img src="images/slide-02.jpg" alt="">
                        <div class="caption">
                            <div class="col-sm-offset-4 col-sm-4">
                                <h2 class="h2">Mini Compressori Atlas Copco
                                </h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                </p>
                                <p class="btn">
                                    scoprili ora <i class="ion ion-chevron-right"></i>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentUpperPh" runat="Server">
    <uc1:VetrinaOfferte02 runat="server" ID="VetrinaOfferte02" Number="4" />
    <asp:PlaceHolder ID="UpperPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="UpperContent" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainPh" runat="Server">
    <asp:PlaceHolder ID="MainPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="MainContent" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentFooterPh" runat="Server">
    <asp:PlaceHolder ID="FooterPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="FooterContent" />
</asp:Content>