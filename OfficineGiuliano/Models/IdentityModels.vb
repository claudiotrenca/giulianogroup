﻿Imports System
Imports System.Threading.Tasks
Imports System.Security.Claims
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin.Security

Public Class ApplicationDbContext
    Inherits IdentityDbContext(Of ApplicationUser, Role, Integer, CustomerLogin, CustomerRole, CustomerClaim)
    Public Sub New()
        MyBase.New("Accesso") ' , throwIfV1Schema:=False
    End Sub

    Public Shared Function Create() As ApplicationDbContext
        Return New ApplicationDbContext
    End Function

    Protected Overrides Sub OnModelCreating(modelBuilder As System.Data.Entity.DbModelBuilder)
        MyBase.OnModelCreating(modelBuilder)

        modelBuilder.Entity(Of ApplicationUser)().ToTable("Customer").[Property](Function(p) p.Id).HasColumnName("PK")
        modelBuilder.Entity(Of CustomerRole)().ToTable("CustomerRole")
        modelBuilder.Entity(Of CustomerLogin)().ToTable("CustomerLogin")
        modelBuilder.Entity(Of CustomerClaim)().ToTable("CustomerClaim")
        modelBuilder.Entity(Of Role)().ToTable("Role")

    End Sub
End Class

' È possibile aggiungere i dati di profilo per l'utente con l'aggiunta di altre proprietà alla classe utente, si prega di visitare http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
'Public Class ApplicationUser
'    Inherits IdentityUser

'    Public Function GenerateUserIdentity(manager As ApplicationUserManager) As ClaimsIdentity
'        ' Tenere presente che il valore di authenticationType deve corrispondere a quello definito in CookieAuthenticationOptions.AuthenticationType
'        Dim userIdentity = manager.CreateIdentity(Me, DefaultAuthenticationTypes.ApplicationCookie)
'        ' Aggiungere qui i reclami utente personalizzati
'        Return userIdentity
'    End Function

'    Public Function GenerateUserIdentityAsync(manager As ApplicationUserManager) As Task(Of ClaimsIdentity)
'        Return Task.FromResult(GenerateUserIdentity(manager))
'    End Function
'End Class

Public Class ApplicationUser
    Inherits IdentityUser(Of Integer, CustomerLogin, CustomerRole, CustomerClaim)

    Public Property Attivo As System.Nullable(Of Byte)
    Public Property Cap As String
    Public Property CodiceFiscale As String
    Public Property Cognome As String
    Public Property Comune As String
    Public Property DataInserimento As System.Nullable(Of Date)
    Public Property DataUltimoAccesso As System.Nullable(Of Date)
    Public Property Fax As String
    Public Property Indirizzo As String
    Public Property Nazione As String
    Public Property Newsletter As System.Nullable(Of Byte)
    Public Property Nome As String
    Public Property PartitaIVA As String
    Public Property Provincia As String
    Public Property RagioneSociale As String
    Public Property Regione As String
    Public Property Sconto As System.Nullable(Of Integer)
    Public Property Tel1 As String
    Public Property Tel2 As String
    Public Property Tipo As System.Nullable(Of Integer)

    Public Async Function GenerateUserIdentityAsync(manager As ApplicationUserManager) As Task(Of ClaimsIdentity)
        Dim userIdentity = Await manager.CreateIdentityAsync(Me, DefaultAuthenticationTypes.ApplicationCookie)
        ' Add custom user claims here
        Return userIdentity
    End Function

End Class

Public Class CustomerClaim
    Inherits IdentityUserClaim(Of Integer)
End Class

Public Class CustomerLogin
    Inherits IdentityUserLogin(Of Integer)
End Class

Public Class CustomerRole
    Inherits IdentityUserRole(Of Integer)

End Class
Public Class CustomRoleStore
    Inherits RoleStore(Of Role, Integer, CustomerRole)
    Public Sub New(context As ApplicationDbContext)
        MyBase.New(context)
    End Sub
End Class

Public Class CustomUserStore
    Inherits UserStore(Of ApplicationUser, Role, Integer, CustomerLogin, CustomerRole, CustomerClaim)
    Public Sub New(context As ApplicationDbContext)
        MyBase.New(context)
    End Sub
End Class

Public Class IdentityHelper
    Public Const CodeKey As String = "code"
    Public Const ProviderNameKey As String = "providerName"
    Public Const UserIdKey As String = "userId"
    'Utilizzati per XSRF durante il collegamento degli account di accesso esterni
    Public Const XsrfKey As String = "xsrfKey"
    Public Shared Function GetCodeFromRequest(request As HttpRequest) As String
        Return request.QueryString(CodeKey)
    End Function

    Public Shared Function GetProviderNameFromRequest(request As HttpRequest) As String
        Return request.QueryString(ProviderNameKey)
    End Function
    Public Shared Function GetResetPasswordRedirectUrl(code As String, request As HttpRequest) As String
        Dim absoluteUri = "/Account/ResetPassword?" + CodeKey + "=" + HttpUtility.UrlEncode(code)
        Return New Uri(request.Url, absoluteUri).AbsoluteUri.ToString()
    End Function

    Public Shared Function GetUserConfirmationRedirectUrl(code As String, userId As String, request As HttpRequest) As String
        Dim absoluteUri = "/Account/Confirm?" + CodeKey + "=" + HttpUtility.UrlEncode(code) + "&" + UserIdKey + "=" + HttpUtility.UrlEncode(userId)
        Return New Uri(request.Url, absoluteUri).AbsoluteUri.ToString()
    End Function

    Public Shared Function GetUserIdFromRequest(request As HttpRequest) As String
        Return HttpUtility.UrlDecode(request.QueryString(UserIdKey))
    End Function
    Public Shared Sub RedirectToReturnUrl(returnUrl As String, response As HttpResponse)
        If Not [String].IsNullOrEmpty(returnUrl) AndAlso IsLocalUrl(returnUrl) Then
            response.Redirect(returnUrl)
        Else
            response.Redirect("~/")
        End If
    End Sub

    Private Shared Function IsLocalUrl(url As String) As Boolean
        Return Not String.IsNullOrEmpty(url) AndAlso ((url(0) = "/"c AndAlso (url.Length = 1 OrElse (url(1) <> "/"c AndAlso url(1) <> "\"c))) OrElse (url.Length > 1 AndAlso url(0) = "~"c AndAlso url(1) = "/"c))
    End Function
End Class

Public Class Role
    Inherits IdentityRole(Of Integer, CustomerRole)
    Public Sub New()
    End Sub
    Public Sub New(name__1 As String)
        Name = name__1
    End Sub
End Class
#Region "Helpers"
#End Region