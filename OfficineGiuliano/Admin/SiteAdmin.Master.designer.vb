﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Admin
    
    Partial Public Class SiteAdmin
        
        '''<summary>
        '''Controllo Head.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents Head As Global.System.Web.UI.WebControls.ContentPlaceHolder
        
        '''<summary>
        '''Controllo form1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
        
        '''<summary>
        '''Controllo ScriptManager1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
        
        '''<summary>
        '''Controllo NavMenuAdmin.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents NavMenuAdmin As Global.Emmemedia.NavMenuAdmin
        
        '''<summary>
        '''Controllo NavMenuAdminAside.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents NavMenuAdminAside As Global.Emmemedia.NavMenuAside
        
        '''<summary>
        '''Controllo ContentMainPh.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ContentMainPh As Global.System.Web.UI.WebControls.ContentPlaceHolder
        
        '''<summary>
        '''Controllo FooterPlaceHolder.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FooterPlaceHolder As Global.System.Web.UI.WebControls.ContentPlaceHolder
    End Class
End Namespace
