﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="NewsletterSubscription.aspx.vb" Inherits="Emmemedia.Admin.User.NewsletterSubscription" EnableTheming="true" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .form-horizontal .label-wrapper {
            display: table;
            float: right;
            min-height: 28px;
        }

            .form-horizontal .label-wrapper .control-label {
                display: table-cell;
            }
    </style>
    <script type="text/javascript">//<![CDATA[
        window.onload = function () {
            history.pushState(null, null, location.href);
            window.onpopstate = function (event) {
                history.go(1);
            };
        }//]]>

        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="content-header clearfix">
        <h1 class="pull-left">Iscrizioni Newsletter
        </h1>
        <div class="pull-right">
            <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn bg-green"> <i class="fa fa-download"></i>
                Esporta in CSV</asp:LinkButton>

            <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn bg-olive"> <i class="fa fa-upload"></i>
                Importa da un CSV</asp:LinkButton>
        </div>
    </div>
    <div class="content">
        <div class="form-horizontal">
            <div class="panel-group">
                <div class="panel panel-default panel-search">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <div class="label-wrapper">
                                            <label class="control-label" for="StartDate" title="">Start date</label><div class="ico-help" title="The start date for the search.">&nbsp;<i class="fa fa-question-circle"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group ">
                                            <asp:TextBox ID="FormStartDate" runat="server" CssClass="form-control"></asp:TextBox>
                                            <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <div class="label-wrapper">
                                            <label class="control-label" for="EndDate" title="">End date</label><div class="ico-help" title="The end date for the search.">&nbsp;<i class="fa fa-question-circle"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <asp:TextBox ID="FormEndDate" runat="server" CssClass="form-control"></asp:TextBox>
                                            <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <div class="label-wrapper">
                                            <label class="control-label" for="SearchEmail" title="">Email</label><div class="ico-help" title="Search by a specific email.">&nbsp;<i class="fa fa-question-circle"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <asp:TextBox ID="FormEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                            <div class="input-group-addon"><span class="fa fa-envelope"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <div class="label-wrapper">
                                            <label class="control-label" for="ActiveId" title="">Active</label><div class="ico-help" title="Search by a specific status e.g. Active.">&nbsp;<i class="fa fa-question-circle"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control">
                                            <asp:ListItem Selected="True" Value="0">Tutti</asp:ListItem>
                                            <asp:ListItem Value="1">Attivo</asp:ListItem>
                                            <asp:ListItem Value="2">Non Attivo</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <asp:LinkButton ID="btnCerca" runat="server" CssClass="btn btn-primary btn-search"><i class="fa fa-search"></i>
                                            CERCA</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <dx:ASPxGridView ID="Grid" runat="server" DataSourceID="eds" KeyFieldName="PK" Width="100%" AutoGenerateColumns="False" Theme="Moderno">
                            <Columns>
                                <dx:GridViewDataColumn FieldName="PK" Caption="ID" VisibleIndex="4" CellStyle-CssClass="col-sm-1" />
                                <dx:GridViewDataColumn FieldName="Email" Caption="E-mail" VisibleIndex="6" CellStyle-CssClass="col-sm-2" />
                                <dx:GridViewDataColumn FieldName="DataInserimento" Caption="Data Inserimento" VisibleIndex="6" CellStyle-CssClass="col-sm-2" />
                                <dx:GridViewDataCheckColumn FieldName="Attivo" VisibleIndex="3" CellStyle-CssClass="col-md-1">
                                    <DataItemTemplate>
                                        <dx:ASPxCheckBox ID="chk" runat="server" AllowGrayed="false" Value='<%#Eval("Attivo")%>' OnInit="chk_Init" CssClass="checkbox">
                                        </dx:ASPxCheckBox>
                                    </DataItemTemplate>
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataCheckColumn>
                                <dx:GridViewDataTextColumn FieldName="" VisibleIndex="16">
                                    <DataItemTemplate>
                                        <asp:LinkButton ID="ASPxButton1" runat="server" Text="Elimina" CssClass="btn btn-danger danger" CommandArgument="elimina" OnClientClick="return getConfirmation(this, 'Attenzione','Confermi l\'eliminazione ?');"></asp:LinkButton>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowFooter="True" ShowFilterRow="True" ShowGroupPanel="false" ShowStatusBar="Auto" />
                            <Styles AlternatingRow-Enabled="True" AlternatingRow-BackColor="WhiteSmoke"></Styles>
                            <SettingsSearchPanel Visible="true" />
                        </dx:ASPxGridView>
                        <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="CheckboxGridCB" OnCallback="CheckboxGrid_Callback"></dx:ASPxCallback>
                    </div>
                    <ef:EntityDataSource ID="eds" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EntitySetName="NewsLetterSubscription" OrderBy="it.PK  DESC">
                    </ef:EntityDataSource>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterPlaceHolder" runat="server">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">
                        ELIMINA</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>