﻿Imports DevExpress.Data.Linq
Imports DevExpress.Web

Namespace Admin
   Namespace User

      Public Class NewsletterSubscription
         Inherits System.Web.UI.Page

         Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not IsPostBack Then
               Grid.DataBind()
            End If

         End Sub

         Private Sub NewsletterSubscription_Init(sender As Object, e As EventArgs) Handles Me.Init

         End Sub

         Protected Sub chk_Init(ByVal sender As Object, ByVal e As EventArgs)
            Dim chk As ASPxCheckBox = TryCast(sender, ASPxCheckBox)
            Dim container As GridViewDataItemTemplateContainer = TryCast(chk.NamingContainer, GridViewDataItemTemplateContainer)
            chk.ClientSideEvents.CheckedChanged = String.Format("function (s, e) {{ CheckboxGridCB.PerformCallback('{0}|' + s.GetChecked()); }}", container.KeyValue)
         End Sub

         Protected Sub CheckboxGrid_Callback(source As Object, e As CallbackEventArgs)

            Dim parameter() As String = e.Parameter.Split("|"c)
            Dim _PK As Integer = parameter(0)
            Dim _Checked As Boolean = parameter(1)
            Using dataContext As New DataClass.DataEntities
               Dim obj = dataContext.NewsLetterSubscription.FirstOrDefault(Function(p) p.PK = _PK)

               With obj
                  .Attivo = _Checked
               End With

               Try
                  dataContext.SaveChanges()
               Catch ex As Exception
                  Throw ex
               End Try
            End Using

         End Sub

         Private Sub ASPxGridView1_RowCommand(sender As Object, e As ASPxGridViewRowCommandEventArgs) Handles Grid.RowCommand
            If e.CommandArgs.CommandArgument = "elimina" Then
               '   DataClass.NewsLetterSubscription.Delete(e.KeyValue)

               Grid.DataBind()
            End If
         End Sub

         Private Sub btnCerca_Click(sender As Object, e As EventArgs) Handles btnCerca.Click

            If Not String.IsNullOrEmpty(FormStartDate.Text) And Not String.IsNullOrEmpty(FormEndDate.Text) Then
               Grid.FilterExpression = "DataInserimento >= #" & FormatDateTime(FormStartDate.Text, DateFormat.ShortDate) & "# AND DataInserimento <= #" & FormatDateTime(FormEndDate.Text, DateFormat.ShortDate) & "#"
            ElseIf Not String.IsNullOrEmpty(FormStartDate.Text) Then
               Grid.FilterExpression = "DataInserimento >= #" & FormatDateTime(FormStartDate.Text, DateFormat.ShortDate) & "#"
            ElseIf Not String.IsNullOrEmpty(FormEndDate.Text) Then
               Grid.FilterExpression = "DataInserimento <= #" & FormatDateTime(FormEndDate.Text, DateFormat.ShortDate) & "#"
            End If

            If Not String.IsNullOrEmpty(FormEmail.Text) Then
               Grid.FilterExpression = "(" & Grid.FilterExpression & ") AND Email like '%" & FormEmail.Text & "%'"
            End If

            Grid.DataBind()
         End Sub
      End Class
   End Namespace
End Namespace