﻿Namespace Admin

    Public Class SiteAdmin
        Inherits System.Web.UI.MasterPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        End Sub

        Private Sub SiteAdmin_Init(sender As Object, e As EventArgs) Handles Me.Init
            If Session("AdminLogin") = Nothing Then
                Response.Redirect("/admin/Login")
            End If
        End Sub

    End Class

End Namespace