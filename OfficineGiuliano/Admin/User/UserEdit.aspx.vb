﻿Namespace Admin
    Namespace User
        Public Class UserEdit
            Inherits System.Web.UI.Page

            Private _PK As Integer = Nothing

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                If Not IsPostBack Then
                    If Request.QueryString("id") IsNot Nothing Then
                        _PK = Request.QueryString("id")
                        ViewState("PK") = _PK

                        BindData()

                        '   btnElimina.Enabled = True
                    Else
                        '   LabelModalità.Text = "NUOVO UTENTE"
                        '   btnElimina.Enabled = False
                    End If
                Else
                    _PK = ViewState("PK")
                End If
            End Sub

            Private Sub BindData()
                Dim row As New DataClass.Utenti

                Using dataContext As New DataClass.DataEntities
                    row = dataContext.Utenti.FirstOrDefault(Function(p) p.PK = _PK)
                End Using

                If row IsNot Nothing Then
                    UtenteNome.Text = row.Nome
                    UtenteCognome.Text = row.Cognome
                    UtenteRagioneSociale.Text = row.RagioneSociale
                    UtenteRegione.Text = row.Regione
                    UtenteTelefono.Text = row.Telefono
                    UtenteUsername.Text = row.Username
                    UtenteCAP.Text = row.Cap
                    UtenteCellulare.Text = row.Cellulare
                    UtenteCodice.Text = row.PK
                    UtenteCodiceFiscale.Text = row.CodiceFiscale
                    UtenteComune.Text = row.Comune
                    UtenteIndirizzo.Text = row.Indirizzo
                    UtenteMail.Text = row.Email
                    UtentePartitaIVA.Text = row.PartitaIVA
                    UtentePassword.Text = row.Password
                    UtenteProvincia.Text = row.Provincia
                    UtenteAttivo.Checked = row.FlagAttivo
                End If
            End Sub

            Private Sub btnSalva_Click(sender As Object, e As EventArgs) Handles btnSalva.Click
                SaveData()
            End Sub

            Private Sub SaveData()

                Using context As New DataClass.DataEntities

                    If _PK = Nothing Then

                        Dim row As New DataClass.Utenti
                        row.Nome = UtenteNome.Text
                        row.Cognome = UtenteCognome.Text
                        row.RagioneSociale = UtenteRagioneSociale.Text
                        row.Regione = UtenteRegione.Text
                        row.Telefono = UtenteTelefono.Text
                        row.Username = UtenteUsername.Text
                        row.Cap = UtenteCAP.Text
                        row.Cellulare = UtenteCellulare.Text
                        row.PK = UtenteCodice.Text
                        row.CodiceFiscale = UtenteCodiceFiscale.Text
                        row.Comune = UtenteComune.Text
                        row.Indirizzo = UtenteIndirizzo.Text
                        row.Email = UtenteMail.Text
                        row.PartitaIVA = UtentePartitaIVA.Text
                        row.Password = UtentePassword.Text
                        row.Provincia = UtenteProvincia.Text
                        row.FlagAttivo = UtenteAttivo.Checked

                        context.Configuration.AutoDetectChangesEnabled = True
                        context.Utenti.Attach(row)
                        context.Entry(row).State = Entity.EntityState.Added
                        context.SaveChanges()
                    Else
                        Dim row = context.Utenti.SingleOrDefault(Function(p) p.PK = _PK)
                        row.Nome = UtenteNome.Text
                        row.Cognome = UtenteCognome.Text
                        row.RagioneSociale = UtenteRagioneSociale.Text
                        row.Regione = UtenteRegione.Text
                        row.Telefono = UtenteTelefono.Text
                        row.Username = UtenteUsername.Text
                        row.Cap = UtenteCAP.Text
                        row.Cellulare = UtenteCellulare.Text
                        row.PK = UtenteCodice.Text
                        row.CodiceFiscale = UtenteCodiceFiscale.Text
                        row.Comune = UtenteComune.Text
                        row.Indirizzo = UtenteIndirizzo.Text
                        row.Email = UtenteMail.Text
                        row.PartitaIVA = UtentePartitaIVA.Text
                        row.Password = UtentePassword.Text
                        row.Provincia = UtenteProvincia.Text
                        row.FlagAttivo = UtenteAttivo.Checked
                        context.SaveChanges()
                    End If

                End Using

            End Sub

        End Class
    End Namespace
End Namespace