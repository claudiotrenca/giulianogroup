﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="UserList.aspx.vb" Inherits="Emmemedia.Admin.User.UserList" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function onCellClick(rowIndex, fieldName) {
            grwPage.PerformCallback(rowIndex + "|" + fieldName);
        }
        function OnEditorKeyPress(editor, e) {
            if (e.htmlEvent.keyCode == 13 || e.htmlEvent.keyCode == 9) {
                grwPage.UpdateEdit();
            }
            else
                if (e.htmlEvent.keyCode == 27)
                    grwPage.CancelEdit();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>MENU
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default" NavigateUrl="~/Admin/content/ContentEdit.aspx">AGGIUNGI NUOVO</asp:HyperLink>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-inline">
                <div class="form-group">
                </div>
                <asp:Button ID="Button1" runat="server" Text="Applica il filtro" CssClass="btn btn-default BtnFiltra" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <dx:ASPxGridView ID="grwPage" ClientInstanceName="grwPage" runat="server" DataSourceID="emdPage" Width="100%" AutoGenerateColumns="False" EnableTheming="True" CssClass="table " KeyFieldName="PK" Theme="Moderno">
                <SettingsSearchPanel Visible="True" />
                <Settings ShowFilterRow="true" ShowGroupPanel="False" />
                <SettingsEditing Mode="Inline" />
                <Columns>
                    <dx:GridViewDataColumn FieldName="RagioneSociale" Caption="Titolo" VisibleIndex="1" />
                    <dx:GridViewDataColumn FieldName="Comune" VisibleIndex="2" />
                    <dx:GridViewDataCheckColumn FieldName="FlagAttivo" VisibleIndex="3" CellStyle-CssClass="col-md-1">
                        <DataItemTemplate>
                            <dx:ASPxCheckBox ID="chk" runat="server" AllowGrayed="false" Value='<%#Eval("FlagAttivo")%>' OnInit="chk_Init" CssClass="checkbox">
                            </dx:ASPxCheckBox>
                        </DataItemTemplate>
                        <EditFormSettings Visible="False" />
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewDataTextColumn FieldName="" VisibleIndex="3">
                        <DataItemTemplate>

                            <asp:LinkButton ID="btn" runat="server" Text="Seleziona" CssClass="btn btn-default" CommandArgument="seleziona"></asp:LinkButton>
                            <asp:LinkButton ID="ASPxButton1" runat="server" Text="Elimina" CssClass="btn btn-danger" CommandArgument="elimina" OnClientClick="return getConfirmation(this, 'Attenzione','Confermi l\'eliminazione ?');"></asp:LinkButton>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
            <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="grwPageCB" OnCallback="grwPage_Callback"></dx:ASPxCallback>
            <dx:EntityServerModeDataSource ID="emdPage" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EnableDelete="True" EnableUpdate="True" TableName="Utenti" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterPlaceHolder" runat="server">
</asp:Content>