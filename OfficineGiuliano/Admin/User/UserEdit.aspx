﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="UserEdit.aspx.vb" Inherits="Emmemedia.Admin.User.UserEdit" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPh" runat="server">
    <div class="row">
        <div class="col-md-6">
            <h3>NUOVO PRODOTTO</h3>
        </div>
        <div class="col-md-6 text-right">
            <br />

            <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text=" TORNA ALL'ELENCO " NavigateUrl="~/Admin/User/UserList.aspx" CssClass="btn btn-default">
            </dx:ASPxHyperLink>
            &nbsp;
               <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default" NavigateUrl="~/Admin/User/UserEdit.aspx">AGGIUNGI NUOVO</asp:HyperLink>
            &nbsp;
            <dx:ASPxButton ID="btnSalva" runat="server" Text="SALVA UTENTE" CssClass="btn btn-default btn-danger" Theme="Moderno"></dx:ASPxButton>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="100%" Theme="Moderno">
                <TabPages>
                    <dx:TabPage Name="Anagrafica" Text="Anagrafica">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="UtenteCodice" class="col-md-2 control-label">CODICE</label>
                                        <div class="col-md-1">
                                            <asp:TextBox ID="UtenteCodice" runat="server" ClientIDMode="Static" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="UtenteCognome" class="col-md-2 control-label">Cognome</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteCognome" runat="server" CssClass="form-control" ClientInstanceName="UtenteCognome">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="UtenteNome" class="col-md-2 control-label">Nome</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteNome" runat="server" CssClass="form-control" ClientInstanceName="UtenteNome">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="UtenteRagioneSociale" class="col-md-2 control-label">Ragione Sociale</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteRagioneSociale" runat="server" CssClass="form-control" ClientInstanceName="UtenteRagioneSociale">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="UtenteCodiceFiscale" class="col-md-2 control-label">Codice Fiscale</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteCodiceFiscale" runat="server" CssClass="form-control" ClientInstanceName="UtenteCodiceFiscale">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="UtentePartitaIVA" class="col-md-2 control-label">Partita IVA</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtentePartitaIVA" runat="server" CssClass="form-control" ClientInstanceName="UtentePartitaIVA">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                </div>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Recapiti" Text="Recapiti">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="UtenteIndirizzo" class="col-md-2 control-label">Indirizzo</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteIndirizzo" runat="server" CssClass="form-control" ClientInstanceName="UtenteIndirizzo">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="UtenteCAP" class="col-md-2 control-label">CAP</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteCAP" runat="server" CssClass="form-control" ClientInstanceName="UtenteCAP">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="UtenteRegione" class="col-md-2 control-label">Regione</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteRegione" runat="server" CssClass="form-control" ClientInstanceName="UtenteRegione">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="UtenteProvincia" class="col-md-2 control-label">Provincia</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteProvincia" runat="server" CssClass="form-control" ClientInstanceName="UtenteProvincia">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="UtenteComune" class="col-md-2 control-label">Comune</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteComune" runat="server" CssClass="form-control" ClientInstanceName="UtenteComune">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                </div>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Contatti" Text="Contatti">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="UtenteTelefono" class="col-md-2 control-label">Telefono</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteTelefono" runat="server" CssClass="form-control" ClientInstanceName="UtenteTelefono">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="UtenteCellulare" class="col-md-2 control-label">Cellulare</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteCellulare" runat="server" CssClass="form-control" ClientInstanceName="UtenteCellulare">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="UtenteMail" class="col-md-2 control-label">Mail</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteMail" runat="server" CssClass="form-control" ClientInstanceName="UtenteMail">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                </div>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Accesso" Text="Accesso Sito">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="UtenteUsername" class="col-md-2 control-label">Username</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtenteUsername" runat="server" CssClass="form-control" ClientInstanceName="UtenteUsername">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="UtentePassword" class="col-md-2 control-label">Password</label>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="UtentePassword" runat="server" CssClass="form-control" ClientInstanceName="UtentePassword">
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <hr />

                                    <div class="form-group">
                                        <div class="col-md-offset-2 col-md-10">
                                            <label for="UtenteAttivo">
                                                <dx:ASPxCheckBox ID="UtenteAttivo" runat="server" Theme="Moderno"></dx:ASPxCheckBox>
                                                Attivo</label>
                                        </div>
                                    </div>
                                </div>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>
            </dx:ASPxPageControl>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FooterPlaceHolder" runat="server">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>