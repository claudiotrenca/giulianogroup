﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Admin.User
    
    Partial Public Class UserEdit
        
        '''<summary>
        '''Controllo ASPxHyperLink1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ASPxHyperLink1 As Global.DevExpress.Web.ASPxHyperLink
        
        '''<summary>
        '''Controllo HyperLink1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HyperLink1 As Global.System.Web.UI.WebControls.HyperLink
        
        '''<summary>
        '''Controllo btnSalva.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnSalva As Global.DevExpress.Web.ASPxButton
        
        '''<summary>
        '''Controllo ASPxPageControl1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ASPxPageControl1 As Global.DevExpress.Web.ASPxPageControl
        
        '''<summary>
        '''Controllo UtenteCodice.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteCodice As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo UtenteCognome.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteCognome As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteNome.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteNome As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteRagioneSociale.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteRagioneSociale As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteCodiceFiscale.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteCodiceFiscale As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtentePartitaIVA.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtentePartitaIVA As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteIndirizzo.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteIndirizzo As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteCAP.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteCAP As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteRegione.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteRegione As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteProvincia.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteProvincia As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteComune.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteComune As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteTelefono.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteTelefono As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteCellulare.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteCellulare As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteMail.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteMail As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteUsername.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteUsername As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtentePassword.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtentePassword As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo UtenteAttivo.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UtenteAttivo As Global.DevExpress.Web.ASPxCheckBox
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.Admin.SiteAdmin
            Get
                Return CType(MyBase.Master,Emmemedia.Admin.SiteAdmin)
            End Get
        End Property
    End Class
End Namespace
