﻿Imports DevExpress.Data.Linq
Imports DevExpress.Web
Imports DevExpress.Web.ASPxTreeList

Namespace Admin
	Namespace User
		Public Class UserList
			Inherits System.Web.UI.Page

			Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
				If Not IsPostBack Then
					grwPage.DataBind()
				End If
			End Sub

			Protected Sub chk_Init(ByVal sender As Object, ByVal e As EventArgs)
				Dim chk As ASPxCheckBox = TryCast(sender, ASPxCheckBox)
				Dim container As GridViewDataItemTemplateContainer = TryCast(chk.NamingContainer, GridViewDataItemTemplateContainer)
				chk.ClientSideEvents.CheckedChanged = String.Format("function (s, e) {{ grwPageCB.PerformCallback('{0}|' + s.GetChecked()); }}", container.KeyValue)
			End Sub

			Protected Sub grwPage_Callback(source As Object, e As CallbackEventArgs)

				Dim parameter() As String = e.Parameter.Split("|"c)
				Dim _PK As Integer = parameter(0)
				Dim _Checked As Boolean = parameter(1)
				Using dataContext As New DataClass.DataEntities
					Dim obj = dataContext.Utenti.FirstOrDefault(Function(p) p.PK = _PK)

					With obj
						.FlagAttivo = _Checked
					End With

					Try
						dataContext.SaveChanges()
					Catch ex As Exception
						Throw ex
					End Try
				End Using

			End Sub

			Private Sub EntityServerModeDataSource1_Selecting(sender As Object, e As LinqServerModeDataSourceSelectEventArgs) Handles emdPage.Selecting
				Dim dataContext As New DataClass.DataEntities
				e.KeyExpression = "PK"
			End Sub

			Private Sub grwPage_RowCommand(sender As Object, e As ASPxGridViewRowCommandEventArgs) Handles grwPage.RowCommand
				If e.CommandArgs.CommandArgument = "seleziona" Then
					Response.Redirect("/admin/user/useredit.aspx?id=" & e.KeyValue)
				ElseIf e.CommandArgs.CommandArgument = "elimina" Then
					DataClass.Utenti.Delete(e.KeyValue)
				End If
			End Sub

		End Class
	End Namespace
End Namespace