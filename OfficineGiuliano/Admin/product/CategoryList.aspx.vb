﻿Imports DevExpress.Data.Linq
Imports DevExpress.Web


Namespace Admin
    Namespace Product
        Public Class CategoryList
            Inherits System.Web.UI.Page

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                If Not IsPostBack Then
                    ASPxGridView1.DataBind()
                End If
            End Sub

            Protected Sub detailGrid_DataSelect(ByVal sender As Object, ByVal e As EventArgs)
                Session("PK1") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
            End Sub

            Protected Sub detailGrid2_DataSelect(ByVal sender As Object, ByVal e As EventArgs)
                Session("PK2") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
            End Sub

            Protected Sub chkSingleExpanded_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ASPxGridView1.DetailRowExpandedChanged
                If ASPxGridView1.SettingsDetail.AllowOnlyOneMasterRowExpanded Then
                    ASPxGridView1.DetailRows.CollapseAllRows()
                End If
            End Sub

            Private Sub EntityServerModeDataSource1_Selecting(sender As Object, e As LinqServerModeDataSourceSelectEventArgs) Handles EntityServerModeDataSource1.Selecting
                Dim dataContext As New DataClass.DataEntities
                e.KeyExpression = "PK"
                e.QueryableSource = dataContext.Menu.Where(Function(p) p.ParentID Is Nothing And p.Tipo = "categoria")
            End Sub

            Private Sub EntityServerModeDataSource2_Selecting(sender As Object, e As LinqServerModeDataSourceSelectEventArgs) Handles EntityServerModeDataSource2.Selecting
                Dim dataContext As New DataClass.DataEntities
                Dim _parentID As Integer = Session("PK1")
                e.KeyExpression = "PK"
                e.QueryableSource = dataContext.Menu.Where(Function(p) p.ParentID.Value = _parentID And p.Tipo = "categoria")
            End Sub

            Private Sub EntityServerModeDataSource3_Selecting(sender As Object, e As LinqServerModeDataSourceSelectEventArgs) Handles EntityServerModeDataSource3.Selecting
                Dim dataContext As New DataClass.DataEntities
                Dim _parentID As Integer = Session("PK2")
                e.KeyExpression = "PK"
                e.QueryableSource = dataContext.Menu.Where(Function(p) p.ParentID.Value = _parentID And p.Tipo = "categoria")
            End Sub

            Protected Sub chk_Init(ByVal sender As Object, ByVal e As EventArgs)
                Dim chk As ASPxCheckBox = TryCast(sender, ASPxCheckBox)
                Dim container As GridViewDataItemTemplateContainer = TryCast(chk.NamingContainer, GridViewDataItemTemplateContainer)
                chk.ClientSideEvents.CheckedChanged = String.Format("function (s, e) {{ CheckboxGridCB.PerformCallback('{0}|' + s.GetChecked()); }}", container.KeyValue)
            End Sub

            Protected Sub CheckboxGrid_Callback(source As Object, e As CallbackEventArgs)

                Dim parameter() As String = e.Parameter.Split("|"c)
                Dim _PK As Integer = parameter(0)
                Dim _Checked As Boolean = parameter(1)
                Using dataContext As New DataClass.DataEntities
                    Dim obj = dataContext.Menu.FirstOrDefault(Function(p) p.PK = _PK)

                    With obj
                        .FlagVisibile = _Checked
                    End With

                    Try
                        dataContext.SaveChanges()
                    Catch ex As Exception
                        Throw ex
                    End Try
                End Using

            End Sub


        End Class
    End Namespace
End Namespace
