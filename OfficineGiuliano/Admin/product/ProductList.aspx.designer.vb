﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Admin.Product
    
    Partial Public Class ProductList
        
        '''<summary>
        '''Controllo HyperLink1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HyperLink1 As Global.System.Web.UI.WebControls.HyperLink
        
        '''<summary>
        '''Controllo MenuMacro.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MenuMacro As Global.DevExpress.Web.ASPxDropDownEdit
        
        '''<summary>
        '''Controllo edsCategorie.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents edsCategorie As Global.Microsoft.AspNet.EntityDataSource.EntityDataSource
        
        '''<summary>
        '''Controllo Button1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents Button1 As Global.System.Web.UI.WebControls.Button
        
        '''<summary>
        '''Controllo ASPxGridView1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ASPxGridView1 As Global.DevExpress.Web.ASPxGridView
        
        '''<summary>
        '''Controllo edsProdotti.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents edsProdotti As Global.Microsoft.AspNet.EntityDataSource.EntityDataSource
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.Admin.SiteAdmin
            Get
                Return CType(MyBase.Master,Emmemedia.Admin.SiteAdmin)
            End Get
        End Property
    End Class
End Namespace
