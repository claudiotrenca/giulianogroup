﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="CategoryEdit.aspx.vb" Inherits="Emmemedia.Admin.Product.CategoryEdit" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>

<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">//<![CDATA[
        window.onload = function () {
            history.pushState(null, null, location.href);
            window.onpopstate = function (event) {
                history.go(1);
            };
        }//]]>

        function ButtonClickHandler(s, e) {
            if (e.buttonIndex == 0) {
                MenuMacro.SetKeyValue(null);
                SynchronizeFocusedNode();
            }
        }
        function DropDownHandler(s, e) {
            SynchronizeFocusedNode();
        }
        function TreeListInitHandler(s, e) {
            SynchronizeFocusedNode();
        }
        function TreeListEndCallbackHandler(s, e) {
            MenuMacro.SetKeyValue(TreeList.GetFocusedNodeKey())
            MenuMacro.AdjustDropDownWindow();
            UpdateEditBox();
        }
        function TreeListNodeClickHandler(s, e) {
            MenuMacro.SetKeyValue(e.nodeKey);
            MenuMacro.SetText(TreeList.cpCategoryNames[e.nodeKey]);
            MenuMacro.HideDropDown();
        }
        function SynchronizeFocusedNode() {
            var keyValue = MenuMacro.GetKeyValue();
            TreeList.SetFocusedNodeKey(keyValue);
            UpdateEditBox();
        }
        function UpdateEditBox() {
            var focusedCategoryName = '';
            var nodeKey = TreeList.GetFocusedNodeKey();
            if (nodeKey != 'null' && nodeKey != '')
                focusedCategoryName = TreeList.cpCategoryNames[nodeKey];
            var CategoryNameInEditBox = MenuMacro.GetText();
            if (CategoryNameInEditBox != focusedCategoryName)
                MenuMacro.SetText(focusedCategoryName);
        }

        // CategoryMenuLinkTree
        function CategoryMenuLinkTreeButtonClickHandler(s, e) {
            if (e.buttonIndex == 0) {
                CategoryMenuLink.SetKeyValue(null);
                CategoryMenuLinkTreeSynchronizeFocusedNode();
            }
        }
        function CategoryMenuLinkTreeDropDownHandler(s, e) {
            CategoryMenuLinkTreeSynchronizeFocusedNode();
        }
        function CategoryMenuLinkTreeInitHandler(s, e) {
            CategoryMenuLinkTreeSynchronizeFocusedNode();
        }
        function CategoryMenuLinkTreeEndCallbackHandler(s, e) {
            CategoryMenuLink.SetKeyValue(CategoryMenuLinkTree.GetFocusedNodeKey())
            CategoryMenuLink.AdjustDropDownWindow();
            CategoryMenuLinkTreeUpdateEditBox();
            ctreecb.PerformCallback(CategoryMenuLink.GetText());
        }
        function CategoryMenuLinkTreeNodeClickHandler(s, e) {
            CategoryMenuLink.SetKeyValue(e.nodeKey);
            CategoryMenuLink.SetText(CategoryMenuLinkTree.cpCategoryNames[e.nodeKey]);
            ctreecb.PerformCallback(CategoryMenuLink.GetText());
            CategoryMenuLink.HideDropDown();
        }
        function CategoryMenuLinkTreeSynchronizeFocusedNode() {
            var keyValue = CategoryMenuLink.GetKeyValue();
            CategoryMenuLinkTree.SetFocusedNodeKey(keyValue);
            CategoryMenuLinkTreeUpdateEditBox();
        }
        function CategoryMenuLinkTreeUpdateEditBox() {
            var focusedCategoryName = '';
            var nodeKey = CategoryMenuLinkTree.GetFocusedNodeKey();
            if (nodeKey != 'null' && nodeKey != '')
                focusedCategoryName = CategoryMenuLinkTree.cpCategoryNames[nodeKey];
            var CategoryNameInEditBox = CategoryMenuLink.GetText();
            if (CategoryNameInEditBox != focusedCategoryName)
                CategoryMenuLink.SetText(focusedCategoryName);
        }

        function OnNodeExpanding(s, e) {
            s.PerformCallback(e.nodeKey);
            e.cancel = true;
        }

        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>NUOVA CATEGORIA
            </h3>
            <div class="pull-right">
                <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text=" TORNA ALL'ELENCO" NavigateUrl="~/Admin/product/CategoryList.aspx" CssClass="btn btn-default ">
                </dx:ASPxHyperLink>
                &nbsp; &nbsp;
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default " NavigateUrl="~/Admin/product/CategoryEdit.aspx">AGGIUNGI NUOVA</asp:HyperLink>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Nome</label>
                <dx:ASPxTextBox ID="MenuNome" runat="server" CssClass="form-control">
                    <ClientSideEvents TextChanged="function (s, e) { cb.PerformCallback(s.GetText()); }" />
                    <ClientSideEvents LostFocus="function (s, e) { cb.PerformCallback(s.GetText()); }" />
                </dx:ASPxTextBox>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Collega alla voce del menu principale</label>
                <dx:ASPxDropDownEdit ID="CategoryMenuLink" runat="server" ClientInstanceName="CategoryMenuLink" AllowUserInput="False" EnableAnimation="False" CssClass="form-control" HorizontalAlign="Left">
                    <DropDownWindowStyle>
                        <Border BorderWidth="0px" />
                    </DropDownWindowStyle>
                    <DropDownWindowTemplate>
                        <dx:ASPxTreeList ID="CategoryMenuLinkTree" runat="server" AutoGenerateColumns="False" ClientInstanceName="CategoryMenuLinkTree"
                            OnCustomJSProperties="CategoryMenuLinkTree_CustomJSProperties" OnDataBound="CategoryMenuLinkTree_DataBound" OnCustomCallback="CategoryMenuLinkTree_CustomCallback"
                            DataSourceID="EntityDataSource2" KeyFieldName="PK" ParentFieldName="ParentID">
                            <Border BorderStyle="Solid" />
                            <SettingsBehavior AllowFocusedNode="true" />
                            <SettingsEditing ConfirmDelete="true" />
                            <SettingsPager Mode="ShowAllNodes">
                            </SettingsPager>
                            <Columns>
                                <dx:TreeListTextColumn FieldName="descrizione" VisibleIndex="0">
                                </dx:TreeListTextColumn>
                                <dx:TreeListTextColumn FieldName="ParentID" VisibleIndex="1" Visible="false">
                                </dx:TreeListTextColumn>
                            </Columns>
                            <ClientSideEvents Init="CategoryMenuLinkTreeInitHandler" EndCallback="CategoryMenuLinkTreeEndCallbackHandler" NodeClick="CategoryMenuLinkTreeNodeClickHandler" NodeExpanding="OnNodeExpanding" />
                        </dx:ASPxTreeList>
                    </DropDownWindowTemplate>
                    <Buttons>
                        <dx:EditButton Text="X"></dx:EditButton>
                    </Buttons>
                    <ButtonStyle BackColor="LightGray" ForeColor="Gray">
                        <PressedStyle BackColor="#cccccc">
                        </PressedStyle>
                        <Border BorderColor="#eeeeee" />
                    </ButtonStyle>
                    <CaptionSettings Position="Top" />
                    <ClientSideEvents DropDown="CategoryMenuLinkTreeDropDownHandler" ButtonClick="CategoryMenuLinkTreeButtonClickHandler" />
                </dx:ASPxDropDownEdit>
                <ef:EntityDataSource ID="EntityDataSource2" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EntitySetName="Menu" Where="IT.tipo = 'link'">
                </ef:EntityDataSource>
                <dx:ASPxCallback ID="ctreecb" ClientInstanceName="ctreecb" runat="server">
                    <ClientSideEvents CallbackComplete="function (s, e) { MenuLink.SetText(e.result); }" />
                </dx:ASPxCallback>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Link</label>
                <div class="input-group">
                    <dx:ASPxTextBox ID="MenuLink" ClientInstanceName="MenuLink" runat="server" CssClass="form-control"></dx:ASPxTextBox>
                    <span class="input-group-btn">
                        <asp:Button ID="btnModificaLink" runat="server" Text="Modifica" CssClass="btn btn-default" />
                    </span>
                </div>
                <dx:ASPxCallback ID="cb" ClientInstanceName="cb" runat="server">
                    <ClientSideEvents CallbackComplete="function (s, e) { MenuLink.SetText(e.result); }" />
                </dx:ASPxCallback>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Lingua</label>
                <asp:DropDownList ID="MenuCulture" runat="server" CssClass="form-control dropdown">
                    <asp:ListItem Text="Seleziona" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Italiano" Value="it-IT"></asp:ListItem>
                    <asp:ListItem Text="Inglese" Value="en-UK"></asp:ListItem>
                    <asp:ListItem Text="Spagnolo" Value="es-ES"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Macro</label>
                <dx:ASPxDropDownEdit ID="MenuMacro" runat="server" ClientInstanceName="MenuMacro" AllowUserInput="False" EnableAnimation="False" CssClass="form-control" HorizontalAlign="Left">
                    <DropDownWindowStyle>
                        <Border BorderWidth="0px" />
                    </DropDownWindowStyle>
                    <DropDownWindowTemplate>
                        <dx:ASPxTreeList ID="ASPxTreeList1" runat="server" AutoGenerateColumns="False" ClientInstanceName="TreeList"
                            OnCustomJSProperties="TreeList_CustomJSProperties" OnDataBound="TreeList_DataBound" OnCustomCallback="ASPxTreeList1_CustomCallback"
                            DataSourceID="EntityDataSource1" KeyFieldName="PK" ParentFieldName="ParentID">
                            <Border BorderStyle="Solid" />
                            <SettingsBehavior AllowFocusedNode="true" />
                            <SettingsEditing ConfirmDelete="true" />
                            <SettingsPager Mode="ShowAllNodes">
                            </SettingsPager>
                            <Columns>
                                <dx:TreeListTextColumn FieldName="descrizione" VisibleIndex="0">
                                </dx:TreeListTextColumn>
                                <dx:TreeListTextColumn FieldName="ParentID" VisibleIndex="1" Visible="false">
                                </dx:TreeListTextColumn>
                            </Columns>
                            <ClientSideEvents Init="TreeListInitHandler" EndCallback="TreeListEndCallbackHandler" NodeClick="TreeListNodeClickHandler" NodeExpanding="OnNodeExpanding" />
                        </dx:ASPxTreeList>
                    </DropDownWindowTemplate>
                    <Buttons>
                        <dx:EditButton Text="X"></dx:EditButton>
                    </Buttons>
                    <ButtonStyle BackColor="LightGray" ForeColor="Gray">
                        <PressedStyle BackColor="#cccccc">
                        </PressedStyle>
                        <Border BorderColor="#eeeeee" />
                    </ButtonStyle>
                    <CaptionSettings Position="Top" />
                    <ClientSideEvents DropDown="DropDownHandler" ButtonClick="ButtonClickHandler" />
                </dx:ASPxDropDownEdit>
                <ef:EntityDataSource ID="EntityDataSource1" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EntitySetName="Menu" Where="IT.tipo = 'categoria'">
                </ef:EntityDataSource>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Visibile</label>
                <div class="checkbox">
                    <dx:ASPxCheckBox ID="MenuVisibile" runat="server" Theme="Moderno"></dx:ASPxCheckBox>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Includi nel menu laterale</label>
                <div class="checkbox">
                    <dx:ASPxCheckBox ID="IncludeTopMenu" runat="server" Theme="Moderno" Checked="true"></dx:ASPxCheckBox>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <asp:LinkButton ID="btnSalva" runat="server" Text="SALVA" CssClass="btn btn-default" OnClientClick="return getConfirmation(this, 'Attenzione','Confermi il salvataggio?');" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="content3" runat="server" ContentPlaceHolderID="FooterPlaceHolder">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">
                        SALVA</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>