﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="ProductEdit.aspx.vb" Inherits="Emmemedia.Admin.Product.ProductEdit" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">//<![CDATA[
        window.onload = function () {
            history.pushState(null, null, location.href);
            window.onpopstate = function (event) {
                history.go(1);
            };
        }//]]>

        function ButtonClickHandler(s, e) {
            if (e.buttonIndex == 0) {
                MenuMacro.SetKeyValue(null);
                SynchronizeFocusedNode();
            }
        }
        function DropDownHandler(s, e) {
            SynchronizeFocusedNode();
        }
        function tlCategoriaInitHandler(s, e) {
            SynchronizeFocusedNode();
        }
        function tlCategoriaEndCallbackHandler(s, e) {
            MenuMacro.SetKeyValue(tlCategoria.GetFocusedNodeKey())
            MenuMacro.AdjustDropDownWindow();
            UpdateEditBox();
        }
        function tlCategoriaNodeClickHandler(s, e) {
            MenuMacro.SetKeyValue(e.nodeKey);
            MenuMacro.SetText(tlCategoria.cpCategoryNames[e.nodeKey]);
            MenuMacro.HideDropDown();
        }
        function SynchronizeFocusedNode() {
            var keyValue = MenuMacro.GetKeyValue();
            tlCategoria.SetFocusedNodeKey(keyValue);
            UpdateEditBox();
        }
        function UpdateEditBox() {
            var focusedCategoryName = '';
            var nodeKey = tlCategoria.GetFocusedNodeKey();
            if (nodeKey != 'null' && nodeKey != '')
                focusedCategoryName = tlCategoria.cpCategoryNames[nodeKey];
            var CategoryNameInEditBox = MenuMacro.GetText();
            if (CategoryNameInEditBox != focusedCategoryName)
                MenuMacro.SetText(focusedCategoryName);
        }

        function OnNodeExpanding(s, e) {
            s.PerformCallback(e.nodeKey);
            e.cancel = true;
        }

        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>
    <script type="text/javascript">
        function OnClickButtonDel(s, e) {
            gridSottoCat.PerformCallback('Delete');
        }
        function onUploadControlFileUploadComplete(s, e) {
            if (e.isValid)
                document.getElementById('<%=uploadedImage.ClientID %>').src = "/public/images/product/" + e.callbackData;
            setElementVisible('<%=uploadedImage.ClientID %>', e.isValid);
        }
        function onImageLoad() {
            var externalDropZone = document.getElementById("externalDropZone");
            var uploadedImage = document.getElementById('<%=uploadedImage.ClientID %>');
            uploadedImage.style.left = (externalDropZone.clientWidth - uploadedImage.width) / 2 + "px";
            uploadedImage.style.top = (externalDropZone.clientHeight - uploadedImage.height) / 2 + "px";
            setElementVisible("dragZone", false);
        }
        function setElementVisible(elementId, visible) {
            document.getElementById(elementId).className = visible ? "" : "hidden";
        }

        function Browse() {
            document.getElementById(<%=uploadedImage.ClientID %>).click();
        }
    </script>
    <style type="text/css">
        .dropZoneExternal > div {
            left: 0;
            width: 100%;
        }

        .dropZoneExternal > img {
            position: absolute;
        }

        .dropZoneExternal {
            position: relative;
            border: 3px dashed #91A0A8 !important;
            cursor: pointer;
        }

        .dropZoneExternal,
        .dragZoneText {
            width: 100%;
            height: 260px;
            margin: 30px 0;
            padding: 5px;
        }

        .dropZoneText {
            width: 100%;
            height: 150px;
            color: #fff;
            background-color: #888;
        }

        #dropZone {
            top: 0;
            padding: 100px 25px;
        }

        .uploadControlDropZone,
        .hidden {
            display: none;
        }

        .dropZoneText,
        .dragZoneText {
            display: block;
            vertical-align: middle;
            text-align: center;
            font-size: 24px;
            padding: 25px 15px;
        }

        .dragZoneText {
            color: #8597A0;
        }

        .dxucInlineDropZoneSys span {
            color: #fff !important;
            font-size: 10pt;
            font-weight: normal !important;
        }

        .uploadControlProgressBar {
            width: 100% !important;
        }

        .validationMessage {
            padding: 0 20px;
            text-align: center;
        }

        .uploadControl {
            margin: 30px 0;
        }

        .Note {
            width: 100%;
        }

        .dxucProgressBarIndicator_Moderno {
            background: #82939c;
        }

        .dxucErrorCell_Moderno {
            padding: 15px 0;
            color: crimson;
        }
    </style>

    <style type="text/css">
        #GalleryDropZone {
            padding: 20px;
            margin: -20px;
        }

        .ResultFileName {
            text-overflow: ellipsis;
        }

        .contentFooter {
            clear: both;
            padding-top: 20px;
        }
    </style>
    <script id="GalleryScript" type="text/javascript">
        var uploadInProgress = false,
            submitInitiated = false,
            uploadErrorOccurred = false;
        uploadedFiles = [];

        function onGalleryFileUploadComplete(s, e) {
            var callbackData = e.callbackData.split("|"),
                uploadedFileName = callbackData[0],
                isSubmissionExpired = callbackData[1] === "True";
            uploadedFiles.push(uploadedFileName);
            if (e.errorText.length > 0 || !e.isValid)
                uploadErrorOccurred = true;
            if (isSubmissionExpired && UploadedFilesTokenBox.GetText().length > 0) {
                var removedAfterTimeoutFiles = UploadedFilesTokenBox.GetTokenCollection().join("\n");
                alert("I seguenti files sono stati rimossi dal server per scadenza del periodo di timeout (5min): \n\n" + removedAfterTimeoutFiles);
                UploadedFilesTokenBox.ClearTokenCollection();
            }
        }
        function onGalleryFileUploadStart(s, e) {
            uploadInProgress = true;
            uploadErrorOccurred = false;
            UploadedFilesTokenBox.SetIsValid(true);
        }
        function onGalleryFilesUploadComplete(s, e) {
            uploadInProgress = false;
            for (var i = 0; i < uploadedFiles.length; i++)
                UploadedFilesTokenBox.AddToken(uploadedFiles[i]);
            updateTokenBoxVisibility();
            uploadedFiles = [];
            if (submitInitiated) {
                SubmitButton.SetEnabled(true);
                SubmitButton.DoClick();
            }
        }
        function onSubmitButtonClick(s, e) {
            ASPxClientEdit.ValidateGroup();
            if (!formIsValid())
                e.processOnServer = false;
            else if (uploadInProgress) {
                s.SetEnabled(false);
                submitInitiated = true;
                e.processOnServer = false;
            }
        }
        function onTokenBoxValidation(s, e) {
            var isValid = ProdottoGalleryUploadControl.GetText().length > 0 || UploadedFilesTokenBox.GetText().length > 0;
            e.isValid = isValid;
            if (!isValid) {
                e.errorText = "Nessun file è stato caricato.";
            }
        }
        function onTokenBoxValueChanged(s, e) {
            updateTokenBoxVisibility();
        }
        function updateTokenBoxVisibility() {
            var isTokenBoxVisible = UploadedFilesTokenBox.GetTokenCollection().length > 0;
            UploadedFilesTokenBox.SetVisible(isTokenBoxVisible);
        }
        function formIsValid() {
            return !ValidationSummary.IsVisible() && DescriptionTextBox.GetIsValid() && UploadedFilesTokenBox.GetIsValid() && !uploadErrorOccurred;
        }
    </script>

    <script type="text/javascript">
        function textCounter(field, field2, maxlimit) {
            debugger;
            var countfield = document.getElementById(field2);
            if (field.value.length > maxlimit) {
                field.value = field.value.substring(0, maxlimit);
                return false;
            } else {
                countfield.textContent = ("Il testo sarà limitato a " + maxlimit + " caratteri, rimasti : " + (maxlimit - field.value.length));
            }
        }
    </script>
    <script id="UploadFileScript" type="text/javascript">
        function GetUploadedFileName(s, e) {
            cbProdottoAllegato.PerformCallback();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-6">
            <h3>NUOVO PRODOTTO</h3>
        </div>
        <div class="col-md-6 text-right">
            <br />
            <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text=" TORNA ALL'ELENCO " NavigateUrl="ProductList.aspx" CssClass="btn btn-default">
            </dx:ASPxHyperLink>
            &nbsp;
               <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default" NavigateUrl="~/Admin/Product/ProductEdit.aspx">AGGIUNGI NUOVO</asp:HyperLink>
            &nbsp;
            <dx:ASPxButton ID="btnSalva" runat="server" Text="SALVA PRODOTTO" CssClass="btn btn-default btn-danger" Theme="Moderno"></dx:ASPxButton>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-8">
            <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="100%" Theme="Moderno">
                <TabPages>
                    <dx:TabPage Name="Intestazione" Text="Intestazione Prodotto">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div class="form-horizontal">

                                    <div class="form-group">
                                        <label for="ProdottoCodiceInterno" class="col-md-2 control-label">CODICE INTERNO</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="ProdottoCodiceInterno" runat="server" ClientIDMode="Static" class="form-control"></asp:TextBox>
                                        </div>
                                        <label for="ProdottoCodiceFornitore" class="col-md-2 control-label">CODICE FORNITORE</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="ProdottoCodiceFornitore" runat="server" ClientIDMode="Static" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ProdottoModello" class="col-md-2 control-label">NOME PRODOTTO</label>
                                        <div class="col-md-10">
                                            <dx:ASPxTextBox ID="ProdottoModello" runat="server" CssClass="form-control" ClientInstanceName="ProdottoModello">
                                                <ClientSideEvents TextChanged="function (s, e) { cb.PerformCallback(s.GetText()); }" />
                                                <ClientSideEvents LostFocus="function (s, e) { cb.PerformCallback(s.GetText()); }" />
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ProdottoDescrizioneBreve" class="col-md-2 control-label">DESCRIZIONE BREVE PRODOTTO </label>
                                        <div class="col-md-10">
                                            <asp:TextBox ID="ProdottoDescrizioneBreve" runat="server" ClientIDMode="Static" class="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ProdottoFamiglia" class="col-md-2 control-label">FAMIGLIA</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="ProdottoFamiglia" runat="server" ClientIDMode="Static" class="form-control"></asp:TextBox>
                                        </div>
                                        <label for="ProdottoMarca" class="col-md-2 control-label">MARCA</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="ProdottoMarca" runat="server" ClientIDMode="Static" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ProdottoCodiceInterno" class="col-md-2 control-label">Categoria Omogenea</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ProdottoCategoriaOmogenea" runat="server" class="form-control"></asp:DropDownList>
                                        </div>
                                        <label for="ProdottoCodiceFornitore" class="col-md-2 control-label">Gruppo Merceologico</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ProdottoGruppoMerceologico" runat="server" class="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="descrizione" Text="Descrizione Estesa">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxHtmlEditor ID="ProdottoDescrizioneEstesa" runat="server" EnableTheming="true" Theme="Moderno" Width="100%">
                                    <SettingsDialogs>
                                        <InsertImageDialog>
                                            <SettingsImageUpload UploadFolder="~/public/Images/product/content/">
                                                <ValidationSettings AllowedFileExtensions=".jpe,.jpeg,.jpg,.gif,.png" MaxFileSize="500000">
                                                </ValidationSettings>
                                            </SettingsImageUpload>
                                        </InsertImageDialog>
                                    </SettingsDialogs>
                                </dx:ASPxHtmlEditor>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="descrizione" Text="Scheda Tecnica">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxHtmlEditor ID="ProdottoSchedaTecnica" runat="server" EnableTheming="true" Theme="Moderno" Width="100%">
                                    <SettingsDialogs>
                                        <InsertImageDialog>
                                            <SettingsImageUpload UploadFolder="~/public/Images/product/content/">
                                                <ValidationSettings AllowedFileExtensions=".jpe,.jpeg,.jpg,.gif,.png" MaxFileSize="500000">
                                                </ValidationSettings>
                                            </SettingsImageUpload>
                                        </InsertImageDialog>
                                    </SettingsDialogs>
                                </dx:ASPxHtmlEditor>
                                <br />
                                <div class="form-horizontal">

                                    <div class="form-group">
                                        <label for="ProdottoAllegato" class="col-md-2 control-label">ALLEGA DOCUMENTO</label>
                                        <div class="col-md-8">
                                            <dx:ASPxTextBox ID="ProdottoAllegato" runat="server" Width="170px" CssClass="form-control" ClientInstanceName="ProdottoAllegato">
                                            </dx:ASPxTextBox>
                                            <dx:ASPxUploadControl ID="ProdottoAllegatoUpload" runat="server" AutoStartUpload="true" ShowProgressPanel="true" ShowTextBox="true" UploadMode="Auto" Theme="Moderno" ButtonStyle-CssClass="btn btn-default">
                                                <ClientSideEvents FileUploadComplete="GetUploadedFileName" />
                                            </dx:ASPxUploadControl>
                                            <dx:ASPxCallback ID="cbProdottoAllegato" ClientIDMode="Static" ClientInstanceName="cbProdottoAllegato" runat="server">
                                                <ClientSideEvents CallbackComplete="function (s, e) { debugger; ProdottoAllegato.SetText(e.result); }" />
                                            </dx:ASPxCallback>
                                        </div>
                                    </div>
                                </div>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="PrezziSpedizioni" Text="Prezzi - Spedizioni">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server">
                                    <Items>
                                        <dx:LayoutGroup Caption="PREZZI">
                                            <Items>
                                                <dx:LayoutItem ShowCaption="False">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <div class="form-horizontal">
                                                                <div class="form-group">
                                                                    <label for="PaginaTitolo" class="col-md-2 control-label">LISTINO</label>
                                                                    <div class="col-md-2">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon">€</div>
                                                                            <asp:TextBox ID="ProdottoPrezzoListino" runat="server" ClientIDMode="Static" class="form-control" placeholder="123,00"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <label for="ProdottoPrezzoEndUser" class="col-md-2 control-label">END USER</label>
                                                                    <div class="col-md-2">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon">€</div>
                                                                            <asp:TextBox ID="ProdottoPrezzoEndUser" runat="server" ClientIDMode="Static" class="form-control" placeholder="123,00"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <label for="ProdottoRivenditore" class="col-md-2 control-label">RIVENDITORE</label>
                                                                    <div class="col-md-2">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon">€</div>
                                                                            <asp:TextBox ID="ProdottoPrezzoRivenditore" runat="server" ClientIDMode="Static" class="form-control" placeholder="123,00"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                        </dx:LayoutGroup>
                                        <dx:LayoutGroup Caption="SPEDIZIONI">
                                            <Items>
                                                <dx:LayoutItem ShowCaption="False">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <div class="form-horizontal">
                                                                <div class="form-group">
                                                                    <label for="ProdottoPeso" class="col-md-2 control-label">PESO</label>
                                                                    <div class="col-md-2">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon">Kg</div>
                                                                            <asp:TextBox ID="ProdottoPeso" runat="server" ClientIDMode="Static" class="form-control" placeholder="123,00"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="ProdottoAltezza" class="col-md-2 control-label">ALTEZZA</label>
                                                                    <div class="col-md-2">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon">cm</div>
                                                                            <asp:TextBox ID="ProdottoAltezza" runat="server" ClientIDMode="Static" class="form-control" placeholder="123,00"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <label for="ProdottoLarghezza" class="col-md-2 control-label">LARGHEZZA</label>
                                                                    <div class="col-md-2">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon">cm</div>
                                                                            <asp:TextBox ID="ProdottoLarghezza" runat="server" ClientIDMode="Static" class="form-control" placeholder="123,00"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <label for="ProdottoProfondita" class="col-md-2 control-label">PROFONDITA'</label>
                                                                    <div class="col-md-2">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon">cm</div>
                                                                            <asp:TextBox ID="ProdottoProfondita" runat="server" ClientIDMode="Static" class="form-control" placeholder="123,00"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                        </dx:LayoutGroup>
                                    </Items>
                                </dx:ASPxFormLayout>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Seo" Text="SEO">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="ProdottoMetaTitle" class="col-md-2 control-label">META TITLE</label>
                                        <div class="col-md-6">
                                            <dx:ASPxTextBox ID="ProdottoMetaTitle" ClientInstanceName="ProdottoMetaTitle" runat="server" CssClass="form-control" onkeyup="textCounter(this,'remainingPageTitle',60);"></dx:ASPxTextBox>
                                            <span id='remainingPageTitle'></span>
                                            <dx:ASPxCallback ID="cb" ClientInstanceName="cb" runat="server">
                                                <ClientSideEvents CallbackComplete="function (s, e) { ProdottoMetaTitle.SetText(e.result); ProdottoMetaDescription.SetText(e.result); ProdottoMetaKeywords.SetText(e.result); }" />
                                            </dx:ASPxCallback>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ProdottoMetaDescription1" class="col-md-2 control-label">META DESCRIPTION</label>
                                        <div class="col-md-6">
                                            <dx:ASPxTextBox ID="ProdottoMetaDescription" ClientInstanceName="ProdottoMetaDescription" runat="server" CssClass="form-control" onkeyup="textCounter(this,'remainingPageTitle',60);"></dx:ASPxTextBox>

                                            <span id='remainingPageDescription'></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ProdottoMetaKeywords1" class="col-md-2 control-label">META KEYWORDS</label>
                                        <div class="col-md-6">
                                            <dx:ASPxTextBox ID="ProdottoMetaKeywords" ClientInstanceName="ProdottoMetaKeywords" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="2" onkeyup="textCounter(this,'remainingPageTitle',60);"></dx:ASPxTextBox>

                                            <span id='remainingPageKeywords'></span>
                                        </div>
                                    </div>
                                </div>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Fotogallery" Text="Fotogallery">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxHiddenField runat="server" ID="HiddenField" ClientInstanceName="HiddenField"></dx:ASPxHiddenField>
                                <dx:ASPxFormLayout ID="FormLayout" runat="server" Width="800px" ColCount="2" UseDefaultPaddings="false">
                                    <Items>
                                        <dx:LayoutGroup ShowCaption="False" GroupBoxDecoration="None" Width="400px" UseDefaultPaddings="false">
                                            <Items>
                                                <dx:LayoutItem Caption="Description" CaptionSettings-Location="Top">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxTextBox runat="server" ID="DescriptionTextBox" ClientInstanceName="DescriptionTextBox" NullText="Descrizione fotogallery"
                                                                Width="200px" EncodeHtml="true" CssClass="form-control">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" ValidationGroup="DescriptionValidation">
                                                                    <RequiredField IsRequired="true" ErrorText="La descrizione è obbligatoria" />
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutGroup Caption="Immagini">
                                                    <Items>
                                                        <dx:LayoutItem ShowCaption="False">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <div id="GalleryDropZone">
                                                                        <dx:ASPxUploadControl runat="server" ID="ProdottoGalleryUploadControl" ClientInstanceName="ProdottoGalleryUploadControl" Width="100%"
                                                                            AutoStartUpload="true" ShowProgressPanel="True" ShowTextBox="false" BrowseButton-Text="Aggiungi files" FileUploadMode="OnPageLoad"
                                                                            Theme="Moderno">
                                                                            <AdvancedModeSettings
                                                                                EnableMultiSelect="true" EnableDragAndDrop="true" ExternalDropZoneID="GalleryDropZone">
                                                                            </AdvancedModeSettings>
                                                                            <ValidationSettings AllowedFileExtensions=".jpeg, .jpg, .gif, .png" MaxFileSize="4194304">
                                                                            </ValidationSettings>
                                                                            <ClientSideEvents
                                                                                FileUploadComplete="onGalleryFileUploadComplete"
                                                                                FilesUploadComplete="onGalleryFilesUploadComplete"
                                                                                FilesUploadStart="onGalleryFileUploadStart" />
                                                                        </dx:ASPxUploadControl>
                                                                        <br />
                                                                        <dx:ASPxTokenBox runat="server" Width="100%" ID="UploadedFilesTokenBox" ClientInstanceName="UploadedFilesTokenBox"
                                                                            NullText="Selezionare i file da caricare" AllowCustomTokens="false" ClientVisible="false">
                                                                            <ClientSideEvents Init="updateTokenBoxVisibility" ValueChanged="onTokenBoxValueChanged" Validation="onTokenBoxValidation" />
                                                                            <ValidationSettings EnableCustomValidation="true"></ValidationSettings>
                                                                        </dx:ASPxTokenBox>
                                                                        <br />
                                                                        <p class="Note">
                                                                            <dx:ASPxLabel ID="AllowedFileExtensionsLabel" runat="server" Text="Estensioni file accettate: .jpg, .jpeg, .gif, .png." Font-Size="8pt">
                                                                            </dx:ASPxLabel>
                                                                            <br />
                                                                            <dx:ASPxLabel ID="MaxFileSizeLabel" runat="server" Text="Dimensione massima file : 4 MB." Font-Size="8pt">
                                                                            </dx:ASPxLabel>
                                                                        </p>
                                                                        <dx:ASPxValidationSummary runat="server" ID="ValidationSummary" ClientInstanceName="ValidationSummary" RenderMode="Table" Width="250px" ShowErrorAsLink="false">
                                                                        </dx:ASPxValidationSummary>
                                                                    </div>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                    </Items>
                                                </dx:LayoutGroup>
                                                <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxButton runat="server" ID="ProdottoGalleryInvia" ClientInstanceName="ProdottoGalleryInvia" Text="Carica immagini" AutoPostBack="False" ValidateInvisibleEditors="true" Theme="Moderno">
                                                                <ClientSideEvents Click="onSubmitButtonClick" />
                                                            </dx:ASPxButton>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                        </dx:LayoutGroup>
                                        <dx:LayoutGroup GroupBoxDecoration="None" ShowCaption="False" Name="ResultGroup" Visible="false" Width="400px" UseDefaultPaddings="false">
                                            <Items>
                                                <dx:LayoutItem ShowCaption="False">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxRoundPanel ID="RoundPanel" runat="server" HeaderText="Uploaded files" Width="100%">
                                                                <PanelCollection>
                                                                    <dx:PanelContent>
                                                                        <b>Descrizione :</b>
                                                                        <dx:ASPxLabel runat="server" ID="DescriptionLabel"></dx:ASPxLabel>
                                                                        <br />
                                                                        <br />
                                                                        <dx:ASPxListBox ID="SubmittedFilesListBox" runat="server" Width="100%" Height="150px">
                                                                            <ItemStyle CssClass="ResultFileName" />
                                                                            <Columns>
                                                                                <dx:ListBoxColumn FieldName="OriginalFileName" />
                                                                                <dx:ListBoxColumn FieldName="FileSize" Width="15%" />
                                                                            </Columns>
                                                                        </dx:ASPxListBox>
                                                                    </dx:PanelContent>
                                                                </PanelCollection>
                                                            </dx:ASPxRoundPanel>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                        </dx:LayoutGroup>
                                    </Items>
                                </dx:ASPxFormLayout>
                                <br />
                                <br />
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:ListView ID="lvFotogallery" runat="server" DataKeyNames="PK" GroupItemCount="4">
                                            <LayoutTemplate>
                                                <asp:PlaceHolder runat="server" ID="GroupPlaceHolder"></asp:PlaceHolder>
                                            </LayoutTemplate>
                                            <GroupTemplate>
                                                <div class="row">
                                                    <asp:PlaceHolder runat="server" ID="ItemPlaceHolder"></asp:PlaceHolder>
                                                </div>
                                            </GroupTemplate>
                                            <ItemTemplate>
                                                <div class="col-md-3">
                                                    <img src="/public/images/product/gallery/<%# Eval("PercorsoImmagine") %>" class="img-responsive" style="max-width: 200px" />
                                                    <div style="text-align: center; font-size: 11px;">
                                                        <asp:LinkButton ID="LnkElimina" runat="server" CommandName="Elimina" CommandArgument='<%# Eval("PK") %>' OnClientClick="return getConfirmation(this, 'Conferma Elimina','Sei sicuro di voler eliminare questo elemento?');">ELIMINA</asp:LinkButton>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>
            </dx:ASPxPageControl>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <dx:ASPxFormLayout runat="server" ID="settingsFormLayout" AlignItemCaptionsInAllGroups="True" SettingsItemCaptions-HorizontalAlign="left" SettingsAdaptivity-AdaptivityMode="Off">
                <Items>
                    <dx:LayoutGroup Caption="Impostazioni" GroupBoxDecoration="HeadingLine">
                        <Items>
                            <dx:LayoutItem Caption="CATEGORIA">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxDropDownEdit ID="MenuMacro" runat="server" ClientInstanceName="MenuMacro" AllowUserInput="False" EnableAnimation="False" CssClass="form-control" HorizontalAlign="Left">
                                            <DropDownWindowStyle>
                                                <border borderwidth="0px" />
                                            </DropDownWindowStyle>
                                            <DropDownWindowTemplate>
                                                <dx:ASPxTreeList ID="tlCategoria" runat="server" AutoGenerateColumns="False" ClientInstanceName="tlCategoria" OnCustomJSProperties="tlCategoria_CustomJSProperties" OnDataBound="tlCategoria_DataBound" OnCustomCallback="tlCategoria_CustomCallback" DataSourceID="EntityDataSource1" KeyFieldName="PK" ParentFieldName="ParentID">
                                                    <Border BorderStyle="Solid" />
                                                    <SettingsBehavior AllowFocusedNode="true" />
                                                    <SettingsEditing ConfirmDelete="true" />
                                                    <SettingsPager Mode="ShowAllNodes">
                                                    </SettingsPager>
                                                    <Columns>
                                                        <dx:TreeListTextColumn FieldName="descrizione" VisibleIndex="0">
                                                        </dx:TreeListTextColumn>
                                                        <dx:TreeListTextColumn FieldName="ParentID" VisibleIndex="1" Visible="false">
                                                        </dx:TreeListTextColumn>
                                                    </Columns>
                                                    <ClientSideEvents Init="tlCategoriaInitHandler" EndCallback="tlCategoriaEndCallbackHandler" NodeClick="tlCategoriaNodeClickHandler" NodeExpanding="OnNodeExpanding" />
                                                </dx:ASPxTreeList>
                                            </DropDownWindowTemplate>
                                            <Buttons>
                                                <dx:EditButton Text="X"></dx:EditButton>
                                            </Buttons>
                                            <ButtonStyle BackColor="LightGray" ForeColor="Gray">
                                                <PressedStyle BackColor="#cccccc">
                                                </PressedStyle>
                                                <border bordercolor="#eeeeee" />
                                            </ButtonStyle>
                                            <CaptionSettings Position="Top" />
                                            <ClientSideEvents DropDown="DropDownHandler" ButtonClick="ButtonClickHandler" />
                                        </dx:ASPxDropDownEdit>
                                        <ef:EntityDataSource ID="EntityDataSource1" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EntitySetName="vwMenuTree">
                                        </ef:EntityDataSource>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Visibile">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxCheckBox ID="ProdottoVisibile" runat="server" Theme="Moderno" TextAlign="Left"></dx:ASPxCheckBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Vetrina">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxCheckBox ID="ProdottoVetrina" runat="server" Theme="Moderno" TextAlign="Left"></dx:ASPxCheckBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Promo">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxCheckBox ID="ProdottoPromo" runat="server" Theme="Moderno" TextAlign="Left"></dx:ASPxCheckBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Noleggio">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxCheckBox ID="ProdottoNoleggio" runat="server" Theme="Moderno" TextAlign="Left"></dx:ASPxCheckBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Privato">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxCheckBox ID="ProdottoPrivato" runat="server" Theme="Moderno" TextAlign="Left"></dx:ASPxCheckBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Data Creazione" CaptionStyle-Font-Bold="true">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxLabel ID="ProdottoDataCreazione" runat="server" Theme="Moderno">
                                        </dx:ASPxLabel>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>

                                <CaptionStyle Font-Bold="True"></CaptionStyle>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Data Modifica" CaptionStyle-Font-Bold="true">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxLabel ID="ProdottoDataModifica" runat="server" Theme="Moderno">
                                        </dx:ASPxLabel>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>

                                <CaptionStyle Font-Bold="True"></CaptionStyle>
                            </dx:LayoutItem>
                        </Items>
                    </dx:LayoutGroup>
                    <dx:LayoutGroup Name="ImmaginePrincipale" Caption="Immagine Principale" GroupBoxDecoration="HeadingLine">
                        <Items>
                            <dx:LayoutItem ShowCaption="False">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <div id="externalDropZone" class="dropZoneExternal">
                                            <div id="dragZone">
                                                <span class="dragZoneText" style="text-align: center">Clicca o trascina qui la foto</span>
                                            </div>
                                            <img id="uploadedImage" src="#" class="hidden" alt="" runat="server">
                                            <div id="dropZone" class="hidden">
                                                <span class="dropZoneText" style="text-align: center">Clicca o trascina qui la foto</span>
                                            </div>
                                        </div>
                                        <dx:ASPxUploadControl ID="ProdottoImmagineUpload" ClientInstanceName="ProdottoImmagineUpload" runat="server" UploadMode="Standard" AutoStartUpload="True" Width="100%" ShowProgressPanel="True" CssClass="uploadControl" DialogTriggerID="externalDropZone" ShowUploadButton="false" ShowAddRemoveButtons="false" ShowTextBox="false" ShowClearFileSelectionButton="false">
                                            <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="False" ExternalDropZoneID="externalDropZone" DropZoneText="" />
                                            <ValidationSettings MaxFileSize="4194304" ShowErrors="true" AllowedFileExtensions=".jpg, .jpeg, .gif, .png" ErrorStyle-CssClass="validationMessage">
                                                <ErrorStyle CssClass="validationMessage"></ErrorStyle>
                                            </ValidationSettings>
                                            <BrowseButton Text="Seleziona un'immagine..." />
                                            <DropZoneStyle CssClass="uploadControlDropZone" />
                                            <ProgressBarStyle CssClass="uploadControlProgressBar" />
                                            <ClientSideEvents DropZoneEnter="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', true); }" DropZoneLeave="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', false); }" FileUploadComplete="onUploadControlFileUploadComplete"></ClientSideEvents>
                                        </dx:ASPxUploadControl>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                        </Items>
                    </dx:LayoutGroup>
                </Items>
                <SettingsItemCaptions HorizontalAlign="Left"></SettingsItemCaptions>
            </dx:ASPxFormLayout>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterPlaceHolder" runat="server">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>