﻿Imports DevExpress.Data.Linq
Imports DevExpress.Web
Imports DevExpress.Web.ASPxTreeList
Imports Microsoft.AspNet.EntityDataSource

Namespace Admin
    Namespace Product
        Public Class ProductList
            Inherits System.Web.UI.Page

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            End Sub

            Private Sub ProductList_Init(sender As Object, e As EventArgs) Handles Me.Init

            End Sub

            Private Sub ProductList_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete

            End Sub

            Private Sub ProductList_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

            End Sub

            Protected Sub TreeList_DataBound(ByVal sender As Object, ByVal e As EventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                If (Not IsCallback) AndAlso (Not IsPostBack) Then
                    tree.CollapseAll()
                End If
            End Sub

            Protected Sub TreeList_CustomJSProperties(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxTreeList.TreeListCustomJSPropertiesEventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                Dim CategoryNames As New Hashtable()
                For Each node As TreeListNode In tree.GetVisibleNodes()
                    CategoryNames.Add(node.Key, node("Descrizione"))
                Next node
                e.Properties("cpCategoryNames") = CategoryNames
            End Sub

            Protected Sub ASPxTreeList1_CustomCallback(sender As Object, e As TreeListCustomCallbackEventArgs)
                Dim ASPxTreeList1 As ASPxTreeList = TryCast(sender, ASPxTreeList)
                ASPxTreeList1.CollapseAll()
                Dim node As TreeListNode = ASPxTreeList1.FindNodeByKeyValue(e.Argument)
                While node.ParentNode IsNot Nothing
                    node.Expanded = True
                    node = node.ParentNode
                End While
            End Sub

            Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
                If MenuMacro.KeyValue <> "" Then
                    ASPxGridView1.FilterExpression = "FkCategoria = " & MenuMacro.KeyValue
                Else
                    ASPxGridView1.FilterExpression = String.Empty
                End If
            End Sub

        End Class


    End Namespace
End Namespace
