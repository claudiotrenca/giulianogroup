﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Admin.Product
    
    Partial Public Class ProductEdit
        
        '''<summary>
        '''Controllo ASPxHyperLink1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ASPxHyperLink1 As Global.DevExpress.Web.ASPxHyperLink
        
        '''<summary>
        '''Controllo HyperLink1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HyperLink1 As Global.System.Web.UI.WebControls.HyperLink
        
        '''<summary>
        '''Controllo btnSalva.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnSalva As Global.DevExpress.Web.ASPxButton
        
        '''<summary>
        '''Controllo ASPxPageControl1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ASPxPageControl1 As Global.DevExpress.Web.ASPxPageControl
        
        '''<summary>
        '''Controllo ProdottoCodiceInterno.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoCodiceInterno As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoCodiceFornitore.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoCodiceFornitore As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoModello.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoModello As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo ProdottoDescrizioneBreve.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoDescrizioneBreve As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoFamiglia.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoFamiglia As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoMarca.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoMarca As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoCategoriaOmogenea.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoCategoriaOmogenea As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo ProdottoGruppoMerceologico.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoGruppoMerceologico As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo ProdottoDescrizioneEstesa.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoDescrizioneEstesa As Global.DevExpress.Web.ASPxHtmlEditor.ASPxHtmlEditor
        
        '''<summary>
        '''Controllo ProdottoSchedaTecnica.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoSchedaTecnica As Global.DevExpress.Web.ASPxHtmlEditor.ASPxHtmlEditor
        
        '''<summary>
        '''Controllo ProdottoAllegato.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoAllegato As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo ProdottoAllegatoUpload.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoAllegatoUpload As Global.DevExpress.Web.ASPxUploadControl
        
        '''<summary>
        '''Controllo cbProdottoAllegato.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents cbProdottoAllegato As Global.DevExpress.Web.ASPxCallback
        
        '''<summary>
        '''Controllo ASPxFormLayout1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ASPxFormLayout1 As Global.DevExpress.Web.ASPxFormLayout
        
        '''<summary>
        '''Controllo ProdottoPrezzoListino.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoPrezzoListino As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoPrezzoEndUser.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoPrezzoEndUser As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoPrezzoRivenditore.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoPrezzoRivenditore As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoPeso.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoPeso As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoAltezza.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoAltezza As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoLarghezza.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoLarghezza As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoProfondita.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoProfondita As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ProdottoMetaTitle.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoMetaTitle As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo cb.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents cb As Global.DevExpress.Web.ASPxCallback
        
        '''<summary>
        '''Controllo ProdottoMetaDescription.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoMetaDescription As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo ProdottoMetaKeywords.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoMetaKeywords As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo HiddenField.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HiddenField As Global.DevExpress.Web.ASPxHiddenField
        
        '''<summary>
        '''Controllo FormLayout.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormLayout As Global.DevExpress.Web.ASPxFormLayout
        
        '''<summary>
        '''Controllo DescriptionTextBox.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents DescriptionTextBox As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo ProdottoGalleryUploadControl.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoGalleryUploadControl As Global.DevExpress.Web.ASPxUploadControl
        
        '''<summary>
        '''Controllo UploadedFilesTokenBox.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UploadedFilesTokenBox As Global.DevExpress.Web.ASPxTokenBox
        
        '''<summary>
        '''Controllo AllowedFileExtensionsLabel.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents AllowedFileExtensionsLabel As Global.DevExpress.Web.ASPxLabel
        
        '''<summary>
        '''Controllo MaxFileSizeLabel.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MaxFileSizeLabel As Global.DevExpress.Web.ASPxLabel
        
        '''<summary>
        '''Controllo ValidationSummary.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ValidationSummary As Global.DevExpress.Web.ASPxValidationSummary
        
        '''<summary>
        '''Controllo ProdottoGalleryInvia.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoGalleryInvia As Global.DevExpress.Web.ASPxButton
        
        '''<summary>
        '''Controllo RoundPanel.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents RoundPanel As Global.DevExpress.Web.ASPxRoundPanel
        
        '''<summary>
        '''Controllo DescriptionLabel.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents DescriptionLabel As Global.DevExpress.Web.ASPxLabel
        
        '''<summary>
        '''Controllo SubmittedFilesListBox.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents SubmittedFilesListBox As Global.DevExpress.Web.ASPxListBox
        
        '''<summary>
        '''Controllo UpdatePanel1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
        
        '''<summary>
        '''Controllo lvFotogallery.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents lvFotogallery As Global.System.Web.UI.WebControls.ListView
        
        '''<summary>
        '''Controllo settingsFormLayout.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents settingsFormLayout As Global.DevExpress.Web.ASPxFormLayout
        
        '''<summary>
        '''Controllo MenuMacro.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MenuMacro As Global.DevExpress.Web.ASPxDropDownEdit
        
        '''<summary>
        '''Controllo EntityDataSource1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents EntityDataSource1 As Global.Microsoft.AspNet.EntityDataSource.EntityDataSource
        
        '''<summary>
        '''Controllo ProdottoVisibile.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoVisibile As Global.DevExpress.Web.ASPxCheckBox
        
        '''<summary>
        '''Controllo ProdottoVetrina.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoVetrina As Global.DevExpress.Web.ASPxCheckBox
        
        '''<summary>
        '''Controllo ProdottoPromo.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoPromo As Global.DevExpress.Web.ASPxCheckBox
        
        '''<summary>
        '''Controllo ProdottoNoleggio.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoNoleggio As Global.DevExpress.Web.ASPxCheckBox
        
        '''<summary>
        '''Controllo ProdottoPrivato.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoPrivato As Global.DevExpress.Web.ASPxCheckBox
        
        '''<summary>
        '''Controllo ProdottoDataCreazione.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoDataCreazione As Global.DevExpress.Web.ASPxLabel
        
        '''<summary>
        '''Controllo ProdottoDataModifica.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoDataModifica As Global.DevExpress.Web.ASPxLabel
        
        '''<summary>
        '''Controllo uploadedImage.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents uploadedImage As Global.System.Web.UI.HtmlControls.HtmlImage
        
        '''<summary>
        '''Controllo ProdottoImmagineUpload.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoImmagineUpload As Global.DevExpress.Web.ASPxUploadControl
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.Admin.SiteAdmin
            Get
                Return CType(MyBase.Master,Emmemedia.Admin.SiteAdmin)
            End Get
        End Property
    End Class
End Namespace
