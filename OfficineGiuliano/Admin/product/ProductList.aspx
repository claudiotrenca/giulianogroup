﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="ProductList.aspx.vb" Inherits="Emmemedia.Admin.Product.ProductList" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .BtnFiltra {
            vertical-align: bottom;
        }
    </style>
    <script type="text/javascript">//<![CDATA[
        window.onload = function () {
            history.pushState(null, null, location.href);
            window.onpopstate = function (event) {
                history.go(1);
            };
        }//]]>

        function ButtonClickHandler(s, e) {
            if (e.buttonIndex == 0) {
                MenuMacro.SetKeyValue(null);
                SynchronizeFocusedNode();
            }
        }
        function DropDownHandler(s, e) {
            SynchronizeFocusedNode();
        }
        function TreeListInitHandler(s, e) {
            SynchronizeFocusedNode();
        }
        function TreeListEndCallbackHandler(s, e) {
            MenuMacro.SetKeyValue(TreeList.GetFocusedNodeKey())
            MenuMacro.AdjustDropDownWindow();
            UpdateEditBox();
        }
        function TreeListNodeClickHandler(s, e) {
            MenuMacro.SetKeyValue(e.nodeKey);
            MenuMacro.SetText(TreeList.cpCategoryNames[e.nodeKey]);
            MenuMacro.HideDropDown();
        }
        function SynchronizeFocusedNode() {
            var keyValue = MenuMacro.GetKeyValue();
            TreeList.SetFocusedNodeKey(keyValue);
            UpdateEditBox();
        }
        function UpdateEditBox() {
            var focusedCategoryName = '';
            var nodeKey = TreeList.GetFocusedNodeKey();
            if (nodeKey != 'null' && nodeKey != '')
                focusedCategoryName = TreeList.cpCategoryNames[nodeKey];
            var CategoryNameInEditBox = MenuMacro.GetText();
            if (CategoryNameInEditBox != focusedCategoryName)
                MenuMacro.SetText(focusedCategoryName);
        }
        function OnNodeExpanding(s, e) {
            s.PerformCallback(e.nodeKey);
            e.cancel = true;
        }

        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">

    <div class="row">
        <div class="col-md-12">
            <h3>PRODOTTI&nbsp;
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default" NavigateUrl="~/Admin/Product/ProductEdit.aspx">AGGIUNGI NUOVO</asp:HyperLink>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-inline">
                        <div class="form-group">
                            <dx:ASPxDropDownEdit ID="MenuMacro" runat="server" ClientInstanceName="MenuMacro" AllowUserInput="False" EnableAnimation="False" CssClass="form-control" HorizontalAlign="Left" Caption="Filtra per categoria :">
                                <DropDownWindowStyle>
                                    <Border BorderWidth="0px" />
                                </DropDownWindowStyle>
                                <DropDownWindowTemplate>
                                    <dx:ASPxTreeList ID="ASPxTreeList1" runat="server" AutoGenerateColumns="False" ClientInstanceName="TreeList"
                                        OnCustomJSProperties="TreeList_CustomJSProperties" OnDataBound="TreeList_DataBound" OnCustomCallback="ASPxTreeList1_CustomCallback"
                                        DataSourceID="edsCategorie" KeyFieldName="PK" ParentFieldName="ParentID">
                                        <Border BorderStyle="Solid" />
                                        <SettingsBehavior AllowFocusedNode="true" />
                                        <SettingsEditing ConfirmDelete="true" />
                                        <SettingsPager Mode="ShowAllNodes">
                                        </SettingsPager>
                                        <Columns>
                                            <dx:TreeListTextColumn FieldName="descrizione" VisibleIndex="0">
                                            </dx:TreeListTextColumn>
                                            <dx:TreeListTextColumn FieldName="ParentID" VisibleIndex="1" Visible="false">
                                            </dx:TreeListTextColumn>
                                        </Columns>
                                        <ClientSideEvents Init="TreeListInitHandler" EndCallback="TreeListEndCallbackHandler" NodeClick="TreeListNodeClickHandler" NodeExpanding="OnNodeExpanding" />
                                    </dx:ASPxTreeList>
                                </DropDownWindowTemplate>
                                <Buttons>
                                    <dx:EditButton Text="X"></dx:EditButton>
                                </Buttons>
                                <ButtonStyle BackColor="LightGray" ForeColor="Gray">
                                    <PressedStyle BackColor="#cccccc">
                                    </PressedStyle>
                                    <Border BorderColor="#eeeeee" />
                                </ButtonStyle>
                                <CaptionSettings Position="Top" />
                                <ClientSideEvents DropDown="DropDownHandler" ButtonClick="ButtonClickHandler" />
                            </dx:ASPxDropDownEdit>
                            <ef:EntityDataSource ID="edsCategorie" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EntitySetName="Menu" Where="IT.tipo = 'categoria'">
                            </ef:EntityDataSource>
                            
                        </div><asp:Button ID="Button1" runat="server" Text="Applica il filtro" CssClass="btn btn-default BtnFiltra"   />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" DataSourceID="edsProdotti" KeyFieldName="PK" Width="100%" AutoGenerateColumns="False" Theme="Moderno">
                        <Columns>
                            <dx:GridViewDataColumn FieldName="PK" Caption="ID" VisibleIndex="1" />
                            <dx:GridViewDataColumn>
                                <DataItemTemplate>
                                    <dx:ASPxImage runat="server" ID="imgTemplate" CssClass="img-responsive" Width="150px"
                                        ImageUrl='<%# "/public/images/product/" & Eval("ImmagineProdotto")  %>'>
                                    </dx:ASPxImage>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="Codice" Caption="Codice Prodotto" VisibleIndex="3" />
                            <dx:GridViewDataColumn FieldName="Modello" Caption="Modello" VisibleIndex="5" />
                            <dx:GridViewDataColumn FieldName="fk_categoria" Caption="categoria" Visible="false" />

                            <dx:GridViewCommandColumn ShowDeleteButton="True" VisibleIndex="7" Caption="Elimina" ShowEditButton="False" ShowApplyFilterButton="false">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataHyperLinkColumn FieldName="PK" VisibleIndex="9" Caption="Modifica">
                                <PropertiesHyperLinkEdit NavigateUrlFormatString="/admin/product/ProductEdit.aspx?id={0}" Text="Modifica" />
                            </dx:GridViewDataHyperLinkColumn>
                        </Columns>
                        <Settings ShowFooter="True" ShowFilterRow="True" ShowGroupPanel="false" ShowStatusBar="Auto" />
                        <Styles AlternatingRow-Enabled="True" AlternatingRow-BackColor="WhiteSmoke"></Styles>
                        <SettingsSearchPanel Visible="true" />
                    </dx:ASPxGridView>
                </div>
                <ef:EntityDataSource ID="edsProdotti" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EntitySetName="Prodotti" EnableDelete="true" OrderBy="it.pk DESC" >
                </ef:EntityDataSource>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterPlaceHolder" runat="server">
</asp:Content>