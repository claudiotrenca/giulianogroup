﻿Imports DevExpress.Data.Linq
Imports DevExpress.Web
Imports DevExpress.Web.ASPxTreeList
Imports Microsoft.AspNet.EntityDataSource

Namespace Admin
    Namespace Product
        Public Class CategoryEdit
            Inherits System.Web.UI.Page

            Private _PK As Integer = Nothing

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                If Not IsPostBack Then
                    If Request.QueryString("id") IsNot Nothing Then
                        _PK = Request.QueryString("id")
                        ViewState("PK") = _PK

                        BindData()
                    End If
                Else
                    _PK = ViewState("PK")
                End If

                MenuLink.ReadOnly = True

            End Sub

            Protected Sub TreeList_DataBound(ByVal sender As Object, ByVal e As EventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                If (Not IsCallback) AndAlso (Not IsPostBack) Then
                    tree.CollapseAll()
                End If
            End Sub

            Protected Sub TreeList_CustomJSProperties(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxTreeList.TreeListCustomJSPropertiesEventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                Dim CategoryNames As New Hashtable()
                For Each node As TreeListNode In tree.GetVisibleNodes()
                    CategoryNames.Add(node.Key, node("Descrizione"))
                Next node
                e.Properties("cpCategoryNames") = CategoryNames
            End Sub

            Protected Sub ASPxTreeList1_CustomCallback(sender As Object, e As TreeListCustomCallbackEventArgs)
                Dim ASPxTreeList1 As ASPxTreeList = TryCast(sender, ASPxTreeList)
                ASPxTreeList1.CollapseAll()
                Dim node As TreeListNode = ASPxTreeList1.FindNodeByKeyValue(e.Argument)
                While node.ParentNode IsNot Nothing
                    node.Expanded = True
                    node = node.ParentNode
                End While
            End Sub

            Protected Sub CategoryMenuLinkTree_DataBound(ByVal sender As Object, ByVal e As EventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                If (Not IsCallback) AndAlso (Not IsPostBack) Then
                    tree.CollapseAll()
                End If
            End Sub

            Protected Sub CategoryMenuLinkTree_CustomJSProperties(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxTreeList.TreeListCustomJSPropertiesEventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                Dim CategoryNames As New Hashtable()
                For Each node As TreeListNode In tree.GetVisibleNodes()
                    CategoryNames.Add(node.Key, node("Descrizione"))
                Next node
                e.Properties("cpCategoryNames") = CategoryNames
            End Sub

            Protected Sub CategoryMenuLinkTree_CustomCallback(sender As Object, e As TreeListCustomCallbackEventArgs)
                Dim CategoryMenuLinkTree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                CategoryMenuLinkTree.CollapseAll()
                Dim node As TreeListNode = CategoryMenuLinkTree.FindNodeByKeyValue(e.Argument)
                While node.ParentNode IsNot Nothing
                    node.Expanded = True
                    node = node.ParentNode
                End While
            End Sub

            Private Function FindAvailableLink(ByVal _str As String) As String

                Dim rv As String = String.Empty
                Dim Suffix As String = String.Empty
                Dim Index As Integer = 0
                Dim Link As String = Classi.Utility.StringToUrl(_str)

                If _PK <> Nothing Then
                    Using context As New DataClass.DataEntities
                        While (From row In context.Menu Where row.Link = Link And row.PK <> _PK And row.Tipo = "categoria" Select row).FirstOrDefault IsNot Nothing
                            Index += 1
                            Suffix = "-" & CStr(Index)
                            Link = (Classi.Utility.StringToUrl(_str) & Suffix).Trim
                        End While
                        rv = Link
                    End Using
                Else
                    Using context As New DataClass.DataEntities
                        While (From row In context.Menu Where row.Link = Link And row.Tipo = "categoria" Select row).FirstOrDefault IsNot Nothing
                            Index += 1
                            Suffix = "-" & CStr(Index)
                            Link = (Classi.Utility.StringToUrl(_str) & Suffix).Trim
                        End While
                        rv = Link
                    End Using
                End If

                Return rv

            End Function

            Protected Sub cb_Callback(source As Object, e As CallbackEventArgs) Handles cb.Callback
                e.Result = String.Format(FindAvailableLink(e.Parameter))
            End Sub

            Protected Sub ctreecbb_Callback(source As Object, e As CallbackEventArgs) Handles ctreecb.Callback
                e.Result = String.Format(Classi.Utility.StringToUrl(e.Parameter))
            End Sub

            Private Sub BindData()

                Dim row As New DataClass.Menu

                Using dataContext As New DataClass.DataEntities
                    row = dataContext.Menu.FirstOrDefault(Function(p) p.PK = _PK)
                End Using

                If row IsNot Nothing Then
                    MenuNome.Text = row.Descrizione
                    MenuLink.Text = row.Link
                    MenuCulture.SelectedValue = row.Culture.ToString.Trim
                    MenuVisibile.Checked = row.FlagVisibile

                    If row.FkMenu IsNot Nothing Then

                        Dim treeList As ASPxTreeList = DirectCast(CategoryMenuLink.FindControl("CategoryMenuLinkTree"), ASPxTreeList)
                        treeList.UnselectAll()
                        Dim node As TreeListNode = treeList.FindNodeByKeyValue(row.FkMenu)
                        node.Focus()
                        node.Selected = True
                        CategoryMenuLink.KeyValue = row.FkMenu
                        CategoryMenuLink.Text = DataClass.Menu.GetNameFromID(row.FkMenu)

                    End If

                    If row.ParentID IsNot Nothing Then
                        Dim treeList As ASPxTreeList = DirectCast(MenuMacro.FindControl("ASPxTreeList1"), ASPxTreeList)
                        treeList.UnselectAll()
                        Dim node As TreeListNode = treeList.FindNodeByKeyValue(row.ParentID)
                        node.Focus()
                        node.Selected = True
                        MenuMacro.KeyValue = row.ParentID
                        MenuMacro.Text = DataClass.Menu.GetNameFromID(row.ParentID)
                    End If
                End If

            End Sub

            Private Sub SaveData()

                Using context As New DataClass.DataEntities

                    If _PK = Nothing Then

                        Dim mt As New DataClass.Menu
                        mt.Tipo = "categoria"
                        mt.Descrizione = MenuNome.Text
                        mt.Link = MenuLink.Text
                        mt.Culture = MenuCulture.SelectedValue
                        mt.FlagVisibile = MenuVisibile.Checked
                        mt.IncludeInTopMenu = IncludeTopMenu.Checked
                        mt.HasCategory = False
                        mt.FlagPrivato = False

                        If MenuMacro.KeyValue <> "" Then
                            mt.ParentID = CInt(MenuMacro.KeyValue)
                        End If

                        If CategoryMenuLink.KeyValue <> "" Then
                            Dim id As Integer = CInt(CategoryMenuLink.KeyValue)
                            mt.FkMenu = id
                        End If

                        context.Configuration.AutoDetectChangesEnabled = True
                        context.Menu.Attach(mt)
                        context.Entry(mt).State = Entity.EntityState.Added
                        context.SaveChanges()

                        _PK = mt.PK

                        ViewState("PK") = _PK

                    Else
                        Dim mt = context.Menu.SingleOrDefault(Function(p) p.PK = _PK)
                        mt.Descrizione = MenuNome.Text
                        mt.Link = MenuLink.Text
                        mt.Culture = MenuCulture.SelectedValue
                        mt.FlagVisibile = MenuVisibile.Checked
                        mt.HasCategory = False
                        mt.FlagPrivato = False

                        If MenuMacro.KeyValue <> "" Then
                            mt.ParentID = CInt(MenuMacro.KeyValue)
                        Else
                            mt.ParentID = Nothing
                        End If

                        If CategoryMenuLink.KeyValue <> "" Then
                            Dim id As Integer = CInt(CategoryMenuLink.KeyValue)
                            mt.FkMenu = id
                        End If

                        context.SaveChanges()
                    End If

                    If CategoryMenuLink.KeyValue <> "" Then
                        Dim id As Integer = CInt(CategoryMenuLink.KeyValue)
                        Dim mtMenu = context.Menu.SingleOrDefault(Function(p) p.PK = id)
                        mtMenu.FkCategory = _PK
                        mtMenu.HasCategory = True
                        context.SaveChanges()
                    End If

                End Using

            End Sub

            Private Sub btnModificaLink_Click(sender As Object, e As EventArgs) Handles btnModificaLink.Click

                If ViewState("ModificaLink") = True Then
                    MenuLink.Text = String.Format(FindAvailableLink(MenuLink.Text))
                    ViewState("ModificaLink") = False
                    MenuLink.ReadOnly = True
                    btnModificaLink.Text = "Modifica"
                Else
                    MenuLink.ReadOnly = False
                    btnModificaLink.Text = "SALVA"
                    ViewState("ModificaLink") = True
                End If

            End Sub

            Private Sub btnMenuSalva_Click(sender As Object, e As EventArgs) Handles btnSalva.Click
                SaveData()
            End Sub

        End Class

    End Namespace

End Namespace