﻿Imports DevExpress.Data.Linq
Imports DevExpress.Web
Imports DevExpress.Web.ASPxTreeList
Imports DevExpress.Web.Internal
Imports System.Drawing
Imports System.IO
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports DevExpress.Utils
Imports System.Reflection

Namespace Admin
    Namespace Product

        Public Class ProductEdit
            Inherits System.Web.UI.Page

            Private Const UploadImageDirectory As String = "~/public/images/product/"
            Private Const UploadGalleryDirectory As String = "~/public/images/product/Gallery/"
            Private Const UploadDocDirectory As String = "~/public/doc/product/"

            Private _PK As Integer
            Private _PKMeta As Integer
            Private _FileImageName As String = String.Empty
            Private _FileDocName As String = String.Empty

            Protected Property SubmissionID() As String
                Get
                    Return HiddenField.Get("SubmissionID").ToString()
                End Get
                Set(ByVal value As String)
                    HiddenField.Set("SubmissionID", value)
                End Set
            End Property

            Private ReadOnly Property UploadedFilesStorage() As UploadedFilesStorage
                Get
                    Return UploadControlHelper.GetUploadedFilesStorageByKey(SubmissionID)
                End Get
            End Property

            Private Sub ProductEdit_Init(sender As Object, e As EventArgs) Handles Me.Init

                If Request.RawUrl.ToString.EndsWith(".css") OrElse Request.RawUrl.ToString.EndsWith(".js") OrElse Request.RawUrl.ToString.EndsWith(".jpg") OrElse Request.RawUrl.ToString.EndsWith(".png") Then
                    Exit Sub
                End If

                If (Not IsPostBack) Then

                    Session("PkProduct") = Nothing
                    Session("PkProductMeta") = Nothing
                    Session("ProductImageName") = Nothing
                    Session("FileDocName") = Nothing

                    InitCategoriaOmogenea()
                    initGruppoMerceologico()

                    _PK = IIf(Request.QueryString("id") IsNot Nothing, Request.QueryString("id"), Nothing)

                    If Not _PK = Nothing Then
                        BindData()
                        BindSeo()
                        BindGallery()
                    End If
                End If

            End Sub

            Private Sub ProductEdit_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete

            End Sub

            Private Sub InitCategoriaOmogenea()

                Dim row As List(Of DataClass.CategoriaOmogenea)

                Using dataContext As New DataClass.DataEntities
                    row = (From p In dataContext.CategoriaOmogenea Select p Order By p.CategoriaOmogenea1).ToList
                End Using

                Dim itm As New DataClass.CategoriaOmogenea
                itm.PK = 0
                itm.CategoriaOmogenea1 = "Seleziona..."

                row.Insert(0, itm)

                ProdottoCategoriaOmogenea.DataSource = row
                ProdottoCategoriaOmogenea.DataTextField = "CategoriaOmogenea1"
                ProdottoCategoriaOmogenea.DataValueField = "PK"
                ProdottoCategoriaOmogenea.DataBind()

            End Sub

            Private Sub initGruppoMerceologico()
                Dim row As List(Of DataClass.GruppoMerceologico)

                Using dataContext As New DataClass.DataEntities
                    row = (From p In dataContext.GruppoMerceologico Select p Order By p.GruppoMerceologico1).ToList
                End Using

                Dim itm As New DataClass.GruppoMerceologico
                itm.PK = 0
                itm.GruppoMerceologico1 = "Seleziona..."

                row.Insert(0, itm)

                ProdottoGruppoMerceologico.DataSource = row
                ProdottoGruppoMerceologico.DataTextField = "GruppoMerceologico1"
                ProdottoGruppoMerceologico.DataValueField = "PK"
                ProdottoGruppoMerceologico.DataBind()
            End Sub

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                If (Not IsPostBack) Then
                    Session("PkProduct") = _PK
                Else
                    _PK = Session("PkProduct")

                    If Session("PkProductMeta") IsNot Nothing Then
                        _PKMeta = Session("PkProductMeta")
                    End If

                    If Session("ProductImageName") IsNot Nothing Then
                        _FileImageName = Session("ProductImageName")
                    End If
                    If Session("FileDocName") IsNot Nothing Then
                        _FileDocName = Session("FileDocName")
                    End If

                End If

                uploadedImage.Attributes.Add("onload", "onImageLoad()")

                If (Not IsPostBack) Then
                    SubmissionID = UploadControlHelper.GenerateUploadedFilesStorageKey()
                    UploadControlHelper.AddUploadedFilesStorage(SubmissionID)
                End If

                FormLayout.FindItemOrGroupByName("ResultGroup").Visible = False
            End Sub

            Private Sub ProductEdit_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
                UploadControlHelper.RemoveOldStorages()
            End Sub

            Private Sub ProductEdit_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
                If _PK = Nothing Then
                    '  ASPxPageControl1.TabPages(4).Enabled = False
                    ASPxPageControl1.TabPages(5).Enabled = False
                    ProdottoAllegatoUpload.Enabled = False
                    ProdottoImmagineUpload.Enabled = False
                End If
            End Sub

            Private Sub InitTextEditor()
                ProdottoDescrizioneEstesa.SettingsHtmlEditing.UpdateBoldItalic = False
                ProdottoDescrizioneEstesa.SettingsHtmlEditing.AllowIFrames = True
                ProdottoDescrizioneEstesa.SettingsHtmlEditing.AllowHTML5MediaElements = True
                ProdottoDescrizioneEstesa.SettingsHtmlEditing.AllowObjectAndEmbedElements = True
                ProdottoDescrizioneEstesa.SettingsHtmlEditing.AllowScripts = True
            End Sub

            Protected Sub tlCategoria_DataBound(ByVal sender As Object, ByVal e As EventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                If (Not IsCallback) AndAlso (Not IsPostBack) Then
                    tree.CollapseAll()
                End If
            End Sub

            Protected Sub tlCategoria_CustomJSProperties(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxTreeList.TreeListCustomJSPropertiesEventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                Dim CategoryNames As New Hashtable()
                For Each node As TreeListNode In tree.GetVisibleNodes()
                    CategoryNames.Add(node.Key, node("Descrizione"))
                Next node
                e.Properties("cpCategoryNames") = CategoryNames
            End Sub

            Protected Sub tlCategoria_CustomCallback(sender As Object, e As TreeListCustomCallbackEventArgs)
                Dim tlc As ASPxTreeList = TryCast(sender, ASPxTreeList)
                tlc.CollapseAll()
                Dim node As TreeListNode = tlc.FindNodeByKeyValue(e.Argument)
                While node.ParentNode IsNot Nothing
                    node.Expanded = True
                    node = node.ParentNode
                End While
            End Sub

            Protected Sub ProdottoImmagineUpload_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs) Handles ProdottoImmagineUpload.FileUploadComplete

                e.CallbackData = SavePostedFile(e.UploadedFile, UploadImageDirectory)
                Dim id As Integer = Session("PkProduct")

                _FileImageName = e.UploadedFile.FileName
                _FileImageName = id & "_" & _FileImageName
                Session("ProductImageName") = _FileImageName
                e.UploadedFile.SaveAs(CombinePath(_FileImageName, UploadImageDirectory))

                Using context As New DataClass.DataEntities
                    Dim row = context.Prodotti.SingleOrDefault(Function(p) p.PK = id)
                    row.ImmagineProdotto = _FileImageName
                    context.SaveChanges()
                End Using

            End Sub

            Protected Sub ProdottoAllegatoUpload_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs) Handles ProdottoAllegatoUpload.FileUploadComplete
                '  e.CallbackData = SavePostedFile(e.UploadedFile, UploadDocDirectory)
                _FileDocName = e.UploadedFile.FileName
                _FileDocName = Session("PkProduct") & "_" & _FileDocName
                Session("FileDocName") = _FileDocName
                e.UploadedFile.SaveAs(CombinePath(_FileDocName, UploadDocDirectory))
                ProdottoAllegato.Text = CombinePath(_FileDocName, UploadDocDirectory)

            End Sub

            Protected Function SavePostedFile(ByVal uploadedFile As UploadedFile, ByVal _path As String) As String
                Try
                    If (Not uploadedFile.IsValid) Then
                        Return String.Empty
                    End If

                    Dim fileName As String = System.IO.Path.ChangeExtension(System.IO.Path.GetRandomFileName(), ".jpg")
                    Dim fullFileName As String = CombinePath(fileName, _path)

                    Using original As Image = Image.FromStream(uploadedFile.FileContent)

                        Using thumbnail As Image = PhotoUtils.Inscribe(CType(original, System.Drawing.Bitmap), 550, 250) '   ImageUtils.CreateThumbnailImage(CType(original, System.Drawing.Bitmap), ImageSizeMode.ActualSizeOrFit, New Size(550, 250))

                            ImageUtils.SaveToJpeg(CType(thumbnail, System.Drawing.Bitmap), fullFileName)
                        End Using

                    End Using

                    LibUploadingUtils.RemoveFileWithDelay(fileName, fullFileName, 5)

                    Return fileName
                Catch ex As Exception
                    Throw ex
                End Try

            End Function

            Protected Function CombinePath(ByVal fileName As String, ByVal _path As String) As String
                Return System.IO.Path.Combine(Server.MapPath(_path), fileName)
            End Function

            Protected Sub ProcessSubmit(ByVal description As String, ByVal fileInfos As List(Of UploadedFileInfo))

                DescriptionLabel.Value = Server.HtmlEncode(description)

                Using context As New DataClass.DataEntities

                    Dim i As Integer = 1
                    For Each fileInfo As UploadedFileInfo In fileInfos
                        Dim fileContent As Byte() = File.ReadAllBytes(fileInfo.FilePath)
                        Dim fileName As String = CStr(_PK) & "_" & CStr(i) & "_" & fileInfo.UniqueFileName
                        Dim img As Image = byteArrayToImage(fileContent)
                        img.Save(CombinePath(fileName, UploadGalleryDirectory))

                        Dim row As New DataClass.ProdottiGallery
                        row.FkProdotto = _PK
                        row.Descrizione = DescriptionTextBox.Text
                        row.PercorsoImmagine = fileName

                        context.Configuration.AutoDetectChangesEnabled = True
                        context.ProdottiGallery.Attach(row)
                        context.Entry(row).State = Entity.EntityState.Added
                        context.SaveChanges()
                        i = i + 1

                        row = Nothing
                    Next fileInfo

                End Using

                SubmittedFilesListBox.DataSource = fileInfos
                SubmittedFilesListBox.DataBind()

                FormLayout.FindItemOrGroupByName("ResultGroup").Visible = True

                BindGallery()

            End Sub

            Private Function byteArrayToImage(byteArrayIn As Byte()) As Image
                Dim ms As New MemoryStream(byteArrayIn)
                Dim returnImage As Image = Image.FromStream(ms)
                Return returnImage
            End Function

            Private Sub LtwFoto_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvFotogallery.ItemCommand
                If e.CommandName = "Elimina" Then
                    Dim _idGallery As Integer = e.CommandArgument
                    Using context As New DataClass.DataEntities
                        Dim obj = context.ProdottiGallery.FirstOrDefault(Function(p) p.PK = _idGallery)
                        If obj IsNot Nothing Then
                            Try
                                context.ProdottiGallery.Remove(obj)
                                context.SaveChanges()
                            Catch ex As Exception
                                Throw ex
                            End Try
                        End If
                    End Using
                    BindGallery()
                End If
            End Sub

            Protected Sub ProdottoGalleryUploadControl_FileUploadComplete(ByVal sender As Object, ByVal e As DevExpress.Web.FileUploadCompleteEventArgs) Handles ProdottoGalleryUploadControl.FileUploadComplete
                Dim isSubmissionExpired As Boolean = False
                If UploadedFilesStorage Is Nothing Then
                    isSubmissionExpired = True
                    UploadControlHelper.AddUploadedFilesStorage(SubmissionID)
                End If
                Dim tempFileInfo As UploadedFileInfo = UploadControlHelper.AddUploadedFileInfo(SubmissionID, e.UploadedFile.FileName)

                e.UploadedFile.SaveAs(tempFileInfo.FilePath)

                If e.IsValid Then
                    e.CallbackData = tempFileInfo.UniqueFileName & "|" & isSubmissionExpired
                End If
            End Sub

            Protected Sub ProdottoGalleryInvia_Click(sender As Object, e As EventArgs) Handles ProdottoGalleryInvia.Click
                Dim resultFileInfos As New List(Of UploadedFileInfo)()

                Dim description As String = DescriptionTextBox.Value.ToString()
                Dim allFilesExist As Boolean = True

                If UploadedFilesStorage Is Nothing Then
                    UploadedFilesTokenBox.Tokens = New TokenCollection()
                End If

                For Each fileName As String In UploadedFilesTokenBox.Tokens
                    Dim UpFileInfo As UploadedFileInfo = UploadControlHelper.GetDemoFileInfo(SubmissionID, fileName)
                    Dim fileInfo As New FileInfo(UpFileInfo.FilePath)

                    If fileInfo.Exists Then
                        UpFileInfo.FileSize = LibUploadingUtils.FormatSize(fileInfo.Length)
                        resultFileInfos.Add(UpFileInfo)
                    Else
                        allFilesExist = False
                    End If
                Next fileName

                If allFilesExist AndAlso resultFileInfos.Count > 0 Then
                    ProcessSubmit(description, resultFileInfos)

                    UploadControlHelper.RemoveUploadedFilesStorage(SubmissionID)

                    ASPxEdit.ClearEditorsInContainer(FormLayout, True)
                Else
                    UploadedFilesTokenBox.ErrorText = "Caricamento fallito a causa di un errore di timeout (5 min)"
                    UploadedFilesTokenBox.IsValid = False
                End If
            End Sub

            Private Sub BindData()
                Dim row As New DataClass.Prodotti

                Using dataContext As New DataClass.DataEntities
                    row = dataContext.Prodotti.FirstOrDefault(Function(p) p.PK = _PK)
                End Using

                If row IsNot Nothing Then

                    ProdottoCodiceFornitore.Text = row.CodiceFornitore
                    ProdottoCodiceInterno.Text = row.Codice
                    ProdottoDataCreazione.Text = row.DataInserimento
                    ProdottoDescrizioneBreve.Text = row.DescrizioneBreve
                    ProdottoDescrizioneEstesa.Html = row.DescrizioneEstesa
                    ProdottoSchedaTecnica.Html = row.Caratteristiche
                    ProdottoFamiglia.Text = row.Famiglia
                    ProdottoMarca.Text = row.Marca
                    ProdottoModello.Text = row.Modello
                    ProdottoAllegato.Text = row.Allegato

                    ProdottoPrezzoEndUser.Text = row.PrezzoFinale
                    ProdottoPrezzoListino.Text = row.PrezzoListino
                    ProdottoPrezzoRivenditore.Text = row.PrezzoRivenditore

                    ProdottoPeso.Text = row.Peso
                    ProdottoLarghezza.Text = row.Larghezza
                    ProdottoAltezza.Text = row.Altezza
                    ProdottoProfondita.Text = row.Profondita

                    ProdottoPrivato.Checked = row.FlagPrivato
                    ProdottoNoleggio.Checked = row.FlagNoleggio
                    ProdottoPromo.Checked = row.FlagPromo
                    ProdottoVetrina.Checked = row.FlagVetrina
                    ProdottoVisibile.Checked = row.FlagAttivo

                    ProdottoCategoriaOmogenea.SelectedValue = row.FkCategoriaOmogenea
                    ProdottoGruppoMerceologico.SelectedValue = row.FkGruppoMerceologico

                    If Not String.IsNullOrEmpty(row.ImmagineProdotto) Then
                        uploadedImage.Src = "/public/images/product/" & row.ImmagineProdotto
                        uploadedImage.Attributes.Item("class") = ""
                        uploadedImage.Attributes.Item("style") = ""
                        uploadedImage.Style.Clear()
                        uploadedImage.Height = 240
                    End If

                    If row.FkCategoria IsNot Nothing Then
                        Dim treeList As ASPxTreeList = DirectCast(MenuMacro.FindControl("tlCategoria"), ASPxTreeList)
                        treeList.UnselectAll()
                        Dim node As TreeListNode = treeList.FindNodeByKeyValue(row.FkCategoria)
                        node.Focus()
                        node.Selected = True
                        MenuMacro.KeyValue = row.FkCategoria
                        MenuMacro.Text = DataClass.Menu.GetNameFromID(row.FkCategoria)
                    End If

                End If

            End Sub

            Private Sub BindSeo()

                Dim row As New DataClass.ProdottiMeta

                Using dataContext As New DataClass.DataEntities
                    row = dataContext.ProdottiMeta.FirstOrDefault(Function(p) p.FKProdotto = _PK)
                End Using

                If row IsNot Nothing Then
                    _PKMeta = row.PK

                    ProdottoMetaTitle.Text = row.ProductTitle
                    ProdottoMetaDescription.Text = row.ProductDescription
                    ProdottoMetaKeywords.Text = row.ProductKeywords

                    Session("PkProductMeta") = _PKMeta

                End If

            End Sub

            Private Sub BindGallery()
                Dim gallery As New List(Of DataClass.ProdottiGallery)
                Using context As New DataClass.DataEntities
                    gallery = (From row In context.ProdottiGallery Where row.FkProdotto = _PK Select row).ToList
                End Using
                lvFotogallery.DataSource = gallery
                lvFotogallery.DataBind()
            End Sub

            Private Function SaveData() As Integer

                Dim FkCategoria As Integer

                If MenuMacro.KeyValue <> "" Then
                    FkCategoria = CInt(MenuMacro.KeyValue)
                Else
                    FkCategoria = Nothing
                End If

                Using context As New DataClass.DataEntities

                    If _PK = Nothing Then

                        Dim row As New DataClass.Prodotti

                        row.CodiceFornitore = ProdottoCodiceFornitore.Text
                        row.Codice = ProdottoCodiceInterno.Text
                        row.DescrizioneBreve = ProdottoDescrizioneBreve.Text
                        row.DescrizioneEstesa = ProdottoDescrizioneEstesa.Html
                        row.Caratteristiche = ProdottoSchedaTecnica.Html
                        row.Famiglia = ProdottoFamiglia.Text
                        row.Marca = ProdottoMarca.Text
                        row.Modello = ProdottoModello.Text
                        row.Allegato = ProdottoAllegato.Text

                        row.PrezzoFinale = Classi.Utility.EvalDec(ProdottoPrezzoEndUser.Text)
                        row.PrezzoListino = Classi.Utility.EvalDec(ProdottoPrezzoListino.Text)
                        row.PrezzoRivenditore = Classi.Utility.EvalDec(ProdottoPrezzoRivenditore.Text)

                        row.Qta = 1
                        row.Peso = Classi.Utility.EvalDec(ProdottoPeso.Text)
                        row.Larghezza = Classi.Utility.EvalDec(ProdottoLarghezza.Text)
                        row.Altezza = Classi.Utility.EvalDec(ProdottoAltezza.Text)
                        row.Profondita = Classi.Utility.EvalDec(ProdottoProfondita.Text)

                        row.FlagPrivato = ProdottoPrivato.Checked
                        row.FlagNoleggio = ProdottoNoleggio.Checked
                        row.FlagPromo = ProdottoPromo.Checked
                        row.FlagVetrina = ProdottoVetrina.Checked
                        row.FlagAttivo = ProdottoVisibile.Checked

                        row.DataInserimento = Date.Now
                        row.DataUltimaModifica = Date.Now

                        row.FkCategoria = FkCategoria

                        row.FkCategoriaOmogenea = ProdottoCategoriaOmogenea.SelectedValue
                        row.FkGruppoMerceologico = ProdottoGruppoMerceologico.SelectedValue

                        context.Configuration.AutoDetectChangesEnabled = True
                        context.Prodotti.Attach(row)
                        context.Entry(row).State = Entity.EntityState.Added
                        context.SaveChanges()

                        _PK = row.PK

                    Else
                        Dim row = context.Prodotti.SingleOrDefault(Function(p) p.PK = _PK)

                        row.CodiceFornitore = ProdottoCodiceFornitore.Text
                        row.Codice = ProdottoCodiceInterno.Text
                        row.DescrizioneBreve = ProdottoDescrizioneBreve.Text
                        row.DescrizioneEstesa = ProdottoDescrizioneEstesa.Html
                        row.Caratteristiche = ProdottoSchedaTecnica.Html
                        row.Famiglia = ProdottoFamiglia.Text
                        row.Marca = ProdottoMarca.Text
                        row.Modello = ProdottoModello.Text
                        row.Allegato = ProdottoAllegato.Text

                        row.PrezzoFinale = Classi.Utility.EvalDec(ProdottoPrezzoEndUser.Text)
                        row.PrezzoListino = Classi.Utility.EvalDec(ProdottoPrezzoListino.Text)
                        row.PrezzoRivenditore = Classi.Utility.EvalDec(ProdottoPrezzoRivenditore.Text)

                        row.Qta = 1
                        row.Peso = Classi.Utility.EvalDec(ProdottoPeso.Text)
                        row.Larghezza = Classi.Utility.EvalDec(ProdottoLarghezza.Text)
                        row.Altezza = Classi.Utility.EvalDec(ProdottoAltezza.Text)
                        row.Profondita = Classi.Utility.EvalDec(ProdottoProfondita.Text)

                        row.FlagPrivato = ProdottoPrivato.Checked
                        row.FlagNoleggio = ProdottoNoleggio.Checked
                        row.FlagPromo = ProdottoPromo.Checked
                        row.FlagVetrina = ProdottoVetrina.Checked
                        row.FlagAttivo = ProdottoVisibile.Checked

                        row.DataUltimaModifica = Date.Now

                        row.FkCategoria = FkCategoria
                        row.FkCategoriaOmogenea = ProdottoCategoriaOmogenea.SelectedValue
                        row.FkGruppoMerceologico = ProdottoGruppoMerceologico.SelectedValue

                        If Not String.IsNullOrEmpty(_FileDocName) Then
                            row.Allegato = _FileDocName
                            ProdottoAllegato.Text = _FileDocName
                        End If

                        If Not String.IsNullOrEmpty(_FileImageName) Then
                            row.ImmagineProdotto = _FileImageName

                            uploadedImage.Src = "/public/images/product/" & _FileImageName
                            uploadedImage.Attributes.Item("class") = ""
                            uploadedImage.Attributes.Item("style") = ""
                            uploadedImage.Style.Clear()
                            uploadedImage.Height = 240

                        End If

                        context.SaveChanges()

                    End If

                End Using

                SaveSeo()

                Return _PK

            End Function

            Private Sub SaveSeo()

                Using context As New DataClass.DataEntities
                    If _PKMeta = Nothing Then
                        Dim row As New DataClass.ProdottiMeta
                        row.FKProdotto = _PK
                        row.ProductTitle = ProdottoMetaTitle.Text
                        row.ProductDescription = ProdottoMetaDescription.Text
                        row.ProductKeywords = ProdottoMetaKeywords.Text

                        context.Configuration.AutoDetectChangesEnabled = True
                        context.ProdottiMeta.Attach(row)
                        context.Entry(row).State = Entity.EntityState.Added
                        context.SaveChanges()

                        _PKMeta = row.PK
                        Session("PkProductMeta") = _PKMeta
                    Else
                        Dim row = context.ProdottiMeta.SingleOrDefault(Function(p) p.PK = _PKMeta)

                        row.ProductTitle = ProdottoMetaTitle.Text
                        row.ProductDescription = ProdottoMetaDescription.Text
                        row.ProductKeywords = ProdottoMetaKeywords.Text
                        context.SaveChanges()
                    End If
                End Using
            End Sub

            Protected Sub btnSalva_Click(sender As Object, e As EventArgs) Handles btnSalva.Click
                If MenuMacro.KeyValue <> "" Then
                    If SaveData() <> Nothing Then
                        Response.Redirect("/admin/product/ProductEdit.aspx?id=" & _PK)
                    End If
                Else
                    ScriptManager.RegisterStartupScript(Page, Page.GetType, "ERROR", "$('#spnTitle').text('ERRORE');$('#btnConfirm').attr('onclick',""$('#modalPopUp').modal('hide')""); $('#spnMsg').text('SELEZIONARE LA CATEGORIA DI APPARTENENZA'); $('#modalPopUp').modal('show');", True)
                End If

            End Sub

            Protected Sub cb_Callback(source As Object, e As CallbackEventArgs) Handles cb.Callback
                e.Result = String.Format(e.Parameter)
            End Sub

            Protected Sub UpdatePanel_Unload(sender As Object, e As EventArgs) Handles UpdatePanel1.Unload
                Dim methodInfo As MethodInfo = GetType(ScriptManager).GetMethods(BindingFlags.NonPublic Or BindingFlags.Instance).Where(Function(i) i.Name.Equals("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel")).First()
                methodInfo.Invoke(ScriptManager.GetCurrent(Page), New Object() {TryCast(sender, UpdatePanel)})
            End Sub

            Private Sub cbProdottoAllegato_Callback(source As Object, e As CallbackEventArgs) Handles cbProdottoAllegato.Callback
                e.Result = Session("FileDocName")
            End Sub

        End Class

    End Namespace

End Namespace