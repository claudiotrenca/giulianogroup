﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="ContentList.aspx.vb" Inherits="Emmemedia.Admin.Content.ContentList" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function onCellClick(rowIndex, fieldName) {
            grwPage.PerformCallback(rowIndex + "|" + fieldName);
        }
        function OnEditorKeyPress(editor, e) {
            if (e.htmlEvent.keyCode == 13 || e.htmlEvent.keyCode == 9) {
                grwPage.UpdateEdit();
            }
            else
                if (e.htmlEvent.keyCode == 27)
                    grwPage.CancelEdit();
        }
        // DROPDOWN TREELIST

        function ButtonClickHandler(s, e) {
            if (e.buttonIndex == 0) {
                MenuMacro.SetKeyValue(null);
                SynchronizeFocusedNode();
            }
        }
        function DropDownHandler(s, e) {
            SynchronizeFocusedNode();
        }
        function TreeListInitHandler(s, e) {
            SynchronizeFocusedNode();
        }
        function TreeListEndCallbackHandler(s, e) {
            MenuMacro.SetKeyValue(TreeList.GetFocusedNodeKey())
            MenuMacro.AdjustDropDownWindow();
            UpdateEditBox();
        }
        function TreeListNodeClickHandler(s, e) {
            MenuMacro.SetKeyValue(e.nodeKey);
            MenuMacro.SetText(TreeList.cpCategoryNames[e.nodeKey]);
            MenuMacro.HideDropDown();
        }
        function SynchronizeFocusedNode() {
            var keyValue = MenuMacro.GetKeyValue();
            TreeList.SetFocusedNodeKey(keyValue);
            UpdateEditBox();
        }
        function UpdateEditBox() {
            var focusedCategoryName = '';
            var nodeKey = TreeList.GetFocusedNodeKey();
            if (nodeKey != 'null' && nodeKey != '')
                focusedCategoryName = TreeList.cpCategoryNames[nodeKey];
            var CategoryNameInEditBox = MenuMacro.GetText();
            if (CategoryNameInEditBox != focusedCategoryName)
                MenuMacro.SetText(focusedCategoryName);
        }
        function OnNodeExpanding(s, e) {
            s.PerformCallback(e.nodeKey);
            e.cancel = true;
        }

        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>MENU
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default" NavigateUrl="~/Admin/content/ContentEdit.aspx">AGGIUNGI NUOVO</asp:HyperLink>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-inline">
                <div class="form-group">
                    <dx:ASPxDropDownEdit ID="MenuMacro" runat="server" ClientInstanceName="MenuMacro" AllowUserInput="False" EnableAnimation="False" CssClass="form-control" HorizontalAlign="Left" Caption="Filtra per categoria :">
                        <DropDownWindowStyle>
                            <Border BorderWidth="0px" />
                        </DropDownWindowStyle>
                        <DropDownWindowTemplate>
                            <dx:ASPxTreeList ID="ASPxTreeList1" runat="server" AutoGenerateColumns="False" ClientInstanceName="TreeList"
                                OnCustomJSProperties="TreeList_CustomJSProperties" OnDataBound="TreeList_DataBound" OnCustomCallback="ASPxTreeList1_CustomCallback"
                                DataSourceID="edsCategorie" KeyFieldName="PK" ParentFieldName="ParentID">
                                <Border BorderStyle="Solid" />
                                <SettingsBehavior AllowFocusedNode="true" />
                                <SettingsEditing ConfirmDelete="true" />
                                <SettingsPager Mode="ShowAllNodes">
                                </SettingsPager>
                                <Columns>
                                    <dx:TreeListTextColumn FieldName="descrizione" VisibleIndex="0">
                                    </dx:TreeListTextColumn>
                                    <dx:TreeListTextColumn FieldName="ParentID" VisibleIndex="1" Visible="false">
                                    </dx:TreeListTextColumn>
                                </Columns>
                                <ClientSideEvents Init="TreeListInitHandler" EndCallback="TreeListEndCallbackHandler" NodeClick="TreeListNodeClickHandler" NodeExpanding="OnNodeExpanding" />
                            </dx:ASPxTreeList>
                        </DropDownWindowTemplate>
                        <Buttons>
                            <dx:EditButton Text="X"></dx:EditButton>
                        </Buttons>
                        <ButtonStyle BackColor="LightGray" ForeColor="Gray">
                            <PressedStyle BackColor="#cccccc">
                            </PressedStyle>
                            <Border BorderColor="#eeeeee" />
                        </ButtonStyle>
                        <CaptionSettings Position="Top" />
                        <ClientSideEvents DropDown="DropDownHandler" ButtonClick="ButtonClickHandler" />
                    </dx:ASPxDropDownEdit>
                    <ef:EntityDataSource ID="edsCategorie" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EntitySetName="Menu" Where="IT.tipo = 'categoria'">
                    </ef:EntityDataSource>
                </div>
                <asp:Button ID="Button1" runat="server" Text="Applica il filtro" CssClass="btn btn-default BtnFiltra" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <dx:ASPxGridView ID="grwPage" ClientInstanceName="grwPage" runat="server" DataSourceID="emdPage" Width="100%" AutoGenerateColumns="False" EnableTheming="True" CssClass="table " KeyFieldName="PK" Theme="Moderno">
                <SettingsSearchPanel Visible="True" />
                <Settings ShowFilterRow="true" ShowGroupPanel="False" />
                <SettingsEditing Mode="Inline" />
                <Columns>
                    <dx:GridViewDataColumn FieldName="Titolo" Caption="Titolo" VisibleIndex="1" />
                    <dx:GridViewDataColumn FieldName="Link" VisibleIndex="2" />
                    <dx:GridViewDataCheckColumn FieldName="FlagVisibile" VisibleIndex="3" CellStyle-CssClass="col-md-1">
                        <DataItemTemplate>
                            <dx:ASPxCheckBox ID="chk" runat="server" AllowGrayed="false" Value='<%#Eval("FlagVisibile")%>' OnInit="chk_Init" CssClass="checkbox">
                            </dx:ASPxCheckBox>
                        </DataItemTemplate>
                        <EditFormSettings Visible="False" />
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewDataTextColumn FieldName="" VisibleIndex="3">
                        <DataItemTemplate>

                            <asp:LinkButton ID="btn" runat="server" Text="Seleziona" CssClass="btn btn-default" CommandArgument="seleziona"></asp:LinkButton>
                            <asp:LinkButton ID="ASPxButton1" runat="server" Text="Elimina" CssClass="btn btn-danger dxgvControl_Moderno2" CommandArgument="elimina" OnClientClick="return getConfirmation(this, 'Attenzione','Confermi l\'eliminazione ?');"></asp:LinkButton>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
            <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="grwPageCB" OnCallback="grwPage_Callback"></dx:ASPxCallback>
            <dx:EntityServerModeDataSource ID="emdPage" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EnableDelete="True" EnableUpdate="True" TableName="Post" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="content3" runat="server" ContentPlaceHolderID="FooterPlaceHolder">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">
                        ELIMINA</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>