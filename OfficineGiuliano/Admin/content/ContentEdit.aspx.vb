﻿Imports DevExpress.Data.Linq
Imports DevExpress.Web
Imports DevExpress.Web.ASPxTreeList
Imports DevExpress.Web.Internal
Imports System.Drawing
Imports System.IO
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports DevExpress.Utils
Imports System.Reflection

Namespace Admin
    Namespace Content
        Public Class ContentEdit
            Inherits System.Web.UI.Page

            Private Const UploadDirectory As String = "~/public/images/content/"
            Private Const UploadGalleryDirectory As String = "~/public/images/content/Gallery/"

            Private _PK As Integer
            Private _FileName As String = String.Empty
            Private _PKMeta As Integer
            Private _FileImageName As String = String.Empty

            Protected Property SubmissionID() As String
                Get
                    Return HiddenField.Get("SubmissionID").ToString()
                End Get
                Set(ByVal value As String)
                    HiddenField.Set("SubmissionID", value)
                End Set
            End Property

            Private ReadOnly Property UploadedFilesStorage() As UploadedFilesStorage
                Get
                    Return UploadControlHelper.GetUploadedFilesStorageByKey(SubmissionID)
                End Get
            End Property

            Private Sub PageEdit_Init(sender As Object, e As EventArgs) Handles Me.Init

                If Request.RawUrl.ToString.EndsWith(".css") OrElse Request.RawUrl.ToString.EndsWith(".js") OrElse Request.RawUrl.ToString.EndsWith(".jpg") OrElse Request.RawUrl.ToString.EndsWith(".png") Then
                    Exit Sub
                End If

                If Not IsPostBack Then
                    Session("PkContent") = Nothing
                    Session("PkContentMeta") = Nothing
                    Session("ContentFileImageName") = Nothing
                    Session("ContentFileDocName") = Nothing

                    _PK = IIf(Request.QueryString("id") IsNot Nothing, Request.QueryString("id"), Nothing)
                    Session("PkContent") = _PK

                    If Not _PK = Nothing Then
                        BindData()
                        BindSeo()
                        BindGallery()
                    End If
                Else
                    _PK = Session("PkContent")

                    If Session("PkContentMeta") IsNot Nothing Then
                        _PKMeta = Session("PkContentMeta")
                    End If

                    If Session("ContentFileImageName") IsNot Nothing Then
                        _FileImageName = Session("PageFileImageName")
                    End If
                    If Session("ContentFileDocName") IsNot Nothing Then
                        _FileImageName = Session("ContentFileDocName")
                    End If
                End If

            End Sub

            Private Sub PageEdit_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete

            End Sub

            Private Sub PageEdit_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
                UploadControlHelper.RemoveOldStorages()
            End Sub

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                uploadedImage.Attributes.Add("onload", "onImageLoad()")

                SubmissionID = UploadControlHelper.GenerateUploadedFilesStorageKey()
                UploadControlHelper.AddUploadedFilesStorage(SubmissionID)

                FormLayout.FindItemOrGroupByName("ResultGroup").Visible = False

            End Sub

            Private Sub PageEdit_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
                If _PK = Nothing Then
                    ASPxPageControl1.TabPages(3).Enabled = False
                    UploadControl.Enabled = False
                End If
            End Sub

            Protected Sub TreeListMenu_DataBound(ByVal sender As Object, ByVal e As EventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                If (Not IsCallback) AndAlso (Not IsPostBack) Then
                    tree.CollapseAll()
                End If
            End Sub

            Protected Sub TreeListMenu_CustomJSProperties(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxTreeList.TreeListCustomJSPropertiesEventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                Dim CategoryNames As New Hashtable()
                For Each node As TreeListNode In tree.GetVisibleNodes()
                    CategoryNames.Add(node.Key, node("Descrizione"))
                Next node
                e.Properties("cpCategoryNames") = CategoryNames
            End Sub

            Protected Sub TreeListMenu_CustomCallback(sender As Object, e As TreeListCustomCallbackEventArgs)
                Dim TreeListMenu As ASPxTreeList = TryCast(sender, ASPxTreeList)
                TreeListMenu.CollapseAll()
                Dim node As TreeListNode = TreeListMenu.FindNodeByKeyValue(e.Argument)
                While node.ParentNode IsNot Nothing
                    node.Expanded = True
                    node = node.ParentNode
                End While
            End Sub

            Private Sub BindData()
                Dim row As New DataClass.Post

                Using dataContext As New DataClass.DataEntities
                    row = dataContext.Post.FirstOrDefault(Function(p) p.PK = _PK)
                End Using

                If row IsNot Nothing Then

                    PaginaContenutoBreve.Text = row.ContenutoBreve
                    PaginaContenutoInferiore.Html = row.ContenutoInferiore
                    PaginaContenutoPrincipale.Html = row.Contenuto
                    PaginaContenutoSuperiore.Html = row.ContenutoSuperiore
                    PaginaData.Text = FormatDateTime(row.Data)
                    PaginaSezione.Text = row.Sezione
                    PaginaSottotitolo.Text = row.SottoTitolo
                    PaginaTitolo.Text = row.Titolo
                    PaginaFondo.Text = row.Fondo
                    PaginaVisibile.Checked = row.FlagVisibile

                    If Not String.IsNullOrEmpty(row.Immagine) Then
                        uploadedImage.Src = UploadDirectory & row.Immagine
                        uploadedImage.Attributes.Item("class") = ""
                        uploadedImage.Attributes.Item("style") = ""
                        uploadedImage.Style.Clear()
                        uploadedImage.Height = 240
                    End If

                    If row.FkMenu IsNot Nothing Then
                        Dim treeList As ASPxTreeList = DirectCast(MenuMacro.FindControl("TreeListMenu"), ASPxTreeList)
                        treeList.UnselectAll()
                        Dim node As TreeListNode = treeList.FindNodeByKeyValue(row.FkMenu)
                        node.Focus()
                        node.Selected = True
                        MenuMacro.KeyValue = row.FkMenu
                        MenuMacro.Text = DataClass.Menu.GetNameFromID(row.FkMenu)
                    End If
                End If

            End Sub

            Private Function SaveData() As Integer

                Dim rif As Integer

                If MenuMacro.KeyValue <> "" Then
                    rif = CInt(MenuMacro.KeyValue)
                Else
                    rif = Nothing
                End If

                If rif <> Nothing Then

                    Using context As New DataClass.DataEntities
                        If _PK = Nothing Then
                            Dim row As New DataClass.Post

                            row.Link = Classi.Utility.StringToUrl(PaginaPageTitle.Text)
                            row.ContenutoBreve = PaginaContenutoBreve.Text
                            row.ContenutoInferiore = PaginaContenutoInferiore.Html
                            row.Contenuto = PaginaContenutoPrincipale.Html
                            row.ContenutoSuperiore = PaginaContenutoSuperiore.Html
                            row.Data = Date.Now
                            row.Sezione = PaginaSezione.Text
                            row.SottoTitolo = PaginaSottotitolo.Text
                            row.Titolo = PaginaTitolo.Text
                            row.Fondo = PaginaFondo.Text
                            row.FlagVisibile = PaginaVisibile.Checked
                            row.TipoPagina = 2
                            row.FkMenu = rif

                            context.Configuration.AutoDetectChangesEnabled = True
                            context.Post.Attach(row)
                            context.Entry(row).State = Entity.EntityState.Added
                            context.SaveChanges()

                            _PK = row.PK
                        Else
                            Dim row = context.Post.SingleOrDefault(Function(p) p.PK = _PK)
                            row.Link = Classi.Utility.StringToUrl(PaginaPageTitle.Text)
                            row.ContenutoBreve = PaginaContenutoBreve.Text
                            row.ContenutoInferiore = PaginaContenutoInferiore.Html
                            row.Contenuto = PaginaContenutoPrincipale.Html
                            row.ContenutoSuperiore = PaginaContenutoSuperiore.Html
                            row.DataUltimaModifica = Date.Now
                            row.Fondo = PaginaFondo.Text
                            row.Sezione = PaginaSezione.Text
                            row.SottoTitolo = PaginaSottotitolo.Text
                            row.Titolo = PaginaTitolo.Text
                            row.FlagVisibile = PaginaVisibile.Checked
                            row.TipoPagina = 2
                            row.FkMenu = rif
                            If Not String.IsNullOrEmpty(_FileImageName) Then
                                row.Immagine = _FileImageName
                                uploadedImage.Src = UploadDirectory & _FileImageName
                                uploadedImage.Attributes.Item("class") = ""
                                uploadedImage.Attributes.Item("style") = ""
                                uploadedImage.Style.Clear()
                                uploadedImage.Height = 240
                            End If
                            context.SaveChanges()
                        End If
                    End Using

                    SaveSeo()
                Else
                    _PK = Nothing
                End If

                Return _PK

            End Function

            Private Sub UploadControl_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs) Handles UploadControl.FileUploadComplete

                e.CallbackData = SavePostedFile(e.UploadedFile, UploadDirectory)

                Dim id As Integer = Session("PkContent")

                _FileImageName = e.UploadedFile.FileName
                _FileImageName = id & "_" & _FileImageName
                Session("ContentFileImageName") = _FileImageName
                e.UploadedFile.SaveAs(CombinePath(_FileImageName, UploadDirectory))

                Using context As New DataClass.DataEntities
                    Dim row = context.Post.SingleOrDefault(Function(p) p.PK = id)
                    row.Immagine = _FileImageName
                    context.SaveChanges()
                End Using

            End Sub

            Protected Function SavePostedFile(ByVal uploadedFile As UploadedFile, ByVal _path As String) As String

                Try
                    If (Not uploadedFile.IsValid) Then
                        Return String.Empty
                    End If

                    Dim fileName As String = System.IO.Path.ChangeExtension(System.IO.Path.GetRandomFileName(), ".jpg")
                    Dim fullFileName As String = CombinePath(fileName, _path)

                    Using original As Image = Image.FromStream(uploadedFile.FileContent)

                        Using thumbnail As Image = PhotoUtils.Inscribe(CType(original, System.Drawing.Bitmap), 550, 250) ' ImageUtils.CreateThumbnailImage(CType(original, System.Drawing.Bitmap), ImageSizeMode.ActualSizeOrFit, New Size(550, 250))
                            ImageUtils.SaveToJpeg(CType(thumbnail, System.Drawing.Bitmap), fullFileName)
                        End Using

                    End Using

                    LibUploadingUtils.RemoveFileWithDelay(fileName, fullFileName, 5)

                    Return fileName
                Catch ex As Exception
                    Throw ex
                End Try

            End Function

            Protected Function CombinePath(ByVal fileName As String, ByVal _path As String) As String
                Return System.IO.Path.Combine(Server.MapPath(_path), fileName)
            End Function

            Private Sub btnSalva_Click(sender As Object, e As EventArgs) Handles btnSalva.Click
                If SaveData() <> Nothing Then
                    Session("PkContent") = _PK
                    Response.Redirect("/admin/content/contentedit?id=" & _PK)
                    ASPxPageControl1.TabPages(3).Enabled = True
                    UploadControl.Enabled = True
                End If
            End Sub

            Protected Sub ProcessSubmit(ByVal description As String, ByVal fileInfos As List(Of UploadedFileInfo))

                DescriptionLabel.Value = Server.HtmlEncode(description)

                Using context As New DataClass.DataEntities

                    Dim i As Integer = 1
                    For Each fileInfo As UploadedFileInfo In fileInfos
                        Dim fileContent As Byte() = File.ReadAllBytes(fileInfo.FilePath)
                        Dim fileName As String = CStr(_PK) & "_" & CStr(i) & "_" & fileInfo.UniqueFileName
                        Dim img As Image = byteArrayToImage(fileContent)
                        img.Save(CombinePath(fileName, UploadGalleryDirectory))

                        Dim row As New DataClass.ProdottiGallery
                        row.FkProdotto = _PK
                        row.Descrizione = DescriptionTextBox.Text
                        row.PercorsoImmagine = fileName

                        context.Configuration.AutoDetectChangesEnabled = True
                        context.ProdottiGallery.Attach(row)
                        context.Entry(row).State = Entity.EntityState.Added
                        context.SaveChanges()
                        i = i + 1

                        row = Nothing
                    Next fileInfo

                End Using

                SubmittedFilesListBox.DataSource = fileInfos
                SubmittedFilesListBox.DataBind()

                FormLayout.FindItemOrGroupByName("ResultGroup").Visible = True

                BindGallery()

            End Sub

            Private Function byteArrayToImage(byteArrayIn As Byte()) As Image
                Dim ms As New MemoryStream(byteArrayIn)
                Dim returnImage As Image = Image.FromStream(ms)
                Return returnImage
            End Function

            Private Sub LtwFoto_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvFotogallery.ItemCommand
                If e.CommandName = "Elimina" Then
                    Dim _idGallery As Integer = e.CommandArgument
                    Using context As New DataClass.DataEntities
                        Dim obj = context.ProdottiGallery.FirstOrDefault(Function(p) p.PK = _idGallery)
                        If obj IsNot Nothing Then
                            Try
                                context.ProdottiGallery.Remove(obj)
                                context.SaveChanges()
                            Catch ex As Exception
                                Throw ex
                            End Try
                        End If
                    End Using
                    BindGallery()
                End If
            End Sub

            Protected Sub ProdottoGalleryUploadControl_FileUploadComplete(ByVal sender As Object, ByVal e As DevExpress.Web.FileUploadCompleteEventArgs) Handles ProdottoGalleryUploadControl.FileUploadComplete
                Dim isSubmissionExpired As Boolean = False
                If UploadedFilesStorage Is Nothing Then
                    isSubmissionExpired = True
                    UploadControlHelper.AddUploadedFilesStorage(SubmissionID)
                End If
                Dim tempFileInfo As UploadedFileInfo = UploadControlHelper.AddUploadedFileInfo(SubmissionID, e.UploadedFile.FileName)

                e.UploadedFile.SaveAs(tempFileInfo.FilePath)

                If e.IsValid Then
                    e.CallbackData = tempFileInfo.UniqueFileName & "|" & isSubmissionExpired
                End If
            End Sub

            Protected Sub ProdottoGalleryInvia_Click(sender As Object, e As EventArgs) Handles ProdottoGalleryInvia.Click
                Dim resultFileInfos As New List(Of UploadedFileInfo)()

                Dim description As String = DescriptionTextBox.Value.ToString()
                Dim allFilesExist As Boolean = True

                If UploadedFilesStorage Is Nothing Then
                    UploadedFilesTokenBox.Tokens = New TokenCollection()
                End If

                For Each fileName As String In UploadedFilesTokenBox.Tokens
                    Dim UpFileInfo As UploadedFileInfo = UploadControlHelper.GetDemoFileInfo(SubmissionID, fileName)
                    Dim fileInfo As New FileInfo(UpFileInfo.FilePath)

                    If fileInfo.Exists Then
                        UpFileInfo.FileSize = LibUploadingUtils.FormatSize(fileInfo.Length)
                        resultFileInfos.Add(UpFileInfo)
                    Else
                        allFilesExist = False
                    End If
                Next fileName

                If allFilesExist AndAlso resultFileInfos.Count > 0 Then
                    ProcessSubmit(description, resultFileInfos)

                    UploadControlHelper.RemoveUploadedFilesStorage(SubmissionID)

                    ASPxEdit.ClearEditorsInContainer(FormLayout, True)
                Else
                    UploadedFilesTokenBox.ErrorText = "Caricamento fallito a causa di un errore di timeout (5 min)"
                    UploadedFilesTokenBox.IsValid = False
                End If
            End Sub

            Private Sub BindGallery()
                Dim gallery As New List(Of DataClass.ProdottiGallery)
                Using context As New DataClass.DataEntities
                    gallery = (From row In context.ProdottiGallery Where row.FkProdotto = _PK Select row).ToList
                End Using
                lvFotogallery.DataSource = gallery
                lvFotogallery.DataBind()
            End Sub

            Private Sub BindSeo()

                Dim row As New DataClass.PostMeta

                Using dataContext As New DataClass.DataEntities
                    row = dataContext.PostMeta.FirstOrDefault(Function(p) p.FkContenuto = _PK)
                End Using

                If row IsNot Nothing Then
                    _PKMeta = row.PK

                    PaginaPageTitle.Text = row.MetaTitle
                    PaginaPageDescription.Text = row.MetaDescription
                    PaginaPageKeywords.Text = row.MetaKeywords

                    Session("PkContentMeta") = _PKMeta

                End If

            End Sub

            Private Sub SaveSeo()

                Using context As New DataClass.DataEntities
                    If _PKMeta = Nothing Then
                        Dim row As New DataClass.PostMeta
                        row.FkContenuto = _PK
                        row.MetaTitle = PaginaPageTitle.Text
                        row.MetaDescription = PaginaPageDescription.Text
                        row.MetaKeywords = PaginaPageKeywords.Text

                        context.Configuration.AutoDetectChangesEnabled = True
                        context.PostMeta.Attach(row)
                        context.Entry(row).State = Entity.EntityState.Added
                        context.SaveChanges()

                        _PKMeta = row.PK
                        Session("PkContentMeta") = _PKMeta
                    Else
                        Dim row = context.PostMeta.SingleOrDefault(Function(p) p.PK = _PKMeta)
                        row.MetaTitle = PaginaPageTitle.Text
                        row.MetaDescription = PaginaPageDescription.Text
                        row.MetaKeywords = PaginaPageKeywords.Text
                        context.SaveChanges()
                    End If
                End Using
            End Sub

            Protected Sub UpdatePanel_Unload(sender As Object, e As EventArgs) Handles UpdatePanel1.Unload
                Dim methodInfo As MethodInfo = GetType(ScriptManager).GetMethods(BindingFlags.NonPublic Or BindingFlags.Instance).Where(Function(i) i.Name.Equals("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel")).First()
                methodInfo.Invoke(ScriptManager.GetCurrent(Page), New Object() {TryCast(sender, UpdatePanel)})
            End Sub

            Protected Sub cb_Callback(source As Object, e As CallbackEventArgs) Handles cb.Callback
                e.Result = String.Format(e.Parameter)
            End Sub

        End Class

    End Namespace

End Namespace