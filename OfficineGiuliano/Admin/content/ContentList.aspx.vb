﻿Imports DevExpress.Data.Linq
Imports DevExpress.Web
Imports DevExpress.Web.ASPxTreeList
Imports Microsoft.AspNet.EntityDataSource

Namespace Admin
	Namespace Content

		Public Class ContentList
			Inherits System.Web.UI.Page

			Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
				If Not IsPostBack Then
					grwPage.DataBind()
				End If
			End Sub

			Protected Sub chk_Init(ByVal sender As Object, ByVal e As EventArgs)
				Dim chk As ASPxCheckBox = TryCast(sender, ASPxCheckBox)
				Dim container As GridViewDataItemTemplateContainer = TryCast(chk.NamingContainer, GridViewDataItemTemplateContainer)
				chk.ClientSideEvents.CheckedChanged = String.Format("function (s, e) {{ grwPageCB.PerformCallback('{0}|' + s.GetChecked()); }}", container.KeyValue)
			End Sub

			Protected Sub grwPage_Callback(source As Object, e As CallbackEventArgs)

				Dim parameter() As String = e.Parameter.Split("|"c)
				Dim _PK As Integer = parameter(0)
				Dim _Checked As Boolean = parameter(1)
				Using dataContext As New DataClass.DataEntities
					Dim obj = dataContext.Post.FirstOrDefault(Function(p) p.PK = _PK)

					With obj
						.FlagVisibile = _Checked
					End With

					Try
						dataContext.SaveChanges()
					Catch ex As Exception
						Throw ex
					End Try
				End Using

			End Sub

			Private Sub EntityServerModeDataSource1_Selecting(sender As Object, e As LinqServerModeDataSourceSelectEventArgs) Handles emdPage.Selecting
				Dim dataContext As New DataClass.DataEntities
				e.KeyExpression = "PK"
				e.QueryableSource = dataContext.Post.AsQueryable.Where(Function(p) p.TipoPagina = 2)
			End Sub

			Private Sub grwPage_RowCommand(sender As Object, e As ASPxGridViewRowCommandEventArgs) Handles grwPage.RowCommand
				If e.CommandArgs.CommandArgument = "seleziona" Then
					Response.Redirect("/Admin/content/ContentEdit.aspx?id=" & e.KeyValue)
				End If
			End Sub

			Protected Sub TreeList_DataBound(ByVal sender As Object, ByVal e As EventArgs)
				Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
				If (Not IsCallback) AndAlso (Not IsPostBack) Then
					tree.CollapseAll()
				End If
			End Sub

			Protected Sub TreeList_CustomJSProperties(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxTreeList.TreeListCustomJSPropertiesEventArgs)
				Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
				Dim CategoryNames As New Hashtable()
				For Each node As TreeListNode In tree.GetVisibleNodes()
					CategoryNames.Add(node.Key, node("Descrizione"))
				Next node
				e.Properties("cpCategoryNames") = CategoryNames
			End Sub

			Protected Sub ASPxTreeList1_CustomCallback(sender As Object, e As TreeListCustomCallbackEventArgs)
				Dim ASPxTreeList1 As ASPxTreeList = TryCast(sender, ASPxTreeList)
				ASPxTreeList1.CollapseAll()
				Dim node As TreeListNode = ASPxTreeList1.FindNodeByKeyValue(e.Argument)
				While node.ParentNode IsNot Nothing
					node.Expanded = True
					node = node.ParentNode
				End While
			End Sub

			Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
				If MenuMacro.KeyValue <> "" Then
					grwPage.FilterExpression = "Fk = " & MenuMacro.KeyValue
				Else
					grwPage.FilterExpression = String.Empty
				End If
			End Sub
		End Class
	End Namespace
End Namespace