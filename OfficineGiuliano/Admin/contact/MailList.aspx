<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="MailList.aspx.vb" Inherits="Emmemedia.Admin.Mail.MailList" %>

<%@ Register Assembly="Syncfusion.EJ.Web, Version=15.4460.0.17, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89" Namespace="Syncfusion.JavaScript.Web" TagPrefix="ej" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>

<%@ Register assembly="Syncfusion.EJ, Version=15.4460.0.17, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89" namespace="Syncfusion.JavaScript.Models" tagprefix="ej" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .BtnFiltra {
            vertical-align: bottom;
        }
    </style>
    <script type="text/javascript">//<![CDATA[
        window.onload = function () {
            history.pushState(null, null, location.href);
            window.onpopstate = function (event) {
                history.go(1);
            };
        }//]]>

        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>
       

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">

    <div class="row">
        <div class="col-md-12">
            <h3>MAIL&nbsp;
            </h3>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <ej:Splitter ID="Splitter1" runat="server" IsResponsive="true" Orientation="Vertical" EnableAutoResize="true" >
                <ej:SplitPane MinSize="400px" Collapsible="false" PaneSize="500px">
                    <PaneContent>
                        <div class="row">
                            <div class="col-md-12">
                                <dx:ASPxGridView ID="Grid" runat="server" DataSourceID="eds" KeyFieldName="PK" Width="100%" AutoGenerateColumns="False" Theme="Moderno">
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="PK" Caption="ID" VisibleIndex="3" CellStyle-CssClass="col-sm-1" Visible="false">
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="DateAdd" Caption="Data" VisibleIndex="0" CellStyle-CssClass="col-sm-1">
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Email" Caption="Mittente" VisibleIndex="4" CellStyle-CssClass="col-sm-2">
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Subject" Caption="Oggetto" VisibleIndex="9" CellStyle-CssClass="col-sm-3">
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataCheckColumn VisibleIndex="11" Caption="Nuova" FieldName="New">
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataTextColumn FieldName="" VisibleIndex="16">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Visualizza" CommandArgument='<%#Eval("pk") %>' CommandName="Select" CssClass="btn btn-default"></asp:LinkButton>

                                                <%--                            <asp:HyperLink ID="hyperlink" runat="server" NavigateUrl='<%# "~/Admin/mail/mailEdit.aspx?id=" & Eval("pk") %>' CssClass="btn btn-default">Seleziona</asp:HyperLink>--%>
                                                <asp:LinkButton ID="ASPxButton1" runat="server" Text="Elimina" CssClass="btn btn-danger danger" CommandName="Delete" OnClientClick="return getConfirmation(this, 'Attenzione','Confermi l\'eliminazione ?');"></asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Settings ShowFooter="True" ShowFilterRow="True" ShowGroupPanel="false" ShowStatusBar="Auto" />
                                    <Styles AlternatingRow-Enabled="True" AlternatingRow-BackColor="WhiteSmoke">
                                        <AlternatingRow Enabled="True" BackColor="WhiteSmoke"></AlternatingRow>
                                    </Styles>
                                    <SettingsSearchPanel Visible="true" />
                                </dx:ASPxGridView>
                            </div>
                        </div>
                    </PaneContent>
                </ej:SplitPane>
                 <ej:SplitPane>
                    <PaneContent>
                        <asp:Panel ID="pnlDetail" runat="server">
                        <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Data</label>
                                            <div class="col-sm-9">
                                                <asp:Label ID="MailData" runat="server" CssClass="form-control"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">Mittente</label>
                                            <div class="col-sm-9">
                                                <asp:Label ID="MailSender" runat="server" CssClass="form-control"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">Testo</label>
                                            <div class="col-sm-9">
                                               
                                                <asp:Literal ID="MailText" runat="server"   ></asp:Literal>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                        </asp:Panel>
                    </PaneContent>
                </ej:SplitPane>
            </ej:Splitter>
        </ContentTemplate>
    </asp:UpdatePanel>
    <ef:EntityDataSource ID="eds" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EntitySetName="MailContact" EnableDelete="true" OrderBy="it.PK Desc">
    </ef:EntityDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterPlaceHolder" runat="server">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">
                        ELIMINA</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>