﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Admin.Mail
    
    Partial Public Class MailList
        
        '''<summary>
        '''Controllo UpdatePanel1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
        
        '''<summary>
        '''Controllo Splitter1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents Splitter1 As Global.Syncfusion.JavaScript.Web.Splitter
        
        '''<summary>
        '''Controllo Grid.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents Grid As Global.DevExpress.Web.ASPxGridView
        
        '''<summary>
        '''Controllo pnlDetail.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents pnlDetail As Global.System.Web.UI.WebControls.Panel
        
        '''<summary>
        '''Controllo MailData.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MailData As Global.System.Web.UI.WebControls.Label
        
        '''<summary>
        '''Controllo MailSender.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MailSender As Global.System.Web.UI.WebControls.Label
        
        '''<summary>
        '''Controllo MailText.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MailText As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo eds.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents eds As Global.Microsoft.AspNet.EntityDataSource.EntityDataSource
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.Admin.SiteAdmin
            Get
                Return CType(MyBase.Master,Emmemedia.Admin.SiteAdmin)
            End Get
        End Property
    End Class
End Namespace
