﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class MailEdit
    
    '''<summary>
    '''Controllo ASPxHyperLink1.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ASPxHyperLink1 As Global.DevExpress.Web.ASPxHyperLink
    
    '''<summary>
    '''Controllo HyperLink1.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents HyperLink1 As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''Controllo ddlTipo.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ddlTipo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Controllo txtNome.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents txtNome As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Controllo txtOggetto.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents txtOggetto As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Controllo txtTesto.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents txtTesto As Global.DevExpress.Web.ASPxHtmlEditor.ASPxHtmlEditor
    
    '''<summary>
    '''Controllo btnSalva.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents btnSalva As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Proprietà Master.
    '''</summary>
    '''<remarks>
    '''Proprietà generata automaticamente.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Emmemedia.Admin.SiteAdmin
        Get
            Return CType(MyBase.Master,Emmemedia.Admin.SiteAdmin)
        End Get
    End Property
End Class
