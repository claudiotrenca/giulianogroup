﻿Imports DevExpress.Web

Namespace Admin

    Namespace Mail

        Public Class MailList
            Inherits System.Web.UI.Page

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                pnlDetail.Visible = False
            End Sub

            Private Sub gridProdotti_RowCommand(sender As Object, e As ASPxGridViewRowCommandEventArgs) Handles Grid.RowCommand

                If e.CommandArgs.CommandArgument = "Delete" Then
                    DataClass.MailContact.Delete(e.KeyValue)
                    Grid.DataBind()
                ElseIf e.CommandArgs.CommandName = "Select" Then
                    pnlDetail.Visible = True
                    Dim row As New DataClass.MailContact
                    Dim id As Integer = CInt(e.CommandArgs.CommandArgument)
                    Using dc As New DataClass.DataEntities
                        row = (From r In dc.MailContact Where r.PK = id Select r).FirstOrDefault
                        row.[New] = False
                        dc.SaveChanges()
                    End Using
                    MailData.Text = row.DateAdd
                    MailSender.Text = row.Email
                    MailText.Text = row.Text

                    Grid.DataBind()

                End If

            End Sub

        End Class

    End Namespace

End Namespace