﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="MailEdit.aspx.vb" Inherits="Emmemedia.MailEdit" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>NUOVA CATEGORIA
            </h3>
            <div class="pull-right">
                <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text=" TORNA ALL'ELENCO" NavigateUrl="~/Admin/mail/mailList.aspx" CssClass="btn btn-default ">
                </dx:ASPxHyperLink>
                &nbsp; &nbsp;
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default " NavigateUrl="~/Admin/mail/mailedit.aspx">AGGIUNGI NUOVA</asp:HyperLink>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Nome</label>
                <asp:DropDownList ID="ddlTipo" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Nome</label>
                <asp:TextBox ID="txtNome" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Oggetto</label>
                <asp:TextBox ID="txtOggetto" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Testo</label>
                <dx:ASPxHtmlEditor ID="txtTesto" runat="server" EnableTheming="true" Theme="Moderno" Width="100%">
                    <SettingsDialogs>
                        <InsertImageDialog>
                            <SettingsImageUpload UploadFolder="~/public/Images/mail/">
                                <ValidationSettings AllowedFileExtensions=".jpe,.jpeg,.jpg,.gif,.png" MaxFileSize="500000">
                                </ValidationSettings>
                            </SettingsImageUpload>
                        </InsertImageDialog>
                    </SettingsDialogs>
                </dx:ASPxHtmlEditor>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <asp:Button ID="btnSalva" runat="server" Text="SALVA" CssClass="btn btn-default col-md-3" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterPlaceHolder" runat="server">
</asp:Content>