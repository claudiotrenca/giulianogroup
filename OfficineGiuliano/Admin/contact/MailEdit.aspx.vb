﻿Imports DevExpress.Web.ASPxHtmlEditor

Public Class MailEdit
   Inherits System.Web.UI.Page

   Private PK As Integer = 0

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      PK = IIf(Request.QueryString("id") IsNot Nothing, Request.QueryString("id"), Nothing)

      If Not IsPostBack Then
         BindTipologie()

         If PK <> Nothing Then
            BindForm()
         End If
      End If

   End Sub

   Private Sub BindForm()

      Dim row As New DataClass.Mail
      Using dc As New DataClass.DataEntities
         row = (From r In dc.Mail Where r.PK = PK Select r).FirstOrDefault
      End Using

      txtNome.Text = row.Nome
      ddlTipo.SelectedValue = row.FkTipo
      txtOggetto.Text = row.Oggetto
      txtTesto.Html = row.Corpo

   End Sub

   Private Sub BindTipologie()

      Dim lst As New List(Of DataClass.MailTipo)
      Using dc As New DataClass.DataEntities
         lst = (From r In dc.MailTipo Select r).ToList
      End Using

      Dim row As New DataClass.MailTipo
      row.Nome = "Tutti"
      row.PK = 0
      lst.Insert(0, row)

      ddlTipo.DataTextField = "Nome"
      ddlTipo.DataValueField = "PK"
      ddlTipo.DataSource = lst
      ddlTipo.DataBind()

   End Sub

   Private Sub btnSalva_Click(sender As Object, e As EventArgs) Handles btnSalva.Click
      If PK = Nothing Then
         DataClass.Mail.Insert(txtNome.Text, txtOggetto.Text, txtTesto.Html, ddlTipo.SelectedValue)
      Else
         DataClass.Mail.Modify(txtNome.Text, txtOggetto.Text, txtTesto.Html, ddlTipo.SelectedValue, PK)
      End If
   End Sub

   Private Sub txtTesto_HtmlCorrecting(sender As Object, e As HtmlCorrectingEventArgs) Handles txtTesto.HtmlCorrecting
      e.Handled = True
   End Sub
End Class