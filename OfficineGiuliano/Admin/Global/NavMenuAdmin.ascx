﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="NavMenuAdmin.ascx.vb" Inherits="Emmemedia.NavMenuAdmin" %>
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="/admin/css/dist/img/User.Png" class="user-image" alt="User Image">
                    <span class="hidden-xs"><%= Session("nome") %></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="/admin/css/dist/img/User.Png" class="img-circle" alt="User Image">
                        <p>
                            <%= Session("nome") %> ADMIN
                      <small>Emmemedia Developer 2016</small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-right">
                            <a href="Logout" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>