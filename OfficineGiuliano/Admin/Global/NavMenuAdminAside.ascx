﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="NavMenuAdminAside.ascx.vb" Inherits="Emmemedia.NavMenuAside" %>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/admin/css/dist/img/User.Png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><%= Session("nome") %></p>
                <a href="#"><i class="fa fa-circle text-success"></i>Online</a>
            </div>
        </div>
        <!-- search form -->
        <div class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">NAVIGATION</li>
            <li class="active treeview">
                <a href="Default.aspx">
                    <i class="fa fa-dashboard"></i><span>Dashboard</span>
                </a>
            </li>
            <!-- GESTIONE DEI CONTENUTI -->
            <li class="treeview" runat="server" id="PnlAdminMenu2" visible="false">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>Front-End</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/layout/MenuGroupList"><i class="fa fa-circle-o"></i>Elenco Gruppi Menu</a></li>
                    <li><a href="/admin/layout/MenuGroupEdit"><i class="fa fa-circle-o"></i>Aggiungi Gruppo Menu</a></li>
                    <li><a href="/admin/layout/MenuList"><i class="fa fa-circle-o"></i>Elenco Menu</a></li>
                    <li><a href="/admin/layout/MenuEdit"><i class="fa fa-circle-o"></i>Aggiungi Menu</a></li>
                    <li><a href="/admin/layout/MenuOrder"><i class="fa fa-circle-o"></i>Ordine Menu</a></li>
                    <li><a href="/admin/layout/PageList"><i class="fa fa-circle-o"></i>Elenco Pagine</a></li>
                    <li><a href="/admin/layout/PageEdit"><i class="fa fa-circle-o"></i>Aggiungi Pagina</a></li>

                    <li><a href="/admin/layout/HeaderEdit"><i class="fa fa-circle-o"></i>Header</a></li>
                    <li><a href="/admin/layout/FooterEdit"><i class="fa fa-circle-o"></i>Footer</a></li>
                </ul>
            </li>
            <!-- FINE GESTIONE DEI CONTENUTI -->

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i><span>Articoli</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/content/ContentEdit"><i class="fa fa-circle-o"></i>Aggiungi nuovo</a></li>
                    <li><a href="/admin/content/ContentList"><i class="fa fa-circle-o"></i>Elenco Articoli</a></li>
                </ul>
            </li>

            <li class="treeview hide">
                <a href="#">
                    <i class="fa fa-table"></i><span>News</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="AdminNews"><i class="fa fa-circle-o"></i>Elenco News</a></li>
                    <li><a href="AdminNewsAM"><i class="fa fa-circle-o"></i>Nuova News</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-th"></i>
                    <span>Prodotti</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/product/CategoryList"><i class="fa fa-circle-o"></i>Elenco Categorie</a></li>
                    <li><a href="/admin/product/CategoryEdit"><i class="fa fa-circle-o"></i>Aggiungi Categorie</a></li>
                    <li><a href="/admin/product/ProductEdit"><i class="fa fa-circle-o"></i>Aggiungi Prodotto</a></li>
                    <li><a href="/admin/product/ProductList"><i class="fa fa-circle-o"></i>Elenco Prodotti</a></li>
                </ul>
            </li>

            <li class="treeview hide">
                <a href="#">
                    <i class="fa fa-cube"></i><span>Ordini</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i>Tutti gli Ordini</a></li>
                    <li><a href="#?new"><i class="fa fa-circle-o"></i>Ordini Nuovi</a></li>
                    <li><a href="#?wait"><i class="fa fa-circle-o"></i>Ordini non Completi</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i><span>Utenti</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/user/UserList"><i class="fa fa-circle-o"></i>Elenco Utenti</a></li>
                    <li><a href="/admin/user/NewsletterList"><i class="fa fa-circle-o"></i>Utenti Newsletter</a></li>
                    <li><a href="/admin/contact/maillist"><i class="fa fa-circle-o"></i>Elenco Contatti</a></li>
                </ul>
            </li>
            <li class="treeview hide">
                <a href="#">
                    <i class="fa fa-cog"></i>
                    <span>Setting Ecommerce</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="AdminTaglie"><i class="fa fa-circle-o"></i>Elenco Taglie</a></li>
                    <li><a href="AdminColori"><i class="fa fa-circle-o"></i>Elenco Colori</a></li>
                </ul>
            </li>

            <li>
                <a href="<%= ConfigurationManager.AppSettings("Webmail") %>" target="_blank">
                    <i class="fa fa-envelope"></i><span>WebMail</span>
                </a>
            </li>
            <li class="header">SETTING</li>
            <li runat="server" id="liconfig" visible="false"><a href="Configurazioni"><i class="fa fa-circle-o text-red"></i><span>Configurazioni Sito</span></a></li>
            <li><a href="DatiGenerali"><i class="fa fa-circle-o text-yellow"></i><span>Dati generali sito</span></a></li>
            <li runat="server" id="liseo" visible="false"><a href="#"><i class="fa fa-circle-o text-aqua"></i><span>SEO</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>