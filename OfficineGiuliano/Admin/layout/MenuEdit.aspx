﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="MenuEdit.aspx.vb" Inherits="Emmemedia.Admin.Layout.MenuEdit" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>

<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">//<![CDATA[
        window.onload = function () {
            history.pushState(null, null, location.href);
            window.onpopstate = function (event) {
                history.go(1);
            };
        }//]]>

        function ButtonClickHandler(s, e) {
            if (e.buttonIndex == 0) {
                MenuMacro.SetKeyValue(null);
                SynchronizeFocusedNode();
            }
        }
        function DropDownHandler(s, e) {
            SynchronizeFocusedNode();
        }
        function TreeListInitHandler(s, e) {
            SynchronizeFocusedNode();
        }
        function TreeListEndCallbackHandler(s, e) {
            MenuMacro.SetKeyValue(TreeList.GetFocusedNodeKey())
            MenuMacro.AdjustDropDownWindow();
            UpdateEditBox();
        }
        function TreeListNodeClickHandler(s, e) {
            MenuMacro.SetKeyValue(e.nodeKey);
            MenuMacro.SetText(TreeList.cpCategoryNames[e.nodeKey]);
            MenuMacro.HideDropDown();
        }
        function SynchronizeFocusedNode() {
            var keyValue = MenuMacro.GetKeyValue();
            TreeList.SetFocusedNodeKey(keyValue);
            UpdateEditBox();
        }
        function UpdateEditBox() {
            var focusedCategoryName = '';
            var nodeKey = TreeList.GetFocusedNodeKey();
            if (nodeKey != 'null' && nodeKey != '')
                focusedCategoryName = TreeList.cpCategoryNames[nodeKey];
            var CategoryNameInEditBox = MenuMacro.GetText();
            if (CategoryNameInEditBox != focusedCategoryName)
                MenuMacro.SetText(focusedCategoryName);
        }

        function OnNodeExpanding(s, e) {
            s.PerformCallback(e.nodeKey);
            e.cancel = true;
        }

        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>

    <script type="text/javascript">

        function onUploadControlFileUploadComplete(s, e) {
            if (e.isValid)
                document.getElementById('<%=uploadedImage.ClientID %>').src = "/public/images/menu/" + e.callbackData;
            setElementVisible('<%=uploadedImage.ClientID %>', e.isValid);
        }
        function onImageLoad() {
            var externalDropZone = document.getElementById("externalDropZone");
            var uploadedImage = document.getElementById('<%=uploadedImage.ClientID %>');
            uploadedImage.style.left = (externalDropZone.clientWidth - uploadedImage.width) / 2 + "px";
            uploadedImage.style.top = (externalDropZone.clientHeight - uploadedImage.height) / 2 + "px";
            setElementVisible("dragZone", false);
        }
        function setElementVisible(elementId, visible) {
            document.getElementById(elementId).className = visible ? "" : "hidden";
        }

        function Browse() {
            document.getElementById(<%=uploadedImage.ClientID %>).click();
        }
        function RemoveImage() {
            debugger;
            document.getElementById('<%=uploadedImage.ClientID %>').src = "";
        }
    </script>

    <style type="text/css">
        .dropZoneExternal > div {
            left: 0;
            width: 100%;
        }

        .dropZoneExternal > img {
            position: absolute;
        }

        .dropZoneExternal {
            position: relative;
            border: 3px dashed #91A0A8 !important;
            cursor: pointer;
        }

        .dropZoneExternal,
        .dragZoneText {
            width: 100%;
            height: 260px;
            margin: 30px 0;
            padding: 5px;
        }

        .dropZoneText {
            width: 100%;
            height: 150px;
            color: #fff;
            background-color: #888;
        }

        #dropZone {
            top: 0;
            padding: 100px 25px;
        }

        .uploadControlDropZone,
        .hidden {
            display: none;
        }

        .dropZoneText,
        .dragZoneText {
            display: block;
            vertical-align: middle;
            text-align: center;
            font-size: 24px;
            padding: 25px 15px;
        }

        .dragZoneText {
            color: #8597A0;
        }

        .dxucInlineDropZoneSys span {
            color: #fff !important;
            font-size: 10pt;
            font-weight: normal !important;
        }

        .uploadControlProgressBar {
            width: 100% !important;
        }

        .validationMessage {
            padding: 0 20px;
            text-align: center;
        }

        .uploadControl {
            margin: 30px 0;
        }

        .Note {
            width: 100%;
        }

        .dxucProgressBarIndicator_Moderno {
            background: #82939c;
        }

        .dxucErrorCell_Moderno {
            padding: 15px 0;
            color: crimson;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <asp:Label ID="LabelModalità" runat="server" Text="Label"></asp:Label>
            </h3>
            <div class="pull-right">
                <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text=" TORNA ALL'ELENCO" NavigateUrl="MenuList.aspx" CssClass="btn btn-default ">
                </dx:ASPxHyperLink>
                &nbsp; &nbsp;
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default " NavigateUrl="~/Admin/layout/MenuEdit.aspx">AGGIUNGI NUOVO</asp:HyperLink>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Gruppo Menu</label>
                        <asp:DropDownList ID="ddlGruppoMenu" runat="server" CssClass="form-control dropdown" DataTextField="Nome" DataValueField="pk">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Nome</label>
                        <dx:ASPxTextBox ID="MenuNome" runat="server" CssClass="form-control">
                            <ClientSideEvents TextChanged="function (s, e) { cb.PerformCallback(s.GetText()); }" />
                            <ClientSideEvents LostFocus="function (s, e) { cb.PerformCallback(s.GetText()); }" />
                        </dx:ASPxTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="">Link</label>
                        <div class="input-group">
                            <dx:ASPxTextBox ID="MenuLink" ClientInstanceName="MenuLink" runat="server" CssClass="form-control"></dx:ASPxTextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btnModificaLink" runat="server" Text="Modifica" CssClass="btn btn-default" />
                            </span>
                        </div>
                        <dx:ASPxCallback ID="cb" ClientInstanceName="cb" runat="server">
                            <ClientSideEvents CallbackComplete="function (s, e) { MenuLink.SetText(e.result); }" />
                        </dx:ASPxCallback>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Target</label>
                        <asp:DropDownList ID="MenuTarget" runat="server" CssClass="form-control dropdown">
                            <asp:ListItem Text="Self" Value="_self"></asp:ListItem>
                            <asp:ListItem Text="Blank" Value="_blank"></asp:ListItem>
                            <asp:ListItem Text="Top" Value="_top"></asp:ListItem>
                            <asp:ListItem Text="Parent" Value="_parent"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Descrizione</label>
                        <asp:TextBox ID="MenuDescrizione" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Lingua</label>
                        <asp:DropDownList ID="MenuCulture" runat="server" CssClass="form-control dropdown">
                            <asp:ListItem Text="Italiano" Value="it-IT" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inglese" Value="en-UK"></asp:ListItem>
                            <asp:ListItem Text="Spagnolo" Value="es-ES"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Macro</label>
                        <dx:ASPxDropDownEdit ID="MenuMacro" runat="server" ClientInstanceName="MenuMacro" AllowUserInput="False" EnableAnimation="False" CssClass="form-control" HorizontalAlign="Left">
                            <DropDownWindowStyle>
                                <border borderwidth="0px" />
                            </DropDownWindowStyle>
                            <DropDownWindowTemplate>
                                <dx:ASPxTreeList ID="ASPxTreeList1" runat="server" AutoGenerateColumns="False" ClientInstanceName="TreeList"
                                    OnCustomJSProperties="TreeList_CustomJSProperties" OnDataBound="TreeList_DataBound" OnCustomCallback="ASPxTreeList1_CustomCallback"
                                    DataSourceID="EntityDataSource1" KeyFieldName="PK" ParentFieldName="ParentID">
                                    <Border BorderStyle="Solid" />
                                    <SettingsBehavior AllowFocusedNode="true" />
                                    <SettingsEditing ConfirmDelete="true" />
                                    <SettingsPager Mode="ShowAllNodes">
                                    </SettingsPager>
                                    <Columns>
                                        <dx:TreeListTextColumn FieldName="descrizione" VisibleIndex="0">
                                        </dx:TreeListTextColumn>
                                        <dx:TreeListTextColumn FieldName="ParentID" VisibleIndex="1" Visible="false">
                                        </dx:TreeListTextColumn>
                                    </Columns>
                                    <ClientSideEvents Init="TreeListInitHandler" EndCallback="TreeListEndCallbackHandler" NodeClick="TreeListNodeClickHandler" NodeExpanding="OnNodeExpanding" />
                                </dx:ASPxTreeList>
                            </DropDownWindowTemplate>
                            <Buttons>
                                <dx:EditButton Text="X"></dx:EditButton>
                            </Buttons>
                            <ButtonStyle BackColor="LightGray" ForeColor="Gray">
                                <PressedStyle BackColor="#cccccc">
                                </PressedStyle>
                                <border bordercolor="#eeeeee" />
                            </ButtonStyle>
                            <CaptionSettings Position="Top" />
                            <ClientSideEvents DropDown="DropDownHandler" ButtonClick="ButtonClickHandler" />
                        </dx:ASPxDropDownEdit>
                        <ef:EntityDataSource ID="EntityDataSource1" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EntitySetName="Menu" Where="IT.tipo = 'link' OR IT.tipo = 'cartella'">
                        </ef:EntityDataSource>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-inline">
                        <div class="form-group">
                            <div class="checkbox">
                                <dx:ASPxCheckBox ID="MenuVisibile" runat="server" Theme="Moderno" Checked="true"></dx:ASPxCheckBox>
                            </div>
                            <label for="">Visibile</label>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">

                            <div class="checkbox">
                                <dx:ASPxCheckBox ID="MenuPrivato" runat="server" Theme="Moderno"></dx:ASPxCheckBox>
                            </div>
                            <label for="">Privato</label>
                        </div>
                        <br />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-inline">
                        <div class="form-group">
                            <div class="checkbox">
                                <dx:ASPxCheckBox ID="IncludeTopMenu" runat="server" Theme="Moderno" Checked="true"></dx:ASPxCheckBox>
                            </div>
                            <label for="">Includi nel menu principale</label>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <div class="checkbox">
                                <dx:ASPxCheckBox ID="IncludeFooterMenu" runat="server" Theme="Moderno" Checked="true"></dx:ASPxCheckBox>
                            </div>
                            <label for="">Includi nel menu footer</label>
                            <br />
                        </div>
                    </div>
                </div>
                <div class="col-md-8"></div>
            </div>
            <br />
        </div>
        <div class="col-md-8 pull-left">
            <div id="externalDropZone" class="dropZoneExternal">
                <div id="dragZone">
                    <span class="dragZoneText" style="text-align: center">Clicca o trascina qui la foto</span>
                </div>
                <img id="uploadedImage" class="hidden" alt="" runat="server" />
                <div id="dropZone" class="hidden">
                    <span class="dropZoneText" style="text-align: center">Clicca o trascina qui la foto</span>
                </div>
            </div>
            <dx:ASPxUploadControl ID="UploadControl" ClientInstanceName="UploadControl" runat="server" UploadMode="Standard" AutoStartUpload="True" Width="100%" ShowProgressPanel="True" CssClass="uploadControl" DialogTriggerID="externalDropZone" ShowUploadButton="false" ShowAddRemoveButtons="false" ShowTextBox="false" ShowClearFileSelectionButton="True" BrowseButtonStyle-CssClass="btn btn-default">
                <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="False" ExternalDropZoneID="externalDropZone" DropZoneText="" />
                <ValidationSettings MaxFileSize="4194304" AllowedFileExtensions=".jpg, .jpeg, .gif, .png" ErrorStyle-CssClass="validationMessage">
                    <ErrorStyle CssClass="validationMessage"></ErrorStyle>
                </ValidationSettings>
                <BrowseButton Text="Seleziona un'immagine..." Image-Height="200" />
                <DropZoneStyle CssClass="uploadControlDropZone" />
                <ProgressBarStyle CssClass="uploadControlProgressBar" />
                <ClientSideEvents DropZoneEnter="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', true); }" DropZoneLeave="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', false); }" FileUploadComplete="onUploadControlFileUploadComplete"></ClientSideEvents>
            </dx:ASPxUploadControl>
            <asp:Button ID="brnRimuoviImmagine" runat="server" Text="Elimina l'immagine" CssClass="btn btn-danger" OnClientClick="RemoveImage();" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:LinkButton ID="btnSalva" runat="server" CssClass="btn btn-default" OnClientClick="return getConfirmation(this, 'Nuova Voce','Confermi il salvataggio?');"><span class="ion-checkmark-round"></span>  SALVA  </asp:LinkButton>
            &nbsp;
            <asp:LinkButton ID="btnElimina" runat="server" CssClass="btn btn-danger" OnClientClick="return getConfirmation(this, 'ATTENZIONE !','Confermi l\'eliminazione ?');"><span class="ion-trash-a"></span> ELIMINA </asp:LinkButton>
        </div>
    </div>
</asp:Content>
<asp:Content ID="content3" runat="server" ContentPlaceHolderID="FooterPlaceHolder">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">OK</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>