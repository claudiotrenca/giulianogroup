﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Admin.Layout
    
    Partial Public Class MenuGroupEdit
        
        '''<summary>
        '''Controllo LabelModalità.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents LabelModalità As Global.System.Web.UI.WebControls.Label
        
        '''<summary>
        '''Controllo ASPxHyperLink1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ASPxHyperLink1 As Global.DevExpress.Web.ASPxHyperLink
        
        '''<summary>
        '''Controllo HyperLink1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HyperLink1 As Global.System.Web.UI.WebControls.HyperLink
        
        '''<summary>
        '''Controllo MenuNome.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MenuNome As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo CssClass1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents CssClass1 As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo CssClass2.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents CssClass2 As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo CssClass3.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents CssClass3 As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo MenuVisibile.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MenuVisibile As Global.DevExpress.Web.ASPxCheckBox
        
        '''<summary>
        '''Controllo btnSalva.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnSalva As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Controllo btnElimina.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnElimina As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.Admin.SiteAdmin
            Get
                Return CType(MyBase.Master,Emmemedia.Admin.SiteAdmin)
            End Get
        End Property
    End Class
End Namespace
