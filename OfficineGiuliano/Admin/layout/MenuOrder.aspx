﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="MenuOrder.aspx.vb" Inherits="Emmemedia.Admin.Layout.MenuOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>ORDINA MENU
            </h3>
            <div class="pull-right">
                &nbsp; &nbsp;
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default " NavigateUrl="~/Admin/layout/MenuEdit.aspx">AGGIUNGI NUOVO</asp:HyperLink>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-6">
            <div class="form-inline">
                <div class="form-group">
                    <label>Seleziona il menu</label>
                    <asp:DropDownList ID="ddlMenu" runat="server" CssClass="form-control" AutoPostBack="true">
                        <asp:ListItem Text="Top Menu" Value="top"></asp:ListItem>
                        <asp:ListItem Text="Bottom Menu" Value="bottom"></asp:ListItem>
                    </asp:DropDownList>
                 </div>
                 <div class="form-group">
                    <label>Seleziona la Macro</label>
                    <asp:DropDownList ID="ddlMacro" runat="server" CssClass="form-control" DataTextField="Descrizione" DataValueField="pk" AutoPostBack="true">
                    </asp:DropDownList>
                 </div>
                  <asp:Button ID="btnAggiornaGriglia" CssClass="btn btn-default" runat="server" Text="Aggiorna Griglia" />
            </div>
        </div>
    </div>
     <br />
     <br />
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <asp:GridView ID="GrdMenu" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-hover" DataKeyNames="PK" AllowPaging="true" PagerSettings-Mode="NumericFirstLast" PageSize="50" PagerStyle-CssClass="dataTables_paginate paging_simple_numbers" PagerStyle-Width="30%" PagerStyle-HorizontalAlign="Right" PagerSettings-NextPageText="Next" PagerSettings-PreviousPageText="Previous">
                    <EmptyDataTemplate>
                        Nessuna Categoria presente
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="DESCRIZIONE" HeaderText="DESCRIZIONE" />
                        <%--                        <asp:BoundField DataField="MACRO" HeaderText="GENITORE" />--%>
                        <asp:BoundField DataField="LINK" HeaderText="LINK" />
                        <asp:TemplateField HeaderStyle-Width="1%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <a href="MenuEdit.aspx?id=<%# Eval("PK") %>"><i class="fa fa-pencil-square-o"></i></a>
                                <input type="hidden" name="LocationId" value='<%# Eval("PK") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="1%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="LnkElimina" runat="server" OnClientClick="return getConfirmation(this, 'Conferma Elimina','Sei sicuro di voler eliminare questo elemento?');" CommandName="Elimina" CommandArgument='<%# Eval("PK") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:Button ID="BtnUpdatePreference" CssClass="btn btn-block btn-primary" runat="server" Text="Aggiorna Ordine Menu" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterPlaceHolder" runat="server">
    <script type="text/javascript">
        $(function () {
            $("[id*=GrdMenu]").sortable({
                items: 'tr:not(tr:first-child)',
                cursor: 'pointer',
                axis: 'y',
                dropOnEmpty: false,
                start: function (e, ui) {
                    ui.item.addClass("selected");
                },
                stop: function (e, ui) {
                    ui.item.removeClass("selected");
                },
                receive: function (e, ui) {
                    $(this).find("tbody").append(ui.item);
                }
            });
        });
    </script>
</asp:Content>