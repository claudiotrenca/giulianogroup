﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="MenuList.aspx.vb" Inherits="Emmemedia.Admin.Layout.MenuList" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function onCellClick(rowIndex, fieldName) {
            ASPxGridView1.PerformCallback(rowIndex + "|" + fieldName);
        }
        function OnEditorKeyPress(editor, e) {
            if (e.htmlEvent.keyCode == 13 || e.htmlEvent.keyCode == 9) {
                ASPxGridView1.UpdateEdit();
            }
            else
                if (e.htmlEvent.keyCode == 27)
                    ASPxGridView1.CancelEdit();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>MENU
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default" NavigateUrl="~/Admin/layout/MenuEdit.aspx">AGGIUNGI NUOVO</asp:HyperLink>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" DataSourceID="EntityServerModeDataSource1" Width="100%" AutoGenerateColumns="False" EnableTheming="True" Theme="Moderno" KeyFieldName="PK" CssClass="table">
                <Templates>
                    <DetailRow>
                        MENU : <b>
                            <%# Eval("Descrizione")%></b>
                        <br />

                        <dx:ASPxGridView ID="detailGrid" runat="server" DataSourceID="EntityServerModeDataSource2" KeyFieldName="PK" Width="100%" OnBeforePerformDataSelect="detailGrid_DataSelect" EnableTheming="True" Theme="Moderno" OnRowCommand="detailGrid_RowCommand">
                            <Templates>
                                <DetailRow>
                                    MENU : <b>
                                        <%# Eval("Descrizione")%></b>

                                    <br />
                                    <dx:ASPxGridView ID="detailGrid2" runat="server" DataSourceID="EntityServerModeDataSource3" KeyFieldName="PK" Width="100%" OnBeforePerformDataSelect="detailGrid2_DataSelect" EnableTheming="True" Theme="Moderno" OnRowCommand="detailGrid2_RowCommand">
                                        <Columns>
                                            <dx:GridViewDataColumn FieldName="Descrizione" Caption="Descrizione" VisibleIndex="1" />
                                            <dx:GridViewDataColumn FieldName="Link" VisibleIndex="2" />
                                            <dx:GridViewDataTextColumn FieldName="" VisibleIndex="10">
                                                <DataItemTemplate>
                                                    <asp:HyperLink ID="hyperlink" runat="server" NavigateUrl='<%# "~/Admin/layout/menuedit.aspx?id=" & Eval("pk") %>' CssClass="btn btn-default">Seleziona</asp:HyperLink>
                                                    <asp:LinkButton ID="ASPxButton1" runat="server" Text="Elimina" CssClass="btn btn-danger danger" CommandArgument="elimina" OnClientClick="return getConfirmation(this, 'Attenzione','Confermi l\'eliminazione ?');"></asp:LinkButton>
                                                </DataItemTemplate>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <Settings ShowFooter="True" ShowFilterRow="True" ShowGroupPanel="false" />
                                        <SettingsSearchPanel Visible="true" />
                                    </dx:ASPxGridView>
                                </DetailRow>
                            </Templates>

                            <Columns>
                                <dx:GridViewDataColumn FieldName="Descrizione" Caption="Descrizione" VisibleIndex="1" />
                                <dx:GridViewDataColumn FieldName="Link" VisibleIndex="2" />
                                <dx:GridViewDataTextColumn FieldName="" VisibleIndex="10">
                                    <DataItemTemplate>
                                        <asp:HyperLink ID="hyperlink" runat="server" NavigateUrl='<%# "~/Admin/layout/menuedit.aspx?id=" & Eval("pk") %>' CssClass="btn btn-default">Seleziona</asp:HyperLink>
                                        <asp:LinkButton ID="ASPxButton1" runat="server" Text="Elimina" CssClass="btn btn-danger danger" CommandArgument="elimina" OnClientClick="return getConfirmation(this, 'Attenzione','Confermi l\'eliminazione ?');"></asp:LinkButton>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowFooter="True" ShowFilterRow="True" ShowGroupPanel="false" />
                            <SettingsSearchPanel Visible="true" />
                        </dx:ASPxGridView>
                    </DetailRow>
                </Templates>
                <Columns>
                    <dx:GridViewDataColumn FieldName="Descrizione" Caption="Descrizione" VisibleIndex="1" CellStyle-CssClass="col-md-4" />
                    <dx:GridViewDataColumn FieldName="Link" VisibleIndex="2" CellStyle-CssClass="col-md-3" />
                    <dx:GridViewDataCheckColumn FieldName="FlagVisibile" VisibleIndex="3" CellStyle-CssClass="col-md-1">
                        <DataItemTemplate>
                            <dx:ASPxCheckBox ID="chk" runat="server" AllowGrayed="false" Value='<%#Eval("FlagVisibile")%>' OnInit="chk_Init" CssClass="checkbox">
                            </dx:ASPxCheckBox>
                        </DataItemTemplate>
                        <EditFormSettings Visible="False" />
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewDataTextColumn FieldName="" VisibleIndex="10">
                        <DataItemTemplate>
                            <asp:HyperLink ID="hyperlink" runat="server" NavigateUrl='<%# "~/Admin/layout/menuedit.aspx?id=" & Eval("pk") %>' CssClass="btn btn-default">Seleziona</asp:HyperLink>
                            <asp:LinkButton ID="ASPxButton1" runat="server" Text="Elimina" CssClass="btn btn-danger danger" CommandArgument="elimina" OnClientClick="return getConfirmation(this, 'Attenzione','Confermi l\'eliminazione ?');"></asp:LinkButton>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <Settings ShowFilterRow="true" ShowGroupPanel="False" />
                <SettingsEditing Mode="Inline" />
                <SettingsDetail ShowDetailRow="true" />
            </dx:ASPxGridView>
            <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="CheckboxGridCB" OnCallback="CheckboxGrid_Callback"></dx:ASPxCallback>

            <dx:EntityServerModeDataSource ID="EntityServerModeDataSource1" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EnableDelete="True" EnableUpdate="True" TableName="Menu" />
            <dx:EntityServerModeDataSource ID="EntityServerModeDataSource2" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EnableDelete="True" EnableUpdate="True" TableName="Menu" />
            <dx:EntityServerModeDataSource ID="EntityServerModeDataSource3" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EnableDelete="True" EnableUpdate="True" TableName="Menu" />
        </div>
    </div>
</asp:Content>