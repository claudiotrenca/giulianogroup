﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="MenuGroupList.aspx.vb" Inherits="Emmemedia.Admin.Layout.MenuGroupList" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function onCellClick(rowIndex, fieldName) {
            ASPxGridView1.PerformCallback(rowIndex + "|" + fieldName);
        }
        function OnEditorKeyPress(editor, e) {
            if (e.htmlEvent.keyCode == 13 || e.htmlEvent.keyCode == 9) {
                ASPxGridView1.UpdateEdit();
            }
            else
                if (e.htmlEvent.keyCode == 27)
                    ASPxGridView1.CancelEdit();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>MENU
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default" NavigateUrl="~/Admin/layout/MenuGroupEdit.aspx">AGGIUNGI NUOVO</asp:HyperLink>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" DataSourceID="EntityServerModeDataSource1" Width="100%" AutoGenerateColumns="False" EnableTheming="True" Theme="Moderno" KeyFieldName="PK" CssClass="table">

                <Columns>
                    <dx:GridViewDataColumn FieldName="PK" Caption="ID" VisibleIndex="1" CellStyle-CssClass="col-md-3" />
                    <dx:GridViewDataColumn FieldName="Nome" Caption="Descrizione" VisibleIndex="1" CellStyle-CssClass="col-md-4" />
                    <dx:GridViewDataCheckColumn FieldName="DefaultMenu" VisibleIndex="3" CellStyle-CssClass="col-md-1">
                        <DataItemTemplate>
                            <dx:ASPxCheckBox ID="chk" runat="server" AllowGrayed="false" Value='<%#Eval("DefaultMenu")%>' OnInit="chk_Init" CssClass="checkbox">
                            </dx:ASPxCheckBox>
                        </DataItemTemplate>
                        <EditFormSettings Visible="False" />
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewDataHyperLinkColumn FieldName="PK" VisibleIndex="10" Caption="Modifica" Settings-ShowFilterRowMenu="False" Settings-AllowFilterBySearchPanel="False" Settings-AllowHeaderFilter="False" CellStyle-CssClass="col-md-2">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="/admin/layout/MenuGroupEdit.aspx?id={0}" Text="Modifica" />
                    </dx:GridViewDataHyperLinkColumn>
                    <dx:GridViewCommandColumn ShowDeleteButton="True" VisibleIndex="20" Caption="Elimina" CellStyle-CssClass="btn btn-default col-md-2"></dx:GridViewCommandColumn>
                </Columns>
                <Settings ShowFilterRow="true" ShowGroupPanel="False" />
                <SettingsEditing Mode="Inline" />
                <SettingsDetail ShowDetailRow="true" />
            </dx:ASPxGridView>
            <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="CheckboxGridCB" OnCallback="CheckboxGrid_Callback"></dx:ASPxCallback>

            <dx:EntityServerModeDataSource ID="EntityServerModeDataSource1" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EnableDelete="True" EnableUpdate="True" TableName="MenuGruppo" />
        </div>
    </div>
</asp:Content>