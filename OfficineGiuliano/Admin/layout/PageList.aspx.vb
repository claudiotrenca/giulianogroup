﻿Imports DevExpress.Data.Linq
Imports DevExpress.Web

Namespace Admin
    Namespace Layout

        Public Class PageList
            Inherits System.Web.UI.Page

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                If Not IsPostBack Then
                    grwPage.DataBind()
                End If
            End Sub

            Protected Sub chk_Init(ByVal sender As Object, ByVal e As EventArgs)
                Dim chk As ASPxCheckBox = TryCast(sender, ASPxCheckBox)
                Dim container As GridViewDataItemTemplateContainer = TryCast(chk.NamingContainer, GridViewDataItemTemplateContainer)
                chk.ClientSideEvents.CheckedChanged = String.Format("function (s, e) {{ grwPageCB.PerformCallback('{0}|' + s.GetChecked()); }}", container.KeyValue)
            End Sub

            Protected Sub grwPage_Callback(source As Object, e As CallbackEventArgs)

                Dim parameter() As String = e.Parameter.Split("|"c)
                Dim _PK As Integer = parameter(0)
                Dim _Checked As Boolean = parameter(1)
                Using dataContext As New DataClass.DataEntities
                    Dim obj = dataContext.Post.FirstOrDefault(Function(p) p.PK = _PK)

                    With obj
                        .FlagVisibile = _Checked
                    End With

                    Try
                        dataContext.SaveChanges()
                    Catch ex As Exception
                        Throw ex
                    End Try
                End Using

            End Sub

            Private Sub EntityServerModeDataSource1_Selecting(sender As Object, e As LinqServerModeDataSourceSelectEventArgs) Handles emdPage.Selecting
                Dim dataContext As New DataClass.DataEntities
                e.KeyExpression = "PK"
                e.QueryableSource = dataContext.vwPost.AsQueryable.Where(Function(p) p.TipoPagina = 1)

            End Sub

            Private Sub grwPage_RowCommand(sender As Object, e As ASPxGridViewRowCommandEventArgs) Handles grwPage.RowCommand
                If e.CommandArgs.CommandArgument = "elimina" Then
                    DataClass.Post.Delete(e.KeyValue)

                    grwPage.DataBind()
                End If
            End Sub
        End Class
    End Namespace
End Namespace