﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Admin.Layout
    
    Partial Public Class FooterEdit
        
        '''<summary>
        '''Controllo HeaderNome.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HeaderNome As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo HeaderText.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HeaderText As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo HeaderVisibile.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HeaderVisibile As Global.DevExpress.Web.ASPxCheckBox
        
        '''<summary>
        '''Controllo HeaderLiteralAdd.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HeaderLiteralAdd As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Controllo GruppoMenuList.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents GruppoMenuList As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo HeaderGruppoMenuAdd.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HeaderGruppoMenuAdd As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Controllo btnSalva.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnSalva As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Controllo btnElimina.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnElimina As Global.System.Web.UI.WebControls.LinkButton
    End Class
End Namespace
