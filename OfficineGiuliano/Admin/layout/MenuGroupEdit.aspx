﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="MenuGroupEdit.aspx.vb" Inherits="Emmemedia.Admin.Layout.MenuGroupEdit" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>

<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">//<![CDATA[
        window.onload = function () {
            history.pushState(null, null, location.href);
            window.onpopstate = function (event) {
                history.go(1);
            };
        }//]]>

        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <asp:Label ID="LabelModalità" runat="server" Text="Label"></asp:Label>
            </h3>
            <div class="pull-right">
                <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text=" TORNA ALL'ELENCO" NavigateUrl="~/Admin/layout/MenuGroupList.aspx" CssClass="btn btn-default ">
                </dx:ASPxHyperLink>
                &nbsp; &nbsp;
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default " NavigateUrl="~/Admin/layout/MenuGroupEdit.aspx">AGGIUNGI NUOVO</asp:HyperLink>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Nome</label>
                        <dx:ASPxTextBox ID="MenuNome" runat="server" CssClass="form-control">
                        </dx:ASPxTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">CSS Class 1</label>
                        <dx:ASPxTextBox ID="CssClass1" runat="server" CssClass="form-control">
                        </dx:ASPxTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">CSS Class 2</label>
                        <dx:ASPxTextBox ID="CssClass2" runat="server" CssClass="form-control">
                        </dx:ASPxTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">CSS Class 3</label>
                        <dx:ASPxTextBox ID="CssClass3" runat="server" CssClass="form-control">
                        </dx:ASPxTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-inline">
                        <div class="form-group">
                            <div class="checkbox">
                                <dx:ASPxCheckBox ID="MenuVisibile" runat="server" Theme="Moderno" Checked="true"></dx:ASPxCheckBox>
                            </div>
                            <label for="">Menu Principale</label>
                        </div>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <br />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:LinkButton ID="btnSalva" runat="server" CssClass="btn btn-default" OnClientClick="return getConfirmation(this, 'Nuova Voce','Confermi il salvataggio?');"><span class="ion-checkmark-round"></span>  SALVA  </asp:LinkButton>
            &nbsp;
            <asp:LinkButton ID="btnElimina" runat="server" CssClass="btn btn-danger" OnClientClick="return getConfirmation(this, 'ATTENZIONE !','Confermi l\'eliminazione ?');"><span class="ion-trash-a"></span> ELIMINA </asp:LinkButton>
        </div>
    </div>
</asp:Content>
<asp:Content ID="content3" runat="server" ContentPlaceHolderID="FooterPlaceHolder">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">OK</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>