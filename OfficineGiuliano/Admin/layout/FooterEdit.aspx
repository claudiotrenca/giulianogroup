﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="FooterEdit.aspx.vb" Inherits="Emmemedia.Admin.Layout.FooterEdit" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <label>MODIFICA HEADER SITO</label>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group">
                        <label>NOME HEADER</label>
                        <asp:TextBox ID="HeaderNome" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12">
                    <asp:TextBox ID="HeaderText" runat="server" TextMode="MultiLine" Width="100%" Rows="20" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12">
                    <div class="form-inline">
                        <div class="form-group">
                            <div class="checkbox">
                                <dx:ASPxCheckBox ID="HeaderVisibile" runat="server" Theme="Moderno" Checked="true"></dx:ASPxCheckBox>
                            </div>
                            <label for="">Visibile</label>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    &nbsp;
                </div>
            </div>
            <br />
            <br />
            <div class="row">
                <div class="col-md-12">
                    <asp:LinkButton ID="HeaderLiteralAdd" runat="server" CssClass="btn btn-default">Aggiungi campo HTML</asp:LinkButton>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Gruppo Menu</label>
                        <asp:DropDownList ID="GruppoMenuList" runat="server" CssClass="form-control dropdown" DataTextField="Nome" DataValueField="pk">
                        </asp:DropDownList>
                        <br />
                        <asp:LinkButton ID="HeaderGruppoMenuAdd" runat="server" CssClass="btn btn-default">Aggiungi Menu</asp:LinkButton>
                    </div>
                </div>
            </div>

            <br />
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-md-12">
            <asp:LinkButton ID="btnSalva" runat="server" CssClass="btn btn-default" OnClientClick="return getConfirmation(this, 'Nuova Voce','Confermi il salvataggio?');"><span class="ion-checkmark-round"></span>  SALVA  </asp:LinkButton>
            &nbsp;
            <asp:LinkButton ID="btnElimina" runat="server" CssClass="btn btn-danger" OnClientClick="return getConfirmation(this, 'ATTENZIONE !','Confermi l\'eliminazione ?');"><span class="ion-trash-a"></span> ELIMINA </asp:LinkButton>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterPlaceHolder" runat="server">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">OK</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>