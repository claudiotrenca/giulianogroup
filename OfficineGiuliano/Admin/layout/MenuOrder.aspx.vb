﻿Namespace Admin
    Namespace Layout

        Public Class MenuOrder
            Inherits System.Web.UI.Page

            Private Sub MenuOrder_Init(sender As Object, e As EventArgs) Handles Me.Init
                If Not IsPostBack Then
                    BindMacro()

                    BindGrid()
                End If
            End Sub

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            End Sub

            Private Sub BindMacro()

                Dim lMenu As List(Of DataClass.Menu)
                Using context As New DataClass.DataEntities
                    If ddlMenu.SelectedValue = "top" Then
                        lMenu = (From p In context.Menu Where p.Tipo = "link" And p.ParentID Is Nothing And p.IncludeInTopMenu = True And p.FlagVisibile = True Select p Order By p.Indice Ascending).ToList
                    Else
                        lMenu = (From p In context.Menu Where p.Tipo = "link" And p.ParentID Is Nothing And p.IncludeInFooterMenu = True And p.FlagVisibile = True Select p Order By p.Indice Ascending).ToList
                    End If
                End Using

                Dim obj As New DataClass.Menu
                obj.PK = -1
                obj.Descrizione = "Seleziona..."
                lMenu.Insert(0, obj)

                ddlMacro.DataSource = lMenu
                ddlMacro.DataBind()

            End Sub

            Private Sub BindGrid(Optional ByVal _macro As Integer = -1)
                Dim lMenu As List(Of DataClass.Menu)
                Using context As New DataClass.DataEntities
                    If _macro = -1 Then
                        If ddlMenu.SelectedValue = "top" Then
                            lMenu = (From p In context.Menu Where p.Tipo = "link" And p.ParentID Is Nothing And p.IncludeInTopMenu = True And p.FlagVisibile = True Select p Order By p.Indice Ascending).ToList
                        Else
                            lMenu = (From p In context.Menu Where p.Tipo = "link" And p.ParentID Is Nothing And p.IncludeInFooterMenu = True And p.FlagVisibile = True Select p Order By p.Indice Ascending).ToList
                        End If
                    Else
                        If ddlMenu.SelectedValue = "top" Then
                            lMenu = (From p In context.Menu Where p.Tipo = "link" And p.ParentID = _macro And p.FlagVisibile = True Select p Order By p.Indice Ascending).ToList
                        Else
                            lMenu = (From p In context.Menu Where p.Tipo = "link" And p.ParentID = _macro And p.FlagVisibile = True Select p Order By p.Indice Ascending).ToList
                        End If
                    End If

                End Using

                GrdMenu.DataSource = lMenu
                GrdMenu.DataBind()

            End Sub

            Private Sub BtnUpdatePreference_Click(sender As Object, e As EventArgs) Handles BtnUpdatePreference.Click

                Dim locationIds As Integer() = (From p In Request.Form("LocationId").Split(",")
                                                Select Integer.Parse(p)).ToArray()
                Dim preference As Integer = 1
                For Each locationId As Integer In locationIds
                    Me.UpdatePreference(locationId, preference)
                    preference += 1
                Next

                BindGrid()

            End Sub

            Private Sub UpdatePreference(ByVal locationId As Integer, ByVal preference As Integer)
                Using context As New DataClass.DataEntities
                    Dim mt = context.Menu.SingleOrDefault(Function(p) p.PK = locationId)
                    mt.Indice = preference
                    context.SaveChanges()
                End Using
            End Sub

            Private Sub ddlMenu_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMenu.SelectedIndexChanged
                BindMacro()
            End Sub

            Private Sub ddlMacro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMacro.SelectedIndexChanged
                BindGrid(ddlMacro.SelectedValue)
            End Sub

            Private Sub btnAggiornaGriglia_Click(sender As Object, e As EventArgs) Handles btnAggiornaGriglia.Click
                BindGrid(ddlMacro.SelectedValue)
            End Sub
        End Class

    End Namespace
End Namespace