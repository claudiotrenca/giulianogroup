﻿Namespace Admin
    Namespace Layout
        Public Class HeaderEdit
            Inherits System.Web.UI.Page

            Private _Pk As Integer

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                If Not IsPostBack Then
                    BindGruppoMenu()

                    _Pk = IIf(Request.QueryString("id") IsNot Nothing, Request.QueryString("id"), Nothing)

                    ViewState("PK") = _Pk

                    If Not _Pk = Nothing Then
                        BindData()
                    End If
                Else
                    _Pk = ViewState("PK")
                End If

            End Sub

            Private Sub BindGruppoMenu()
                GruppoMenuList.DataSource = DataClass.MenuGruppo.GetList
                GruppoMenuList.DataBind()
            End Sub

            Private Sub BindData()

                Dim row As New DataClass.Header

                Using Context As New DataClass.DataEntities
                    row = Context.Header.FirstOrDefault(Function(p) p.PK = _Pk)
                End Using

                If row IsNot Nothing Then
                    HeaderNome.Text = row.Nome
                    HeaderText.Text = row.Contenuto
                    HeaderVisibile.Checked = row.FlagVisibile
                End If

            End Sub

            Private Sub SaveData()

                Using context As New DataClass.DataEntities

                    If _Pk = Nothing Then
                        Dim row As New DataClass.Header
                        row.Nome = HeaderNome.Text
                        row.Contenuto = HeaderText.Text
                        row.FlagVisibile = HeaderVisibile.Checked
                        context.SaveChanges()

                        context.Configuration.AutoDetectChangesEnabled = True
                        context.Header.Attach(row)
                        context.Entry(row).State = Entity.EntityState.Added
                        context.SaveChanges()

                        _Pk = row.PK
                    Else
                        Dim row = context.Header.SingleOrDefault(Function(p) p.PK = _Pk)
                        row.Nome = HeaderNome.Text
                        row.Contenuto = HeaderText.Text
                        row.FlagVisibile = HeaderVisibile.Checked
                        context.SaveChanges()
                    End If

                End Using

            End Sub

            Private Sub HeaderGruppoMenuAdd_Click(sender As Object, e As EventArgs) Handles HeaderGruppoMenuAdd.Click
                If HeaderText.Text = String.Empty Then
                    HeaderText.Text = "#NAV#" & "[" & GruppoMenuList.SelectedValue & "]"
                Else
                    HeaderText.Text = HeaderText.Text & "|" & "#NAV#" & "[" & GruppoMenuList.SelectedValue & "]"
                End If
            End Sub

            Private Sub HeaderLiteralAdd_Click(sender As Object, e As EventArgs) Handles HeaderLiteralAdd.Click
                If HeaderText.Text = String.Empty Then
                    HeaderText.Text = "#LITERAL#" & "[" & GruppoMenuList.SelectedValue & "]"
                Else
                    HeaderText.Text = HeaderText.Text & "|" & "#LITERAL#" & " ... INSERISCI QUI IL CONTENUTO HTML ... " & "#LITERAL#"
                End If
            End Sub

            Private Sub btnSalva_Click(sender As Object, e As EventArgs) Handles btnSalva.Click
                SaveData()
            End Sub

        End Class
    End Namespace
End Namespace