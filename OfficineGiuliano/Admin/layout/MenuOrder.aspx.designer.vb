﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Admin.Layout
    
    Partial Public Class MenuOrder
        
        '''<summary>
        '''Controllo HyperLink1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HyperLink1 As Global.System.Web.UI.WebControls.HyperLink
        
        '''<summary>
        '''Controllo ddlMenu.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlMenu As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo ddlMacro.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlMacro As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo btnAggiornaGriglia.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnAggiornaGriglia As Global.System.Web.UI.WebControls.Button
        
        '''<summary>
        '''Controllo GrdMenu.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents GrdMenu As Global.System.Web.UI.WebControls.GridView
        
        '''<summary>
        '''Controllo BtnUpdatePreference.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents BtnUpdatePreference As Global.System.Web.UI.WebControls.Button
    End Class
End Namespace
