﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/SiteAdmin.Master" CodeBehind="PageList.aspx.vb" Inherits="Emmemedia.Admin.Layout.PageList" %>

<%@ MasterType VirtualPath="~/Admin/SiteAdmin.Master" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function onCellClick(rowIndex, fieldName) {
            grwPage.PerformCallback(rowIndex + "|" + fieldName);
        }
        function OnEditorKeyPress(editor, e) {
            if (e.htmlEvent.keyCode == 13 || e.htmlEvent.keyCode == 9) {
                grwPage.UpdateEdit();
            }
            else
                if (e.htmlEvent.keyCode == 27)
                    grwPage.CancelEdit();
        }
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>MENU
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default" NavigateUrl="~/Admin/layout/PageEdit.aspx">AGGIUNGI NUOVO</asp:HyperLink>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <dx:ASPxGridView ID="grwPage" ClientInstanceName="grwPage" runat="server" DataSourceID="emdPage" Width="100%" AutoGenerateColumns="False" EnableTheming="True" CssClass="table " KeyFieldName="PK" Theme="Moderno">
                <SettingsSearchPanel Visible="True" />
                <Settings ShowFilterRow="true" ShowGroupPanel="False" />
                <SettingsEditing Mode="Inline" />
                <Columns>
                    <dx:GridViewDataColumn FieldName="Titolo" Caption="Titolo" VisibleIndex="1" />
                    <dx:GridViewDataColumn FieldName="Categoria" VisibleIndex="2" />
                    <dx:GridViewDataColumn FieldName="MetaTitle" VisibleIndex="3" />
                    <dx:GridViewDataCheckColumn FieldName="Visibile" VisibleIndex="5" CellStyle-CssClass="col-md-1">
                        <DataItemTemplate>
                            <dx:ASPxCheckBox ID="chk" runat="server" AllowGrayed="false" Value='<%#Eval("FlagVisibile")%>' OnInit="chk_Init" CssClass="checkbox">
                            </dx:ASPxCheckBox>
                        </DataItemTemplate>
                        <EditFormSettings Visible="False" />
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewDataTextColumn FieldName="" VisibleIndex="10">
                        <DataItemTemplate>
                            <asp:HyperLink ID="hyperlink" runat="server" NavigateUrl='<%# "~/Admin/layout/PageEdit.aspx?id=" & Eval("pk") %>' CssClass="btn btn-default">Seleziona</asp:HyperLink>
                            <asp:LinkButton ID="ASPxButton1" runat="server" Text="Elimina" CssClass="btn btn-danger danger" CommandArgument="elimina" OnClientClick="return getConfirmation(this, 'Attenzione','Confermi l\'eliminazione ?');"></asp:LinkButton>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
            <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="grwPageCB" OnCallback="grwPage_Callback"></dx:ASPxCallback>
            <dx:EntityServerModeDataSource ID="emdPage" runat="server" ContextTypeName="Emmemedia.DataClass.DataEntities" EnableDelete="True" EnableUpdate="True" TableName="vwPost" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="content3" runat="server" ContentPlaceHolderID="FooterPlaceHolder">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">
                        ELIMINA</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>