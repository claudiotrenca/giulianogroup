﻿Imports DevExpress.Data.Linq
Imports DevExpress.Web
Imports DevExpress.Web.ASPxTreeList
Imports DevExpress.Web.Internal
Imports System.Drawing
Imports System.IO
Imports System.Reflection

Namespace Admin
    Namespace Layout
        Public Class MenuEdit
            Inherits System.Web.UI.Page

            Private _PK As Integer = Nothing
            Private _FileImageName As String = String.Empty

            Private Const UploadDirectory As String = "~/public/images/menu/"

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                If Not IsPostBack Then
                    BindGruppoMenu()

                    Session("PageFileImageName") = Nothing
                    Session("PkMenu") = Nothing

                    If Request.QueryString("id") IsNot Nothing Then
                        _PK = Request.QueryString("id")
                        Session("PkMenu") = _PK
                        BindData()

                        btnElimina.Enabled = True
                    Else
                        LabelModalità.Text = "NUOVA VOCE DI MENU'"
                        btnElimina.Enabled = False
                    End If
                Else
                    _PK = Session("PkMenu")
                End If

                MenuLink.ReadOnly = True

                uploadedImage.Attributes.Add("onload", "onImageLoad()")

            End Sub

            Private Sub BindGruppoMenu()
                ddlGruppoMenu.DataSource = DataClass.MenuGruppo.GetList
                ddlGruppoMenu.DataBind()
            End Sub

            Private Sub BindData()

                Dim row As New DataClass.Menu

                Using dataContext As New DataClass.DataEntities
                    row = dataContext.Menu.FirstOrDefault(Function(p) p.PK = _PK)
                End Using

                If row IsNot Nothing Then
                    LabelModalità.Text = "MODIFICA : " & row.Descrizione

                    MenuNome.Text = row.Descrizione
                    MenuLink.Text = row.Link
                    MenuDescrizione.Text = row.DescrizioneEstesa
                    MenuCulture.SelectedValue = row.Culture.ToString.Trim
                    MenuVisibile.Checked = row.FlagVisibile
                    MenuPrivato.Checked = row.FlagPrivato
                    IncludeTopMenu.Checked = row.IncludeInTopMenu
                    IncludeFooterMenu.Checked = row.IncludeInFooterMenu
                    ddlGruppoMenu.SelectedValue = row.FkGruppoMenu
                    MenuTarget.SelectedValue = row.Target

                    If Not String.IsNullOrEmpty(row.Immagine) Then
                        uploadedImage.Src = UploadDirectory & row.Immagine
                        uploadedImage.Attributes.Item("class") = ""
                        uploadedImage.Attributes.Item("style") = ""
                        uploadedImage.Style.Clear()
                        uploadedImage.Height = 240
                    End If

                    If row.ParentID IsNot Nothing Then
                        Dim treeList As ASPxTreeList = DirectCast(MenuMacro.FindControl("ASPxTreeList1"), ASPxTreeList)
                        treeList.UnselectAll()
                        Dim node As TreeListNode = treeList.FindNodeByKeyValue(row.ParentID)
                        node.Focus()
                        node.Selected = True
                        MenuMacro.KeyValue = row.ParentID
                        MenuMacro.Text = DataClass.Menu.GetNameFromID(row.ParentID)
                    End If

                End If

            End Sub

            Private Sub SaveData()

                Using context As New DataClass.DataEntities

                    If _PK = Nothing Then

                        Dim mt As New DataClass.Menu
                        mt.Descrizione = MenuNome.Text
                        mt.DescrizioneEstesa = MenuDescrizione.Text
                        mt.Link = MenuLink.Text
                        mt.Tipo = "link"
                        mt.Culture = MenuCulture.SelectedValue
                        mt.FlagVisibile = MenuVisibile.Checked
                        mt.FlagPrivato = MenuPrivato.Checked
                        mt.IncludeInTopMenu = IncludeTopMenu.Checked
                        mt.IncludeInFooterMenu = IncludeFooterMenu.Checked
                        mt.HasCategory = False
                        mt.Indice = 1
                        mt.FkGruppoMenu = ddlGruppoMenu.SelectedValue
                        If MenuMacro.KeyValue <> "" Then
                            mt.ParentID = CInt(MenuMacro.KeyValue)
                        End If
                        context.Configuration.AutoDetectChangesEnabled = True
                        context.Menu.Attach(mt)
                        context.Entry(mt).State = Entity.EntityState.Added
                        context.SaveChanges()
                    Else
                        Dim mt = context.Menu.SingleOrDefault(Function(p) p.PK = _PK)
                        mt.Descrizione = MenuNome.Text
                        mt.DescrizioneEstesa = MenuDescrizione.Text
                        mt.Link = MenuLink.Text
                        mt.Tipo = "link"
                        mt.IncludeInTopMenu = IncludeTopMenu.Checked
                        mt.IncludeInFooterMenu = IncludeFooterMenu.Checked
                        mt.Culture = MenuCulture.SelectedValue
                        mt.FlagVisibile = MenuVisibile.Checked
                        mt.FlagPrivato = MenuPrivato.Checked
                        mt.FkGruppoMenu = ddlGruppoMenu.SelectedValue

                        If MenuMacro.KeyValue <> "" Then
                            mt.ParentID = CInt(MenuMacro.KeyValue)
                        Else
                            mt.ParentID = Nothing
                        End If

                        If Not String.IsNullOrEmpty(_FileImageName) Then
                            mt.Immagine = _FileImageName

                            uploadedImage.Src = "/public/images/menu/" & _FileImageName
                            uploadedImage.Attributes.Item("class") = ""
                            uploadedImage.Attributes.Item("style") = ""
                            uploadedImage.Style.Clear()
                            uploadedImage.Height = 240

                        End If

                        context.SaveChanges()
                    End If

                End Using

            End Sub

            Protected Sub TreeList_DataBound(ByVal sender As Object, ByVal e As EventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                If (Not IsCallback) AndAlso (Not IsPostBack) Then
                    tree.CollapseAll()
                End If
            End Sub

            Protected Sub TreeList_CustomJSProperties(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxTreeList.TreeListCustomJSPropertiesEventArgs)
                Dim tree As ASPxTreeList = TryCast(sender, ASPxTreeList)
                Dim CategoryNames As New Hashtable()
                For Each node As TreeListNode In tree.GetVisibleNodes()
                    CategoryNames.Add(node.Key, node("Descrizione"))
                Next node
                e.Properties("cpCategoryNames") = CategoryNames
            End Sub

            Protected Sub ASPxTreeList1_CustomCallback(sender As Object, e As TreeListCustomCallbackEventArgs)
                Dim ASPxTreeList1 As ASPxTreeList = TryCast(sender, ASPxTreeList)
                ASPxTreeList1.CollapseAll()
                Dim node As TreeListNode = ASPxTreeList1.FindNodeByKeyValue(e.Argument)
                While node.ParentNode IsNot Nothing
                    node.Expanded = True
                    node = node.ParentNode
                End While
            End Sub

            Private Function FindAvailableLink(ByVal _str As String, ByVal modify As Boolean) As String

                Dim rv As String = String.Empty
                Dim Suffix As String = String.Empty
                Dim Link As String = String.Empty

                Dim Index As Integer = 0

                If modify = True Then
                    Link = _str
                Else
                    Link = Classi.Utility.StringToUrl(_str)
                End If

                Dim GruppoMenu As Integer = ddlGruppoMenu.SelectedValue

                If _PK <> Nothing Then
                    Using context As New DataClass.DataEntities
                        While (From row In context.Menu Where row.Link = Link And row.PK <> _PK And row.FkGruppoMenu = GruppoMenu And (row.Tipo = "link" Or row.Tipo = "cartella") Select row).FirstOrDefault IsNot Nothing
                            Index += 1
                            Suffix = "-" & CStr(Index)
                            Link = (Classi.Utility.StringToUrl(_str) & Suffix).Trim
                        End While
                        rv = Link
                    End Using
                Else
                    Using context As New DataClass.DataEntities
                        While (From row In context.Menu Where row.Link = Link And row.FkGruppoMenu = GruppoMenu And (row.Tipo = "link" Or row.Tipo = "cartella") Select row).FirstOrDefault IsNot Nothing
                            Index += 1
                            Suffix = "-" & CStr(Index)
                            Link = (Classi.Utility.StringToUrl(_str) & Suffix).Trim
                        End While
                        rv = Link
                    End Using
                End If

                Return rv

            End Function

            Protected Sub cb_Callback(source As Object, e As CallbackEventArgs) Handles cb.Callback
                e.Result = String.Format(FindAvailableLink(e.Parameter, False))
            End Sub

            Private Sub btnModificaLink_Click(sender As Object, e As EventArgs) Handles btnModificaLink.Click

                If ViewState("ModificaLink") = True Then
                    MenuLink.Text = String.Format(FindAvailableLink(MenuLink.Text, True))
                    ViewState("ModificaLink") = False
                    MenuLink.ReadOnly = True
                    btnModificaLink.Text = "Modifica"
                Else
                    MenuLink.ReadOnly = False
                    btnModificaLink.Text = "SALVA"
                    ViewState("ModificaLink") = True
                End If

            End Sub

            Private Sub btnMenuSalva_Click(sender As Object, e As EventArgs) Handles btnSalva.Click
                SaveData()
            End Sub

            Private Sub btnElimina_Click(sender As Object, e As EventArgs) Handles btnElimina.Click
                DataClass.Menu.Delete(_PK)
            End Sub

            Protected Function CombinePath(ByVal fileName As String, ByVal _path As String) As String
                Return System.IO.Path.Combine(Server.MapPath(_path), fileName)
            End Function

            Private Sub UploadControl_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs) Handles UploadControl.FileUploadComplete

                e.CallbackData = SavePostedFile(e.UploadedFile, UploadDirectory)

                Dim id As Integer = Session("PkMenu")

                _FileImageName = e.UploadedFile.FileName
                _FileImageName = id & "_" & _FileImageName
                Session("MenuFileImageName") = _FileImageName
                e.UploadedFile.SaveAs(CombinePath(_FileImageName, UploadDirectory))

                Using context As New DataClass.DataEntities
                    Dim row = context.Menu.SingleOrDefault(Function(p) p.PK = id)
                    row.Immagine = _FileImageName
                    context.SaveChanges()
                End Using

            End Sub

            Protected Function SavePostedFile(ByVal uploadedFile As UploadedFile, ByVal _path As String) As String

                Try
                    If (Not uploadedFile.IsValid) Then
                        Return String.Empty
                    End If

                    Dim fileName As String = System.IO.Path.ChangeExtension(System.IO.Path.GetRandomFileName(), ".jpg")
                    Dim fullFileName As String = CombinePath(fileName, _path)

                    Using original As Image = Image.FromStream(uploadedFile.FileContent)

                        Using thumbnail As Image = PhotoUtils.Inscribe(CType(original, System.Drawing.Bitmap), 550, 250)  ' ImageUtils.CreateThumbnailImage(CType(original, System.Drawing.Bitmap), ImageSizeMode.ActualSizeOrFit, New Size(550, 250))
                            ImageUtils.SaveToJpeg(CType(thumbnail, System.Drawing.Bitmap), fullFileName)
                        End Using

                    End Using

                    LibUploadingUtils.RemoveFileWithDelay(fileName, fullFileName, 5)

                    Return fileName
                Catch ex As Exception
                    Throw ex
                End Try

            End Function

            Private Sub brnRimuoviImmagine_Click(sender As Object, e As EventArgs) Handles brnRimuoviImmagine.Click
                If Session("PkMenu") IsNot Nothing Then
                    Dim id As Integer = Session("PkMenu")
                    Using context As New DataClass.DataEntities
                        Dim row = context.Menu.SingleOrDefault(Function(p) p.PK = id)
                        row.Immagine = ""
                        context.SaveChanges()
                    End Using
                End If

                BindData()

            End Sub
        End Class

    End Namespace

End Namespace