﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Admin.Layout
    
    Partial Public Class PageEdit
        
        '''<summary>
        '''Controllo ASPxHyperLink1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ASPxHyperLink1 As Global.DevExpress.Web.ASPxHyperLink
        
        '''<summary>
        '''Controllo ASPxPageControl1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ASPxPageControl1 As Global.DevExpress.Web.ASPxPageControl
        
        '''<summary>
        '''Controllo PaginaTitolo.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaTitolo As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo PaginaSottotitolo.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaSottotitolo As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo PaginaSezione.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaSezione As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo PaginaContenutoBreve.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaContenutoBreve As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo PaginaSlider.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaSlider As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo PaginaContenutoSuperiore.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaContenutoSuperiore As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo PaginaContenutoPrincipale.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaContenutoPrincipale As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo PaginaContenutoInferiore.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaContenutoInferiore As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo PaginaPageTitle.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaPageTitle As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo PaginaPageDescription.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaPageDescription As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo PaginaPageKeywords.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaPageKeywords As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo HiddenField.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents HiddenField As Global.DevExpress.Web.ASPxHiddenField
        
        '''<summary>
        '''Controllo FormLayout.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormLayout As Global.DevExpress.Web.ASPxFormLayout
        
        '''<summary>
        '''Controllo DescriptionTextBox.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents DescriptionTextBox As Global.DevExpress.Web.ASPxTextBox
        
        '''<summary>
        '''Controllo ProdottoGalleryUploadControl.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoGalleryUploadControl As Global.DevExpress.Web.ASPxUploadControl
        
        '''<summary>
        '''Controllo UploadedFilesTokenBox.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UploadedFilesTokenBox As Global.DevExpress.Web.ASPxTokenBox
        
        '''<summary>
        '''Controllo AllowedFileExtensionsLabel.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents AllowedFileExtensionsLabel As Global.DevExpress.Web.ASPxLabel
        
        '''<summary>
        '''Controllo MaxFileSizeLabel.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MaxFileSizeLabel As Global.DevExpress.Web.ASPxLabel
        
        '''<summary>
        '''Controllo ValidationSummary.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ValidationSummary As Global.DevExpress.Web.ASPxValidationSummary
        
        '''<summary>
        '''Controllo ProdottoGalleryInvia.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoGalleryInvia As Global.DevExpress.Web.ASPxButton
        
        '''<summary>
        '''Controllo RoundPanel.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents RoundPanel As Global.DevExpress.Web.ASPxRoundPanel
        
        '''<summary>
        '''Controllo DescriptionLabel.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents DescriptionLabel As Global.DevExpress.Web.ASPxLabel
        
        '''<summary>
        '''Controllo SubmittedFilesListBox.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents SubmittedFilesListBox As Global.DevExpress.Web.ASPxListBox
        
        '''<summary>
        '''Controllo UpdatePanel1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
        
        '''<summary>
        '''Controllo lvFotogallery.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents lvFotogallery As Global.System.Web.UI.WebControls.ListView
        
        '''<summary>
        '''Controllo btnSalva.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnSalva As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Controllo btnElimina.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnElimina As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Controllo settingsFormLayout.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents settingsFormLayout As Global.DevExpress.Web.ASPxFormLayout
        
        '''<summary>
        '''Controllo MenuMacro.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MenuMacro As Global.DevExpress.Web.ASPxDropDownEdit
        
        '''<summary>
        '''Controllo EntityDataSource1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents EntityDataSource1 As Global.Microsoft.AspNet.EntityDataSource.EntityDataSource
        
        '''<summary>
        '''Controllo btnSalvaMenu.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnSalvaMenu As Global.System.Web.UI.WebControls.Button
        
        '''<summary>
        '''Controllo MenuMapGrid.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MenuMapGrid As Global.DevExpress.Web.ASPxGridView
        
        '''<summary>
        '''Controllo MenuMapDataSource.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MenuMapDataSource As Global.DevExpress.Data.Linq.EntityServerModeDataSource
        
        '''<summary>
        '''Controllo PaginaVisibile.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaVisibile As Global.DevExpress.Web.ASPxCheckBox
        
        '''<summary>
        '''Controllo PaginaData.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PaginaData As Global.DevExpress.Web.ASPxLabel
        
        '''<summary>
        '''Controllo ddlTemplatePagina.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlTemplatePagina As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo ddlGruppoMenu.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlGruppoMenu As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo uploadedImage.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents uploadedImage As Global.System.Web.UI.HtmlControls.HtmlImage
        
        '''<summary>
        '''Controllo UploadControl.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UploadControl As Global.DevExpress.Web.ASPxUploadControl
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.Admin.SiteAdmin
            Get
                Return CType(MyBase.Master,Emmemedia.Admin.SiteAdmin)
            End Get
        End Property
    End Class
End Namespace
