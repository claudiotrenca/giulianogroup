﻿Namespace Admin

    Public Class Login
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not IsPostBack Then
                LoginError.Visible = False
            End If
        End Sub

        Private Sub BtnLogin_Click(sender As Object, e As EventArgs) Handles BtnLogin.Click

            Dim row As DataClass.UtentiAdmin
            Dim result As LoginStatus

            Using context As New DataClass.DataEntities
                row = (From p In context.UtentiAdmin Where p.Username = AdminUsername.Text.Trim And p.Password = AdminPassword.Text.Trim Select p).FirstOrDefault
            End Using

            If row IsNot Nothing Then
                result = LoginStatus.Success
                If row.Attivo = False Then
                    result = LoginStatus.Locked
                End If
            Else
                result = LoginStatus.Failed
            End If

            Select Case result
                Case LoginStatus.Success
                    LoginError.Visible = False
                    Session("AdminLogin") = True
                    Response.Redirect("/admin/default")
                    Exit Select
                Case LoginStatus.Failed
                    LoginError.Visible = True
                    LoginErrorMessage.Text = "Username o password errata."
                    Exit Select
                Case LoginStatus.Locked
                    LoginError.Visible = True
                    LoginErrorMessage.Text = "Utente bloccato o non attivo"
                    Exit Select
                Case Else
                    LoginError.Visible = True
                    LoginErrorMessage.Text = "Tentativo di accesso non valido"
                    Exit Select
            End Select

        End Sub

    End Class

End Namespace