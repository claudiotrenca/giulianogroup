<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AgenzieDettagli.aspx.vb" Inherits="Emmemedia.AgenzieDettagli" %>

<%@ Register Src="~/Controls/Offerte/OfferteRicercaBoxVerticale.ascx" TagPrefix="uc1" TagName="OfferteRicercaBoxVerticale" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SliderPh" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentUpperPh" runat="server">
    <uc1:Path runat="server" ID="Path" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <!-- title AGENCY -->
    <div class="row">
        <div class="header">
            <div class="col-md-10">
                <h2 class="h2"><strong>
                    <asp:Literal ID="AgenziaNome" runat="server"></asp:Literal></strong></h2>
                <h3 class="h2">
                    <asp:Literal ID="AgenziaIndirizzo2" runat="server"></asp:Literal></h3>
                <br />
                <h4 class="h3">
                    <asp:Literal ID="AgenziaComune2" runat="server"></asp:Literal></h4>
            </div>
            <!--
        <div class="col-md-2">
            <p>social share</p>
        </div>
<-->
            </-->
        </div>
    </div>
    <!-- image agency -->
    <div class="row">
        <div class="col-sm-7 col-md-8">
            <div class="col-md-12 summary no-gutter">
                <div class="gallery rounded-borders">
                    <div id="animated-thumbnails">
                        <a href="/images/agency.jpg" class="gallery-img">
                            <img src="/images/agency.jpg" />
                        </a>
                    </div>
                    <div class="circle text-center">
                        <i class="ion-ios-plus-empty"></i>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- description -->
            <div class="col-md-12 description-for-agency">
                <p class="h3"><b>DESCRIZIONE COMPLETA</b></p>
                <br />
                <br />
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p><b>NOME</b></p>
                        <p>
                            <asp:Literal ID="AgenziaSocieta" runat="server"></asp:Literal>
                        </p>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p><b><i class="ion-android-person"></i>REFERENTE</b></p>
                        <p>
                            <asp:Literal ID="AgenziaReferente" runat="server"></asp:Literal>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p><b>INDIRIZZO</b></p>
                        <p>
                            <asp:Literal ID="AgenziaIndirizzo" runat="server"></asp:Literal>
                        </p>
                        <p>
                            <asp:Literal ID="AgenziaCap" runat="server"></asp:Literal>&nbsp;<asp:Literal ID="AgenziaComune" runat="server"></asp:Literal>
                            <asp:Literal ID="AgenziaProvincia" runat="server" Visible="false"></asp:Literal>
                        </p>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p><b><i class="ion-android-mail"></i>EMAIL</b></p>
                        <p>
                            <a href="">
                                <asp:Literal ID="AgenziaMail" runat="server"></asp:Literal></a>
                        </p>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p><b><i class="ion-information-circled"></i>TELEFONO</b></p>
                        <p>
                            T.
                            <asp:Literal ID="AgenziaTel" runat="server"></asp:Literal>
                        </p>
                        <p>
                            F.
                            <asp:Literal ID="AgenziaFax" runat="server"></asp:Literal>
                        </p>
                        <p>
                            M.
                            <asp:Literal ID="AgenziaCell" runat="server"></asp:Literal>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p><b>PARTITA IVA</b></p>
                        <p>
                            <asp:Literal ID="AgenziaPiva" runat="server"></asp:Literal>
                        </p>
                    </div>
                </div>
                <br />
                <!-- SEND MAIL -->
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <asp:LinkButton runat="server" ID="richInfo" Text="INVIA UNA MAIL" CssClass="btn btn-transparent full-width"></asp:LinkButton>
                    </div>
                </div>
                <canvas class="triangle"></canvas>
            </div>
            <!-- vedi anche -->
            <p class="h3">Vedi <b>Anche</b></p>
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvAgenzie" runat="server">
                        <LayoutTemplate>
                            <div class="table-responsive">
                                <table class="table-results">
                                    <thead>
                                        <th>Agenzia</th>
                                        <th>Città</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                                    </tbody>
                                </table>
                            </div>
                            <hr />
                            <nav>
                                <asp:DataPager runat="server" ID="dpAgenzie" PageSize="27" PagedControlID="lvAgenzie">
                                    <Fields>
                                        <asp:NextPreviousPagerField
                                            ShowFirstPageButton="false" ShowLastPageButton="false" ShowPreviousPageButton="true" ShowNextPageButton="false" ButtonType="Link" NextPageText="Successiva" PreviousPageText="Precedente" ButtonCssClass="btn pagination" />

                                        <asp:NumericPagerField ButtonType="Link" NumericButtonCssClass="btn btn-danger" CurrentPageLabelCssClass="btn disabled" ButtonCount="10" NextPreviousButtonCssClass="pagerProdottiSpan" />

                                        <asp:NextPreviousPagerField
                                            ShowFirstPageButton="false" ShowLastPageButton="false" ShowPreviousPageButton="false" ShowNextPageButton="true" ButtonType="link" NextPageText="Successiva" PreviousPageText="Precedente" ButtonCssClass="btn pagination" />
                                    </Fields>
                                </asp:DataPager>
                                <br />
                            </nav>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><i class="ion-location"></i><%# DataBinder.Eval(Container.DataItem, "AGENZIA")%></strong></td>

                                <td><%# DataBinder.Eval(Container.DataItem, "comune")%>&nbsp;(<%# DataBinder.Eval(Container.DataItem, "provincia")%>)</td>
                                <td class="text-center">
                                    <asp:HyperLink ID="link" runat="server"> dettagli agenzia  </asp:HyperLink></td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="col-sm-5 col-md-4">
            <div id="sidebar">
                <!-- search in sidebar -->
                <uc1:OfferteRicercaBoxVerticale runat="server" ID="OfferteRicercaBoxVerticale" />
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterScriptsPh" runat="server">
    <!-- Modale carrello -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span class="ion-android-close" aria-hidden="true"></span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Richiesta Informazioni</h4>
                </div>
                <div class="modal-body">
                    <img id="ModalProductImage" runat="server" />
                    <div class="prodottoNome">
                        <h5>Informazioni di Contatto:</h5>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cognome e Nome</label>
                                    <asp:TextBox runat="server" ID="txtNome" CssClass="form-control" required="true"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Indirizzo E-Mail</label>
                                    <asp:TextBox runat="server" ID="txtMail" CssClass="form-control" required="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Numero di Telefono  </label>
                                    <asp:TextBox runat="server" ID="txtTel1" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Citt&agrave;</label>
                                    <asp:TextBox runat="server" ID="txtTel2" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Note</label>
                                    <asp:TextBox runat="server" ID="txtNote" TextMode="MultiLine" Rows="3" Width="90%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <asp:LinkButton ID="btnInvia" Text="Invia" runat="server" CssClass="btn btn-secondary full-width"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>