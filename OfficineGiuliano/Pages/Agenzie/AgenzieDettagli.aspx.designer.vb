﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AgenzieDettagli
    
    '''<summary>
    '''Controllo Path.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents Path As Global.Emmemedia.Controls.Common.Path
    
    '''<summary>
    '''Controllo AgenziaNome.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaNome As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaIndirizzo2.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaIndirizzo2 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaComune2.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaComune2 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaSocieta.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaSocieta As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaReferente.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaReferente As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaIndirizzo.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaIndirizzo As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaCap.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaCap As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaComune.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaComune As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaProvincia.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaProvincia As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaMail.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaMail As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaTel.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaTel As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaFax.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaFax As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaCell.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaCell As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo AgenziaPiva.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents AgenziaPiva As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo richInfo.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents richInfo As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Controllo UpdatePanel1.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Controllo lvAgenzie.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents lvAgenzie As Global.System.Web.UI.WebControls.ListView
    
    '''<summary>
    '''Controllo OfferteRicercaBoxVerticale.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents OfferteRicercaBoxVerticale As Global.Emmemedia.Controls.Offerte.OfferteRicercaBoxVerticale
    
    '''<summary>
    '''Controllo ModalProductImage.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ModalProductImage As Global.System.Web.UI.HtmlControls.HtmlImage
    
    '''<summary>
    '''Controllo txtNome.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents txtNome As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Controllo txtMail.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents txtMail As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Controllo txtTel1.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents txtTel1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Controllo txtTel2.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents txtTel2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Controllo txtNote.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents txtNote As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Controllo btnInvia.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents btnInvia As Global.System.Web.UI.WebControls.LinkButton
End Class
