﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AgenzieElenco.aspx.vb" Inherits="Emmemedia.AgenzieElenco1" %>

<%@ Register Src="~/Controls/Offerte/OfferteRicercaBoxVerticale.ascx" TagPrefix="uc1" TagName="OfferteRicercaBoxVerticale" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SliderPh" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentUpperPh" runat="server">
    <uc1:Path runat="server" ID="Path" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <asp:Literal runat="Server" ID="ltCorpoDoc" />
    <asp:PlaceHolder runat="server" ID="phCenter" />
    <div class="row">
        <div class="col-sm-7 col-md-8">
            <!-- search agency box -->
            <div class="search-agency-box">
                <h3 class="h2">Ricerca l'agenzia più vicina a te</h3>
                <br />
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <div class="form-group">
                            <label>Ordina Per</label>
                            <p>
                                <asp:DropDownList ID="ddlOrdinamento" runat="server" CssClass="form-control selectpicker" AutoPostBack="true">
                                    <asp:ListItem Text="Nome" Value="Agenzia" />
                                    <asp:ListItem Text="Città" Value="Comune" />
                                </asp:DropDownList>
                            </p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br />
                    <div class="col-sm-4 col-md-4">
                        <div class="form-group">
                            <label>Filtra per</label>
                            <p>

                                <asp:DropDownList ID="ddlRegione" runat="server" CssClass="form-control selectpicker" DataTextField="regione" DataValueField="regione" AutoPostBack="True" DataSourceID="SqlDataSource1" AppendDataBoundItems="True" />
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <p>

                                <asp:DropDownList ID="ddlProvincia" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control selectpicker" DataSourceID="SqlDataSource2" DataTextField="provincia" DataValueField="sigla">
                                </asp:DropDownList>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <p>

                                <asp:DropDownList ID="ddlComune" runat="server" AppendDataBoundItems="True" AutoPostBack="True" CssClass="form-control selectpicker" DataSourceID="SqlDataSource3" DataTextField="comune" DataValueField="comune"></asp:DropDownList>
                            </p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <asp:ListView ID="lvAgenzie" runat="server">
                <LayoutTemplate>
                    <div class="table-responsive">
                        <table class="table-results">
                            <thead>
                                <th>Agenzia</th>
                                <th>Città</th>
                                <th></th>
                            </thead>
                            <tbody>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                            </tbody>
                        </table>
                    </div>
                    <hr />
                    <nav>
                        <asp:DataPager runat="server" ID="dpAgenzie" PageSize="27" PagedControlID="lvAgenzie">
                            <Fields>
                                <asp:NextPreviousPagerField
                                    ShowFirstPageButton="false" ShowLastPageButton="false" ShowPreviousPageButton="true" ShowNextPageButton="false" ButtonType="Link" NextPageText="Successiva" PreviousPageText="Precedente" ButtonCssClass="btn pagination" />

                                <asp:NumericPagerField ButtonType="Link" NumericButtonCssClass="btn btn-danger" CurrentPageLabelCssClass="btn disabled" ButtonCount="10" NextPreviousButtonCssClass="pagerProdottiSpan" />

                                <asp:NextPreviousPagerField
                                    ShowFirstPageButton="false" ShowLastPageButton="false" ShowPreviousPageButton="false" ShowNextPageButton="true" ButtonType="link" NextPageText="Successiva" PreviousPageText="Precedente" ButtonCssClass="btn pagination" />
                            </Fields>
                        </asp:DataPager>
                        <br />
                    </nav>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td><i class="ion-location"></i><%# DataBinder.Eval(Container.DataItem, "AGENZIA")%></strong></td>

                        <td><%# DataBinder.Eval(Container.DataItem, "comune")%>&nbsp;(<%# DataBinder.Eval(Container.DataItem, "provincia")%>)</td>
                        <td class="text-center">
                            <asp:HyperLink ID="link" runat="server"> dettagli agenzia  </asp:HyperLink></td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
        <div class="col-sm-5 col-md-4">
            <div id="sidebar" class="no-margin">
                <uc1:OfferteRicercaBoxVerticale runat="server" ID="OfferteRicercaBoxVerticale" />
            </div>
        </div>
    </div>
    <hr />

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Accesso %>" SelectCommand="SELECT DISTINCT [regione] FROM [vwAgenzie] ORDER BY [regione]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Accesso %>" SelectCommand="SELECT DISTINCT [provincia], [sigla] FROM [vwAgenzie] WHERE ([regione] = @regione) ORDER BY [provincia]">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlRegione" Name="regione" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:Accesso %>" SelectCommand="SELECT DISTINCT [comune] FROM [vwAgenzie] WHERE ([sigla] = @sigla) ORDER BY [comune]">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlProvincia" Name="sigla" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>