﻿Imports System.Data
Imports System.Data.SqlClient

Public Class AgenzieElenco1
    Inherits BaseClass.SitePage
    Dim _strFilter As String = String.Empty

    Private _Regione As String
    Private _Prov As String
    Private _Localita As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            _Regione = IIf(Page.RouteData.Values("Regione") <> Nothing, Page.RouteData.Values("Regione"), String.Empty)
            _Prov = IIf(Page.RouteData.Values("Provincia") <> Nothing, Page.RouteData.Values("Provincia"), String.Empty)
            _Localita = IIf(Page.RouteData.Values("citta") <> Nothing, Page.RouteData.Values("citta"), String.Empty)

            Session("StartRowIndex") = Nothing

            If _Localita <> String.Empty Then

                _strFilter = "where comune = '" & _Localita & "'"
            ElseIf _Prov <> String.Empty Then
                _strFilter = "where sigla = '" & _Prov & "'"
            ElseIf _Regione <> String.Empty Then

                _strFilter = ""
            End If

            Session("strFilter") = _strFilter
            clearProvince()
            clearComuni()

        Else
            _strFilter = Session("strFilter")
        End If

        If Session("StartRowIndex") IsNot Nothing Then
            bindElencoAgenzie(_strFilter, Session("StartRowIndex"))
        Else
            bindElencoAgenzie(_strFilter)
        End If

    End Sub

    Private Sub bindElencoAgenzie(ByVal _str As String, Optional ByVal startrecord As Integer = 0)

        Dim sql As String
        Dim idm As String = ConfigurationManager.AppSettings("id_marchant")

        sql = "SELECT pk_codice, agenzia , indirizzo, regione, provincia , sigla , comune , localita FROM vwAgenzie " & _str & " order by " & ddlOrdinamento.SelectedValue & " ASC "

        Using con As New SqlConnection(ConfigurationManager.ConnectionStrings("accesso").ConnectionString)
            Using cmd As New SqlCommand

                cmd.Connection = con
                cmd.CommandType = CommandType.Text
                cmd.CommandText = sql

                con.Open()

                Dim dt As New DataTable
                dt.Load(cmd.ExecuteReader)

                con.Close()

                lvAgenzie.DataSource = dt
                lvAgenzie.DataBind()

                If startrecord <> 0 Then
                    Dim dp As DataPager = lvAgenzie.FindControl("dpAgenzie")

                    If dp IsNot Nothing Then
                        dp.SetPageProperties(startrecord, dp.MaximumRows, False)
                        lvAgenzie.DataBind()

                    End If
                End If

            End Using

        End Using
    End Sub

    Protected Sub lvAgenzie_ItemCreated(sender As Object, e As ListViewItemEventArgs) Handles lvAgenzie.ItemCreated
        If e.Item.ItemType = ListViewItemType.DataItem Then

            Dim dataItem As ListViewDataItem = DirectCast(e.Item, ListViewDataItem)
            Dim drv As DataRowView = DirectCast(dataItem.DataItem, DataRowView)

        End If
    End Sub

    Protected Sub lvAgenzie_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvAgenzie.ItemDataBound

        Dim li As ListViewItem = e.Item

        If li.ItemType = ListViewItemType.DataItem Then

            Dim di As ListViewDataItem = DirectCast(li, ListViewDataItem)
            Dim dr As System.Data.DataRowView = DirectCast(di.DataItem, System.Data.DataRowView)

            Using lnk As HyperLink = DirectCast(e.Item.FindControl("LINK"), HyperLink)

                lnk.NavigateUrl = "/agenzie-di-viaggi/" & Classi.Utility.StringToUrl(dr("regione").ToString.Trim.ToLower) & "/" & Classi.Utility.StringToUrl(dr("provincia").ToString.Trim.ToLower) & "/" & Classi.Utility.StringToUrl(dr("comune").ToString.Trim.ToLower) & "/" & Classi.Utility.StringToUrl(dr("agenzia").ToString.Trim.ToLower) & "_id" & dr("pk_codice").ToString.Trim & ".htm"

            End Using

        End If
    End Sub

    Protected Sub lvAgenzie_PagePropertiesChanging(sender As Object, e As PagePropertiesChangingEventArgs) Handles lvAgenzie.PagePropertiesChanging

        Dim dp As DataPager
        dp = lvAgenzie.FindControl("dpAgenzie")
        Session("StartRowIndex") = e.StartRowIndex
        dp.SetPageProperties(e.StartRowIndex, e.MaximumRows, False)

        bindElencoAgenzie(Session("strFilter"), Session("StartRowIndex"))

    End Sub

    Private Sub clearRegioni()

    End Sub

    Private Sub clearProvince()
        ddlProvincia.Items.Clear()

        Dim item As New ListItem
        item.Text = "seleziona..."
        item.Value = 0
        'item.Selected = True
        ddlProvincia.Items.Insert(0, item)
    End Sub

    Private Sub clearComuni()
        ddlComune.Items.Clear()

        Dim item As New ListItem
        item.Text = "seleziona..."
        item.Value = 0
        'item.Selected = True
        ddlComune.Items.Insert(0, item)
    End Sub

    Protected Sub ddlProvincia_DataBinding(sender As Object, e As EventArgs) Handles ddlProvincia.DataBinding
        If IsPostBack Then
            clearProvince()
        End If
    End Sub

    Protected Sub ddlComune_DataBinding(sender As Object, e As EventArgs) Handles ddlComune.DataBinding

        If IsPostBack Then
            clearComuni()
        End If

    End Sub

    Protected Sub ddlRegione_DataBinding(sender As Object, e As EventArgs) Handles ddlRegione.DataBinding
        If IsPostBack Then
            clearProvince()
            clearComuni()
        End If
    End Sub

    Protected Sub ddlRegione_DataBound(sender As Object, e As EventArgs) Handles ddlRegione.DataBound
        Dim item As New ListItem
        item.Text = "seleziona..."
        item.Value = 0
        item.Selected = True
        ddlRegione.Items.Insert(0, item)
        ddlRegione.Items(1).Selected = False
        ddlRegione.Items(0).Selected = True
    End Sub

    Protected Sub ddlRegione_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRegione.SelectedIndexChanged

        _strFilter = "where regione = '" & ddlRegione.SelectedValue & "'"
        Session("strFilter") = _strFilter
        bindElencoAgenzie(_strFilter)

    End Sub

    Protected Sub ddlProvincia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvincia.SelectedIndexChanged

        _strFilter = "where sigla = '" & ddlProvincia.SelectedValue & "'"
        Session("strFilter") = _strFilter
        bindElencoAgenzie(_strFilter)

    End Sub

    Protected Sub ddlComune_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlComune.SelectedIndexChanged
        _strFilter = "where comune = '" & ddlComune.SelectedValue & "'"
        Session("strFilter") = _strFilter

        bindElencoAgenzie(_strFilter)

    End Sub

    Protected Sub ddlOrdinamento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlOrdinamento.SelectedIndexChanged
        bindElencoAgenzie(_strFilter)
    End Sub

End Class