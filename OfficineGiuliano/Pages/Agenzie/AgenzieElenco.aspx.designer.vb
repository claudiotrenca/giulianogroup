﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AgenzieElenco1
    
    '''<summary>
    '''Controllo Path.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents Path As Global.Emmemedia.Controls.Common.Path
    
    '''<summary>
    '''Controllo ltCorpoDoc.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ltCorpoDoc As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Controllo phCenter.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents phCenter As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Controllo ddlOrdinamento.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ddlOrdinamento As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Controllo ddlRegione.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ddlRegione As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Controllo ddlProvincia.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ddlProvincia As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Controllo ddlComune.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents ddlComune As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Controllo lvAgenzie.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents lvAgenzie As Global.System.Web.UI.WebControls.ListView
    
    '''<summary>
    '''Controllo OfferteRicercaBoxVerticale.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents OfferteRicercaBoxVerticale As Global.Emmemedia.Controls.Offerte.OfferteRicercaBoxVerticale
    
    '''<summary>
    '''Controllo SqlDataSource1.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents SqlDataSource1 As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Controllo SqlDataSource2.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents SqlDataSource2 As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Controllo SqlDataSource3.
    '''</summary>
    '''<remarks>
    '''Campo generato automaticamente.
    '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
    '''</remarks>
    Protected WithEvents SqlDataSource3 As Global.System.Web.UI.WebControls.SqlDataSource
End Class
