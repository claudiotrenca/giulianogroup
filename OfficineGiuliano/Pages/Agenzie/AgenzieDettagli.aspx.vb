﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail

Public Class AgenzieDettagli
    Inherits BaseClass.SitePage
    Dim _IdAgenzia As Integer
    Dim _strFilter As String = String.Empty

    Protected _Zona As String
    Protected _idzona As String

    Private Sub AgenzieDettagli_Init(sender As Object, e As EventArgs) Handles Me.Init

        _IdAgenzia = Page.RouteData.Values("id").ToString

        If Page.RouteData.Values("rif") IsNot Nothing Then
            findZonaFromId()
            _strFilter = "where comune = '" & _Zona & "' and pk_codice <> " & _IdAgenzia
        Else
            _strFilter = "where provincia = '" & Page.RouteData.Values("provincia") & "'"
        End If

        bindDettagliAgenzia()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Session("StartRowIndex") = Nothing

            Session("strFilter") = _strFilter
        Else
            _strFilter = Session("strFilter")
        End If

        If Session("StartRowIndex") IsNot Nothing Then
            bindElencoAgenzie(_strFilter, Session("StartRowIndex"))
        Else
            bindElencoAgenzie(_strFilter)
        End If

    End Sub

    Private Sub findZonaFromId()

        Using con As New SqlConnection(ConfigurationManager.ConnectionStrings("accesso").ConnectionString)
            Using cmd As New SqlCommand
                cmd.Connection = con
                cmd.CommandType = CommandType.Text
                cmd.CommandText = "select comune, id_comune from vwAgenzie  WHERE PK_CODICE = " & _IdAgenzia

                con.Open()
                Dim dt As New DataTable
                dt.Load(cmd.ExecuteReader)
                con.Close()

                If dt.Rows.Count > 0 Then
                    Dim dr As DataRow = dt.Rows(0)
                    _Zona = dr("comune")
                End If
            End Using
        End Using

    End Sub

    Private Sub bindDettagliAgenzia()

        Using con As New SqlConnection(ConfigurationManager.ConnectionStrings("accesso").ConnectionString)
            Using cmd As New SqlCommand
                cmd.Connection = con
                cmd.CommandType = CommandType.Text
                cmd.CommandText = "SELECT * FROM vwAgenzie WHERE PK_CODICE = " & _IdAgenzia
                con.Open()

                Dim dt As New DataTable
                dt.Load(cmd.ExecuteReader)

                con.Close()

                If dt.Rows.Count > 0 Then
                    Dim dr As DataRow = dt.Rows(0)
                    AgenziaNome.Text = dr("Agenzia").ToString.ToLower
                    AgenziaSocieta.Text = dr("Societa").ToString.ToLower
                    AgenziaCap.Text = dr("cap").ToString.ToLower
                    AgenziaCell.Text = dr("telefono").ToString.ToLower
                    AgenziaComune.Text = dr("localita").ToString.ToLower
                    AgenziaComune2.Text = dr("localita").ToString.ToLower
                    AgenziaFax.Text = dr("fax").ToString.ToLower
                    AgenziaIndirizzo.Text = dr("indirizzo").ToString.ToLower
                    AgenziaIndirizzo2.Text = dr("indirizzo").ToString.ToLower
                    AgenziaMail.Text = dr("email").ToString.ToLower
                    AgenziaPiva.Text = dr("partitaiva").ToString.ToLower
                    AgenziaProvincia.Text = dr("provincia").ToString.ToLower
                    AgenziaReferente.Text = dr("responsabile").ToString.ToLower
                    AgenziaTel.Text = dr("telefono").ToString.ToLower

                End If

            End Using
        End Using

    End Sub

#Region " ELENCO AGENZIE "
    Private Sub bindElencoAgenzie(ByVal _str As String, Optional ByVal startrecord As Integer = 0)

        Dim sql As String
        Dim idm As String = ConfigurationManager.AppSettings("id_marchant")

        sql = "SELECT pk_codice, agenzia , indirizzo, regione, provincia , sigla , comune , localita FROM vwAgenzie " & _str & " order by agenzia ASC "

        Using con As New SqlConnection(ConfigurationManager.ConnectionStrings("accesso").ConnectionString)
            Using cmd As New SqlCommand
                cmd.Connection = con
                cmd.CommandType = CommandType.Text
                cmd.CommandText = sql
                con.Open()

                Dim dt As New DataTable
                dt.Load(cmd.ExecuteReader)

                con.Close()

                lvAgenzie.DataSource = dt
                lvAgenzie.DataBind()

                If startrecord <> 0 Then
                    Dim dp As DataPager = lvAgenzie.FindControl("dpAgenzie")

                    If dp IsNot Nothing Then
                        dp.SetPageProperties(startrecord, dp.MaximumRows, False)
                        lvAgenzie.DataBind()

                    End If
                End If

            End Using

        End Using
    End Sub

    Protected Sub lvAgenzie_ItemCreated(sender As Object, e As ListViewItemEventArgs) Handles lvAgenzie.ItemCreated
        If e.Item.ItemType = ListViewItemType.DataItem Then

            Dim dataItem As ListViewDataItem = DirectCast(e.Item, ListViewDataItem)
            Dim drv As DataRowView = DirectCast(dataItem.DataItem, DataRowView)

        End If
    End Sub

    Protected Sub lvAgenzie_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvAgenzie.ItemDataBound

        Dim li As ListViewItem = e.Item

        If li.ItemType = ListViewItemType.DataItem Then

            Dim di As ListViewDataItem = DirectCast(li, ListViewDataItem)
            Dim dr As System.Data.DataRowView = DirectCast(di.DataItem, System.Data.DataRowView)

            Using lnk As HyperLink = DirectCast(e.Item.FindControl("LINK"), HyperLink)

                lnk.NavigateUrl = "/agenzie-di-viaggi/" & Classi.Utility.StringToUrl(dr("regione").ToString.Trim.ToLower) & "/" & StringToUrl(dr("provincia").ToString.Trim.ToLower) & "/" & StringToUrl(dr("comune").ToString.Trim.ToLower) & "/" & StringToUrl(dr("agenzia").ToString.Trim.ToLower) & "_id" & dr("pk_codice").ToString.Trim & ".htm"

            End Using

        End If
    End Sub

    Protected Sub lvAgenzie_PagePropertiesChanging(sender As Object, e As PagePropertiesChangingEventArgs) Handles lvAgenzie.PagePropertiesChanging

        Dim dp As DataPager
        dp = lvAgenzie.FindControl("dpAgenzie")
        Session("StartRowIndex") = e.StartRowIndex
        dp.SetPageProperties(e.StartRowIndex, e.MaximumRows, False)

        bindElencoAgenzie(Session("strFilter"), Session("StartRowIndex"))

    End Sub

#End Region
    Public Shared Function StringToUrl(ByVal _string As String) As String
        Return str_to_url(_string)
    End Function

    Public Shared Function str_to_url(value As String) As String

        Dim ret As String = value

        ret = Replace(ret, " ", "-")
        ret = Replace(ret, ".", "-")
        ret = Replace(ret, ",", "-")
        ret = Replace(ret, """", "-")
        ret = Replace(ret, ":", "-")
        ret = Replace(ret, ";", "-")
        ret = Replace(ret, "*", "-")
        ret = Replace(ret, "'", "-")
        ret = Replace(ret, "?", "-")
        ret = Replace(ret, "è", "e")
        ret = Replace(ret, "ù", "u")
        ret = Replace(ret, "ò", "o")
        ret = Replace(ret, "à", "a")
        ret = Replace(ret, "#", "-")
        ret = Replace(ret, "*", "-")
        ret = Replace(ret, "&", "-")
        ret = Replace(ret, "?", "-")
        ret = Replace(ret, "!", "-")
        ret = Replace(ret, "(", "-")
        ret = Replace(ret, ")", "-")
        ret = Replace(ret, "=", "-")
        ret = Replace(ret, "$", "-")
        ret = Replace(ret, "%", "-")
        ret = Replace(ret, "€", "-")
        ret = Replace(ret, "[", "-")
        ret = Replace(ret, "]", "-")
        ret = Replace(ret, "@", "-")

        If ret <> "" Then
            Return (ret.ToLower)
        Else
            Return "***errore***"
        End If
    End Function
    Private Sub richInfo_Click(sender As Object, e As EventArgs) Handles richInfo.Click

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "none", "<script>$('#myModal').modal('show');</script>", False)

    End Sub
    Private Sub btnInvia_Click(sender As Object, e As EventArgs) Handles btnInvia.Click

        If txtMail.Text = "" Then
            js_esegui("javascript:alert('campo email obbligatorio');")
        ElseIf txtNome.Text = "" Then
            js_esegui("javascript:alert('campo Cognome e Nome obbligatorio');")
        Else

            Dim strBODY As String
            Dim prodotto As String = ""

            strBODY = "<strong>Richiesta Informazioni  </strong><br /><br /><br />" &
                         "<strong>Cognome e Nome : </strong>" & Replace(txtNome.Text, "'", "''") & " <br />" &
                         "<strong>E-Mail : </strong>" & Replace(txtMail.Text, "'", "''") & " <br />" &
                         "<strong>Numero di Telefono(1) : </strong>" & Replace(txtTel1.Text, "'", "''") & " <br />" &
                         "<strong>Numero di Telefono(2) : </strong>" & Replace(txtTel2.Text, "'", "''") & " <br />" &
                         "<strong>Richiesta : </strong>" & Replace(txtNote.Text, "'", "''")

            If txtMail.Text <> "" Then
                If INSERT_MAIL_FORM(Now(), ConfigurationManager.AppSettings("mail_contatto"), ConfigurationManager.AppSettings("mail_contatto"), Replace(txtMail.Text, "'", "''"), "Richiesta Informazioni " & ConfigurationManager.AppSettings("nome_sito"), strBODY, ConfigurationManager.AppSettings("nome_sito")) Then
                    js_esegui("javascript:alert('<p> Richiesta informazioni inviata correttamente </p>';")
                Else
                    js_esegui("javascript:alert('<p> Errore nell'invio della email ! </p>';")
                End If
            Else
                js_esegui("javascript:alert('<p> attenzione : inserire una mail valida  !</p>';")
            End If

        End If

    End Sub

    Private Sub js_esegui(value As String)

        Dim csname1 As String = "PopupScript"
        Dim csname2 As String = "ButtonClickScript"
        Dim cstype As Type = Me.GetType()
        Dim cs As ClientScriptManager = Page.ClientScript

        If (Not cs.IsStartupScriptRegistered(cstype, csname1)) Then
            Dim cstext1 As String = value
            cs.RegisterStartupScript(cstype, csname1, cstext1, True)
        End If

    End Sub

    Public Shared Function INSERT_MAIL_FORM(ByVal DATA As String, ByVal SITO_FROM As String, ByVal SITO_TO As String, ByVal FROM As String, ByVal OGGETTO As String, ByVal TESTO As String, Optional ByVal nome_sito As String = "Contatti Sito", Optional ByVal idm As String = "", Optional ByVal scrivi As Boolean = True) As Boolean

        INVIA_EMAIL(FROM, SITO_TO, OGGETTO, TESTO, nome_sito)
        INVIA_EMAIL(FROM, SITO_FROM, OGGETTO, TESTO, nome_sito)
        INVIA_EMAIL(SITO_FROM, FROM, OGGETTO, TESTO, nome_sito)

        Return True
    End Function

    Public Shared Function TRASFORMA_DATA(value As String) As String
        Return "CONVERT(DATETIME, '" & Mid(value, 7, 4) & "-" & Mid(value, 4, 2) & "-" & Mid(value, 1, 2) & " 00:00:00', 102)"
    End Function

    Public Shared Function INVIA_EMAIL(ByVal send_from As String, ByVal send_to As String, ByVal mess_obj As String, ByVal msg_body As String, nome_visualizzato As String) As Boolean

        Dim messaggio As MailMessage = New MailMessage()
        messaggio.From = New MailAddress(send_from, nome_visualizzato)
        messaggio.To.Add(New MailAddress(send_to, nome_visualizzato))

        messaggio.Subject = mess_obj
        messaggio.SubjectEncoding = System.Text.Encoding.UTF8
        messaggio.Body = msg_body
        messaggio.BodyEncoding = System.Text.Encoding.UTF8
        messaggio.IsBodyHtml = True

        Try
            Dim msg As SmtpClient = New SmtpClient()
            msg.Send(messaggio)

            Return True
        Catch
            Return False
        End Try

    End Function

End Class