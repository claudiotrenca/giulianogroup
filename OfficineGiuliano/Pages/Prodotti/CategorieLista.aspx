﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CategorieLista.aspx.vb" Inherits="Emmemedia.Pages.Prodotti.CategorieLista" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<%@ Register Assembly="UnorderedListDataPager" Namespace="UnorderedListDataPager.CustomControls" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPh" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="Server">
    <asp:Literal runat="Server" ID="SliderContent" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentUpperPh" runat="Server">
    <uc1:Path runat="server" ID="Path" />
    <asp:PlaceHolder ID="UpperPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="UpperContent" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMainPh" runat="server">
    <asp:Literal runat="Server" ID="MainContent" />

    <div class="lista-prodotti module-short">
        <div class="container-fluid">
            <asp:ListView ID="lvProdotti" runat="server" DataKeyNames="pk">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                </LayoutTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="lnkProdotto" runat="server">
                        <article class="col-sm-4 col-xs-6">
                            <asp:Image ID="ProdottoImmagine" runat="server" class="img-responsive" />
                            <div class="row">
                                <div class="col-xs-offset-1 col-xs-10">
                                    <h2 class="h2">
                                        <asp:Literal ID="ltCategoria" runat="server"></asp:Literal></h2>
                                    <p>
                                        <asp:Literal ID="ltDescrizione" runat="server"></asp:Literal>
                                    </p>
                                    <span class="btn btn-blue btn-block">Guarda i prodotti</span>
                                </div>
                            </div>
                        </article>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentFooterPh" runat="server">
    <asp:Literal runat="Server" ID="FooterContent" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="FooterPh" runat="server">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Hai aggiunto questo articolo nel carrello</h4>
                </div>
                <div class="modal-body">
                    <img id="ModalProductImage" class="img-responsive" runat="server" />
                    <div class="prodottoNome">
                        <h5>
                            <asp:Literal ID="ModalProductName" runat="server"></asp:Literal></h5>
                        <p class="codiceProd">
                            <asp:Literal ID="ModalProductCode" runat="server"></asp:Literal>
                        </p>
                    </div>
                </div>
                <div class="prezzoCartModal">

                    <p class="prezzoOk">
                        <asp:Literal ID="ModalProductPrice" runat="server"></asp:Literal>
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default tornaShop" data-dismiss="modal"><i class="fa fa-chevron-circle-left"></i>Torna allo shopping</button>
                    <a href="/carrello/" class="btn btn-primary goCart">Vai al carrello<i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>