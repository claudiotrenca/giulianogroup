﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ProdottiDettagli.aspx.vb" Inherits="Emmemedia.Pages.Prodotti.ProdottiDettagli" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPh" runat="server">
    <script type="text/javascript">
        function getConfirmation(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="server">
    <div class="slide-top">
        <div class="bg-slide-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4 text-center">
                        <h3 class="h3">Noleggio
                        </h3>
                        <p>
                            <asp:Literal ID="ProdottoCategoria" runat="server"></asp:Literal>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentUpperPh" runat="server">
    <uc1:Path runat="server" ID="Path" />
    <asp:Literal runat="Server" ID="UpperContent" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMainPh" runat="server">
    <asp:Literal runat="Server" ID="MainContent" />
    <!-- Scheda -->
    <div class="wrap">
        <div class="scheda-prodotto">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-md-6 gallery">
                        <asp:ListView ID="lvFotogallery" runat="server">
                            <LayoutTemplate>
                                <div class="sp-wrap">
                                    <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <a href="<%#  Eval("Percorso") %>">
                                    <img src="<%#  Eval("Percorso") %>" alt="<%# Eval("Descrizione") %>">
                                </a>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                    <div class="col-lg-6 col-md-6 info">
                        <h2 class="h2">
                            <asp:Literal ID="ProdottoModello2" runat="server"></asp:Literal></h2>

                        <p class="rif">
                            <asp:Literal ID="ProdottoFamiglia" runat="server"></asp:Literal>
                        </p>
                        <div class="descrizione-cont">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading activeState" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <span class="ion ion-ios-minus-outline"></span>DESCRIZIONE DEL PRODOTTO
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <asp:Literal ID="ProdottoDescrizioneBreve" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <span class="ion ion-ios-plus-outline"></span>Vantaggi per il cliente
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            <asp:Literal ID="ProdottoDescrizioneEstesa" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                <span class="ion ion-ios-plus-outline"></span>SCHEDA TECNICA
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            <asp:Literal ID="ProdottoCaratteristiche" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cont-btns">
                            <div class="row">
                                <div class="col-md-6">
                                    <a id="form-richiesta-info" href="#" class="btn btn-01 btn-block">richiedi informazioni <i class="ion ion-chevron-right"></i>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <asp:HyperLink ID="ProdottoSchedaTecnica" runat="server" CssClass="btn btn-02 btn-block">scarica catalogo <i class="ion ion-chevron-right"></i></asp:HyperLink>
                                </div>
                            </div>
                        </div>
                        <div class="form-richiesta-info">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Nome</label>
                                        <asp:TextBox runat="server" ID="FormNome" placeholder="Nome" CssClass="form-control" ValidationGroup="MiniForm" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="* Campo Richiesto" ForeColor="Red" ControlToValidate="FormNome" EnableClientScript="true" ValidationGroup="MiniForm" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <asp:TextBox runat="server" ID="FormEmail" placeholder="Email" CssClass="form-control" ValidationGroup="MiniForm" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Campo Richiesto" ForeColor="Red" ControlToValidate="FormEmail" EnableClientScript="true" ValidationGroup="MiniForm" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>Città</label>
                                        <asp:TextBox runat="server" ID="FormCitta" placeholder="Email" CssClass="form-control" ValidationGroup="MiniForm" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="* Campo Richiesto" ForeColor="Red" ControlToValidate="FormCitta" EnableClientScript="true" ValidationGroup="MiniForm" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>Messaggio</label>
                                        <asp:TextBox runat="server" ID="FormText" TextMode="MultiLine" Rows="5" placeholder="Lascia un tuo messaggio" CssClass="form-control textAreaHeight" />
                                    </div>
                                    <asp:LinkButton runat="server" ID="FormInvia" CssClass="btn btn-block" ValidationGroup="MiniForm" CausesValidation="true">Invia <i class="ion ion-chevron-right"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentFooterPh" runat="server">
    <asp:Literal runat="Server" ID="FooterContent" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="FooterScriptsPh" runat="server">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Lightgallery -->
</asp:Content>