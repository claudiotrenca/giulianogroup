﻿Imports System.Net.Mail

Namespace Pages

    Namespace Prodotti

        Public Class ProdottiDettagli
            Inherits BaseClass.SitePage

            Protected _Prodotto As DataClass.Products
            Private Categoria As New DataClass.Menu

            Private _PK As Integer
            Private _Categoria As String
            Private _Macro As String
            Private _CodiceMacro As Integer
            Private _CodiceCategoria As Integer

            Private Sub ProdottiDettagli_Init(sender As Object, e As EventArgs) Handles Me.Init

                _PK = IIf(Page.RouteData.Values("PK") <> Nothing, Page.RouteData.Values("PK"), Nothing)
                _Macro = IIf(Page.RouteData.Values("macro") <> Nothing, Page.RouteData.Values("macro"), String.Empty)
                _Categoria = IIf(Page.RouteData.Values("categoria") <> Nothing, Page.RouteData.Values("categoria"), String.Empty)
                _Provincia = IIf(Page.RouteData.Values("provincia") <> Nothing, Page.RouteData.Values("provincia"), ConfigurationManager.AppSettings("default_prov"))

                _CodiceMacro = DataClass.Menu.GetCategoryIdFromLink(_Macro)

                If _Categoria <> String.Empty Then
                    _CodiceCategoria = DataClass.Menu.GetCategoryIdFromLink(_Categoria, _CodiceMacro)
                End If

                If _PK <> Nothing Then

                End If

            End Sub

            Private Sub ProdottiDettagli_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete

            End Sub

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                If Not IsPostBack Then
                    ViewState("pk") = _PK
                Else
                    _PK = ViewState("pk")
                End If

                InitProduct()
                BindGallery()
                BindNavigation()

            End Sub

            Private Sub ProdottiDettagli_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

            End Sub

            Private Sub InitProduct()

                _Prodotto = DataClass.vwProdotti.GetDetail(_PK)

                If _Prodotto IsNot Nothing Then

                    '	ProdottoModello1.Text = _Prodotto.Modello
                    ProdottoCategoria.Text = _Prodotto.Categoria
                    ProdottoModello2.Text = _Prodotto.Modello
                    ProdottoFamiglia.Text = _Prodotto.Famiglia
                    ProdottoDescrizioneBreve.Text = _Prodotto.DescrizioneBreve
                    ProdottoDescrizioneEstesa.Text = _Prodotto.DescrizioneEstesa
                    ProdottoCaratteristiche.Text = _Prodotto.DescrizioneCaratteristiche
                    If String.IsNullOrEmpty(_Prodotto.Allegato) Then
                        ProdottoSchedaTecnica.Visible = False
                    Else
                        ProdottoSchedaTecnica.NavigateUrl = "/public/doc/product/" & _Prodotto.Allegato
                        ProdottoSchedaTecnica.Target = "_blank"

                    End If

                    'If Session("IDUtente") Is Nothing Then
                    '    ProdottoPrezzo.Text = FormatCurrency(_Prodotto.Prezzo, 2)
                    'Else
                    '    ProdottoPrezzo.Text = FormatCurrency(_Prodotto.PrezzoRivenditore1, 2)
                    'End If

                    InitMeta()
                End If

            End Sub

            Private Sub BindGallery()
                Dim gallery1 As New List(Of DataClass.Fotogallery)
                Dim gallery2 As New List(Of DataClass.Fotogallery)
                Dim result As New List(Of DataClass.Fotogallery)
                Dim query1 As New List(Of DataClass.ProdottiGallery)
                Dim query2 As New List(Of DataClass.Prodotti)

                Using context As New DataClass.DataEntities
                    query1 = (From row In context.ProdottiGallery Where row.FkProdotto = _PK Select row).ToList
                    gallery1 = query1.AsEnumerable().Select(Of DataClass.Fotogallery)(Function(p) New DataClass.Fotogallery() With {.Descrizione = p.Descrizione, .Percorso = "/public/images/product/Gallery/" & p.PercorsoImmagine}).ToList
                    query2 = (From row In context.Prodotti Where row.PK = _PK Select row).ToList
                    gallery2 = query2.AsEnumerable().Select(Of DataClass.Fotogallery)(Function(p) New DataClass.Fotogallery() With {.Descrizione = p.Modello, .Percorso = "/public/images/product/" & p.ImmagineProdotto}).ToList
                End Using

                result = (gallery2.Concat(gallery1)).ToList

                lvFotogallery.DataSource = result
                lvFotogallery.DataBind()
            End Sub

            Private Sub BindNavigation()

                Dim query1 As New DataClass.vwProdotti
                Dim query2 As New DataClass.vwProdotti

                Using context As New DataClass.DataEntities
                    query1 = (From row In context.vwProdotti Where row.PK > _PK And row.FkCategoria = _CodiceCategoria Select row).FirstOrDefault
                    query2 = (From row In context.vwProdotti Where row.PK < _PK And row.FkCategoria = _CodiceCategoria Select row).FirstOrDefault
                End Using

                'If query1 IsNot Nothing Then
                '	lnkProdottoSuccessivo.NavigateUrl = "/" & query1.MacroLink & "/" & query1.CategoriaLink & "/" & _Provincia & "/" & Classi.Utility.StringToUrl(query1.Modello) & "_" & query1.PK & ".htm"
                'Else
                '	lnkProdottoSuccessivo.NavigateUrl = "#"
                '	lnkProdottoSuccessivo.Enabled = False
                'End If

                'If query2 IsNot Nothing Then
                '	lnkProdottoPrecedente.NavigateUrl = "/" & query2.MacroLink & "/" & query2.CategoriaLink & "/" & _Provincia & "/" & Classi.Utility.StringToUrl(query2.Modello) & "_" & query2.PK & ".htm"
                'Else
                '	lnkProdottoPrecedente.NavigateUrl = "#"
                '	lnkProdottoPrecedente.Enabled = False
                'End If

                'lnkProdottoCategoria.NavigateUrl = "/" & _Prodotto.MacroLink & "/" & _Prodotto.CategoriaLink & "/" & _Provincia & "/"

            End Sub

            Private Function ValorizeTags(ByVal _str As String) As String

                If Not String.IsNullOrEmpty(_str) Then

                    If _str.Contains("@provincia") Then
                        _str = _str.Replace("@provincia", Page.RouteData.Values("provincia"))
                    End If

                    If _str.Contains("@macro") Then
                        _str = _str.Replace("@macro", _Macro)
                    End If

                    If _str.Contains("@categoria") Then
                        _str = _str.Replace("@categoria", _Categoria)
                    End If
                Else
                    _str = ""
                End If

                Return _str

            End Function

            Private Sub InitContent()

                _Content = DataClass.vwPost.GetDetailFromMenu(_CodiceCategoria)

                If _Content IsNot Nothing Then
                    _PageTitolo = ValorizeTags(_Content.Titolo)
                    _PageSottotitolo = ValorizeTags(_Content.SottoTitolo)
                    _PageSezione = ValorizeTags(_Content.Sezione)
                    _PageContent = ValorizeTags(_Content.Contenuto)
                    _PageFooterContent = ValorizeTags(_Content.ContenutoInferiore)
                    _PageUpperContent = ValorizeTags(_Content.ContenutoSuperiore)
                    _PageContenutoBreve = ValorizeTags(_Content.ContenutoBreve)

                    InitMeta()
                End If

                MainContent.Text = _PageContent
                UpperContent.Text = _PageUpperContent
                FooterContent.Text = _PageFooterContent

            End Sub

            Private Sub InitMeta()
                Dim MetaPage As New MetaClass

                Using context As New DataClass.DataEntities
                    Dim row = context.ProdottiMeta.SingleOrDefault(Function(p) p.FKProdotto = _PK)
                    If row IsNot Nothing Then
                        MetaPage.MetaTitle = row.ProductTitle
                        MetaPage.MetaDescription = row.ProductDescription
                        MetaPage.MetaKeyword = row.ProductKeywords
                    Else
                        MetaPage = Nothing
                    End If
                End Using
                If MetaPage IsNot Nothing Then
                    Me.Master.Meta = MetaPage
                    Me.Master.InitMetaTag()
                End If

            End Sub

            Private Function GetCustomerID() As String
                If Context.User.Identity.Name <> "" Then
                    Return Context.User.Identity.Name
                Else
                    If Session("AnonUID") Is Nothing Then
                        Session("AnonUID") = Guid.NewGuid()
                    End If
                    Return Session("AnonUID").ToString()
                End If
            End Function

            Private Sub InviaMail(sender As Object, e As EventArgs) Handles FormInvia.Click
                If Page.IsValid Then

                    Dim sb As New StringBuilder
                    sb.Append("<strong>Richiesta Informazioni " & _Prodotto.Modello & "</strong><br /><br /><br />")
                    sb.Append("<strong>Nominativo : </strong>" & Replace(FormNome.Text, "'", "''") & " <br />")
                    sb.Append("<strong>E-Mail : </strong>" & Replace(FormEmail.Text, "'", "''") & " <br />")
                    sb.Append("<strong>Località : </strong>" & Replace(FormCitta.Text, "'", "''") & " <br />")
                    sb.Append("<strong>Richiesta : </strong>" & Replace(FormText.Text, "'", "''") & " <br />")
                    sb.Append("<br /><br />")

                    DataClass.MailContact.Insert(FormEmail.Text, "Richiesta Informazioni " & _Prodotto.Modello, sb.ToString)

                    InviaMail(ConfigurationManager.AppSettings("mail_da_invio"), ConfigurationManager.AppSettings("mail_contatto"), "richiesta informazioni" & _Prodotto.Modello, sb.ToString)

                    sb.Clear()

                    sb.Append("<strong>Officine Giuliano</strong><br /><br /><br />")
                    sb.Append("<p>Grazie per averci contattato! Il nostro staff ti contatterà il prima possibile. </p>")
                    sb.Append("<br /><br />")

                    InviaMail(ConfigurationManager.AppSettings("mail_da_invio"), FormEmail.Text, "richiesta informazioni" & _Prodotto.Modello, sb.ToString)

                    Response.Redirect(ConfigurationManager.AppSettings("ThankYouOrdinePath"))

                End If

            End Sub

            Private Function InviaMail(ByVal _from As String, ByVal _to As String, ByVal _object As String, ByVal _body As String) As Boolean
                Try

                    Dim messaggio As MailMessage = New MailMessage()
                    messaggio.From = New MailAddress(_from, _from)
                    messaggio.To.Add(New MailAddress(_to, _to))
                    messaggio.Subject = _object
                    messaggio.SubjectEncoding = System.Text.Encoding.UTF8
                    messaggio.Body = _body
                    messaggio.BodyEncoding = System.Text.Encoding.UTF8
                    messaggio.IsBodyHtml = True
                    Dim msg As SmtpClient = New SmtpClient()
                    msg.Send(messaggio)

                    Return True
                Catch
                    Return False
                End Try
            End Function

        End Class

    End Namespace

End Namespace