﻿Namespace Pages

    Namespace Prodotti

        Public Class ProdottiLista
            Inherits BaseClass.SitePage

            Protected _Prodotti As List(Of DataClass.Products)
            Protected _NumeroArticoli As Integer = 1000
            Private Categoria As New DataClass.Menu

            Private _Categoria As String
            Private _Macro As String
            Private _SubCategoria As String

            Private _CodiceMacro As Integer = 0
            Private _CodiceCategoria As Integer = 0
            Private _CodiceSubCategoria As Integer = 0

            Private _sortExpression As String = "pk"
            Private _SortDirection As String = "Desc"
            Private _PageNumber As Integer
            Private _PageSize As Integer = ConfigurationManager.AppSettings("NumeroArticoli")
            Private _RecordNumber As Integer
            Private _StartRecord As Integer

            Private Sub ProdottiLista_Init(sender As Object, e As EventArgs) Handles Me.Init

                _Macro = IIf(Page.RouteData.Values("macro") <> Nothing, Page.RouteData.Values("macro"), String.Empty)
                _Categoria = IIf(Page.RouteData.Values("categoria") <> Nothing, Page.RouteData.Values("categoria"), String.Empty)
                _Provincia = IIf(Page.RouteData.Values("provincia") <> Nothing, Page.RouteData.Values("provincia"), ConfigurationManager.AppSettings("default_prov"))
                _SubCategoria = IIf(Page.RouteData.Values("SubCategoria") <> Nothing, Page.RouteData.Values("SubCategoria"), String.Empty)

                _CodiceMacro = DataClass.Menu.GetCategoryIdFromLink(_Macro)

                If _Categoria <> String.Empty Then
                    _CodiceCategoria = DataClass.Menu.GetCategoryIdFromLink(_Categoria, _CodiceMacro)
                End If

                If _SubCategoria <> String.Empty Then
                    _CodiceSubCategoria = DataClass.Menu.GetCategoryIdFromLink(_SubCategoria, _CodiceCategoria)
                End If

                If Session("_PageSize") IsNot Nothing Then
                    _PageSize = Session("_PageSize")
                End If

                If Page.RouteData.Values("Pagina") IsNot Nothing Then
                    _PageNumber = Convert.ToInt32(Page.RouteData.Values("Pagina"))
                Else
                    _PageNumber = 1
                End If

                _StartRecord = (_PageNumber - 1) * _PageSize


            End Sub

            Private Sub ProdottiLista_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete

            End Sub

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                bindProduct()
            End Sub

            Private Sub ProdottiLista_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
                InitContent()

                Me.DataBind()
            End Sub

            Private Sub lvProdotti_PreRender(sender As Object, e As EventArgs) Handles lvProdotti.PreRender

                If dpBottom IsNot Nothing Then
                    dpBottom.SetPageProperties(_StartRecord, _PageSize, False)
                End If

                lvProdotti.DataBind()
            End Sub

            Protected Sub lvProdotti_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvProdotti.ItemCommand

                'Dim currentItem As ListViewDataItem = e.Item
                'Dim currentDataKey As DataKey = Me.lvProdotti.DataKeys(currentItem.DataItemIndex)
                'Dim currentValue As Integer = currentDataKey.Value

                Dim lw As ListView = DirectCast(sender, ListView)
                Dim li As ListViewItem = e.Item

                'If e.CommandName = "AddCart" Then

                '    '   Dim cart As New CartDB(ConfigurationManager.AppSettings("id_marchant"))

                '    Dim _CodiceArticolo As String = DirectCast(li.FindControl("ProductCode"), HiddenField).Value
                '    Dim _Modello As String = DirectCast(li.FindControl("ProductName"), HiddenField).Value
                '    Dim _Prezzo As String = DirectCast(li.FindControl("ProductPrice"), HiddenField).Value

                '    ' Dim _PrezzoOrig As String = DirectCast(li.FindControl("ProductListPrice"), Literal).Text
                '    Dim _PathImmagine As String = DirectCast(li.FindControl("ProductImage"), Image).ImageUrl

                '    Dim iva_s As Double = ConnectionClass.leggi("select IVA_CORRENTE from _SISTEMA ", ConnectionClass.cnDBSql, New DataTable).Rows(0)(0)

                '    If cart.Adds_CartItem(GetCustomerID(), e.CommandArgument, _CodiceArticolo, _Modello, _Modello, _Prezzo, 0, "", iva_s) = "OK" Then

                '        ModalProductCode.Text = _CodiceArticolo
                '        ModalProductImage.Src = _PathImmagine
                '        '        ModalProductListPrice.Text = _PrezzoOrig
                '        ModalProductName.Text = _Modello
                '        ModalProductPrice.Text = FormatCurrency(_Prezzo, 2)

                '        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "none", "<script>debugger;$(document).ready(function() {$('#myModal').modal('show');  });</script>", False)
                '    End If

                '    bindProduct()

                'End If

            End Sub

            Protected Sub lvProdotti_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvProdotti.ItemDataBound

                Dim dti As DataClass.Products = TryCast(e.Item.DataItem, DataClass.Products)

                If e.Item.ItemType = ListViewItemType.DataItem Then

                    Dim ltModello As Literal = DirectCast(e.Item.FindControl("ProdottoModello"), WebControls.Literal)
                    Dim ltCategoria As Literal = DirectCast(e.Item.FindControl("ProdottoCategoria"), WebControls.Literal)
                    Dim ltProdottoDescrizioneBreve As Literal = DirectCast(e.Item.FindControl("ProdottoDescrizioneBreve"), Literal)
                    Dim ProdottoImmagine As WebControls.Image = DirectCast(e.Item.FindControl("ProdottoImmagine"), WebControls.Image)
                    Dim lnkProdotto As HyperLink = DirectCast(e.Item.FindControl("lnkProdotto"), HyperLink)
                    'ProdottoDescrizioneBreve

                    If ltModello IsNot Nothing Then ltModello.Text = dti.Modello
                    If ltProdottoDescrizioneBreve IsNot Nothing Then ltProdottoDescrizioneBreve.Text = dti.DescrizioneBreve
                    If ltCategoria IsNot Nothing Then ltCategoria.Text = Classi.Utility.Capitalize(dti.Categoria)
                    'If ltPrezzo IsNot Nothing Then
                    '    If Session("IDUtente") Is Nothing Then
                    '        ltPrezzo.Text = FormatCurrency(dti.Prezzo, 2)
                    '    Else
                    '        ltPrezzo.Text = FormatCurrency(dti.PrezzoRivenditore1, 2)
                    '    End If
                    'End If
                    If lnkProdotto IsNot Nothing Then
                        lnkProdotto.NavigateUrl = "/" & dti.MacroLink & "/" & dti.CategoriaLink & "/" & dti.SubCategoriaLink & "/" & Classi.Utility.StringToUrl(dti.Modello) & "_" & dti.PkProdotto & ".htm" ' "/" & _Provincia &
                    End If

                    If ProdottoImmagine IsNot Nothing Then
                        If dti.ImmagineGrande <> "" Then
                            ProdottoImmagine.ImageUrl = "/public/images/product/" & dti.ImmagineGrande
                        Else
                            ProdottoImmagine.ImageUrl = "/images/menu-prodotti1.png"
                        End If
                        ProdottoImmagine.AlternateText = dti.Modello
                    End If

                    '  hidProductPrice.Value = dti.PrezzoRivenditore2 / Session("iva")

                End If

            End Sub

            Protected Sub lvProdotti_PagePropertiesChanging(sender As Object, e As PagePropertiesChangingEventArgs) Handles lvProdotti.PagePropertiesChanging
                dpBottom.SetPageProperties(_StartRecord, _PageSize, False)
            End Sub

            Protected Sub lvProdotti_DataBound(sender As Object, e As EventArgs) Handles lvProdotti.DataBound
                FixPagerURLs(dpBottom)
            End Sub

            Private Sub InitContent()

                Dim _PkContent As Integer

                If _CodiceSubCategoria <> 0 Then
                    _PkContent = DataClass.PostMenuMapping.GetPostIdFromMenu(_CodiceSubCategoria)
                ElseIf _CodiceCategoria <> 0 Then
                    _PkContent = DataClass.PostMenuMapping.GetPostIdFromMenu(_CodiceCategoria)
                ElseIf _CodiceMacro <> 0 Then
                    _PkContent = DataClass.PostMenuMapping.GetPostIdFromMenu(_CodiceMacro)
                End If

                _Content = DataClass.vwPost.GetDetail(_PkContent)

                If _Content IsNot Nothing Then

                    If _Content.Slider <> "" Then
                        pnlSlider.Visible = True
                        pnlStatic.Visible = False
                        SliderContent.Text = _Content.Slider
                    Else
                        pnlSlider.Visible = False
                        pnlStatic.Visible = True
                    End If

                    _PageTitolo = _Content.Titolo
                    _PageSottotitolo = _Content.SottoTitolo
                    _PageSezione = _Content.Sezione
                    _PageContent = _Content.Contenuto
                    _PageFooterContent = _Content.ContenutoInferiore
                    _PageUpperContent = _Content.ContenutoSuperiore
                    _PageContenutoBreve = _Content.ContenutoBreve

                    BindContent()
                Else

                    _Content = New DataClass.vwPost
                    Dim MenuMeta As New DataClass.Menu

                    If _SubCategoria <> String.Empty Then
                        MenuMeta = DataClass.Menu.GetDetail(_CodiceSubCategoria)

                    ElseIf _Categoria <> String.Empty Then
                        MenuMeta = DataClass.Menu.GetDetail(_CodiceCategoria)
                    Else
                        MenuMeta = DataClass.Menu.GetDetail(_CodiceMacro)
                    End If

                    _Content.Titolo = Utilities.P_LETTER(MenuMeta.Descrizione)
                    _Content.MetaTitle = MenuMeta.Descrizione & " - " & ConfigurationManager.AppSettings("nome_sito_v")
                    _Content.MetaDescription = MenuMeta.Descrizione
                    _Content.MetaKeywords = MenuMeta.Descrizione

                End If

                InitMeta()

            End Sub

            Private Sub BindContent()

                MainContent.Text = _PageContent
                UpperContent.Text = _PageUpperContent
                FooterContent.Text = _PageFooterContent

            End Sub

            Private Sub InitMeta()

                Dim MetaPage As New MetaClass
                MetaPage.MetaTitle = _Content.MetaTitle
                MetaPage.MetaDescription = _Content.MetaDescription
                MetaPage.MetaKeyword = _Content.MetaKeywords
                MetaPage.MetaCanonical = _Content.MetaCanonical

                Me.Master.Meta = MetaPage
                Me.Master.InitMetaTag()

            End Sub

            Private Sub bindProduct(Optional ByVal _startrecord As Integer = 0)

                dpBottom.PageSize = _PageSize

                If _CodiceSubCategoria <> 0 Then
                    _Prodotti = DataClass.vwProdotti.GetListFromSubCategory(_CodiceSubCategoria, _startrecord, _NumeroArticoli, _sortExpression, _SortDirection)
                ElseIf _CodiceCategoria <> 0 Then
                    _Prodotti = DataClass.vwProdotti.GetListFromCategory(_CodiceCategoria, _startrecord, _NumeroArticoli, _sortExpression, _SortDirection)

                ElseIf _CodiceMacro <> 0 Then
                    _Prodotti = DataClass.vwProdotti.GetListFromMacro(_CodiceMacro, _startrecord, _NumeroArticoli, _sortExpression, _SortDirection)
                End If

                lvProdotti.DataSource = _Prodotti
                lvProdotti.DataBind()

            End Sub

            Private Sub FixPagerURLs(ByVal Pager As DataPager)

                For Each Pag As DataPagerFieldItem In Pager.Controls
                    For Each Ctrl In Pag.Controls
                        If TypeOf Ctrl Is HyperLink Then
                            Dim Hyp As HyperLink = Ctrl
                            Dim NumberPage As String

                            Dim Req = Hyp.NavigateUrl.Replace(Request.Path, "").Replace("?", "")
                            Dim Params = Req.Split("&")
                            Dim NewPage = (From item In Params Where item.StartsWith("page")).FirstOrDefault

                            If NewPage IsNot Nothing Then
                                NumberPage = NewPage.Substring(NewPage.IndexOf("=") + 1)
                            Else
                                NumberPage = "1"
                            End If

                            Dim URL = Request.RawUrl
                            If URL.Contains("?") Then
                                URL = URL.Substring(0, URL.IndexOf("?"))
                            End If

                            If Page.RouteData.Values("Pagina") = "1" Then
                                URL = URL.Substring(0, URL.LastIndexOf("/") + 1)
                            Else
                                URL = URL.Substring(0, URL.Substring(0, URL.Length - 1).LastIndexOf("/") + 1)
                            End If

                            Dim QueryStr As String() = Request.RawUrl.Split("?")
                            Dim newUrl As String

                            If NewPage = "page=1" Then
                                newUrl = URL
                            Else
                                newUrl = String.Concat(URL, NumberPage, "/")
                            End If

                            If QueryStr.GetLength(0) > 1 AndAlso QueryStr(1) IsNot Nothing Then
                                If QueryStr(1) <> "" Then
                                    Hyp.NavigateUrl = String.Concat(newUrl, "?", QueryStr(1))
                                End If
                            Else
                                Hyp.NavigateUrl = newUrl
                            End If

                        End If
                    Next
                Next
            End Sub

            Private Function GetCustomerID() As String

                If Context.User.Identity.Name <> "" Then
                    Return Context.User.Identity.Name
                Else
                    If Session("AnonUID") Is Nothing Then
                        Session("AnonUID") = Guid.NewGuid()
                    End If
                    Return Session("AnonUID").ToString()
                End If

            End Function

            Private Function ValorizeTags(ByVal _str As String) As String

                If Not String.IsNullOrEmpty(_str) Then

                    If _str.Contains("@provincia") Then
                        _str = _str.Replace("@provincia", Page.RouteData.Values("provincia"))
                    End If

                    If _str.Contains("@macro") Then
                        _str = _str.Replace("@macro", _Macro)
                    End If

                    If _str.Contains("@categoria") Then
                        _str = _str.Replace("@categoria", _Categoria)
                    End If
                Else
                    _str = ""
                End If

                Return _str

            End Function

        End Class

    End Namespace

End Namespace