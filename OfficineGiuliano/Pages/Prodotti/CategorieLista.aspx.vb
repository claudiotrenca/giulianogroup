﻿Namespace Pages

    Namespace Prodotti
        Public Class CategorieLista
            Inherits BaseClass.SitePage

            Protected _Prodotti As List(Of DataClass.Products)
            Protected _NumeroArticoli As Integer = 1000
            Protected _Menu As New DataClass.Menu

            Private _Categoria As String
            Private _Macro As String
            Private _CodiceMacro As Integer
            Private _CodiceCategoria As Integer

            Private _sortExpression As String = "pk"
            Private _SortDirection As String = "Desc"
            Private _PageNumber As Integer
            Private _PageSize As Integer = 100
            Private _RecordNumber As Integer
            Private _StartRecord As Integer

            Private Sub ProdottiLista_Init(sender As Object, e As EventArgs) Handles Me.Init

                _Macro = IIf(Page.RouteData.Values("macro") <> Nothing, Page.RouteData.Values("macro"), String.Empty)
                _Provincia = IIf(Page.RouteData.Values("provincia") <> Nothing, Page.RouteData.Values("provincia"), ConfigurationManager.AppSettings("default_prov"))

                _CodiceMacro = DataClass.Menu.GetCategoryIdFromLink(_Macro)

                Dim tipo As String = IIf(Request.QueryString("eta") <> Nothing, Request.QueryString("eta"), String.Empty)

                If tipo <> String.Empty Then
                    Server.Transfer("/pages/prodotti/ProdottiLista.aspx", True)
                End If

                If Session("_PageSize") IsNot Nothing Then
                    _PageSize = Session("_PageSize")
                End If

                If Page.RouteData.Values("Pagina") IsNot Nothing Then
                    _PageNumber = Convert.ToInt32(Page.RouteData.Values("Pagina"))
                Else
                    _PageNumber = 1
                End If

                _StartRecord = (_PageNumber - 1) * _PageSize

            End Sub

            Private Sub ProdottiLista_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete

            End Sub

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                InitContent()

                bindCategory2()
            End Sub

            Private Sub InitContent()

                _Content = DataClass.vwPost.GetDetailFromMenu(_CodiceMacro)

                If _Content IsNot Nothing Then

                    SliderContent.Text = _Content.Slider

                    _PageTitolo = _Content.Titolo
                    _PageSottotitolo = _Content.SottoTitolo
                    _PageSezione = _Content.Sezione
                    _PageContent = _Content.Contenuto
                    _PageFooterContent = _Content.ContenutoInferiore
                    _PageUpperContent = _Content.ContenutoSuperiore
                    _PageContenutoBreve = _Content.ContenutoBreve

                    BindContent()
                    InitMeta()
                End If
            End Sub

            Private Sub BindContent()

                MainContent.Text = _PageContent
                UpperContent.Text = _PageUpperContent
                FooterContent.Text = _PageFooterContent

            End Sub

            Private Sub InitMeta()
                Dim MetaPage As New MetaClass
                MetaPage.MetaTitle = _Content.MetaTitle
                MetaPage.MetaDescription = _Content.MetaDescription
                MetaPage.MetaKeyword = _Content.MetaKeywords
                MetaPage.MetaCanonical = _Content.MetaCanonical

                Me.Master.Meta = MetaPage
                Me.Master.InitMetaTag()
            End Sub

            Private Sub bindCategory(Optional ByVal _startrecord As Integer = 0)

                Dim lCategorie As New List(Of DataClass.ListaCategorie)

                Using context As New DataClass.DataEntities

                    Dim queryResult = (From v In context.vwMenu
                                       Where v.ParentID = _CodiceMacro
                                       Select v.PK, v.Descrizione, v.DescrizioneEstesa, v.Link, v.Macro, v.Indice, immagineprodotto = (
                                 ((From p In context.vwProdotti
                                   Where
                                   Not p.ImmagineProdotto Is Nothing And
                                   p.FkCategoria = v.PK
                                   Select New With {
                                   p.ImmagineProdotto
                                 }).OrderBy(Function(x) Guid.NewGuid()).Take(1).FirstOrDefault().ImmagineProdotto)))

                    lCategorie = queryResult.AsEnumerable().Select(Of DataClass.ListaCategorie)(Function(p) New DataClass.ListaCategorie() With {
                                                                                                                                            .PK = p.PK,
                                                                                                                                            .Descrizione = p.Descrizione,
                                                                                                                                            .DescrizioneEstesa = p.DescrizioneEstesa,
                                                                                                                                            .Link = p.Link,
                                                                                                                                            .Macro = p.Macro,
                                                                                                                                            .Indice = p.Indice,
                                                                                                                                            .ImmagineProdotto = p.immagineprodotto
                                                                                                                                         }).ToList

                End Using

                lvProdotti.DataSource = lCategorie
                lvProdotti.DataBind()

            End Sub

            Private Sub bindCategory2(Optional ByVal _startrecord As Integer = 0)

                Dim lCategorie As New List(Of DataClass.ListaCategorie)

                Using context As New DataClass.DataEntities

                    'Dim queryResult = (From v In context.vwMenu
                    '                   Where v.ParentID = _CodiceMacro
                    '                   Select v.PK, v.Descrizione, v.DescrizioneEstesa, v.Link, v.Macro, v.Indice, immagineprodotto = (
                    '             ((From p In context.vwMenu
                    '               Where p.PK = v.FkMenu
                    '               Select New With {
                    '               p.Immagine
                    '             }).OrderBy(Function(x) Guid.NewGuid()).Take(1).FirstOrDefault().Immagine)))

                    Dim queryResult = (From v In context.vwMenu
                                       Where v.ParentID = _CodiceMacro
                                       Select v.PK, v.Descrizione, v.Link, v.Macro, v.Indice, ImmagineProd =
                                 (From p In context.vwMenu
                                  Where p.PK = v.FkMenu
                                  Select p.Immagine).FirstOrDefault, descrizioneEst = (From p In context.vwMenu
                                                                                       Where p.PK = v.FkMenu
                                                                                       Select p.DescrizioneEstesa).FirstOrDefault
                                 )

                    lCategorie = queryResult.AsEnumerable().Select(Of DataClass.ListaCategorie)(Function(p) New DataClass.ListaCategorie() With {
                                                                                                                                            .PK = p.PK,
                                                                                                                                            .Descrizione = p.Descrizione,
                                                                                                                                            .DescrizioneEstesa = p.descrizioneEst,
                                                                                                                                            .Link = p.Link,
                                                                                                                                            .Macro = p.Macro,
                                                                                                                                            .Indice = p.Indice,
                                                                                                                                            .ImmagineProdotto = p.ImmagineProd
                                                                                                                                         }).ToList

                End Using

                lvProdotti.DataSource = lCategorie
                lvProdotti.DataBind()

            End Sub

            Protected Sub lvProdotti_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvProdotti.ItemCommand

                'Dim currentItem As ListViewDataItem = e.Item
                'Dim currentDataKey As DataKey = Me.lvProdotti.DataKeys(currentItem.DataItemIndex)
                'Dim currentValue As Integer = currentDataKey.Value

                Dim lw As ListView = DirectCast(sender, ListView)
                Dim li As ListViewItem = e.Item

                'If e.CommandName = "AddCart" Then

                '    '   Dim cart As New CartDB(ConfigurationManager.AppSettings("id_marchant"))

                '    Dim _CodiceArticolo As String = DirectCast(li.FindControl("ProductCode"), HiddenField).Value
                '    Dim _Modello As String = DirectCast(li.FindControl("ProductName"), HiddenField).Value
                '    Dim _Prezzo As String = DirectCast(li.FindControl("ProductPrice"), HiddenField).Value

                '    ' Dim _PrezzoOrig As String = DirectCast(li.FindControl("ProductListPrice"), Literal).Text
                '    Dim _PathImmagine As String = DirectCast(li.FindControl("ProductImage"), Image).ImageUrl

                '    Dim iva_s As Double = ConnectionClass.leggi("select IVA_CORRENTE from _SISTEMA ", ConnectionClass.cnDBSql, New DataTable).Rows(0)(0)

                '    If cart.Adds_CartItem(GetCustomerID(), e.CommandArgument, _CodiceArticolo, _Modello, _Modello, _Prezzo, 0, "", iva_s) = "OK" Then

                '        ModalProductCode.Text = _CodiceArticolo
                '        ModalProductImage.Src = _PathImmagine
                '        '        ModalProductListPrice.Text = _PrezzoOrig
                '        ModalProductName.Text = _Modello
                '        ModalProductPrice.Text = FormatCurrency(_Prezzo, 2)

                '        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "none", "<script>debugger;$(document).ready(function() {$('#myModal').modal('show');  });</script>", False)
                '    End If

                '    bindProduct()

                'End If

            End Sub

            Protected Sub lvProdotti_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvProdotti.ItemDataBound

                Dim dti As DataClass.ListaCategorie = TryCast(e.Item.DataItem, DataClass.ListaCategorie)

                If e.Item.ItemType = ListViewItemType.DataItem Then

                    Dim ltCategoria As Literal = DirectCast(e.Item.FindControl("ltCategoria"), WebControls.Literal)
                    Dim ltDescrizione As Literal = DirectCast(e.Item.FindControl("ltDescrizione"), WebControls.Literal)
                    Dim ltPrezzo As Literal = DirectCast(e.Item.FindControl("ProdottoPrezzo"), Literal)
                    Dim ProdottoImmagine As WebControls.Image = DirectCast(e.Item.FindControl("ProdottoImmagine"), WebControls.Image)
                    Dim lnkProdotto As HyperLink = DirectCast(e.Item.FindControl("lnkProdotto"), HyperLink)

                    If ltCategoria IsNot Nothing Then ltCategoria.Text = dti.Descrizione

                    If ltDescrizione IsNot Nothing Then ltDescrizione.Text = dti.DescrizioneEstesa

                    If lnkProdotto IsNot Nothing Then
                        lnkProdotto.NavigateUrl = "/" & _Macro & "/" & dti.Link & "/" '& _Provincia & "/"
                    End If

                    If ProdottoImmagine IsNot Nothing Then
                        If dti.ImmagineProdotto <> "" Then
                            ProdottoImmagine.ImageUrl = "/public/images/menu/" & dti.ImmagineProdotto
                        Else
                            ProdottoImmagine.ImageUrl = "/images/menu-prodotti1.png"
                        End If
                        ProdottoImmagine.AlternateText = dti.Descrizione
                    End If

                    '  hidProductPrice.Value = dti.PrezzoRivenditore2 / Session("iva")

                End If

            End Sub

            Private Function GetCustomerID() As String

                If Context.User.Identity.Name <> "" Then
                    Return Context.User.Identity.Name
                Else
                    If Session("AnonUID") Is Nothing Then
                        Session("AnonUID") = Guid.NewGuid()
                    End If
                    Return Session("AnonUID").ToString()
                End If

            End Function

            Private Function ValorizeTags(ByVal _str As String) As String

                If Not String.IsNullOrEmpty(_str) Then

                    If _str.Contains("@provincia") Then
                        _str = _str.Replace("@provincia", Page.RouteData.Values("provincia"))
                    End If

                    If _str.Contains("@macro") Then
                        _str = _str.Replace("@macro", _Macro)
                    End If

                    If _str.Contains("@categoria") Then
                        _str = _str.Replace("@categoria", _Categoria)
                    End If

                Else
                    _str = ""
                End If

                Return _str

            End Function

        End Class
    End Namespace

End Namespace