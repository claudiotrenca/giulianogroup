﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Pages.Prodotti
    
    Partial Public Class ProdottiDettagli
        
        '''<summary>
        '''Controllo ProdottoCategoria.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoCategoria As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo Path.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents Path As Global.Emmemedia.Controls.Common.Path
        
        '''<summary>
        '''Controllo UpperContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UpperContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo MainContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MainContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo lvFotogallery.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents lvFotogallery As Global.System.Web.UI.WebControls.ListView
        
        '''<summary>
        '''Controllo ProdottoModello2.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoModello2 As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoFamiglia.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoFamiglia As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoDescrizioneBreve.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoDescrizioneBreve As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoDescrizioneEstesa.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoDescrizioneEstesa As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoCaratteristiche.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoCaratteristiche As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoSchedaTecnica.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoSchedaTecnica As Global.System.Web.UI.WebControls.HyperLink
        
        '''<summary>
        '''Controllo FormNome.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormNome As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo RequiredFieldValidator2.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents RequiredFieldValidator2 As Global.System.Web.UI.WebControls.RequiredFieldValidator
        
        '''<summary>
        '''Controllo FormEmail.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormEmail As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo RequiredFieldValidator1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents RequiredFieldValidator1 As Global.System.Web.UI.WebControls.RequiredFieldValidator
        
        '''<summary>
        '''Controllo FormCitta.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormCitta As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo RequiredFieldValidator3.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents RequiredFieldValidator3 As Global.System.Web.UI.WebControls.RequiredFieldValidator
        
        '''<summary>
        '''Controllo FormText.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormText As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo FormInvia.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormInvia As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Controllo FooterContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FooterContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.SiteMaster
            Get
                Return CType(MyBase.Master,Emmemedia.SiteMaster)
            End Get
        End Property
    End Class
End Namespace
