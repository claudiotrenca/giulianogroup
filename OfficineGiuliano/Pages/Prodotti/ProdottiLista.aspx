﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ProdottiLista.aspx.vb" Inherits="Emmemedia.Pages.Prodotti.ProdottiLista" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<%@ Register Assembly="UnorderedListDataPager" Namespace="UnorderedListDataPager.CustomControls" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>
<%@ Register Src="~/Controls/Prodotti/MenuCategorie.ascx" TagPrefix="uc1" TagName="MenuCategorie" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPh" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="Server">
    <asp:Panel ID="pnlSlider" runat="server">
        <asp:Literal runat="Server" ID="SliderContent" />
    </asp:Panel>
    <asp:Panel ID="pnlStatic" runat="server">
        <div class="slide-top">
            <div class="bg-slide-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4 text-center">
                            <h1 class="h3"><%# _Content.Titolo %>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentUpperPh" runat="Server">
    <uc1:Path runat="server" ID="Path" />
    <asp:PlaceHolder ID="UpperPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="UpperContent" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMainPh" runat="server">
    <asp:Literal runat="Server" ID="MainContent" />

    <div class="wrap">
        <div class="prodotti-noleggio">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 navigation">
                        <uc1:MenuCategorie runat="server" ID="MenuCategorie" IdGruppoMenu="2" />
                    </div>
                    <div class="col-md-9">
                        <asp:ListView ID="lvProdotti" runat="server" DataKeyNames="PkProdotto">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 prod">
                                    <div class="border-box">
                                        <div class="col-height">
                                            <asp:Image ID="ProdottoImmagine" runat="server" CssClass="img-responsive" />
                                        </div>

                                        <h3 class="h3">
                                            <asp:Literal ID="ProdottoModello" runat="server"></asp:Literal>
                                        </h3>
                                        <h5 class="h5">
                                            <asp:Literal ID="ProdottoCategoria" runat="server"></asp:Literal>
                                        </h5>
                                        <p>
                                            <asp:Literal ID="ProdottoDescrizioneBreve" runat="server"></asp:Literal>
                                        </p>
                                        <hr>
                                        <asp:HyperLink ID="lnkProdotto" runat="server" CssClass="btn btn-block">
                                        scopri ora <i class="ion ion-chevron-right"></i>
                                        </asp:HyperLink>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
        </div>
        <div class="pag">
            <div class="container-fluid">
                <nav>
                    <cc1:UnorderedListDataPager ID="dpBottom" runat="server" class="pagination" PagedControlID="lvProdotti" EnableViewState="true" QueryStringField="page">
                        <Fields>
                            <asp:NextPreviousPagerField ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Link" RenderNonBreakingSpacesBetweenControls="false" FirstPageText="&laquo;&laquo;" PreviousPageText="&laquo;" />
                            <asp:NumericPagerField ButtonType="Link" RenderNonBreakingSpacesBetweenControls="true" CurrentPageLabelCssClass="btn disabled active" ButtonCount="10" />
                            <asp:NextPreviousPagerField ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Link" RenderNonBreakingSpacesBetweenControls="false" LastPageText="&raquo;&raquo;" NextPageText="&raquo;" />
                        </Fields>
                    </cc1:UnorderedListDataPager>
                </nav>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentFooterPh" runat="server">
    <asp:Literal runat="Server" ID="FooterContent" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="FooterScriptsPh" runat="server">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Hai aggiunto questo articolo nel carrello</h4>
                </div>
                <div class="modal-body">
                    <img id="ModalProductImage" class="img-responsive" runat="server" />
                    <div class="prodottoNome">
                        <h5>
                            <asp:Literal ID="ModalProductName" runat="server"></asp:Literal></h5>
                        <p class="codiceProd">
                            <asp:Literal ID="ModalProductCode" runat="server"></asp:Literal>
                        </p>
                    </div>
                </div>
                <div class="prezzoCartModal">

                    <p class="prezzoOk">
                        <asp:Literal ID="ModalProductPrice" runat="server"></asp:Literal>
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default tornaShop" data-dismiss="modal"><i class="fa fa-chevron-circle-left"></i>Torna allo shopping</button>
                    <a href="/carrello/" class="btn btn-primary goCart">Vai al carrello<i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>