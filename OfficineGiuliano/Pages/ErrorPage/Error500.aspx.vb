﻿Namespace ErrorClasses
    Public Class Error500
        Inherits ErrorClasses.SiteErrorPage


        Private Sub Error500_Init(sender As Object, e As EventArgs) Handles Me.Init

        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            _httpStatusCode = Int32.Parse(StatusCode.Value)
            Response.StatusCode = _httpStatusCode

            If Not IsPostBack Then
                Form.Action = Request.Url.PathAndQuery

                If _httpStatusCode = 404 Then
                    CrashReportTitle.Text = "Page Not Found"
                    CrashReportBody.Text = [String].Empty

                    ReportTitle.Value = "Page Not Found"
                    ReportBody.Value = [String].Empty
                Else
                    ReportTitle.Value = "Unhandled Exception"
                    ReportBody.Value = [String].Empty

                    Dim report = TryCast(HttpContext.Current.Cache(ApplicationErrorModule.Settings.Names.CrashReportKey), CrashReport)
                    If report Is Nothing Then
                        Dim ex As Exception = If(HttpContext.Current.Server.GetLastError(), New HttpUnhandledException("Server Error"))
                        report = New CrashReport(ex)
                    End If

                    CrashReportTitle.Text = report.Title
                    CrashReportBody.Text = report.Body

                    ReportTitle.Value = report.Title
                    ReportBody.Value = report.Body

                    NextSteps.EnableTechnicalInformation = True
                End If

                BrokenUrl.Text = GetBrokenUrl()
            Else
                ' If this is a postback then we're OK -- the user is submitting a quick question from the error page.
                '  Response.StatusCode = 200
            End If
        End Sub

        Private Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
            MyBase.SendQuickErrorReport(BrokenUrl.Text, GetUserName(), Request.Browser, ReportTitle.Value, ReportBody.Value)

            RegisterThankYouScript()

            ErrorReportPanel.Visible = False

            Response.StatusCode = 200
        End Sub

        Protected Sub LinkButton1_Click1(sender As Object, e As EventArgs)
            MyBase.SendQuickErrorReport(BrokenUrl.Text, GetUserName(), Request.Browser, ReportTitle.Value, ReportBody.Value)

            RegisterThankYouScript()

            ErrorReportPanel.Visible = False

            Response.StatusCode = 200
        End Sub

        Private Sub Error500_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
            If Not IsPostBack Then
                Form.Action = Request.Url.PathAndQuery
            End If
        End Sub
    End Class
End Namespace