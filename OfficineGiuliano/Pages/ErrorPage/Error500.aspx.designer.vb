﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ErrorClasses
    
    Partial Public Class Error500
        
        '''<summary>
        '''Controllo StatusCode.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents StatusCode As Global.System.Web.UI.WebControls.HiddenField
        
        '''<summary>
        '''Controllo ReportTitle.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ReportTitle As Global.System.Web.UI.WebControls.HiddenField
        
        '''<summary>
        '''Controllo ReportBody.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ReportBody As Global.System.Web.UI.WebControls.HiddenField
        
        '''<summary>
        '''Controllo CrashReportTitle.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents CrashReportTitle As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo BrokenUrl.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents BrokenUrl As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo NextSteps.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents NextSteps As Global.Emmemedia.ErrorClasses.NextStepsLinkList
        
        '''<summary>
        '''Controllo ErrorReportPanel.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ErrorReportPanel As Global.System.Web.UI.WebControls.Panel
        
        '''<summary>
        '''Controllo LinkButton1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents LinkButton1 As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Controllo SubmitQuickErrorReportButton.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents SubmitQuickErrorReportButton As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Controllo CrashReportBody.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents CrashReportBody As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.SiteMaster
            Get
                Return CType(MyBase.Master,Emmemedia.SiteMaster)
            End Get
        End Property
    End Class
End Namespace
