﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Error500.aspx.vb" Inherits="Emmemedia.ErrorClasses.Error500" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPh" runat="server">
    <script type="text/javascript">
        //<![CDATA[
        var theForm = document.forms['Form1'];
        if (!theForm) {
            theForm = document.Form1;
        }
        function doPostBack(eventTarget, eventArgument) {
            if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                theForm.__EVENTTARGET.value = eventTarget;
                theForm.__EVENTARGUMENT.value = eventArgument;
                theForm.submit();
            }
        }
        //]]>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentUpperPh" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMainPh" runat="server">
    <asp:HiddenField runat="server" ID="StatusCode" Value="500" />
    <asp:HiddenField runat="server" ID="ReportTitle" />
    <asp:HiddenField runat="server" ID="ReportBody" />

    <h1>
        <asp:Literal runat="server" ID="CrashReportTitle" /></h1>

    <p>
        <asp:Literal runat="server" ID="BrokenUrl" />
    </p>

    <p>
        Spiecenti, la pagina richiesta ha generato un errore!
    </p>

    <err:NextStepsLinkList runat="server" ID="NextSteps" />

    <asp:Panel runat="server" ID="ErrorReportPanel">

        <h2>Quick Error Report
        </h2>

        <p>
            Puoi segnalare l&#39;errore cliccando sul pulsante qui sotto.
        </p>
        <p>
            Dopo aver cliccato la pagina verrà ricaricata e ci verrà inviato un messaggio con i dati dell&#39;errore
        </p>
        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click1">LinkButton</asp:LinkButton>
        <asp:LinkButton runat="server" ID="SubmitQuickErrorReportButton" Text="Invia la segnalazione dell'errore" CssClass="btn btn-default" />
        <input id="Button1" type="button" class="btn btn-default" value="button" onclick="javascript: doPostBack('Button1', '');" />
        <br />
    </asp:Panel>
    <div id="TechnicalInformation" style="display: none; margin-top: 40px;">
        <h1>Technical Information</h1>
        <asp:Literal runat="server" ID="CrashReportBody" />
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentFooterPh" runat="server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterScriptsPh" runat="server">
    <script lang='javascript'>
        function ToggleTechnicalInformationDisplay() {
            var t = document.getElementById('TechnicalInformation');
            if (t.style.display == 'block') t.style.display = 'none';
            else t.style.display = 'block';
        }
    </script>
</asp:Content>