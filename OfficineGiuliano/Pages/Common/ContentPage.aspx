﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ContentPage.aspx.vb" Inherits="Emmemedia.Pages.Common.ContentPage" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>
<%@ Register Src="~/Controls/Common/Content.ascx" TagPrefix="uc1" TagName="Content" %>
<%@ Register Src="~/Controls/Common/ContentSideLeft.ascx" TagPrefix="uc1" TagName="ContentSideLeft" %>
<%@ Register Src="~/Controls/Common/ContentSideRight.ascx" TagPrefix="uc1" TagName="ContentSideRight" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="Server">
    <asp:Literal runat="Server" ID="SliderContent" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentUpperPh" runat="Server">
    <uc1:Path runat="server" ID="Path" />
    <asp:PlaceHolder ID="UpperPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="UpperContent" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainPh" runat="Server">
    <asp:PlaceHolder ID="LeftPh" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="MainPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="MainContent" />
    <asp:PlaceHolder ID="RightPh" runat="server"></asp:PlaceHolder>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentFooterPh" runat="Server">
    <asp:PlaceHolder ID="FooterPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="FooterContent" />
</asp:Content>