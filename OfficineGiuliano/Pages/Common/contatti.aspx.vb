﻿Imports System.Drawing
Imports System.Net.Mail

Namespace Pages

    Namespace Common

        Public Class contatti
            Inherits BaseClass.SitePage

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            End Sub

            Private Sub ContentPage_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

                InitContent()

            End Sub

            Private Sub InitContent()

                Dim _PkContent As Integer = DataClass.PostMenuMapping.GetPostIdFromMenu(Me.Master.Rif)

                _Content = DataClass.vwPost.GetDetail(_PkContent)

                If _Content IsNot Nothing Then

                    SliderContent.Text = _Content.Slider

                    _PageTitolo = _Content.Titolo
                    _PageSottotitolo = _Content.SottoTitolo
                    _PageSezione = _Content.Sezione
                    _PageContent = _Content.Contenuto
                    _PageFooterContent = _Content.ContenutoInferiore
                    _PageUpperContent = _Content.ContenutoSuperiore
                    _PageContenutoBreve = _Content.ContenutoBreve

                    InitMeta()
                End If

            End Sub

            Private Sub BindContent()

                MainContent.Text = _PageContent
                UpperContent.Text = _PageUpperContent
                FooterContent.Text = _PageFooterContent

            End Sub

            Private Sub InitMeta()
                Dim MetaPage As New MetaClass
                MetaPage.MetaTitle = _Content.MetaTitle
                MetaPage.MetaDescription = _Content.MetaDescription
                MetaPage.MetaKeyword = _Content.MetaKeywords
                MetaPage.MetaCanonical = _Content.MetaCanonical

                Me.Master.Meta = MetaPage
                Me.Master.InitMetaTag()
            End Sub

            Private Sub btnINVIA_Click(sender As Object, e As EventArgs) Handles btnINVIA2.Click
                If FormAgree1.Checked Then

                    If Page.IsValid Then
                        Dim sb As New StringBuilder
                        sb.Append("<strong>Richiesta Informazioni</strong><br /><br /><br />")
                        sb.Append("<strong>Nominativo : </strong>" & Replace(FormNome.Text, "'", "''") & " <br />")
                        sb.Append("<strong>Azienda : </strong>" & Replace(FormAzienda.Text, "'", "''") & " <br />")
                        sb.Append("<strong>Partita IVA : </strong>" & Replace(FormPiva.Text, "'", "''") & " <br />")
                        sb.Append("<strong>Città : </strong>" & Replace(FormCitta.Text, "'", "''") & " <br />")
                        sb.Append("<strong>E-Mail : </strong>" & Replace(FormMail.Text, "'", "''") & " <br />")
                        sb.Append("<strong>Telefono : </strong>" & Replace(FormTelefono.Text, "'", "''") & " <br />")
                        sb.Append("<strong>Richiesta : </strong>" & Replace(FormTesto.Text, "'", "''") & " <br />")
                        sb.Append("<br /><br />")

                        DataClass.MailContact.Insert(FormMail.Text, "Nuova richiesta di contatto", sb.ToString)

                        Using context As New DataClass.DataEntities

                            Dim row = context.NewsLetterSubscription.FirstOrDefault(Function(p) p.Email = FormMail.Text.Trim)
                            Try
                                If row Is Nothing Then
                                    Dim NewRow As New DataClass.NewsLetterSubscription
                                    NewRow.DataInserimento = Date.Now
                                    NewRow.DataModifica = Date.Now
                                    NewRow.Attivo = True
                                    NewRow.Email = FormMail.Text.Trim

                                    context.Configuration.AutoDetectChangesEnabled = True
                                    context.NewsLetterSubscription.Attach(NewRow)
                                    context.Entry(NewRow).State = Entity.EntityState.Added
                                    context.SaveChanges()
                                Else
                                    row.DataModifica = Date.Now
                                    row.Attivo = True
                                    context.SaveChanges()
                                End If
                            Catch ex As Exception
                                Throw ex
                            End Try
                        End Using

                        InviaMail(ConfigurationManager.AppSettings("mail_da_invio"), ConfigurationManager.AppSettings("mail_contatto"), "richiesta informazioni", sb.ToString)

                        sb.Clear()

                        sb.Append("<strong>Officine Giuliano</strong><br /><br /><br />")
                        sb.Append("<p>Grazie per averci contattato! Il nostro staff ti contatterà il prima possibile. </p>")
                        sb.Append("<br /><br />")

                        InviaMail(ConfigurationManager.AppSettings("mail_da_invio"), FormMail.Text, "richiesta informazioni", sb.ToString)

                        Response.Redirect(ConfigurationManager.AppSettings("ThankYouContattiPath"))
                    End If
                Else 
                    LabelMessage.Text = "Per proseguire è necessario autorizzare il trattamento dei dati."
                    LabelMessage.ForeColor = Drawing.Color.red
                End If

            End Sub

            Private Function InviaMail(ByVal _from As String, ByVal _to As String, ByVal _object As String, ByVal _body As String) As Boolean
                Try
                    Dim messaggio As MailMessage = New MailMessage()
                    messaggio.From = New MailAddress(_from, _from)
                    messaggio.To.Add(New MailAddress(_to, _to))
                    messaggio.Subject = _object
                    messaggio.SubjectEncoding = System.Text.Encoding.UTF8
                    messaggio.Body = _body
                    messaggio.BodyEncoding = System.Text.Encoding.UTF8
                    messaggio.IsBodyHtml = True

                    Dim msg As SmtpClient = New SmtpClient()
                    msg.Send(messaggio)

                    Return True
                Catch
                    Return False
                End Try
            End Function

        End Class

    End Namespace

End Namespace