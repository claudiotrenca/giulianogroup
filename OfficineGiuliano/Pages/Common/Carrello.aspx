﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Carrello.aspx.vb" Inherits="Emmemedia.Pages.Carrello" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPh" runat="server">
    <script type="text/javascript">

        function getConfirmation(sender, title, message) {
            if (typeof (Page_ClientValidate) == 'function') {
                Page_ClientValidate();
            }

            if (Page_IsValid) {
                $("#spnTitle").text(title);
                $("#spnMsg").text(message);
                $('#modalPopUp').modal('show');
                $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);return true;");

                return false;
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentUpperPh" runat="server">
    <div class="container-fluid">
        <uc1:Path runat="server" ID="Path" />
        <asp:Literal runat="Server" ID="UpperContent" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="container-fluid">
        <asp:Literal runat="Server" ID="MainContent" />
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                <p class="recap-title">LA TUA LISTA <i class="ion-ios-heart-outline pull-right"></i></p>
                <div class="table-responsive">
                    <asp:ListView ID="lvCarrello" runat="server" EnableViewState="true" DataKeyNames="PK_CODICE">
                        <LayoutTemplate>
                            <div class="table-responsive">
                                <table class="table recap-table">
                                    <tbody>
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                                    </tbody>
                                </table>
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="hidProductCode" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "CODICE_PRODOTTO")%>' />
                            <asp:HiddenField ID="hidProductCartID" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "PK_CODICE", "{0:g}") %>' />
                            <tr>
                                <td width="20%">
                                    <asp:HyperLink ID="ProdottoLink1" runat="server">
                                        <asp:Image ID="ProdottoImmagine" runat="server" class="img-responsive" Width="140px" />
                                    </asp:HyperLink>
                                </td>
                                <td width="40%">
                                    <p class="sub-title">
                                        <asp:HyperLink ID="ProdottoLink2" runat="server"></asp:HyperLink>
                                    </p>
                                    <p>
                                        PREZZO DEL PRODOTTO A PARTIRE DA
                                        <asp:Literal ID="ProdottoPrezzo" runat="server"></asp:Literal>
                                    </p>
                                </td>

                                <td width="20%" class="hidden-xs">
                                    <p class="label-for-category">
                                        <asp:Literal ID="ProdottoCategoria" runat="server"></asp:Literal>
                                    </p>
                                </td>
                                <td width="10%">
                                    <asp:TextBox ID="ProdottoQuantita" runat="server" CssClass="form-control"></asp:TextBox>
                                </td>
                                <td width="10%" class="text-center" style="vertical-align: bottom">
                                    <asp:LinkButton ID="lkbElimina" runat="server" CommandName="Elimina" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PK_CODICE") %>' CausesValidation="false"><i class="ion-ios-trash md" style="margin-right: 20px"></i></asp:LinkButton>
                                    &nbsp;
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Aggiorna" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PK_CODICE") %>' CausesValidation="false"><i class="ion-ios-refresh-empty md"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <p class="sub-title text-center">
                                <br />
                                La tua lista è vuota...
                                <br />
                                <br />
                                <br />
                            </p>
                        </EmptyDataTemplate>
                    </asp:ListView>
                </div>
            </div>
        </div>
        <br />
        <br />
        <asp:Panel ID="FormRichiesta" runat="server">
            <div class="row modules">
                <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <p class="sub-title">INSERISCI I TUOI DATI</p>
                            <br />
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Nome e Cognome</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtNominativo" runat="server" CssClass="form-control custom-input" required></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="txtNominativo" Display="Dynamic">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control custom-input"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="txtEmail" Display="Dynamic">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Campo E-mail non corretto!"
                                            ForeColor="Red" Font-Size="11px" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ControlToValidate="txtEmail">
                                        </asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Città dell'Evento</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtCittaNoleggio" runat="server" CssClass="form-control custom-input"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="txtCittaNoleggio" Display="Dynamic">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Data dell'Evento</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtDataNoleggio" runat="server" CssClass="form-control custom-input text-center" placeholder="GG/MM/AAAA" TextMode="Date"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Campo Obbligatorio!"
                                            ForeColor="Red" Font-Size="11px" ControlToValidate="txtDataNoleggio" Display="Dynamic">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <asp:Panel ID="pnlNoleggio" runat="server">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 control-label">Specifica la consegna in Location o il ritiro in Sede</label>
                                        <div class="col-sm-8">
                                            <asp:RadioButtonList ID="rblConsegna" runat="server" CssClass="radio-inline" RepeatDirection="Vertical">
                                                <asp:ListItem Text="Consegna" Value="Consegna"></asp:ListItem>
                                                <asp:ListItem Text="Ritiro" Value="Ritiro"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                            <p class="sub-title">NOTE AGGIUNTIVE <span class="pull-right hidden-xs max-parole">Max 500 parole <i class="ion-compose"></i></span></p>
                            <br />
                            <asp:TextBox ID="txtInfo" runat="server" TextMode="MultiLine" Rows="8" placeholder="Inserisci qui delle tue note o personalizzazioni aggiuntive al tuo preventivo." CssClass="form-control custom-input"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <asp:LinkButton ID="btnInvia2" runat="server" Text="RICHIEDI PREVENTIVO" class="btn-primary" OnClientClick="return getConfirmation(this, 'Conferma invio richiesta informazioni','Sei sicuro di voler inviare?');" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentFooterPh" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="FooterScriptsPh" runat="server">
    <div id="modalPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="spnTitle"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span id="spnMsg"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ANNULLA</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>