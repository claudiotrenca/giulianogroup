﻿Imports System.Net.Mail

Namespace Pages

    Public Class Carrello
        Inherits BaseClass.SitePage

        Private _FlagNoleggio As Boolean = False
        Private _idUtente As String

        Private lista As List(Of DataClass.vwCarrello)

        Private Sub Carrello_Init(sender As Object, e As EventArgs) Handles Me.Init

        End Sub

        Private Sub Carrello_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad

        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            _idUtente = GetCustomerID()

            If Not IsPostBack Then
                BindCarello()
                CheckNoleggio()
            End If

        End Sub

        Private Sub BindCarello()

            _FlagNoleggio = False

            lista = DataClass.CartMethod.GetList(_idUtente)

            If lista.Count = 0 Then
                FormRichiesta.Visible = False
            End If

            lvCarrello.DataSource = lista
            lvCarrello.DataBind()

        End Sub

        Private Sub CheckNoleggio()
            If _FlagNoleggio = True Then
                pnlNoleggio.Visible = True
            Else
                pnlNoleggio.Visible = False
            End If
        End Sub

        Protected Sub lvCarrello_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvCarrello.ItemDataBound

            Dim dti As DataClass.vwCarrello = TryCast(e.Item.DataItem, DataClass.vwCarrello)

            If e.Item.ItemType = ListViewItemType.DataItem Then

                Dim ltModello As Literal = DirectCast(e.Item.FindControl("ProdottoModello"), WebControls.Literal)
                Dim ltCategoria As Literal = DirectCast(e.Item.FindControl("ProdottoCategoria"), WebControls.Literal)
                Dim ltPrezzo As Literal = DirectCast(e.Item.FindControl("ProdottoPrezzo"), Literal)
                Dim ProdottoImmagine As WebControls.Image = DirectCast(e.Item.FindControl("ProdottoImmagine"), WebControls.Image)
                Dim lnkProdotto1 As HyperLink = DirectCast(e.Item.FindControl("ProdottoLink1"), HyperLink)
                Dim lnkProdotto2 As HyperLink = DirectCast(e.Item.FindControl("ProdottoLink2"), HyperLink)
                Dim ProdottoQuantita As TextBox = DirectCast(e.Item.FindControl("ProdottoQuantita"), TextBox)

                If ltModello IsNot Nothing Then ltModello.Text = dti.Modello
                If ltCategoria IsNot Nothing Then ltCategoria.Text = Classi.Utility.Capitalize(dti.Categoria)
                If ltPrezzo IsNot Nothing Then ltPrezzo.Text = FormatCurrency(dti.PREZZO, 2)
                If ProdottoQuantita IsNot Nothing Then ProdottoQuantita.Text = dti.QTA

                If lnkProdotto1 IsNot Nothing Then
                    lnkProdotto1.NavigateUrl = "/" & dti.MacroLink & "/" & dti.CategoriaLink & "/" & _Provincia & "/" & Classi.Utility.StringToUrl(dti.Modello) & "_" & dti.FK_PRODOTTO & ".htm"
                End If
                If lnkProdotto2 IsNot Nothing Then
                    lnkProdotto2.NavigateUrl = "/" & dti.MacroLink & "/" & dti.CategoriaLink & "/" & _Provincia & "/" & Classi.Utility.StringToUrl(dti.Modello) & "_" & dti.FK_PRODOTTO & ".htm"
                    lnkProdotto2.Text = dti.Modello
                End If

                If ProdottoImmagine IsNot Nothing Then
                    ProdottoImmagine.AlternateText = dti.Modello

                    If dti.ImmagineProdotto <> "" Then
                        ProdottoImmagine.ImageUrl = "/public/images/product/" & dti.ImmagineProdotto
                    Else
                        ProdottoImmagine.ImageUrl = "/public/images/product/no_dispo.jpg"
                    End If
                End If

                If dti.FlagNoleggio = True Then
                    _FlagNoleggio = True
                End If
            End If

        End Sub

        Protected Sub lvCarrello_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvCarrello.ItemCommand

            Dim id As Integer = e.CommandArgument

            If e.CommandName = "Elimina" Then
                DataClass.CartMethod.RemoveItem(id)
            ElseIf e.CommandName = "Aggiorna" Then
                Dim txt As TextBox = DirectCast(e.Item.FindControl("ProdottoQuantita"), TextBox)
                DataClass.CartMethod.UpdateItemQuantity(id, txt.Text)
            End If

            BindCarello()
            CheckNoleggio()

        End Sub

        Private Function GetCustomerID() As String
            If Context.User.Identity.Name <> "" Then
                Return Context.User.Identity.Name
            Else
                If Session("AnonUID") Is Nothing Then
                    Session("AnonUID") = Guid.NewGuid()
                End If
                Return Session("AnonUID").ToString()
            End If
        End Function

        Private Function InviaMail(ByVal _from As String, ByVal _to As String, ByVal _object As String, ByVal _body As String) As Boolean

            Dim messaggio As MailMessage = New MailMessage()
            messaggio.From = New MailAddress(_from, _from)
            messaggio.To.Add(New MailAddress(_to, _to))
            messaggio.Subject = _object
            messaggio.SubjectEncoding = System.Text.Encoding.UTF8
            messaggio.Body = _body
            messaggio.BodyEncoding = System.Text.Encoding.UTF8
            messaggio.IsBodyHtml = True
            Try
                Dim msg As SmtpClient = New SmtpClient()
                msg.Send(messaggio)

                Return True
            Catch
                Return False
            End Try
        End Function

        Private Sub btnInvia_Click(sender As Object, e As EventArgs) Handles btnInvia2.Click

            Dim sb As New StringBuilder

            sb.Append("<strong>Richiesta Informazioni</strong><br /><br /><br />")
            sb.Append("<strong>Nominativo : </strong>" & Replace(txtNominativo.Text, "'", "''") & " <br />")
            sb.Append("<strong>E-Mail : </strong>" & Replace(txtEmail.Text, "'", "''") & " <br />")
            sb.Append("<strong>Richiesta : </strong>" & Replace(txtInfo.Text, "'", "''") & " <br />")
            sb.Append("<br /><br />")
            sb.Append("<table border='0' width='100%'>")

            For Each li As DataClass.vwCarrello In lista
                sb.Append("<tr><td>" & li.FK_PRODOTTO & "</td><td>" & li.Categoria & "</td><td>" & li.Modello & "</td><td>" & li.QTA & "</td></tr>")
            Next

            sb.Append("</table>")

            If _FlagNoleggio = True Then
                sb.Append("<br />")
                sb.Append("<strong>Noleggio</strong>")
                sb.Append("<br />")
                sb.Append("Data Noleggio : " & txtDataNoleggio.Text)
                sb.Append("<br />")
                sb.Append("Luogo Noleggio : " & txtCittaNoleggio.Text)
                sb.Append("<br />")
                sb.Append("Consegna : " & rblConsegna.SelectedValue)
                sb.Append("<br />")
            End If

            InviaMail(ConfigurationManager.AppSettings("mail_da_invio"), ConfigurationManager.AppSettings("mail_contatto"), "richiesta informazioni", sb.ToString)

            Response.Redirect(ConfigurationManager.AppSettings("ThankYouOrdinePath"))

        End Sub

    End Class

End Namespace