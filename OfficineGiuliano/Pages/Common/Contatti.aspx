<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Contatti.aspx.vb" Inherits="Emmemedia.Pages.Common.Contatti" EnableTheming="true" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>

<asp:Content ID="contenHead" runat="server" ContentPlaceHolderID="HeadPh">
    <script type="text/javascript">
        function ValidateCheckBox1(sender, args) {
            debugger;
            if (document.getElementById("<%=FormAgree1.ClientID %>").checked == true) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

      
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SliderPh" runat="Server">
    <asp:Literal runat="Server" ID="SliderContent" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentUpperPh" runat="Server">
    <uc1:Path runat="server" ID="Path" />
    <asp:PlaceHolder ID="UpperPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="UpperContent" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <asp:Literal runat="Server" ID="MainContent" />

    <div class="top-contatti">
        <div class="container-fluid wrap">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="h3">contact us
                    </h3>
                    <h4 class="h4">I Nostri Contatti
                    </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Nome</label>
                        <asp:TextBox ID="FormNome" runat="server" CssClass="form-control" ValidationGroup="form1" placeholder="Nome e Cognome *"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="FormNome" EnableClientScript="true" ValidationGroup="form1" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <asp:TextBox ID="FormMail" runat="server" CssClass="form-control" ValidationGroup="form1" TextMode="Email" placeholder="Email *"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="FormMail" EnableClientScript="true" ValidationGroup="form1" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group">
                        <label>Citt�</label>
                        <asp:TextBox ID="FormCitta" runat="server" CssClass="form-control" ValidationGroup="form1" placeholder="Citt� *"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="FormCitta" EnableClientScript="true" ValidationGroup="form1" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Azienda</label>
                        <asp:TextBox ID="FormAzienda" runat="server" CssClass="form-control" placeholder="Azienda"></asp:TextBox>
                        <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="FormAzienda" EnableClientScript="true" ValidationGroup="form1" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form-group">
                        <label>Partita IVA</label>
                        <asp:TextBox ID="FormPiva" runat="server" CssClass="form-control" placeholder="partita iva"></asp:TextBox>
                        <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="FormAzienda" EnableClientScript="true" ValidationGroup="form1" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form-group">
                        <label>Telefono</label>
                        <asp:TextBox ID="FormTelefono" runat="server" CssClass="form-control" ValidationGroup="form1" placeholder="Telefono *"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="FormTelefono" EnableClientScript="true" ValidationGroup="form1" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Messaggio</label>
                        <asp:TextBox runat="server" ID="FormTesto" TextMode="MultiLine" Rows="5" placeholder="Lascia un tuo messaggio" CssClass="form-control textAreaHeight" />
                    </div>
                    <div class="form-group">
                        <label>Trattamento Dati</label>
                        <div class="checkbox">
						<p>Preso atto dell� <a href=�/privacy.htm� target=�_blank�>informativa al trattamento dei dati personali</a>, l�utente:</p>
                            <asp:CheckBox ID="FormAgree1" runat="server" ValidationGroup="form1" CssClass="AcceptedAgreement" /><span>esprime il consenso al trattamento per finalita� di cui all�art 3 lettere a) dell�informativa.</span>
                            <asp:CustomValidator runat="server" ID="CustomValidator1" EnableClientScript="true" ValidationGroup="form1" Display="Dynamic"
                                                 ClientValidationFunction="ValidateCheckBox1" ForeColor="Red">E' necessarrio accettare il trattamento dei dati</asp:CustomValidator>

                            <asp:Label ID="LabelMessage" runat="server"></asp:Label>
                        </div>
                    </div>
                    <asp:LinkButton runat="server" ID="btnINVIA2" ValidationGroup="form1" CssClass="btn btn-block" CausesValidation="True">Invia <i class="ion ion-chevron-right"></i></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <div class="corpo-contatti">
        <div class="container-fluid wrap">
            <div class="row">
                <div class="col-lg-12 title">
                    <h3 class="h3">dove puoi trovarci
                    </h3>
                    <h4 class="h4">Dove Siamo
                    </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 maps">
                    <iframe class="framePadded" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d24127.188849332488!2d14.293891!3d40.896039!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x133ba82593feff15%3A0xd21ff0e3a2d75ef0!2sVia+Circumvallazione+esterna+di+Napoli%2C+29%2C+80026+Casoria+NA%2C+Italia!5e0!3m2!1sit!2sus!4v1464343235315" width="100%" height="310" frameborder="0" allowfullscreen></iframe>
                    <h3 class="h3">Napoli, Casoria
                    </h3>
                    <h4 class="h4">Punto noleggio e assistenza di NAPOLI
                    </h4>
                    <p>
                        Via Circumvallazione Esterna, 29<br>
                        80026 - Casoria (NA)<br>
                        Tel. +39 081.7362820<br>
                        Fax +39 081.7364620
                    </p>
                    <hr>
                </div>
                <div class="col-md-6 maps">
                    <iframe class="framePadded" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d23691.20094665128!2d12.594887!3d42.077476!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132f6e6b139ce467%3A0x28391d77bb3c68f8!2sVia+Leonardo+Da+Vinci%2C+00015+Monterotondo+RM%2C+Italia!5e0!3m2!1sit!2sus!4v1464343262578" width="100%" height="310" frameborder="0" allowfullscreen></iframe>
                    <h3 class="h3">Roma
                    </h3>
                    <h4 class="h4">Punto noleggio e assistenza di Roma
                    </h4>
                    <p>
                        Via L. Da Vinci, 42/44<br>
                        00015 - Z. ind. Monterotondo Scalo (Roma)<br>
                        Tel. +39 06.90060607<br>
                        Fax +39 06.9069925
                    </p>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 maps">
                    <iframe class="framePadded" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d44736.181714122045!2d10.194412!3d45.50985!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478173e963966433%3A0xc87bf8ccaaa694ce!2sVia+Enrico+Mattei%2C+28%2C+25030+Torbole+Casaglia+BS%2C+Italia!5e0!3m2!1sit!2sus!4v1464343323649" width="100%" height="310" frameborder="0" allowfullscreen></iframe>
                    <h3 class="h3">Brescia
                    </h3>
                    <h4 class="h4">Punto noleggio e assistenza di Brescia
                    </h4>
                    <p>
                        Via E. Mattei, 28<br>
                        25030 - Torbole Casaglia (Brescia)<br>
                        Tel. +39 030.2151019<br>
                        Fax +39 030.2158056
                    </p>
                    <hr>
                </div>
                <div class="col-md-6 maps">
                    <iframe class="framePadded" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d22576.70692596623!2d7.744394000000001!3d44.982516000000004!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47880c6af652fac3%3A0xcbd80a1cbe084a99!2sVia+Torino%2C+258%2C+10028+Trofarello+TO%2C+Italia!5e0!3m2!1sit!2sus!4v1464343375816" width="100%" height="310" frameborder="0" allowfullscreen></iframe>
                    <h3 class="h3">Torino
                    </h3>
                    <h4 class="h4">Punto noleggio e assistenza di Torino
                    </h4>
                    <p>
                        Via Torino, 258<br>
                        10028 - Trofarello (Torino)<br>
                        Tel. +39 011.6482711<br>
                        Fax +39 011.6482715
                    </p>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentFooterPh" runat="Server">
    <asp:PlaceHolder ID="FooterPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="FooterContent" />
</asp:Content>