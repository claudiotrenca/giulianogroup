﻿Namespace Pages
    Namespace Common
        Public Class ContentPage
            Inherits BaseClass.SitePage

            Dim _PageTemplateType As Integer

            Private Sub ContentPage_Init(sender As Object, e As EventArgs) Handles Me.Init

            End Sub

            Private Sub ContentPage_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete

            End Sub

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            End Sub

            Private Sub ContentPage_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

                InitContent()

                InitPageType()

            End Sub

            Private Sub InitContent()

                Dim _PkContent As Integer = DataClass.PostMenuMapping.GetPostIdFromMenu(Me.Master.Rif)

                _Content = DataClass.vwPost.GetDetail(_PkContent)

                If _Content IsNot Nothing Then

                    SliderContent.Text = _Content.Slider

                    _PageTitolo = _Content.Titolo
                    _PageSottotitolo = _Content.SottoTitolo
                    _PageSezione = _Content.Sezione
                    _PageContent = _Content.Contenuto
                    _PageFooterContent = _Content.ContenutoInferiore
                    _PageUpperContent = _Content.ContenutoSuperiore
                    _PageContenutoBreve = _Content.ContenutoBreve

                    _PageTemplateType = _Content.TemplatePagina

                    InitMeta()
                End If

            End Sub

            Private Sub InitPageType()

                Select Case _PageTemplateType

                    Case PageTemplate.OneColumn

                        BindContent()

                    Case PageTemplate.TwoColumnLeftSide

                        Dim NavSide As New Controls.Common.Nav.NavSide
                        NavSide = DirectCast(LoadControl("/Controls/Common/NavSide.ascx"), Controls.Common.Nav.NavSide)
                        NavSide.IdGruppoMenu = _Content.FkGruppoMenu
                        LeftPh.Controls.Add(NavSide)

                        BindContent()

                    Case PageTemplate.TwoColumnRightSide

                        Dim NavSide As New Controls.Common.Nav.NavSide
                        NavSide = DirectCast(LoadControl("/Controls/Common/NavSide.ascx"), Controls.Common.Nav.NavSide)
                        NavSide.IdGruppoMenu = _Content.FkGruppoMenu
                        RightPh.Controls.Add(NavSide)

                        BindContent()

                End Select

            End Sub

            Private Sub BindContent()

                MainContent.Text = _PageContent
                UpperContent.Text = _PageUpperContent
                FooterContent.Text = _PageFooterContent

            End Sub

            Private Sub InitMeta()
                Dim MetaPage As New MetaClass
                MetaPage.MetaTitle = _Content.MetaTitle
                MetaPage.MetaDescription = _Content.MetaDescription
                MetaPage.MetaKeyword = _Content.MetaKeywords
                MetaPage.MetaCanonical = _Content.MetaCanonical

                Me.Master.Meta = MetaPage
                Me.Master.InitMetaTag()
            End Sub

        End Class
    End Namespace
End Namespace