﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ThankYouWishlist.aspx.vb" Inherits="Emmemedia.ThankYouWishlist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadPh" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentUpperPh" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="risposta_ok">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<i class="ion ion-ios-flag"></i>
					<p>
						La tua richiesta &egrave; stata inoltrata. Il nostro staff la contatter&aacute; al pi&uacute; presto!
					</p>
					<a href="/">Torna alla Homepage</a>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentFooterPh" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="FooterPh" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="FooterScriptsPh" runat="server">
</asp:Content>
