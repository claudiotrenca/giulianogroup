﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Pages.Common
    
    Partial Public Class Contatti
        
        '''<summary>
        '''Controllo SliderContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents SliderContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo Path.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents Path As Global.Emmemedia.Controls.Common.Path
        
        '''<summary>
        '''Controllo UpperPh.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UpperPh As Global.System.Web.UI.WebControls.PlaceHolder
        
        '''<summary>
        '''Controllo UpperContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UpperContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo MainContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MainContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo FormNome.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormNome As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo RequiredFieldValidator2.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents RequiredFieldValidator2 As Global.System.Web.UI.WebControls.RequiredFieldValidator
        
        '''<summary>
        '''Controllo FormMail.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormMail As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo RequiredFieldValidator8.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents RequiredFieldValidator8 As Global.System.Web.UI.WebControls.RequiredFieldValidator
        
        '''<summary>
        '''Controllo FormCitta.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormCitta As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo RequiredFieldValidator3.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents RequiredFieldValidator3 As Global.System.Web.UI.WebControls.RequiredFieldValidator
        
        '''<summary>
        '''Controllo FormAzienda.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormAzienda As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo FormPiva.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormPiva As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo FormTelefono.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormTelefono As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo RequiredFieldValidator9.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents RequiredFieldValidator9 As Global.System.Web.UI.WebControls.RequiredFieldValidator
        
        '''<summary>
        '''Controllo FormTesto.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormTesto As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo FormAgree1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FormAgree1 As Global.System.Web.UI.WebControls.CheckBox
        
        '''<summary>
        '''Controllo CustomValidator1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents CustomValidator1 As Global.System.Web.UI.WebControls.CustomValidator
        
        '''<summary>
        '''Controllo LabelMessage.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents LabelMessage As Global.System.Web.UI.WebControls.Label
        
        '''<summary>
        '''Controllo btnINVIA2.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnINVIA2 As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Controllo FooterPh.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FooterPh As Global.System.Web.UI.WebControls.PlaceHolder
        
        '''<summary>
        '''Controllo FooterContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FooterContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.SiteMaster
            Get
                Return CType(MyBase.Master,Emmemedia.SiteMaster)
            End Get
        End Property
    End Class
End Namespace
