<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="OfferteElenco.aspx.vb" Inherits="Emmemedia.Pages.Offerte.OfferteElenco" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<%@ PreviousPageType TypeName="Emmemedia._Default" %>

<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>
<%@ Register Src="~/Controls/Blog/ArticoliCategoria.ascx" TagPrefix="uc1" TagName="ArticoliCategoria" %>

<%@ Reference Control="~/Controls/Offerte/OfferteRicercaBoxVerticale.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPh" runat="server">
    <style type="text/css">
        .image2 {
            height: 200px;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="server">
    <asp:Literal runat="Server" ID="SliderContent" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentUpperPh" runat="server">
    <uc1:Path runat="server" ID="Path" />
    <asp:Literal runat="Server" ID="UpperContent" />

    <asp:Panel ID="PnlIntroCategoria" runat="server">
        <!-- description -->
        <div class="row">
            <div class="header">
                <div class="col-sm-6">
                    <p>Abbiamo selezionato per te le location pi&uacute; suggestive per farti vivere una vacanza indimenticabile. Dalla natura e dai mari Italiani fino agli incantevoli scenari visibili in crociera.</p>
                    <p><strong>Via con noi</strong>, gli specialisti della vacanza ha scelto per te le migliori strutture per offrirti una vacanza da sogno.</p>
                </div>
                <!--
            <div class="col-sm-6 text-right">
                <p>social share</p>
            </div>
-->
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlFormRicerca" runat="server">
        <div class="search">
            <div class="col-sm-12">
                <p class="h2">Affina la ricerca</p>
            </div>
            <div class="form-group col-sm-3">
                <label>Destinazione</label>
                <p>
                    <asp:TextBox ID="FormDestinazione" runat="server" CssClass="form-control" placeholder="Destinazione"></asp:TextBox>
                    <i class="ion-map"></i>
                </p>
            </div>
            <div class="form-group col-sm-3">
                <label>Quando vuoi partire?</label>
                <p>
                    <asp:TextBox ID="FormData" runat="server" CssClass="datepicker form-control" placeholder="Scegli la data" data-provide="datepicker" TextMode="Date"></asp:TextBox>
                    <i class="ion-android-calendar"></i>
                </p>
            </div>
            <div class="form-group col-sm-3">
                <label>Qual &egrave; il tuo budget?</label>
                <asp:TextBox ID="FormBudget" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
            </div>
            <div class=" col-sm-3">
                <br />
                <asp:LinkButton ID="FormInvia" runat="server" CssClass="btn btn-default btn-primary full-width">RICERCA</asp:LinkButton>
            </div>
            <div class="clearfix"></div>
        </div>
    </asp:Panel>

    <br class="hidden-xs" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMainPh" runat="server">
    <asp:Literal runat="Server" ID="MainContent" />
    <asp:Panel ID="pnlCategorie" runat="server">
        <asp:ListView ID="lvCategorie" runat="server">
            <LayoutTemplate>
                <div class="row">
                    <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="col-md-4 col-sm-6">
                    <div class="result-box categorie">
                        <div class="image image2" runat="server" id="CategoriaImmagine">
                        </div>
                        <div class="details">
                            <h2 class="h3">
                                <strong>
                                    <asp:Literal ID="CategoriaTitolo" runat="server"></asp:Literal>
                                </strong>
                            </h2>
                        </div>
                        <asp:HyperLink ID="CategoriaLink" runat="server"></asp:HyperLink>
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>
    <asp:Panel ID="pnlOfferte" runat="server">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <p class="h2">
                    Offerte disponibili -
                    <asp:Literal ID="ltCategoria" runat="server"></asp:Literal>
                </p>
            </div>
            <div class="col-sm-6 col-xs-12 text-right hidden-xs">
                <p class="h3"><a href="/offerte-viaggi.htm">SCOPRI TUTTE LE OFFERTE</a></p>
            </div>
        </div>
        <br />
        <br />
        <!-- features results -->

        <asp:ListView ID="lvRisultati" runat="server">
            <LayoutTemplate>
                <div class="row">
                    <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="col-md-4 col-sm-6">
                    <div class="result-box">
                        <div class="image image2" runat="server" id="OffertaImmagine">
                        </div>
                        <div class="details">
                            <h2 class="h3"><strong>
                                <asp:Literal ID="OffertaLocalita" runat="server"></asp:Literal>
                            </strong></h2>
                            <h3 class="h4">
                                <asp:Literal ID="OffertaStruttura" runat="server"></asp:Literal></h3>
                            <span class="label">
                                <asp:Literal ID="OffertaMacro" runat="server"></asp:Literal>
                            </span>

                            <p class="period">
                                <asp:Literal ID="OffertaPeriodo" runat="server"></asp:Literal>
                            </p>
                            <p class="price">
                                a partire da <span>
                                    <asp:Literal ID="OffertaPrezzo" runat="server"></asp:Literal>
                                </span>
                            </p>
                        </div>
                        <asp:HyperLink ID="OffertaLink" runat="server"></asp:HyperLink>
                    </div>
                </div>
            </ItemTemplate>
            <EmptyDataTemplate>
                <div class="col-md-6 col-sm-6">
                    <h2 class="h3">
                        <strong>Spiacenti</strong>, non esistoni pacchetti con i parametri inseriti;
                    </h2>
                </div>
            </EmptyDataTemplate>
        </asp:ListView>

        <br />
        <br />
        <!-- features articles -->
        <div class="row">
            <div class="col-sm-12">
                <p class="h2">Via con il <b>blog</b></p>
            </div>

            <div class="blog clearfix">
                <uc1:ArticoliCategoria runat="server" ID="ArticoliCategoria" />
            </div>
        </div>
        <!-- claim -->
        <div class="claim-box clearfix">
            <div class="col-sm-3">
                <p class="h2">perch&egrave; scegliere</p>
                <p class="h2"><a href="http://www.viaconnoi.it">viaconnoi.it</a></p>
            </div>
            <div class="col-sm-3">
                <i class="ion-thumbsup"></i>
                <p>Perch&egrave; i nostri pacchetti viaggio hanno davvero prezzi irresistibili.</p>
            </div>
            <div class="col-sm-3">
                <i class="ion-ios-people"></i>
                <p>Perch&egrave; abbiamo più di 100 agenzie affiliate in tutta italia.</p>
            </div>
            <div class="col-sm-3">
                <i class="ion-android-globe"></i>
                <p>Perch&egrave; il nostro portale offre oltre 100 pacchetti viaggio.</p>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentFooterPh" runat="server">
    <asp:Literal runat="Server" ID="FooterContent" />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterScriptsPh" runat="server">
</asp:Content>