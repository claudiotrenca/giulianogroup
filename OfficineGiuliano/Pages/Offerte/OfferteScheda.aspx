<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="OfferteScheda.aspx.vb" Inherits="Emmemedia.Pages.Offerte.OfferteScheda" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>
<%@ Register Src="~/Controls/Blog/ArticoliCategoria.ascx" TagPrefix="uc1" TagName="ArticoliCategoria" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentUpperPh" runat="server">
    <uc1:Path runat="server" ID="Path" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMainPh" runat="server">
    <!-- title scheda -->

    <div class="row">
        <div class="header">
            <div class="col-md-10">
                <h2 class="h2"><strong>
                    <asp:Literal ID="ProdottoLocalita" runat="server"></asp:Literal></strong></h2>
                <h3 class="h2">
                    <asp:Literal ID="ProdottoDescrizione2" runat="server"></asp:Literal></h3>
            </div>
            <!--
            <div class="col-md-2">
                <p>social share</p>
            </div>
-->
        </div>
    </div>
    <!-- summary with slider -->
    <div class="row">
        <div class="col-md-12">
            <div class="summary">
                <div class="row">
                    <div class="col-md-7 col-sm-7 gallery">
                        <div id="animated-thumbnails">
                            <asp:HyperLink ID="ProdottoImmagineLink" runat="server" CssClass="gallery-img">
                                <asp:Image ID="ProdottoImmagine" runat="server" />
                            </asp:HyperLink>
                            <!-- nasconde le immagini nel div ma le mostra nella gallery -->
                            <div class="hide-img">
                                <asp:HyperLink ID="ProdottoImmagineLink2" runat="server" CssClass="gallery-img">
                                    <asp:Image ID="ProdottoImmagine2" runat="server" />
                                </asp:HyperLink>
                            </div>
                        </div>
                        <div class="circle text-center">
                            <i class="ion-ios-plus-empty"></i>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-5 details">
                        <canvas class="triangle hidden-xs"></canvas>
                        <p class="info">Durata</p>
                        <p>
                            <asp:Literal ID="ProdottoDurata" runat="server"></asp:Literal>
                        </p>
                        <br class="hidden-sm" />
                        <p class="info">Periodo Validit&agrave;</p>
                        <p>
                            <asp:Literal ID="ProdottoValidita" runat="server"></asp:Literal>
                        </p>
                        <br class="hidden-sm" />
                        <p class="info">Tipologia</p>
                        <p>
                            <asp:Literal ID="ProdottoTipologia" runat="server"></asp:Literal>
                        </p>
                        <br class="hidden-sm" />
                        <p class="info">Trattamento</p>
                        <p>
                            <asp:Literal ID="ProdottoTrattamento" runat="server"></asp:Literal>
                        </p>
                        <br class="hidden-sm" />
                        <br />
                        <p class="info">Prezzo a partire da</p>
                        <p>
                            <asp:Literal ID="ProdottoPrezzo" runat="server"></asp:Literal>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- description -->
    <div class="row">
        <div class="col-md-12 ">
            <div class="description">
                <h3 class="h3"><b>DESCRIZIONE</b></h3>
                <asp:Literal ID="ProdottoDescrizione" runat="server"></asp:Literal>

                <div class="row">

                    <div class="col-md-12">
                        <div class="search-pacchetto">
                            <canvas class="triangle hidden-xs"></canvas>
                            <h3 class="h3"><b>RICERCA PACCHETTO DISPONIBILE</b></h3>

                            <div class="col-md-4 col-sm-6 col-md-offset-2">
                                <div class="form-group">
                                    <label>Localit&aacute; di partenza</label>

                                    <asp:DropDownList ID="ddlLocalita" runat="server" AutoPostBack="true" data-provide="datepicker" CssClass="form-control"></asp:DropDownList>
                                    <!--                                        <i class="ion-map"></i>-->
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <label>Data di partenza</label>

                                    <asp:DropDownList ID="ddlDate" runat="server" AutoPostBack="true" data-provide="datepicker" CssClass="form-control"></asp:DropDownList>
                                    <!--                                        <i class="ion-android-calendar"></i>-->
                                </div>
                            </div>
                            <div class="col-md-12 ">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <th>Servizio</th>
                                            <th>Prezzo Listino</th>
                                            <th>Prezzo Scontato</th>
                                            <th>Risparmio</th>
                                        </thead>
                                        <tbody>
                                            <asp:PlaceHolder ID="phPrezzi" runat="server"></asp:PlaceHolder>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <canvas class="triangle"></canvas>
                </div>

                <div class="row">
                    <div class="col-md-12 action-buttons">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-md-offset-2">
                                <asp:LinkButton runat="server" ID="richInfo" Text="RICHIEDI INFO" CssClass="btn btn-secondary full-width"></asp:LinkButton>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <button class="btn btn-transparent full-width" onclick="window.print()">STAMPA QUESTA PAGINA</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--
        <%-- <div class="row">
            <div class="col-sm-6 col-xs-12 hidden-xs">
                <p class="h2">Potrebbe <b>interessarti</b> anche</p>
            </div>
            <div class="col-sm-6 col-xs-12 text-right">
                <p class="h3"><a href="">SCOPRI TUTTE LE OFFERTE</a></p>
            </div>
        </div>--%>
-->
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentFooterPh" runat="server">
    <div class="row">
        <div class="col-sm-12">
            <p class="h2">Via con il <b>blog</b></p>
        </div>
        <div class="blog clearfix">
            <uc1:ArticoliCategoria runat="server" ID="ArticoliCategoria" />
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterScriptsPh" runat="server">
    <!-- Modale carrello -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span class="ion-android-close" aria-hidden="true"></span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Richiesta Informazioni</h4>
                </div>
                <div class="modal-body">
                    <img id="ModalProductImage" runat="server" />
                    <div class="prodottoNome">
                        <h5>Informazioni Pacchetto:</h5>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nome Struttura</label>
                                    <asp:TextBox runat="server" ID="txtStrutturaRiferimento" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Periodo di riferimento</label>
                                    <asp:TextBox runat="server" ID="txtPeriodRif" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Localit&aacute; di partenza</label>
                                    <asp:DropDownList runat="server" ID="ddlLocPartenza" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Durata</label>
                                    <asp:DropDownList runat="server" ID="ddlDurata" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="7">7 Notti</asp:ListItem>
                                        <asp:ListItem Value="10">10 Notti</asp:ListItem>
                                        <asp:ListItem Value="14">14 Notti</asp:ListItem>
                                        <asp:ListItem Value="0">Altro</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Adulti</label>
                                    <asp:DropDownList runat="server" ID="ddlAdulti" CssClass="form-control">
                                        <asp:ListItem Text="0" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Bambini</label>
                                    <asp:DropDownList runat="server" ID="ddlBambini" CssClass="form-control">
                                        <asp:ListItem Text="0" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <h5>Informazioni di Contatto:</h5>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cognome e Nome</label>
                                    <asp:TextBox runat="server" ID="txtNome" CssClass="form-control" required="true"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Indirizzo E-Mail</label>
                                    <asp:TextBox runat="server" ID="txtMail" CssClass="form-control" required="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Numero di Telefono (1)</label>
                                    <asp:TextBox runat="server" ID="txtTel1" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Numero di Telefono (2)</label>
                                    <asp:TextBox runat="server" ID="txtTel2" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Note</label>
                                    <asp:TextBox runat="server" ID="txtNote" TextMode="MultiLine" Rows="3" Width="90%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <asp:LinkButton ID="btnInvia" Text="Invia" runat="server" CssClass="btn btn-secondary full-width"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <!-- Modale carrello -->
    <script src="/scripts/lightGallery/lightgallery.min.js"></script>
    <script src="/scripts/lightGallery/lg-thumbnail.min.js"></script>
    <script src="/scripts/lightGallery/lg-fullscreen.min.js"></script>
    <script type="text/javascript">
        // LIGHTGALLERY
        $('#animated-thumbnails').lightGallery({
            thumbnail: true,
            selector: '.gallery-img'
        });
    </script>
</asp:Content>