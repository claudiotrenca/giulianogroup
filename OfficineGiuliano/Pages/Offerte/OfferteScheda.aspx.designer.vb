﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Pages.Offerte
    
    Partial Public Class OfferteScheda
        
        '''<summary>
        '''Controllo Path.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents Path As Global.Emmemedia.Controls.Common.Path
        
        '''<summary>
        '''Controllo ProdottoLocalita.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoLocalita As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoDescrizione2.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoDescrizione2 As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoImmagineLink.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoImmagineLink As Global.System.Web.UI.WebControls.HyperLink
        
        '''<summary>
        '''Controllo ProdottoImmagine.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoImmagine As Global.System.Web.UI.WebControls.Image
        
        '''<summary>
        '''Controllo ProdottoImmagineLink2.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoImmagineLink2 As Global.System.Web.UI.WebControls.HyperLink
        
        '''<summary>
        '''Controllo ProdottoImmagine2.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoImmagine2 As Global.System.Web.UI.WebControls.Image
        
        '''<summary>
        '''Controllo ProdottoDurata.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoDurata As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoValidita.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoValidita As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoTipologia.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoTipologia As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoTrattamento.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoTrattamento As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoPrezzo.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoPrezzo As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ProdottoDescrizione.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ProdottoDescrizione As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ddlLocalita.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlLocalita As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo ddlDate.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlDate As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo phPrezzi.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents phPrezzi As Global.System.Web.UI.WebControls.PlaceHolder
        
        '''<summary>
        '''Controllo richInfo.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents richInfo As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Controllo ArticoliCategoria.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ArticoliCategoria As Global.Emmemedia.Controls.Blog.ArticoliCategoria
        
        '''<summary>
        '''Controllo ModalProductImage.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ModalProductImage As Global.System.Web.UI.HtmlControls.HtmlImage
        
        '''<summary>
        '''Controllo txtStrutturaRiferimento.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents txtStrutturaRiferimento As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo txtPeriodRif.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents txtPeriodRif As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo ddlLocPartenza.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlLocPartenza As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo ddlDurata.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlDurata As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo ddlAdulti.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlAdulti As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo ddlBambini.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlBambini As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo txtNome.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents txtNome As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo txtMail.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents txtMail As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo txtTel1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents txtTel1 As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo txtTel2.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents txtTel2 As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo txtNote.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents txtNote As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''Controllo btnInvia.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents btnInvia As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.SiteMaster
            Get
                Return CType(MyBase.Master,Emmemedia.SiteMaster)
            End Get
        End Property
    End Class
End Namespace
