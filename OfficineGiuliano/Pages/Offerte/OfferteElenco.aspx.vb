﻿Namespace Pages
    Namespace Offerte
        Public Class OfferteElenco
            Inherits BaseClass.SitePage

            Private _DataPartenza As DateTime
            Private _Destinazione As String = String.Empty
            Private _Budget As Decimal

            Private _Macro As String = String.Empty
            Private _Categoria As String = String.Empty
            Private _Offerta As String = String.Empty

            Private _CodiceMacro As Integer = 0
            Private _CodiceCategoria As Integer = 0
            Private _CodiceOfferta As Integer = 0

            Private Macro As DataClass.MV_CATEGORIE
            Private Categoria As DataClass.MV_CATEGORIE
            Private lst As New List(Of DataClass.Offerta)

            Dim utenza As Integer = ConfigurationManager.AppSettings("Utenza")

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                _Macro = IIf(Page.RouteData.Values("macro") IsNot Nothing, Page.RouteData.Values("macro"), String.Empty)
                _Categoria = IIf(Page.RouteData.Values("Categoria") IsNot Nothing, Page.RouteData.Values("Categoria"), String.Empty)

                If _Categoria <> String.Empty And _Macro <> String.Empty Then
                    InitMacro(_Macro)
                    InitCategoria(_Categoria, _CodiceMacro)
                ElseIf _Categoria = String.Empty And _Macro <> String.Empty Then
                    InitMacro(_Macro)
                End If

                If Request.UrlReferrer IsNot Nothing And Request.QueryString("search") IsNot Nothing Then
                    _Destinazione = IIf(Session("FormDestinazione") <> Nothing, Session("FormDestinazione"), String.Empty)
                    _Budget = IIf(Session("FormBudget") <> Nothing, Session("FormBudget"), Nothing)
                    _DataPartenza = IIf(Session("FormData") <> Nothing, Session("FormData"), Nothing)
                Else
                    Session("FormDestinazione") = Nothing
                    Session("FormBudget") = Nothing
                    Session("FormData") = Nothing
                End If

                If _CodiceCategoria <> 0 And _CodiceMacro <> 0 Then

                    PnlIntroCategoria.Visible = False
                    pnlCategorie.Visible = False
                    pnlFormRicerca.Visible = True
                    pnlOfferte.Visible = True

                    BindOfferte(_CodiceMacro, _CodiceCategoria)

                ElseIf _CodiceCategoria = 0 And _CodiceMacro <> 0 Then

                    PnlIntroCategoria.Visible = True
                    pnlCategorie.Visible = True
                    pnlFormRicerca.Visible = False
                    pnlOfferte.Visible = False
                    bindElencoCategorie(_CodiceMacro)

                ElseIf _CodiceCategoria = 0 And _CodiceMacro = 0 Then

                    PnlIntroCategoria.Visible = False
                    pnlCategorie.Visible = False
                    pnlFormRicerca.Visible = True
                    pnlOfferte.Visible = True
                    BindOfferte()

                End If

                If Not String.IsNullOrEmpty(_Categoria) Then
                    ArticoliCategoria.Categoria = _Categoria
                Else
                    ArticoliCategoria.Categoria = _Macro
                End If

                InitContent()

            End Sub

            Private Sub InitMacro(ByVal _str As String)

                Using context As New DataClass.ViaggiEntities
                    Macro = (From row In context.MV_CATEGORIE Where row.CODICE_RIF = 0 And row.LINK_CATEGORIA = _str Select row).FirstOrDefault
                End Using

                _CodiceMacro = Macro.CODICE_CATEGORIA
                ltCategoria.Text = Macro.TITOLO_CATEGORIA

            End Sub

            Private Sub InitCategoria(ByVal _str As String, ByVal _CodiceRif As Integer)

                Using context As New DataClass.ViaggiEntities
                    Categoria = (From row In context.MV_CATEGORIE Where row.CODICE_RIF = _CodiceRif And row.LINK_CATEGORIA = _str Select row).FirstOrDefault
                End Using

                _CodiceCategoria = Categoria.CODICE_CATEGORIA

            End Sub

            Private Sub InitMetaTags()

                MetaPage.MetaTitle = "Offerte Viaggi - " & Categoria.TITOLO_CATEGORIA
                MetaPage.MetaDescription = Categoria.DESCRIZIONE_CATEGORIA

            End Sub

            Private Sub InitContent()

                '    Dim _PkContent As Integer = DataClass.PostMenuMapping.GetPostIdFromMenu(Me.Master.Rif)

                Dim _CodiceMacroContent As Integer = Nothing
                Dim _CodiceCategoriaContent As Integer = Nothing

                _CodiceMacroContent = DataClass.Menu.GetCategoryIdFromLink(_Macro)

                If _Categoria <> String.Empty Then
                    _CodiceCategoriaContent = DataClass.Menu.GetCategoryIdFromLink(_Categoria, _CodiceMacroContent)
                    _Content = DataClass.vwPost.GetDetailFromMenu(_CodiceCategoriaContent)
                ElseIf _Macro <> String.Empty And _Categoria = String.Empty Then
                    _Content = DataClass.vwPost.GetDetailFromMenu(_CodiceMacroContent)
                End If

                If _Content IsNot Nothing Then

                    SliderContent.Text = _Content.Slider

                    _PageTitolo = _Content.Titolo
                    _PageSottotitolo = _Content.SottoTitolo
                    _PageSezione = _Content.Sezione
                    _PageContent = _Content.Contenuto
                    _PageFooterContent = _Content.ContenutoInferiore
                    _PageUpperContent = _Content.ContenutoSuperiore
                    _PageContenutoBreve = _Content.ContenutoBreve

                    BindContent()

                    InitMeta()

                Else
                    Dim MetaPage As New MetaClass

                    If Categoria IsNot Nothing Then
                        MetaPage.MetaTitle = "Offerte Viaggi - " & Categoria.TITOLO_CATEGORIA
                        MetaPage.MetaDescription = Categoria.DESCRIZIONE_CATEGORIA
                        MetaPage.MetaKeyword = Categoria.TITOLO_CATEGORIA

                    Else
                        MetaPage.MetaTitle = "Offerte Viaggi - " & Macro.TITOLO_CATEGORIA
                        MetaPage.MetaDescription = Macro.DESCRIZIONE_CATEGORIA
                        MetaPage.MetaKeyword = Macro.TITOLO_CATEGORIA

                    End If

                    Me.Master.Meta = MetaPage
                    Me.Master.InitMetaTag()

                End If

            End Sub

            Private Sub BindContent()

                MainContent.Text = _PageContent
                UpperContent.Text = _PageUpperContent
                FooterContent.Text = _PageFooterContent

            End Sub

            Private Sub InitMeta()
                Dim MetaPage As New MetaClass
                MetaPage.MetaTitle = _Content.MetaTitle
                MetaPage.MetaDescription = _Content.MetaDescription
                MetaPage.MetaKeyword = _Content.MetaKeywords
                MetaPage.MetaCanonical = _Content.MetaCanonical

                Me.Master.Meta = MetaPage
                Me.Master.InitMetaTag()
            End Sub

            Private Sub bindElencoCategorie(ByVal _IdMacro As Integer)

                Dim lst As List(Of DataClass.MV_VW_SOTTOCAT_ATTIVE)

                Using dc As New DataClass.ViaggiEntities
                    lst = (From p In dc.MV_VW_SOTTOCAT_ATTIVE Where p.CODICEFAMIGLIA_PRODOTTOFAMIGLIA = utenza And p.CODICE_RIF = _IdMacro Select p Order By p.TITOLO_CATEGORIA).ToList
                End Using

                Dim distinctItems As List(Of DataClass.MV_VW_SOTTOCAT_ATTIVE) = lst.GroupBy(Function(x) x.TITOLO_CATEGORIA).[Select](Function(y) y.First()).ToList
                distinctItems = distinctItems.AsQueryable.Where(Function(p) p.IMMAGINE_CATEGORIA <> "").ToList

                lvCategorie.DataSource = distinctItems
                lvCategorie.DataBind()

            End Sub

            Private Sub BindOfferte(Optional ByVal _IdMacro As String = "", Optional ByVal _IdCategoria As String = "")

                Using dc As New DataClass.ViaggiEntities

                    Dim query1 As IQueryable(Of DataClass.MV_VW_PRODOTTI_UTENZE)

                    If _IdCategoria <> "" Then
                        query1 = (From row In dc.MV_VW_PRODOTTI_UTENZE Where row.CODCAT_PRODOTTO = _IdMacro And row.CODSOTTOCAT_PRODOTTO = _IdCategoria And row.CODICEFAMIGLIA_PRODOTTOFAMIGLIA = utenza Select row Order By row.TITOLO_PRODOTTO)

                    ElseIf _IdMacro <> "" Then
                        query1 = (From row In dc.MV_VW_PRODOTTI_UTENZE Where row.CODCAT_PRODOTTO = _IdMacro And row.CODICEFAMIGLIA_PRODOTTOFAMIGLIA = utenza Select row Order By row.TITOLO_PRODOTTO)

                    Else

                        query1 = (From row In dc.MV_VW_PRODOTTI_UTENZE Where row.CODICEFAMIGLIA_PRODOTTOFAMIGLIA = utenza Select row Order By row.TITOLO_PRODOTTO)

                    End If

                    Dim queryResult = (From v In query1
                                       Select v.CODICE_PRODOTTO, v.STRUTTURA_PRODOTTO, v.PERIODO_PRODOTTO, v.PREZZO_PRODOTTO, v.LOCALITA_PRODOTTO, v.TITOLO_CATEGORIA, v.TITOLO_MACRO, v.LINK_CATEGORIA, v.LINK_MACRO, ImmagineProdotto = (
                                     ((From p In dc.MV_FOTOPRODOTTO
                                       Where
                                       Not p.URL_FOTOPRODOTTO Is Nothing And
                                       p.CODPRODOTTO_FOTOPRODOTTO = v.CODICE_PRODOTTO
                                       Select New With {
                                       p.URL_FOTOPRODOTTO
                                     }).Take(1).FirstOrDefault().URL_FOTOPRODOTTO)), DataInizio = (((From d In dc.MV_PREZZIPRODOTTO
                                                                                                     Where d.CODPRODOTTO_PREZZOPRODOTTO = v.CODICE_PRODOTTO
                                                                                                     Select New With {
                                                                                                                    d.DATAINIZIO_PREZZOPRODOTTO
                                                                                                                     }).OrderBy(Function(y) y.DATAINIZIO_PREZZOPRODOTTO).Take(1).FirstOrDefault().DATAINIZIO_PREZZOPRODOTTO)), DataFine = (((From d In dc.MV_PREZZIPRODOTTO
                                                                                                                                                                                                                                             Where d.CODPRODOTTO_PREZZOPRODOTTO = v.CODICE_PRODOTTO
                                                                                                                                                                                                                                             Select New With {d.DATAFINE_PREZZOPRODOTTO}).OrderByDescending(Function(y) y.DATAFINE_PREZZOPRODOTTO).Take(1).FirstOrDefault().DATAFINE_PREZZOPRODOTTO))).ToList

                    If _DataPartenza <> Nothing Then
                        queryResult = queryResult.AsEnumerable().Where(Function(p) p.DataInizio <= _DataPartenza And p.DataFine >= _DataPartenza).ToList
                    End If

                    If _Destinazione <> String.Empty Then
                        queryResult = queryResult.AsEnumerable().Where(Function(p) p.LOCALITA_PRODOTTO.ToLower.Contains(_Destinazione.ToLower)).ToList
                    End If

                    If _Budget <> Nothing Then
                        queryResult = queryResult.AsEnumerable().Where(Function(p) (CDec(p.PREZZO_PRODOTTO) - 50) <= _Budget And (CDec(p.PREZZO_PRODOTTO) + 50) >= _Budget).ToList
                    End If

                    lst = queryResult.AsEnumerable().Select(Of DataClass.Offerta)(Function(p) New DataClass.Offerta() With {
                                                                                                                        .IdProdotto = p.CODICE_PRODOTTO,
                                                                                                                        .Struttura = p.STRUTTURA_PRODOTTO,
                                                                                                                        .Categoria = p.TITOLO_CATEGORIA,
                                                                                                                        .Macro = p.TITOLO_MACRO,
                                                                                                                        .LinkCategoria = p.LINK_CATEGORIA,
                                                                                                                        .LinkMacro = p.LINK_MACRO,
                                                                                                                        .Localita = p.LOCALITA_PRODOTTO,
                                                                                                                        .Periodo = p.PERIODO_PRODOTTO,
                                                                                                                        .Immagine = p.ImmagineProdotto,
                                                                                                                        .Prezzo = p.PREZZO_PRODOTTO
                                                                                                                        }).ToList

                    lvRisultati.DataSource = lst
                    lvRisultati.DataBind()

                End Using

            End Sub

            Private Sub lvRisultati_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvRisultati.ItemDataBound

                If e.Item.ItemType = ListViewItemType.DataItem Then

                    Dim dti As DataClass.Offerta = TryCast(e.Item.DataItem, DataClass.Offerta)

                    ' Dim OffertaImmagine As WebControls.Image = DirectCast(e.Item.FindControl("OffertaImmagine"), WebControls.Image)
                    Dim OffertaImmagine As HtmlControl = DirectCast(e.Item.FindControl("OffertaImmagine"), HtmlControl)

                    'background: url(images/bg.jpg) no-repeat center center fixed;
                    Dim OffertaLocalita As Literal = DirectCast(e.Item.FindControl("OffertaLocalita"), WebControls.Literal)
                    Dim OffertaStruttura As Literal = DirectCast(e.Item.FindControl("OffertaStruttura"), WebControls.Literal)
                    Dim OffertaMacro As Literal = DirectCast(e.Item.FindControl("OffertaMacro"), WebControls.Literal)
                    Dim OffertaPeriodo As Literal = DirectCast(e.Item.FindControl("OffertaPeriodo"), WebControls.Literal)
                    Dim OffertaPrezzo As Literal = DirectCast(e.Item.FindControl("OffertaPrezzo"), WebControls.Literal)
                    Dim OffertaLink As HyperLink = DirectCast(e.Item.FindControl("OffertaLink"), WebControls.HyperLink)

                    If OffertaImmagine IsNot Nothing Then
                        OffertaImmagine.Attributes.Add("style", "background: url(" & "http://www.vantaggiirresistibili.it/frontend" & (dti.Immagine.Replace("\", "/")).Replace(" ", "%20") & ") no-repeat center center; -webkit-background-size: cover; background-size: cover;")

                    End If

                    If OffertaLocalita IsNot Nothing Then
                        OffertaLocalita.Text = dti.Localita
                    End If
                    If OffertaStruttura IsNot Nothing Then
                        OffertaStruttura.Text = dti.Struttura
                    End If
                    If OffertaMacro IsNot Nothing Then
                        OffertaMacro.Text = dti.Macro
                    End If
                    If OffertaPeriodo IsNot Nothing Then
                        OffertaPeriodo.Text = dti.Periodo
                    End If
                    If OffertaPrezzo IsNot Nothing Then
                        If IsNumeric(dti.Prezzo) Then
                            OffertaPrezzo.Text = FormatCurrency(dti.Prezzo, 2)
                        Else
                            OffertaPrezzo.Text = dti.Prezzo
                        End If
                    End If
                    If OffertaLink IsNot Nothing Then
                        OffertaLink.NavigateUrl = "/offerte-viaggi/" & dti.LinkMacro & "/" & dti.LinkCategoria & "/" & Emmemedia.Classi.Utility.StringToUrl(dti.Struttura) & "_" & dti.IdProdotto & ".htm"
                    End If

                End If
            End Sub

            Private Sub lvCategoria_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvCategorie.ItemDataBound
                Dim li As ListViewItem = e.Item

                If li.ItemType = ListViewItemType.DataItem Then

                    Dim dti As DataClass.MV_VW_SOTTOCAT_ATTIVE = TryCast(e.Item.DataItem, DataClass.MV_VW_SOTTOCAT_ATTIVE)

                    Dim CategoriaImmagine As HtmlControl = DirectCast(e.Item.FindControl("CategoriaImmagine"), HtmlControl)
                    Dim CategoriaTitolo As Literal = DirectCast(e.Item.FindControl("CategoriaTitolo"), WebControls.Literal)
                    Dim CategoriaLink As HyperLink = DirectCast(e.Item.FindControl("CategoriaLink"), WebControls.HyperLink)

                    If CategoriaImmagine IsNot Nothing Then
                        CategoriaImmagine.Attributes.Add("style", "background: url(" & "http://www.vantaggiirresistibili.it/frontend/imgCategorie/" & (dti.IMMAGINE_CATEGORIA.Replace("\", "/")).Replace(" ", "%20") & ") no-repeat center center; -webkit-background-size: cover; background-size: cover;")

                        ' "/images/placeholder.jpg"
                    End If

                    If CategoriaTitolo IsNot Nothing Then
                        CategoriaTitolo.Text = dti.TITOLO_CATEGORIA
                    End If

                    If CategoriaLink IsNot Nothing Then
                        CategoriaLink.NavigateUrl = "/offerte-viaggi/" & Page.RouteData.Values("macro") & "/" & dti.LINK_CATEGORIA & "/"
                    End If

                End If
            End Sub

            Private Sub FormInvia_Click(sender As Object, e As EventArgs) Handles FormInvia.Click

                If FormData.Text <> "" And IsDate(FormData.Text) Then
                    _DataPartenza = CDate(FormData.Text)
                End If

                If FormDestinazione.Text <> "" Then
                    _Destinazione = FormDestinazione.Text
                End If

                If FormBudget.Text <> "" And IsNumeric(FormBudget.Text) Then
                    _Budget = FormBudget.Text
                End If

                If _CodiceCategoria <> 0 And _CodiceMacro <> 0 Then

                    ' visualizza tutti i pacchetti di una categoria

                    'pnlMacro.Visible = False
                    'pnlCategoria.Visible = False
                    'pnlOfferte.Visible = True

                    'bindElencoOfferte()
                    'bindTitoloCategoria()

                    'Dim ps As OfferteElenco = DirectCast(Me.Page, OfferteElenco)
                    'ps.MainLiteralContent.Visible = False
                    BindOfferte(_CodiceMacro, _CodiceCategoria)

                ElseIf _CodiceCategoria = 0 And _CodiceMacro <> 0 Then

                    ' visualizza tutte le categorie di una macro

                    'pnlMacro.Visible = False
                    'pnlCategoria.Visible = True
                    'pnlOfferte.Visible = False

                    'bindElencoCategorie()
                    'bindTitoloCategoria()
                    BindOfferte(_CodiceMacro)

                ElseIf _CodiceCategoria = 0 And _CodiceMacro = 0 Then

                    ' visualizza tutte le macro

                    'pnlMacro.Visible = True
                    'pnlCategoria.Visible = False
                    'pnlOfferte.Visible = False

                    'bindElencoMacro()
                    BindOfferte()

                End If

            End Sub

        End Class
    End Namespace
End Namespace