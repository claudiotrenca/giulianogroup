﻿Imports System.Data.SqlClient
Imports System.Net.Mail

Namespace Pages
    Namespace Offerte
        Public Class OfferteScheda
            Inherits BaseClass.SitePage

            Private _Codice As Integer
            Dim _listaDate As New List(Of DataClass.ListaDate)

            Dim _ListaLocalita As New List(Of DataClass.ListaLocalita)

            Private _Macro As String = String.Empty
            Private _Categoria As String = String.Empty
            Private _Offerta As String = String.Empty

            Private _CodiceMacro As Integer = 0
            Private _CodiceCategoria As Integer = 0
            Private _CodiceOfferta As Integer = 0

            Protected Categoria As DataClass.MV_CATEGORIE
            Protected _Prodotto As New DataClass.MV_VW_PRODOTTO

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                _Codice = IIf(Page.RouteData.Values("codice") <> Nothing, Page.RouteData.Values("codice"), Nothing)

                _Macro = IIf(Page.RouteData.Values("macro") IsNot Nothing, Page.RouteData.Values("macro"), String.Empty)
                _Categoria = IIf(Page.RouteData.Values("Categoria") IsNot Nothing, Page.RouteData.Values("Categoria"), String.Empty)

                If _Categoria <> String.Empty And _Macro <> String.Empty Then
                    InitMacro(_Macro)
                    InitCategoria(_Categoria, _CodiceMacro)

                ElseIf _Categoria = String.Empty And _Macro <> String.Empty Then
                    InitMacro(_Macro)

                ElseIf _Categoria = String.Empty And _Macro = String.Empty Then

                End If

                Binddata()
                If Not IsPostBack Then
                    CaricaComboDate(_Codice)
                    CaricaComboLocalita(_Codice)
                End If
                If Not String.IsNullOrEmpty(_Categoria) Then
                    ArticoliCategoria.Categoria = _Macro
                Else
                    ArticoliCategoria.Categoria = _Macro
                End If

                CaricaTabellaPrezzi(_Codice, ddlLocalita.SelectedValue, ddlDate.SelectedValue)

            End Sub

            Private Sub Binddata()

                Using db As New DataClass.ViaggiEntities
                    _Prodotto = (From p In db.MV_VW_PRODOTTO Where p.CODICE_PRODOTTO = _Codice Select p).FirstOrDefault
                End Using

                ProdottoDescrizione.Text = _Prodotto.DESCRIZIONE_PRODOTTO
                ProdottoDescrizione2.Text = _Prodotto.STRUTTURA_PRODOTTO
                ProdottoLocalita.Text = _Prodotto.LOCALITA_PRODOTTO
                ProdottoDurata.Text = _Prodotto.DURATA_PRODOTTO
                ProdottoImmagine.ImageUrl = "http://www.vantaggiirresistibili.it/frontend" & (_Prodotto.FOTO_PRODOTTO.Replace("\", "/")).Replace(" ", "%20")
                ProdottoImmagineLink.NavigateUrl = "http://www.vantaggiirresistibili.it/frontend" & (_Prodotto.FOTO_PRODOTTO.Replace("\", "/")).Replace(" ", "%20")
                ProdottoImmagine2.ImageUrl = "http://www.vantaggiirresistibili.it/frontend" & (_Prodotto.FOTO_PRODOTTO.Replace("\", "/")).Replace(" ", "%20")
                ProdottoImmagineLink2.NavigateUrl = "http://www.vantaggiirresistibili.it/frontend" & (_Prodotto.FOTO_PRODOTTO.Replace("\", "/")).Replace(" ", "%20")

                If IsNumeric(_Prodotto.PREZZO_PRODOTTO) Then
                    ProdottoPrezzo.Text = FormatCurrency(EvalDec(_Prodotto.PREZZO_PRODOTTO), 2)
                Else
                    ProdottoPrezzo.Text = EvalDec(_Prodotto.PREZZO_PRODOTTO)
                End If

                ProdottoTipologia.Text = _Prodotto.TIPOLOGIA
                ProdottoTrattamento.Text = _Prodotto.TRATTAMENTO
                ProdottoValidita.Text = _Prodotto.PERIODO_PRODOTTO

            End Sub

            Private Function EvalDec(ByVal _obj As Object) As Decimal

                If _obj IsNot DBNull.Value Then
                    Return CDec(_obj)
                Else
                    Return 0
                End If

            End Function

            Private Sub InitMacro(ByVal _str As String)

                Using context As New DataClass.ViaggiEntities
                    Categoria = (From row In context.MV_CATEGORIE Where row.CODICE_RIF = 0 And row.LINK_CATEGORIA = _str Select row).FirstOrDefault
                End Using

                _CodiceMacro = Categoria.CODICE_CATEGORIA

            End Sub

            Private Sub InitCategoria(ByVal _str As String, ByVal _CodiceRif As Integer)

                Using context As New DataClass.ViaggiEntities
                    Categoria = (From row In context.MV_CATEGORIE Where row.CODICE_RIF = _CodiceRif And row.LINK_CATEGORIA = _str Select row).FirstOrDefault
                End Using

                _CodiceCategoria = Categoria.CODICE_CATEGORIA

            End Sub

            Private Sub InitMetaTags()

                MetaPage.MetaTitle = "Offerte Viaggi - " & Categoria.TITOLO_CATEGORIA
                MetaPage.MetaDescription = Categoria.DESCRIZIONE_CATEGORIA

            End Sub

            Private Sub CaricaTabellaPrezzi(ByVal codiceprodotto As Integer, ByVal localita As String, ByVal data As String)

                Dim lst As New List(Of DataClass.MV_PREZZIPRODOTTO)

                Using dc As New DataClass.ViaggiEntities
                    lst = (From r In dc.MV_PREZZIPRODOTTO Where r.CODPRODOTTO_PREZZOPRODOTTO = codiceprodotto And r.LOCALITAPART_PREZZOPRODOTTO = localita And r.DATE_PREZZOPRODOTTO = data Select r).ToList
                End Using

                Dim htmlRiga As String
                Dim htmlFinale As String = String.Empty
                Dim litTabPrezzi As New Literal()

                Dim _PrezzoListino As Decimal

                Dim _PrezzoViaConNoi As Decimal

                '  Dim _PrezzoVendita As Decimal

                Dim d_percsconto_prezzi As Decimal

                For Each prezzo As DataClass.MV_PREZZIPRODOTTO In lst

                    _PrezzoListino = CDec(EvalDec(prezzo.PREZZOLISTINO_PREZZOPRODOTTO))
                    _PrezzoViaConNoi = CDec(EvalDec(prezzo.PREZZOVENDITA1_PREZZOPRODOTTO))

                    d_percsconto_prezzi = 100 - ((_PrezzoViaConNoi * 100) / _PrezzoListino)

                    Dim desc_prezzi As String = prezzo.DESCRIZIONE_PREZZOPRODOTTO.ToString
                    Dim plistino_prezzi As String = prezzo.PREZZOLISTINO_PREZZOPRODOTTO.ToString() + " €"
                    Dim psconto_prezzi As String = _PrezzoViaConNoi.ToString() + " €"
                    Dim percsconto_prezzi As String = Math.Round(d_percsconto_prezzi).ToString + " %"

                    htmlRiga = "<tr><th scope='row'>" + desc_prezzi + "</th><td>" + plistino_prezzi + "</td><td>" + psconto_prezzi + "</td><td>" + percsconto_prezzi + "</td></tr>"
                    htmlFinale = htmlFinale + htmlRiga

                Next

                litTabPrezzi.Text = htmlFinale
                phPrezzi.Controls.Add(litTabPrezzi)

            End Sub

            Private Sub CaricaComboLocalita(ByVal codice As Integer)

                Dim lst As New List(Of DataClass.ListaLocalita)
                Dim queryResult As IQueryable(Of DataClass.MV_PREZZIPRODOTTO)

                Using dc As New DataClass.ViaggiEntities

                    queryResult = (From r In dc.MV_PREZZIPRODOTTO Where r.CODPRODOTTO_PREZZOPRODOTTO = codice And r.LOCALITAPART_PREZZOPRODOTTO <> "" Select r)

                    lst = queryResult.AsEnumerable().Select(Of DataClass.ListaLocalita)(Function(p) New DataClass.ListaLocalita() With {
                                                                                                    .Localita = p.LOCALITAPART_PREZZOPRODOTTO
                                                                                                              }).ToList
                    _ListaLocalita = lst.GroupBy(Function(x) x.Localita).[Select](Function(y) y.First()).ToList

                End Using

                If _ListaLocalita.Count > 0 Then
                    ddlLocalita.DataSource = _ListaLocalita
                    ddlLocalita.DataTextField = "Localita"
                    ddlLocalita.DataValueField = "Localita"
                    ddlLocalita.DataBind()
                Else
                    ddlLocalita.Enabled = False
                End If
            End Sub

            Private Sub CaricaComboDate(ByVal codice As Integer)
                Dim lst As New List(Of DataClass.ListaDate)
                Dim queryResult As IQueryable(Of DataClass.MV_PREZZIPRODOTTO)

                Using dc As New DataClass.ViaggiEntities

                    queryResult = (From r In dc.MV_PREZZIPRODOTTO Where r.CODPRODOTTO_PREZZOPRODOTTO = codice Select r)

                    lst = queryResult.AsEnumerable().Select(Of DataClass.ListaDate)(Function(p) New DataClass.ListaDate() With {
                                                                                                    .DatePartenza = p.DATE_PREZZOPRODOTTO
                                                                                                              }).ToList

                    _listaDate = lst.GroupBy(Function(x) x.DatePartenza).[Select](Function(y) y.First()).ToList
                End Using

                ddlDate.DataSource = _listaDate
                ddlDate.DataTextField = "DatePartenza"
                ddlDate.DataValueField = "DatePartenza"

                ddlDate.DataBind()
            End Sub

            Private Sub CaricaComboLocalitaModal(ByVal codice As Integer)

                If _ListaLocalita.Count > 0 Then
                    ddlLocPartenza.DataSource = _ListaLocalita
                    ddlLocPartenza.DataTextField = "Localita"
                    ddlLocPartenza.DataValueField = "Localita"
                    ddlLocPartenza.DataBind()
                Else
                    ddlLocPartenza.Enabled = False
                End If

            End Sub

            Private Sub richInfo_Click(sender As Object, e As EventArgs) Handles richInfo.Click

                txtStrutturaRiferimento.Text = _Prodotto.STRUTTURA_PRODOTTO

                txtPeriodRif.Text = ddlDate.SelectedItem.Text
                CaricaComboLocalitaModal(_Codice)
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "none", "<script>$('#myModal').modal('show');</script>", False)

            End Sub

            Private Sub btnInvia_Click(sender As Object, e As EventArgs) Handles btnInvia.Click

                If txtMail.Text = "" Then
                    js_esegui("javascript:alert('campo email obbligatorio');")
                ElseIf txtNome.Text = "" Then
                    js_esegui("javascript:alert('campo Cognome e Nome obbligatorio');")
                Else

                    Dim strBODY As String
                    Dim prodotto As String = ""

                    strBODY = "<strong>Richiesta Informazioni  </strong><br /><br /><br />" &
                         "<strong>Nome Struttura: </strong>" & Replace(txtStrutturaRiferimento.Text, "'", "''") & " <br />" &
                         "<strong>Data di Partenza: </strong>" & Replace(txtPeriodRif.Text, "'", "''") & " <br />" &
                         "<strong>Durata Soggiorno: </strong>" & Replace(ddlDurata.SelectedItem.Text, "'", "''") & " <br />" &
                         "<strong>Località di Partenza: </strong>" & Replace(ddlLocPartenza.SelectedValue, "'", "''") & " <br />" &
                         "<strong>Adulti: </strong>" & Replace(ddlAdulti.SelectedValue, "'", "''") & " <br />" &
                         "<strong>Bambini: </strong>" & Replace(ddlBambini.SelectedValue, "'", "''") & " <br />" &
                         "<strong>Cognome e Nome : </strong>" & Replace(txtNome.Text, "'", "''") & " <br />" &
                         "<strong>E-Mail : </strong>" & Replace(txtMail.Text, "'", "''") & " <br />" &
                         "<strong>Numero di Telefono(1) : </strong>" & Replace(txtTel1.Text, "'", "''") & " <br />" &
                         "<strong>Numero di Telefono(2) : </strong>" & Replace(txtTel2.Text, "'", "''") & " <br />" &
                         "<strong>Richiesta : </strong>" & Replace(txtNote.Text, "'", "''")

                    If txtMail.Text <> "" Then
                        If INSERT_MAIL_FORM(Now(), ConfigurationManager.AppSettings("mail_contatto"), ConfigurationManager.AppSettings("mail_contatto"), Replace(txtMail.Text, "'", "''"), "Richiesta Informazioni " & ConfigurationManager.AppSettings("nome_sito"), strBODY, ConfigurationManager.AppSettings("nome_sito")) Then
                            js_esegui("javascript:alert('<p> Richiesta informazioni inviata correttamente </p>';")
                        Else
                            js_esegui("javascript:alert('<p> Errore nell'invio della email ! </p>';")
                        End If
                    Else
                        js_esegui("javascript:alert('<p> attenzione : inserire una mail valida nella tabella sistema !</p>';")
                    End If

                End If

            End Sub

            Private Sub js_esegui(value As String)

                Dim csname1 As String = "PopupScript"
                Dim csname2 As String = "ButtonClickScript"
                Dim cstype As Type = Me.GetType()
                Dim cs As ClientScriptManager = Page.ClientScript

                If (Not cs.IsStartupScriptRegistered(cstype, csname1)) Then
                    Dim cstext1 As String = value
                    cs.RegisterStartupScript(cstype, csname1, cstext1, True)
                End If

            End Sub

            Public Shared Function INSERT_MAIL_FORM(ByVal DATA As String, ByVal SITO_FROM As String, ByVal SITO_TO As String, ByVal FROM As String, ByVal OGGETTO As String, ByVal TESTO As String, Optional ByVal nome_sito As String = "Contatti Sito", Optional ByVal idm As String = "", Optional ByVal scrivi As Boolean = True) As Boolean
                Dim sql As String
                sql = " insert into MV_MAIL_FORM (DATA, FROM_MAIL, OGGETTO, TESTO, CODICE_GRANDE_UTENTE) values (" & TRASFORMA_DATA(DATA) & ", '" & FROM & "', '" & OGGETTO & "', '" & TESTO & "', '" & idm & "')"

                INVIA_EMAIL(FROM, SITO_TO, OGGETTO, TESTO, nome_sito)
                INVIA_EMAIL(FROM, SITO_FROM, OGGETTO, TESTO, nome_sito)
                INVIA_EMAIL(SITO_FROM, FROM, OGGETTO, TESTO, nome_sito)

                Return True
            End Function

            Public Shared Function TRASFORMA_DATA(value As String) As String
                Return "CONVERT(DATETIME, '" & Mid(value, 7, 4) & "-" & Mid(value, 4, 2) & "-" & Mid(value, 1, 2) & " 00:00:00', 102)"
            End Function

            Public Shared Function INVIA_EMAIL(ByVal send_from As String, ByVal send_to As String, ByVal mess_obj As String, ByVal msg_body As String, nome_visualizzato As String) As Boolean

                Dim messaggio As MailMessage = New MailMessage()
                messaggio.From = New MailAddress(send_from, nome_visualizzato)
                messaggio.To.Add(New MailAddress(send_to, nome_visualizzato))

                messaggio.Subject = mess_obj
                messaggio.SubjectEncoding = System.Text.Encoding.UTF8
                messaggio.Body = msg_body
                messaggio.BodyEncoding = System.Text.Encoding.UTF8
                messaggio.IsBodyHtml = True

                Try
                    Dim msg As SmtpClient = New SmtpClient()
                    msg.Send(messaggio)

                    Return True
                Catch
                    Return False
                End Try

            End Function
        End Class

    End Namespace
End Namespace