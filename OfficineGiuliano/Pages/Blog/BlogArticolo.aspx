﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="BlogArticolo.aspx.vb" Inherits="Emmemedia.Pages.Blog.BlogArticolo" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>

<%@ Register Src="~/Controls/Blog/CategorieBlog.ascx" TagPrefix="uc1" TagName="CategorieBlog" %>
<%@ Register Src="~/Controls/Blog/PostLetti.ascx" TagPrefix="uc1" TagName="PostLetti" %>
<%@ Register Src="~/Controls/Blog/TagCloud.ascx" TagPrefix="uc1" TagName="TagCloud" %>
<%@ Register Src="~/Controls/Blog/BlogArchivio.ascx" TagPrefix="uc1" TagName="BlogArchivio" %>

<asp:Content ID="HeadContent" runat="server" ContentPlaceHolderID="HeadPh">
    <!-- LIn -->
    <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: it_IT</script>
    <!-- G+ -->
    <script src="https://apis.google.com/js/platform.js" async defer> {lang: 'it'}</script>
    <!-- TW -->
    <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); } }(document, 'script', 'twitter-wjs');</script>
    <!-- FB -->
    <div id="fb-root"></div>
    <script>(function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v2.5"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script>
    <style type="text/css">
        .fb_iframe_widget {
            display: inline-flex !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="SliderPh" runat="server">
    <div class="slide-top">

        <div class="bg-slide-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                        <h3 class="h3">Blog
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentUpperPh" runat="Server">
    <uc1:Path runat="server" ID="Path1" />
    <asp:PlaceHolder ID="UpperPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="Literal1" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainPh" runat="server">
    <div class="blog-article-section">
        <div class="container-fluid wrap">

            <div class="row">
                <div class="col-md-8">

                    <!--art1-->
                    <article>
                        <p class="date">
                            <asp:Literal ID="ArticoloData" runat="server"></asp:Literal>
                        </p>
                        <h1 class="h2">
                            <asp:Literal ID="ArticoloTitolo" runat="server"></asp:Literal></h1>
                        <figure>
                            <asp:Image ID="ArticoloImmagine" runat="server" CssClass="img-responsive" />
                        </figure>

                        <br />
                        <h3 class="h3">
                            <asp:Literal ID="ArticoloSottotitolo" runat="server"></asp:Literal></h3>

                        <asp:Literal ID="ArticoloTesto" runat="server"></asp:Literal>
                    </article>
                </div>
                <!--fine Articolo-->
                <div class="col-md-offset-1 col-md-3 navigation">
                    <div id="sidebar">
                        <uc1:CategorieBlog runat="server" ID="CategorieBlog" />
                        <uc1:PostLetti runat="server" ID="PostLetti" />
                        <uc1:TagCloud runat="server" ID="TagCloud" />
                        <uc1:BlogArchivio runat="server" ID="BlogArchivio" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>