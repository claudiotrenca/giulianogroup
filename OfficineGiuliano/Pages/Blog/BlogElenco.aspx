<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="BlogElenco.aspx.vb" Inherits="Emmemedia.Pages.Blog.BlogElenco" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<%@ Register Assembly="UnorderedListDataPager" Namespace="UnorderedListDataPager.CustomControls" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>

<%@ Register Src="~/Controls/Blog/CategorieBlog.ascx" TagPrefix="uc1" TagName="CategorieBlog" %>
<%@ Register Src="~/Controls/Blog/PostLetti.ascx" TagPrefix="uc1" TagName="PostLetti" %>
<%@ Register Src="~/Controls/Blog/TagCloud.ascx" TagPrefix="uc1" TagName="TagCloud" %>
<%@ Register Src="~/Controls/Blog/BlogArchivio.ascx" TagPrefix="uc1" TagName="BlogArchivio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPh" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SliderPh" runat="Server">
    <asp:Literal runat="Server" ID="SliderContent" />
    <div class="slide-top">

        <div class="bg-slide-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                        <h3 class="h3">Blog
                        </h3>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentUpperPh" runat="Server">
    <uc1:Path runat="server" ID="Path" />
    <asp:PlaceHolder ID="UpperPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="UpperContent" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainPh" runat="server">
    <asp:Literal ID="MainContent" runat="server"></asp:Literal>

    <div class="blog">
        <div class="container-fluid wrap">
            <div class="row">
                <div class="col-md-12">
                    <h2>
                        <asp:Literal ID="TipoRicerca" runat="server"></asp:Literal></h2>
                </div>
            </div>
            <asp:ListView ID="lvElencoArticoli" runat="server" DataKeyNames="PK">
                <LayoutTemplate>

                      <div class="row">
                        <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                    </div>
                </LayoutTemplate>
               

                <ItemTemplate>
                    <div class="col-md-3 col-xs-6 blog-article">
                        <asp:HyperLink ID="ArticoloLink1" CssClass="img-wrap" runat="server">
                            <asp:Image ID="ArticoloImmagine" runat="server" CssClass="imgResponsive" AlternateText='<%# Eval("Titolo") %>' />
                        </asp:HyperLink>
                        <h3 class="h3"><%# FormatDateTime(Eval("Data"), DateFormat.ShortDate) %></h3>
                        <h4 class="h4">
                            <%# Eval("Titolo") %>
                        </h4>
                        <asp:HyperLink ID="ArticoloLink2" runat="server" cssclass="btn btn-block">leggi di pi&uacute; <i class="ion ion-chevron-right"></i></asp:HyperLink>
                    </div>
                </ItemTemplate>
                <AlternatingItemTemplate>
                
                        <div class="col-md-3 col-xs-6 blog-article">
                            <asp:HyperLink ID="ArticoloLink1" CssClass="img-wrap" runat="server">
                                <asp:Image ID="ArticoloImmagine" runat="server" CssClass="imgResponsive" AlternateText='<%# Eval("Titolo") %>' />
                            </asp:HyperLink>
                            <h3 class="h3"><%# FormatDateTime(Eval("Data"), DateFormat.ShortDate) %></h3>
                            <h4 class="h4">
                                <%# Eval("Titolo") %>
                            </h4>
                            <asp:HyperLink ID="ArticoloLink2" runat="server" cssclass="btn btn-block">leggi di pi&uacute; <i class="ion ion-chevron-right"></i></asp:HyperLink>
                     
                    </div>
                </AlternatingItemTemplate>
                <EmptyDataTemplate>
                    <h2 class="h3">Nessun Articolo Presente</h2>
                </EmptyDataTemplate>
            </asp:ListView>
            <div class="row">
                <div class="col-md-12">
                    <cc1:UnorderedListDataPager ID="dpBottom" runat="server" class="pagination" PagedControlID="lvElencoArticoli" EnableViewState="true" QueryStringField="page">
                        <Fields>
                            <asp:NextPreviousPagerField ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Link" RenderNonBreakingSpacesBetweenControls="false" FirstPageText="&laquo;&laquo;" PreviousPageText="&laquo;" />
                            <asp:NumericPagerField ButtonType="Link" RenderNonBreakingSpacesBetweenControls="true" CurrentPageLabelCssClass="btn disabled" ButtonCount="10" />
                            <asp:NextPreviousPagerField ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Link" RenderNonBreakingSpacesBetweenControls="false" LastPageText="&raquo;&raquo;" NextPageText="&raquo;" />
                        </Fields>
                    </cc1:UnorderedListDataPager>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentFooterPh" runat="Server">
    <asp:PlaceHolder ID="FooterPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="FooterContent" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="FooterPh" runat="server"></asp:Content>