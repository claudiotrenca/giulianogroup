﻿Imports System.Data
Imports Emmemedia.DataClass

Namespace Pages
    Namespace Blog

        Partial Public Class BlogArticolo
            Inherits BaseClass.SitePage

            Protected provincia, corpo_doc, categoria As String
            Protected rif, pagina As String

            Dim _IdArticolo As Integer = Nothing
            Dim _Categoria As String
            Dim _CodiceCategoria As String

            Dim lst As New List(Of vwPost)

            Protected Articolo As New vwPost

            Private Menu As New DataClass.Menu

            Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

                _IdArticolo = IIf(Page.RouteData.Values("IDArticolo") <> Nothing, Page.RouteData.Values("IDArticolo"), Nothing)
                _Categoria = IIf(Page.RouteData.Values("Categoria") <> Nothing, Page.RouteData.Values("Categoria"), String.Empty)

                If _IdArticolo <> Nothing Then

                    BindArticolo()

                    InitMeta()

                    If _Categoria <> String.Empty Then
                        _CodiceCategoria = Menu.GetIdFromLink(_Categoria, ConfigurationManager.AppSettings("RifBlog"))

                        '   BindAltriArticoli()
                    End If
                End If

            End Sub

            Private Sub BlogArticolo_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

            End Sub

            Private Sub InitMeta()

                MetaPage.MetaTitle = Articolo.MetaTitle
                MetaPage.MetaDescription = Articolo.MetaDescription
                MetaPage.MetaKeyword = Articolo.MetaKeywords
                MetaPage.MetaCanonical = Request.Url.Host & Request.RawUrl

                Me.Master._Meta = MetaPage
                Me.Master.InitMetaTag()

            End Sub

            Private Sub BindArticolo()

                Using dc As New DataEntities
                    Articolo = (From row In dc.vwPost Where row.PK = _IdArticolo Select row).FirstOrDefault
                End Using

                ArticoloTitolo.Text = Articolo.Titolo
                ArticoloSottotitolo.Text = Articolo.Fondo
                ArticoloImmagine.AlternateText = Articolo.Titolo
                ArticoloImmagine.ImageUrl = "/public/images/content/" & Articolo.Immagine
                ArticoloTesto.Text = Articolo.Contenuto

            End Sub

            'Private Sub BindAltriArticoli()

            '    Using dc As New DataEntities
            '        lst = (From r In dc.vwPost Where r.FlagVisibile = True And r.CategoriaPK = _CodiceCategoria And r.PK <> _IdArticolo Select r Order By r.Data Descending).Take(4).ToList
            '    End Using

            '    AltriArticoli.DataSource = lst
            '    AltriArticoli.DataBind()

            'End Sub

            'Private Sub AltriArticoli_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles AltriArticoli.ItemDataBound

            '    Dim dti As DataClass.vwPost = TryCast(e.Item.DataItem, vwPost)

            '    If e.Item.ItemType = ListViewItemType.DataItem Then

            '        Dim img As WebControls.Image = DirectCast(e.Item.FindControl("AltriArticoliImmagine"), WebControls.Image)
            '        Dim lnk1 As HyperLink = DirectCast(e.Item.FindControl("AltriArticoliTitoloLink"), WebControls.HyperLink)

            '        If lnk1 IsNot Nothing Then
            '            lnk1.NavigateUrl = "/blog/" & dti.CategoriaLink & "/" & Classi.Utility.StringToUrl(dti.Titolo) & "_" & dti.PK & ".htm"
            '            lnk1.Text = dti.Titolo
            '            lnk1.Attributes.Add("title", dti.Titolo)
            '        End If

            '        If img IsNot Nothing Then
            '            If dti.Immagine <> "" Then
            '                img.ImageUrl = "/public/images/content/" & dti.Immagine
            '                img.AlternateText = dti.Titolo
            '            Else
            '                img.ImageUrl = "/public/image/noimage.gif"
            '            End If
            '        End If

            '    End If

            'End Sub

        End Class

    End Namespace
End Namespace