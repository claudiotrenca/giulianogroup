﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Pages.Blog
    
    Partial Public Class BlogArticolo
        
        '''<summary>
        '''Controllo Path1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents Path1 As Global.Emmemedia.Controls.Common.Path
        
        '''<summary>
        '''Controllo UpperPh.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UpperPh As Global.System.Web.UI.WebControls.PlaceHolder
        
        '''<summary>
        '''Controllo Literal1.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents Literal1 As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ArticoloData.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ArticoloData As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ArticoloTitolo.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ArticoloTitolo As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ArticoloImmagine.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ArticoloImmagine As Global.System.Web.UI.WebControls.Image
        
        '''<summary>
        '''Controllo ArticoloSottotitolo.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ArticoloSottotitolo As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo ArticoloTesto.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ArticoloTesto As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo CategorieBlog.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents CategorieBlog As Global.Emmemedia.CategorieBlog
        
        '''<summary>
        '''Controllo PostLetti.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents PostLetti As Global.Emmemedia.PostLetti
        
        '''<summary>
        '''Controllo TagCloud.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents TagCloud As Global.Emmemedia.TagCloud
        
        '''<summary>
        '''Controllo BlogArchivio.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents BlogArchivio As Global.Emmemedia.BlogArchivio
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.SiteMaster
            Get
                Return CType(MyBase.Master,Emmemedia.SiteMaster)
            End Get
        End Property
    End Class
End Namespace
