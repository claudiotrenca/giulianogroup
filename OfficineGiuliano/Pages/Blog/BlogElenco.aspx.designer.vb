﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Pages.Blog
    
    Partial Public Class BlogElenco
        
        '''<summary>
        '''Controllo SliderContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents SliderContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo Path.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents Path As Global.Emmemedia.Controls.Common.Path
        
        '''<summary>
        '''Controllo UpperPh.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UpperPh As Global.System.Web.UI.WebControls.PlaceHolder
        
        '''<summary>
        '''Controllo UpperContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UpperContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo MainContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MainContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo TipoRicerca.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents TipoRicerca As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo lvElencoArticoli.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents lvElencoArticoli As Global.System.Web.UI.WebControls.ListView

        '''<summary>
        '''Controllo dpBottom.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents dpBottom As UnorderedListDataPager.CustomControls.UnorderedListDataPager

        '''<summary>
        '''Controllo FooterPh.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FooterPh As Global.System.Web.UI.WebControls.PlaceHolder
        
        '''<summary>
        '''Controllo FooterContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FooterContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.SiteMaster
            Get
                Return CType(MyBase.Master,Emmemedia.SiteMaster)
            End Get
        End Property
    End Class
End Namespace
