﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="StoreLocator.aspx.vb" Inherits="Emmemedia.Pages.Store.StoreLocator" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Src="~/Controls/Common/Path.ascx" TagPrefix="uc1" TagName="Path" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPh" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="SliderPh" runat="server">
    <asp:Literal runat="Server" ID="SliderContent" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentUpperPh" runat="server">
    <uc1:Path runat="server" ID="Path" />
    <asp:PlaceHolder ID="UpperPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="UpperContent" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMainPh" runat="server">
    <asp:PlaceHolder ID="LeftPh" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="MainPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="MainContent" />
    <asp:PlaceHolder ID="RightPh" runat="server"></asp:PlaceHolder>
    <div class="store-locator-in">
        <div class="container-fluid">
            <div class="row">
                <div class="search">
                    <div class="col-sm-4">
                        <h2 class="h2">Seleziona la Regione di appartenenza:</h2>
                        <div class="dropdown">
                            <asp:DropDownList ID="ddlRegione" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <h2 class="h2">Seleziona la Provincia di appartenenza:</h2>
                        <div class="dropdown">
                            <asp:DropDownList ID="ddlProvincia" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <h2 class="h2">Seleziona la Città di appartenenza:</h2>
                        <div class="dropdown">
                            <asp:DropDownList ID="ddlComune" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="row module">
                <asp:ListView ID="lvRisultati" runat="server">
                    <LayoutTemplate>
                        <ul class="list-inline">
                            <asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
                        </ul>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li class="col-md-4 col-sm-4 col-xs-12 text-center">
                            <p>
                                <b><%# Eval("Nome") %></b>
                            </p>
                            <p>
                                <%# Eval("Indirizzo") %>
                                <br />
                                <%# Eval("Localita") %> (<%# Eval("Sigla") %>)
                                        <br />
                                <%# Eval("Telefono") %>
                            </p>
                        </li>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentFooterPh" runat="server">
    <asp:PlaceHolder ID="FooterPh" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="Server" ID="FooterContent" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="FooterPh" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="FooterScriptsPh" runat="server">
</asp:Content>