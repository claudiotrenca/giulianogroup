﻿Namespace Pages
    Namespace Store
        Public Class StoreLocator
            Inherits BaseClass.SitePage

            Private Sub StoreLocator_PreInit(sender As Object, e As EventArgs) Handles Me.PreInit

            End Sub

            Private Sub StoreLocator_Init(sender As Object, e As EventArgs) Handles Me.Init

            End Sub

            Private Sub StoreLocator_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete

            End Sub

            Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

                If Not IsPostBack Then

                    BindRegione()
                    BindProvincia("0")
                    BindComune("0")

                End If

            End Sub

            Private Sub StoreLocator_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
                InitContent()
            End Sub

            Private Sub StoreLocator_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender

            End Sub

            Private Sub InitContent()

                Dim _PkContent As Integer = DataClass.PostMenuMapping.GetPostIdFromMenu(Me.Master.Rif)

                _Content = DataClass.vwPost.GetDetail(_PkContent)

                If _Content IsNot Nothing Then

                    SliderContent.Text = _Content.Slider

                    _PageTitolo = _Content.Titolo
                    _PageSottotitolo = _Content.SottoTitolo
                    _PageSezione = _Content.Sezione
                    _PageContent = _Content.Contenuto
                    _PageFooterContent = _Content.ContenutoInferiore
                    _PageUpperContent = _Content.ContenutoSuperiore
                    _PageContenutoBreve = _Content.ContenutoBreve

                    InitMeta()

                    BindContent()

                End If

            End Sub

            Private Sub BindContent()

                MainContent.Text = _PageContent
                UpperContent.Text = _PageUpperContent
                FooterContent.Text = _PageFooterContent

            End Sub

            Private Sub InitMeta()
                Dim MetaPage As New MetaClass
                MetaPage.MetaTitle = _Content.MetaTitle
                MetaPage.MetaDescription = _Content.MetaDescription
                MetaPage.MetaKeyword = _Content.MetaKeywords
                MetaPage.MetaCanonical = _Content.MetaCanonical

                Me.Master.Meta = MetaPage
                Me.Master.InitMetaTag()
            End Sub

            Private Sub BindRegione()

                Dim lst As New List(Of DataClass.vwStore)
                Using dc As New DataClass.DataEntities
                    lst = (From p In dc.vwStore Where p.Regione <> "" Select p).ToList
                End Using

                Dim distinctItems = lst.GroupBy(Function(x) x.Regione).[Select](Function(y) y.First()).ToList
                distinctItems = distinctItems.AsQueryable.Where(Function(p) p.Regione <> "").ToList
                Dim itm As New DataClass.vwStore
                itm.Regione = "Seleziona"
                distinctItems.Insert(0, itm)

                ddlRegione.DataSource = distinctItems
                ddlRegione.DataTextField = "regione"
                ddlRegione.DataValueField = "regione"
                ddlRegione.DataBind()

            End Sub

            Private Sub BindProvincia(ByVal _regione As String)

                Dim lst As New List(Of DataClass.vwStore)
                Using dc As New DataClass.DataEntities
                    lst = (From p In dc.vwStore Where p.Regione = _regione And p.Sigla.Trim <> "" Select p).ToList
                End Using

                Dim distinctItems = lst.GroupBy(Function(x) x.Sigla).[Select](Function(y) y.First()).ToList
                distinctItems = distinctItems.AsQueryable.Where(Function(p) p.Sigla <> "").ToList
                Dim itm As New DataClass.vwStore
                itm.Provincia = "Seleziona"
                itm.Sigla = "0"
                distinctItems.Insert(0, itm)

                ddlProvincia.DataSource = distinctItems
                ddlProvincia.DataTextField = "provincia"
                ddlProvincia.DataValueField = "sigla"
                ddlProvincia.DataBind()

            End Sub

            Private Sub BindComune(ByVal _sigla As String)

                Dim lst As New List(Of DataClass.vwStore)
                Using dc As New DataClass.DataEntities
                    lst = (From p In dc.vwStore Where p.Sigla = _sigla And p.localita.Trim <> "" Select p).ToList
                End Using

                Dim distinctItems = lst.GroupBy(Function(x) x.localita).[Select](Function(y) y.First()).ToList
                distinctItems = distinctItems.AsQueryable.Where(Function(p) p.localita <> "").ToList

                Dim itm As New DataClass.vwStore
                itm.localita = "Seleziona"

                distinctItems.Insert(0, itm)

                ddlComune.DataSource = distinctItems
                ddlComune.DataTextField = "localita"
                ddlComune.DataValueField = "localita"
                ddlComune.DataBind()

            End Sub

            Private Sub BindData(ByVal _localita As String)

                Dim lst As New List(Of DataClass.vwStore)
                Using dc As New DataClass.DataEntities
                    lst = (From p In dc.vwStore Where p.localita = _localita Select p).ToList
                End Using

                lvRisultati.DataSource = lst
                lvRisultati.DataBind()

            End Sub

            Private Sub ddlRegione_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRegione.SelectedIndexChanged
                BindProvincia(ddlRegione.SelectedValue)
            End Sub

            Private Sub ddlProvincia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvincia.SelectedIndexChanged
                BindComune(ddlProvincia.SelectedValue)
            End Sub

            Private Sub ddlComune_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlComune.SelectedIndexChanged

                If ddlComune.SelectedValue <> "Seleziona" Then
                    BindData(ddlComune.SelectedValue)
                End If

            End Sub

        End Class

    End Namespace
End Namespace