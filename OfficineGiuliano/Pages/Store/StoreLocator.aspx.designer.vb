﻿'------------------------------------------------------------------------------
' <generato automaticamente>
'     Codice generato da uno strumento.
'
'     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
'     il codice viene rigenerato. 
' </generato automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Pages.Store
    
    Partial Public Class StoreLocator
        
        '''<summary>
        '''Controllo SliderContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents SliderContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo Path.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents Path As Global.Emmemedia.Controls.Common.Path
        
        '''<summary>
        '''Controllo UpperPh.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UpperPh As Global.System.Web.UI.WebControls.PlaceHolder
        
        '''<summary>
        '''Controllo UpperContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents UpperContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo LeftPh.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents LeftPh As Global.System.Web.UI.WebControls.PlaceHolder
        
        '''<summary>
        '''Controllo MainPh.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MainPh As Global.System.Web.UI.WebControls.PlaceHolder
        
        '''<summary>
        '''Controllo MainContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents MainContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Controllo RightPh.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents RightPh As Global.System.Web.UI.WebControls.PlaceHolder
        
        '''<summary>
        '''Controllo ddlRegione.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlRegione As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo ddlProvincia.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlProvincia As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo ddlComune.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents ddlComune As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''Controllo lvRisultati.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents lvRisultati As Global.System.Web.UI.WebControls.ListView
        
        '''<summary>
        '''Controllo FooterPh.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FooterPh As Global.System.Web.UI.WebControls.PlaceHolder
        
        '''<summary>
        '''Controllo FooterContent.
        '''</summary>
        '''<remarks>
        '''Campo generato automaticamente.
        '''Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        '''</remarks>
        Protected WithEvents FooterContent As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''Proprietà Master.
        '''</summary>
        '''<remarks>
        '''Proprietà generata automaticamente.
        '''</remarks>
        Public Shadows ReadOnly Property Master() As Emmemedia.SiteMaster
            Get
                Return CType(MyBase.Master,Emmemedia.SiteMaster)
            End Get
        End Property
    End Class
End Namespace
